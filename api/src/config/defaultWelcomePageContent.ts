export default {
  id: "1",
  version: 1,
  rows: [
    {
      id: "tdp85x",
      cells: [
        {
          id: "g78klb",
          size: 12,
          plugin: {
            id: "ory/editor/core/layout/background",
            version: 1,
          },
          dataI18n: {
            fr: {
              background:
                "https://media.istockphoto.com/photos/are-you-ready-to-party-picture-id1181169462?k=20&m=1181169462&s=612x612&w=0&h=Bl6aor50n-318v4vfwHRcg_w3OgMHmXWM6g0kUrSftk=",
              isParallax: false,
              modeFlag: 7,
              backgroundColor: {
                r: 55,
                g: 117,
                b: 225,
                a: 0.5,
              },
              gradients: [
                {
                  deg: 45,
                  opacity: 1,
                  colors: [
                    {
                      color: {
                        r: 55,
                        g: 117,
                        b: 225,
                        a: 0.37,
                      },
                    },
                  ],
                },
              ],
              hasPadding: true,
            },
            default: {
              background:
                "https://media.istockphoto.com/photos/are-you-ready-to-party-picture-id1181169462?k=20&m=1181169462&s=612x612&w=0&h=Bl6aor50n-318v4vfwHRcg_w3OgMHmXWM6g0kUrSftk=",
              isParallax: false,
              modeFlag: 5,
              backgroundColor: {
                r: 55,
                g: 117,
                b: 225,
                a: 0.5,
              },
              gradients: [
                {
                  deg: 115,
                  opacity: 1,
                  colors: [
                    {
                      color: {
                        r: 144,
                        g: 72,
                        b: 98,
                        a: 0.7,
                      },
                    },
                    {
                      color: {
                        r: 71,
                        g: 245,
                        b: 87,
                        a: 0.39,
                      },
                    },
                  ],
                },
              ],
              hasPadding: true,
              darken: 0.1,
              lighten: 0,
            },
          },
          rows: [
            {
              id: "zct582",
              cells: [
                {
                  id: "jx05bx",
                  size: 12,
                  plugin: {
                    id: "quill",
                    version: 1,
                  },
                  dataI18n: {
                    default: {
                      html: '<h1 class="ql-align-center"><strong class="ql-size-huge" style="color: rgb(255, 255, 255);">Bienvenue sur NOÉ !</strong></h1><p class="ql-align-center"><span class="ql-size-large" style="color: rgb(255, 255, 255);">🎉 Ceci est une page d\'accueil d\'exemple 🎉</span></p>',
                      advancedPadding: true,
                      paddingTop: 210,
                      paddingBottom: 200,
                      advancedMargin: false,
                    },
                  },
                  rows: [],
                  inline: null,
                },
              ],
            },
          ],
          inline: null,
        },
      ],
    },
    {
      id: "jem7g7",
      cells: [
        {
          id: "kelcin",
          size: 12,
          plugin: {
            id: "ory/editor/core/layout/background",
            version: 1,
          },
          dataI18n: {
            default: {
              modeFlag: 1,
              backgroundColor: {
                r: 4,
                g: 132,
                b: 63,
                a: 1,
              },
              advancedPadding: false,
              padding: 0,
              advancedMargin: true,
              margin: 0,
              gradients: [
                {
                  deg: 180,
                  opacity: 1,
                  colors: [],
                },
              ],
              hasPadding: false,
              darken: 0,
              marginTop: 10,
              marginBottom: 10,
            },
          },
          rows: [
            {
              id: "9m6624",
              cells: [
                {
                  id: "rvvren",
                  size: 6,
                  plugin: {
                    id: "quill",
                    version: 1,
                  },
                  dataI18n: {
                    default: {
                      html: '<p><strong>Libre à vous de faire ce qu\'il vous plait sur cette page ! </strong>Elle est la vitrine de votre événement sur NOÉ. Mettez-y la présentation de votre projet, et les infos pratiques pour vos participant·es.</p><p><br></p><p><span class="ql-size-large">Vous pouvez librement éditer cette page :</span></p><ul><li><span style="background-color: rgba(35, 70, 129, 0); color: unset;">Ajoutez des blocs avec le </span><strong style="background-color: rgba(35, 70, 129, 0); color: unset;">bouton "+"</strong><span style="background-color: rgba(35, 70, 129, 0); color: unset;"> à droite de l\'écran,</span></li><li>Ajoutez du style à vos blocs avec les <strong>panneaux "Marges" et "Styles"</strong> en bas de l\'écran,</li><li>Changez le <strong style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0);">style</strong> <em>des</em> <u style="color: rgb(161, 0, 0);">textes</u> grâce aux contrôles de l\'éditeur,</li><li><strong>Repositionnez</strong> les éléments par glisser-déposer : vous pouvez en mettre plusieurs par ligne si besoin,</li><li>Incorporez des <strong>images et des vidéos</strong>, avec les blocs "Background", "Image" ou "Video".</li></ul>',
                      backgroundColor: "#23468100",
                      advancedPadding: false,
                      padding: 20,
                      advancedMargin: false,
                    },
                  },
                  rows: [],
                  inline: null,
                },
                {
                  id: "nl2l28",
                  size: 6,
                  plugin: {
                    id: "ory/editor/core/content/image",
                    version: 1,
                  },
                  dataI18n: {
                    default: {
                      src: "https://media.istockphoto.com/photos/concert-stage-people-are-visible-waving-and-clapping-silhouettes-are-picture-id1194060639?k=20&m=1194060639&s=612x612&w=0&h=x-0MqkUFwRVvf52GtrM23XrD02r-MB2y_hlOKaQVvvs=",
                      borderWidth: 0,
                      borderRadius: 30,
                      borderColor: "#00000000",
                      textColor: "#000000",
                      backgroundColor: "#0a44dc00",
                      marginTop: 0,
                      marginBottom: 0,
                      marginLeft: 0,
                      marginRight: 0,
                      paddingTop: 20,
                      paddingBottom: 20,
                      paddingLeft: 20,
                      paddingRight: 20,
                      cellSpacingX: 0,
                      cellSpacingY: 0,
                      advancedPadding: false,
                      padding: null,
                      advancedMargin: false,
                      margin: 10,
                    },
                    fr: {
                      src: "https://media.istockphoto.com/photos/concert-stage-people-are-visible-waving-and-clapping-silhouettes-are-picture-id1194060639?k=20&m=1194060639&s=612x612&w=0&h=x-0MqkUFwRVvf52GtrM23XrD02r-MB2y_hlOKaQVvvs=",
                      borderWidth: 0,
                      borderRadius: 0,
                      borderColor: "#000000",
                      textColor: "#000000",
                      backgroundColor: "#ffffff",
                      marginTop: 0,
                      marginBottom: 0,
                      marginLeft: 0,
                      marginRight: 0,
                      paddingTop: 20,
                      paddingBottom: 20,
                      paddingLeft: 20,
                      paddingRight: 20,
                      cellSpacingX: 0,
                      cellSpacingY: 0,
                    },
                  },
                  rows: [],
                  inline: null,
                },
              ],
            },
            {
              id: "h6xngp",
              cells: [
                {
                  id: "brolu2",
                  size: 12,
                  plugin: {
                    id: "ory/editor/core/layout/background",
                    version: 1,
                  },
                  dataI18n: {
                    default: {
                      modeFlag: 3,
                      advancedPadding: false,
                      padding: null,
                      paddingTop: null,
                      paddingBottom: null,
                      advancedMargin: false,
                      margin: 10,
                      borderRadius: 20,
                      backgroundColor: {
                        r: 0,
                        g: 58,
                        b: 245,
                        a: 0.3,
                      },
                      cellSpacingX: null,
                      cellSpacingY: 20,
                      darken: 0,
                    },
                  },
                  rows: [
                    {
                      id: "iltan2",
                      cells: [
                        {
                          id: "9kezpf",
                          size: 12,
                          plugin: {
                            id: "quill",
                            version: 1,
                          },
                          dataI18n: {
                            default: {
                              html: '<h2 class="ql-align-center"><span class="ql-size-large">Et d\'ailleurs, vous avez-vu notre playlist de tutoriels ?</span></h2>',
                              advancedPadding: true,
                              paddingTop: 30,
                              advancedMargin: false,
                              margin: 10,
                            },
                          },
                          rows: [],
                          inline: null,
                        },
                      ],
                    },
                    {
                      id: "wrjlbq",
                      cells: [
                        {
                          id: "ku3pvy",
                          size: 12,
                          plugin: {
                            id: "ory/editor/core/content/video",
                            version: 1,
                          },
                          dataI18n: {
                            default: {
                              src: "https://youtu.be/aWoi08RpSbA?si=qGrF_Dp0LP0WMn32",
                              advancedPadding: false,
                              padding: null,
                              paddingBottom: null,
                              advancedMargin: false,
                              margin: 20,
                              marginBottom: null,
                              borderRadius: 30,
                              borderWidth: null,
                            },
                          },
                          rows: [],
                          inline: null,
                        },
                      ],
                    },
                  ],
                  inline: null,
                },
              ],
            },
          ],
          inline: null,
        },
      ],
    },
  ],
};
