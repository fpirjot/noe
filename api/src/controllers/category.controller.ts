import {Router} from "express";
import {withAuthentication} from "../config/passport";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "../utilities/routeUtilities";
import {
  filterBody,
  permit,
  projectAdmins,
  projectContributors,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activity.model";
import {
  allowedBodyArgs,
  Category,
  CategoryD,
  LightCategoryD,
  schemaDescription,
} from "../models/category.model";
import {getDependenciesList} from "../utilities/controllers/getDependenciesList";
import {listAllEntities} from "../utilities/controllers/actions/listAllEntities";
import {readEntity} from "../utilities/controllers/actions/readEntity";
import {createEntity} from "../utilities/controllers/actions/createEntity";
import {updateEntity} from "../utilities/controllers/actions/updateEntity";
import {deleteEntity} from "../utilities/controllers/actions/deleteEntity";
import {removeNestedObjectsContent} from "../utilities/controllers/removeNestedObjectsContent";
import {GenericQueryParams, parseQueryParams} from "../utilities/controllers/parseQueryParams";
import {lightSelection} from "../utilities/lightSelection";

export const CategoryRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
CategoryRouter.get(...MODIFICATIONS_HISTORY("Category"));
CategoryRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the categories in the project
 * GET /
 */
const LIST_CATEGORIES = endpoint<ProjectId, undefined, Array<LightCategoryD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Category.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.category
      ),
      {name: "asc"},
      res
    );
  }
);
CategoryRouter.get(...LIST_CATEGORIES);

/**
 * Get a category
 * GET /:id
 */
const GET_CATEGORY = endpoint<ProjectIdAndId, undefined, CategoryD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Category.findOne({_id: id, project: projectId}).lean()
    );
  }
);
CategoryRouter.get(...GET_CATEGORY);

/**
 * Create a category
 * POST /
 */
const CREATE_CATEGORY = endpoint<ProjectId, New<CategoryD>, CategoryD>(
  "/",
  [permit(projectContributors), filterBody(allowedBodyArgs), removeNestedObjectsContent("project")],
  async function (req, res) {
    return await createEntity(Category, req.body, res);
  }
);
CategoryRouter.post(...CREATE_CATEGORY);

/**
 * Modify a category
 * PATCH /:id
 */
const MODIFY_CATEGORY = endpoint<ProjectIdAndId, Modified<CategoryD>, CategoryD>(
  "/:id",
  [
    permit(projectContributors),
    filterBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Category,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
CategoryRouter.patch(...MODIFY_CATEGORY);

/**
 * Delete a category
 * DELETE /:id
 */
const DELETE_CATEGORY = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const categoryId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {category: categoryId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(req.t("categories:errors.activitiesDependencies", {dependentActivitiesList}));
    }

    return await deleteEntity(categoryId, Category, req.authenticatedUser, res);
  }
);
CategoryRouter.delete(...DELETE_CATEGORY);
