import {Router} from "express";
import {withAuthentication} from "../config/passport";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "../utilities/routeUtilities";
import {
  filterBody,
  permit,
  projectAdmins,
  projectContributors,
} from "../utilities/permissionsUtilities";

import {
  Activity,
  ActivityD,
  allowedBodyArgs,
  LightActivityD,
  schemaDescription,
} from "../models/activity.model";
import {Session} from "../models/session.model";
import {LightCategoryD} from "../models/category.model";
import {LightPlaceD} from "../models/place.model";
import {LightStewardD} from "../models/steward.model";
import {SlotD} from "../models/slot.model";
import {getDependenciesList} from "../utilities/controllers/getDependenciesList";
import {listAllEntities} from "../utilities/controllers/actions/listAllEntities";
import {readEntity} from "../utilities/controllers/actions/readEntity";
import {createEntity} from "../utilities/controllers/actions/createEntity";
import {updateEntity} from "../utilities/controllers/actions/updateEntity";
import {deleteEntity} from "../utilities/controllers/actions/deleteEntity";
import {removeNestedObjectsContent} from "../utilities/controllers/removeNestedObjectsContent";
import {GenericQueryParams, parseQueryParams} from "../utilities/controllers/parseQueryParams";
import {lightSelection} from "../utilities/lightSelection";

export const ActivityRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
ActivityRouter.get(...MODIFICATIONS_HISTORY("Activity"));
ActivityRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

const activityPopulationOptions = [
  {path: "category", select: lightSelection.category},
  {path: "places", select: lightSelection.place},
  {path: "stewards", select: lightSelection.steward},
  {path: "slots"},
];

type ActivityPopulation = {
  category: LightCategoryD;
  places: LightPlaceD;
  stewards: LightStewardD;
  slots: SlotD;
};
type FullPopulatedActivity = ActivityD & ActivityPopulation;
type LightPopulatedActivity = LightActivityD & ActivityPopulation;

/**
 * List all the activities in the project
 * GET /
 */
const LIST_ACTIVITIES = endpoint<
  ProjectId,
  undefined,
  Array<LightPopulatedActivity>,
  GenericQueryParams
>("/", [withAuthentication], async function (req, res) {
  return await listAllEntities(
    Activity.find(
      {project: req.params.projectId, ...parseQueryParams(req.query)},
      lightSelection.activity
    )
      .populate(activityPopulationOptions)
      .lean(),
    {name: "asc"},
    res
  );
});
ActivityRouter.get(...LIST_ACTIVITIES);

/**
 * Get an activity
 * GET /:id
 */
const GET_ACTIVITY = endpoint<ProjectIdAndId, undefined, FullPopulatedActivity>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Activity.findOne({_id: id, project: projectId}).populate(activityPopulationOptions).lean()
    );
  }
);
ActivityRouter.get(...GET_ACTIVITY);

/**
 * Create an activity
 * POST /
 */
const CREATE_ACTIVITY = endpoint<ProjectId, New<ActivityD>, FullPopulatedActivity>(
  "/",
  [
    permit(projectContributors),
    filterBody(allowedBodyArgs),
    removeNestedObjectsContent("category", "places", "stewards", "project"),
  ],
  async function create(req, res) {
    return await createEntity(Activity, req.body, res, (activity) =>
      activity.populate(activityPopulationOptions)
    );
  }
);
ActivityRouter.post(...CREATE_ACTIVITY);

/**
 * Modify an activity
 * PATCH /:id
 */
const MODIFY_ACTIVITY = endpoint<ProjectIdAndId, Modified<ActivityD>, FullPopulatedActivity>(
  "/:id",
  [
    permit(projectContributors),
    filterBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("category", "places", "stewards", "project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Activity,
      req.body,
      req.authenticatedUser,
      res,
      activityPopulationOptions
    );
  }
);
ActivityRouter.patch(...MODIFY_ACTIVITY);

/**
 * Delete an activity
 * DELETE /:id
 */
const DELETE_ACTIVITY = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const activityId = req.params.id;

    const dependentSessionsList = await getDependenciesList(Session, {activity: activityId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(req.t("activities:errors.sessionsDependencies", {dependentSessionsList}));
    }

    return await deleteEntity(activityId, Activity, req.authenticatedUser, res);
  }
);
ActivityRouter.delete(...DELETE_ACTIVITY);
