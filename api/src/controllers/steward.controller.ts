import {Router} from "express";
import {withAuthentication} from "../config/passport";
import {
  endpoint,
  ErrorString,
  MODIFICATIONS_HISTORY,
  Modified,
  New,
  ProjectId,
  ProjectIdAndId,
  SCHEMA_DESCRIPTION,
} from "../utilities/routeUtilities";
import {
  filterBody,
  permit,
  projectAdmins,
  projectContributors,
} from "../utilities/permissionsUtilities";

import {Activity} from "../models/activity.model";
import {Slot} from "../models/slot.model";
import {
  allowedBodyArgs,
  LightStewardD,
  schemaDescription,
  Steward,
  StewardD,
} from "../models/steward.model";
import {Session} from "../models/session.model";
import {getDependenciesList} from "../utilities/controllers/getDependenciesList";
import {listAllEntities} from "../utilities/controllers/actions/listAllEntities";
import {readEntity} from "../utilities/controllers/actions/readEntity";
import {createEntity} from "../utilities/controllers/actions/createEntity";
import {updateEntity} from "../utilities/controllers/actions/updateEntity";
import {deleteEntity} from "../utilities/controllers/actions/deleteEntity";
import {removeNestedObjectsContent} from "../utilities/controllers/removeNestedObjectsContent";
import {GenericQueryParams, parseQueryParams} from "../utilities/controllers/parseQueryParams";
import {lightSelection} from "../utilities/lightSelection";

export const StewardRouter = Router({mergeParams: true});

/**
 * Generic endpoints
 */
StewardRouter.get(...MODIFICATIONS_HISTORY("Steward"));
StewardRouter.get(...SCHEMA_DESCRIPTION(schemaDescription));

/**
 * List all the stewards in the project
 * GET /
 */
const LIST_STEWARDS = endpoint<ProjectId, undefined, Array<LightStewardD>, GenericQueryParams>(
  "/",
  [withAuthentication],
  async function (req, res) {
    return listAllEntities(
      Steward.find(
        {project: req.params.projectId, ...parseQueryParams(req.query)},
        lightSelection.steward
      ),
      {firstName: "asc"},
      res
    );
  }
);
StewardRouter.get(...LIST_STEWARDS);

/**
 * Get a steward
 * GET /:id
 */
const GET_STEWARD = endpoint<ProjectIdAndId, undefined, StewardD>(
  "/:id",
  [withAuthentication],
  async function (req, res) {
    return await readEntity(req.params.id, req.params.projectId, res, (id, projectId) =>
      Steward.findOne({_id: id, project: projectId}).lean()
    );
  }
);
StewardRouter.get(...GET_STEWARD);

/**
 * Create a steward
 * POST /
 */
const CREATE_STEWARD = endpoint<ProjectId, New<StewardD>, StewardD>(
  "/",
  [permit(projectContributors), filterBody(allowedBodyArgs), removeNestedObjectsContent("project")],
  async function (req, res) {
    return await createEntity(Steward, req.body, res);
  }
);
StewardRouter.post(...CREATE_STEWARD);

/**
 * Modify a steward
 * PATCH /:id
 */
const MODIFY_STEWARD = endpoint<ProjectIdAndId, Modified<StewardD>, StewardD>(
  "/:id",
  [
    permit(projectContributors),
    filterBody(["_id", ...allowedBodyArgs]),
    removeNestedObjectsContent("project"),
  ],
  async function (req, res) {
    return await updateEntity(
      req.params.id,
      req.params.projectId,
      Steward,
      req.body,
      req.authenticatedUser,
      res
    );
  }
);
StewardRouter.patch(...MODIFY_STEWARD);

/**
 * Delete a steward
 * DELETE /:id
 */
const DELETE_STEWARD = endpoint<ProjectIdAndId, undefined, undefined | ErrorString>(
  "/:id",
  [permit(projectAdmins)],
  async function (req, res) {
    const stewardId = req.params.id;

    const dependentActivitiesList = await getDependenciesList(Activity, {stewards: stewardId});
    if (dependentActivitiesList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des activités (${dependentActivitiesList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    // TODO make it work with session slots too!
    const dependentSessionsList = await getDependenciesList(Session, {stewards: stewardId});
    if (dependentSessionsList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des sessions (${dependentSessionsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }
    const dependentSlotsList = await getDependenciesList(Slot, {stewards: stewardId}, async (e) => {
      await e.populate("session");
      return e.session._id;
    });
    if (dependentSlotsList) {
      return res
        .status(405)
        .send(
          `Cet encadrant est encore référencé par des plages de sessions (${dependentSlotsList}). ` +
            "Assurez-vous qu'il ne soit plus référencé avant de le supprimer."
        );
    }

    return await deleteEntity(stewardId, Steward, req.authenticatedUser, res);
  }
);
StewardRouter.delete(...DELETE_STEWARD);
