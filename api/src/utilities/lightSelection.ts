export const lightSelection = {
  activity: {
    name: 1,
    summary: 1,
    volunteeringCoefficient: 1,
    maxNumberOfParticipants: 1,
    description: 1,
    tags: 1,
    secondaryCategories: 1,
    deletedAt: 1,
  },
  team: {name: 1, deletedAt: 1},
  place: {name: 1, maxNumberOfParticipants: 1, summary: 1, availabilitySlots: 1, deletedAt: 1}, // Avail slots for agenda
  steward: {firstName: 1, lastName: 1, phoneNumber: 1, availabilitySlots: 1, deletedAt: 1}, // Avail slots for agenda
  category: {color: 1, name: 1, summary: 1, deletedAt: 1},
  user: {email: 1, firstName: 1, lastName: 1, locale: 1, shownTours: 1, deletedAt: 1},
  project: {
    name: 1,
    start: 1,
    end: 1,
    slug: 1,
    content: 1,
    openingState: 1,
    full: 1,
    theme: 1,
    deletedAt: 1,
  },
};
