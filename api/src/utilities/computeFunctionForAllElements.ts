export async function computeFunctionForAllElements<T>(
  elements: Array<T>,
  functionToCompute: (el: T) => Promise<void>
): Promise<void> {
  // Gain of the async version is around 20% to 40% compared to synchronous version

  // Create all the promises
  const arrayOfPromises = elements.map(functionToCompute);

  // // Resolve them all in the same time
  await Promise.all(arrayOfPromises);
}
