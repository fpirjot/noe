import {FilterQuery, Model} from "mongoose";

export const getDependenciesList = async <T>(
  entityClass: Model<T>,
  conditions: FilterQuery<T>,
  getEntityName = (e: T & {name?: string; _id: any}) => e?.name || e._id
): Promise<string> => {
  const dependentEntities = await entityClass.find(conditions);
  if (dependentEntities.length === 0) return;

  // Need to wait for promises to be resolved if getEntityName is async
  const entityNames = await Promise.all(dependentEntities.map(getEntityName));
  return entityNames.join(", ");
};
