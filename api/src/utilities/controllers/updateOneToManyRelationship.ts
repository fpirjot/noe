import {Model} from "mongoose";

/**
 * Update a one to many relationship whose link is stored in the "to many" elements.
 * For example: the "many sessions / one team" link is stored in the sessions with field team.
 * To update the links, you can call updateOneToManyRelationship(team, sessionsIds, Session, "team").
 * @param elementToRelateTo The base element that is linked/unlinked with all the others
 * @param manyOtherElementsIds The other elements ids
 * @param Entity The type of the other elements in `manyOtherElementsIds`
 * @param fieldName The field name of the base element, in the schema definition of `Entity`
 * @param additionalFieldsToAdd Additional fields to add or modify in the updated document
 */
export const updateOneToManyRelationship = async (
  elementToRelateTo: any,
  manyOtherElementsIds: string[],
  Entity: Model<any>,
  fieldName: string,
  additionalFieldsToAdd?: any
): Promise<string[]> => {
  // -- Create selections --
  // Select and update elements to link with the element
  const entitiesToLinkConditions = {
    [fieldName]: {$not: {$in: elementToRelateTo}},
    _id: manyOtherElementsIds,
  };
  // Select and update entities to unlink from the element
  const entitiesToUnlinkConditions = {
    [fieldName]: elementToRelateTo,
    _id: {$not: {$in: manyOtherElementsIds}},
  };

  // -- Get all the ids of the entities to update (before they get updated, after the update we will not be able to find them anymore) --
  const entitiesToUpdate = await Entity.find(
    {$or: [entitiesToLinkConditions, entitiesToUnlinkConditions]},
    "_id"
  ).lean();
  const entitiesToUpdateIds = entitiesToUpdate.map((s) => s._id);

  // -- Make the updates --
  await Entity.updateMany(entitiesToLinkConditions, {
    [fieldName]: elementToRelateTo,
    ...additionalFieldsToAdd,
  });
  await Entity.updateMany(entitiesToUnlinkConditions, {
    $unset: {[fieldName]: true},
  });

  return entitiesToUpdateIds.map((id) => id.toString());
};
