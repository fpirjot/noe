// Replace nested objects content with only their ID, so we don't save nested objects information
// that is not correct anymore (if somebody modified that in between)
import {RequestHandler} from "express";

export const removeNestedObjectsContent =
  (...args: string[]): RequestHandler =>
  (req, res, next) => {
    for (const arg of args) {
      if (req.body[arg]?._id) {
        // If this is a simple nested object, replace it with the id
        req.body[arg] = req.body[arg]._id;
      } else if (Array.isArray(req.body[arg])) {
        // If this is an array of nested objects, replace each object in the array with its ID, if it has one
        req.body[arg] = req.body[arg].map((el: any) => (el._id ? el._id : el));
      }
    }
    next();
  };
