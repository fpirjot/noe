import * as Sentry from "@sentry/node";
import {Document, HydratedDocument, Model, model, QueryWithHelpers, Schema, Types} from "mongoose";
import {RegistrationD} from "./registration.model";
import {AvailabilitySlotD, AvailabilitySlotSchema} from "./availabilitySlot.model";
import fetch from "node-fetch";
import AbortController from "abort-controller";
import {addDiffHistory, isValidObjectId, softDelete} from "../config/mongoose";
import defaultWelcomePageContent from "../config/defaultWelcomePageContent";
import momentTz from "moment-timezone";
import {logger} from "../services/logger";
import {
  HelloAssoConfig,
  HelloAssoConfigSchema,
  TiBilletConfig,
  TiBilletConfigSchema,
} from "./ticketing.model";
import moment from "moment";
import {lightSelection} from "../utilities/lightSelection";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const mongooseSlugPlugin = require("mongoose-slug-plugin");

// For reference: https://jasonching2005.medium.com/complete-guide-for-using-typescript-in-mongoose-with-lean-function-e55adf1189dc

/**************************************************************
 *            TYPES
 **************************************************************/

type CockpitPreferences = {
  figures: Array<string>;
  charts: {
    useful: Array<string>;
    answers: Array<{key: string; displayMode: "perDay" | "absolute"}>;
  };
};

export type WebhookAction = "create" | "update" | "delete";
export type WebhookEndpoint =
  | "categories"
  | "places"
  | "stewards"
  | "activities"
  | "sessions"
  | "registrations";
type Webhook = {
  actions: Array<WebhookAction>;
  endpoints: Array<WebhookEndpoint>;
  url: string;
  apiKey?: string;
  failedTimes?: number;
};
const WebhookSchema = new Schema<Webhook>({
  actions: {type: [String], required: true},
  endpoints: {type: [String], required: true},
  url: {type: String, required: true},
  apiKey: String,
});

type FormComponentBase = {
  key: string;
  type: string;
  input: boolean;
  label?: string;
  conditional?: {
    show: string;
    when: string;
    eq: string;
  };
  placeholder?: string;
  description?: string;
  displayName?: string;
  disabled?: boolean;
  required?: boolean;
  options?: Array<{label: string; value: string}>;
  content?: string;
  inPdfExport?: boolean;
};
export type FormComponent = FormComponentBase & {
  components?: Array<FormComponentBase>;
};

/**
 * DEFINITION
 */
export type ProjectD = Document & {
  name: string;
  contactEmail: string;
  slug: string;

  // Form data
  formComponents: Array<FormComponent>;
  // SMS message template when contacting participants
  smsMessageTemplate: string;
  // Welcome page content
  content: unknown;

  // Project opening times
  start: string;
  end: string;
  availabilitySlots: Types.DocumentArray<AvailabilitySlotD>;

  // Project opening state management
  openingState: string;
  isPublic: boolean;
  blockSubscriptions: boolean;
  full: boolean;
  secretSchedule: boolean;

  // Enable features in project
  usePlaces: boolean;
  useTeams: boolean;
  useAI: boolean;

  // Ticketing
  // Mode chosen for the ticketing. If no mode is chosen, it means that
  // the ticketing is not mandatory to register, and is deactivated
  ticketingMode?: string;
  customTicketing?: string; // Validation URL to validate ticket numbers
  helloAsso?: HelloAssoConfig; // Hello Asso credentials
  tiBillet?: TiBilletConfig;

  // Volunteering
  minMaxVolunteering: Array<number>;
  blockVolunteeringUnsubscribeIfBeginsSoon: boolean;
  hideVolunteeringJaugeForParticipants: boolean;

  // Orga notes
  notes: string;

  // Allow time overlap when subscribing to sessions
  notAllowOverlap: boolean;

  // Meal times
  breakfastTime: Date;
  lunchTime: Date;
  dinnerTime: Date;

  cockpitPreferences: CockpitPreferences;

  // Customization
  theme: {
    bg: string;
    main: string;
    accent1: string;
    accent2: string;
    imageUrl: string;
  };
  sessionNameTemplate?: string;

  webhooks?: Array<Webhook>;

  deletedAt: Date;
};

export type LightProjectD = Pick<ProjectD, keyof typeof lightSelection.project>;

/**
 * QUERY HELPERS
 */
type ProjectQueryHelpers = {
  findByIdOrSlug<ReturnType = ProjectD>(
    id: string | number | Types.ObjectId,
    ...args: any[]
  ): QueryWithHelpers<HydratedDocument<ReturnType>, HydratedDocument<ReturnType>>;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

const defaultHour = (hour: string) => momentTz.tz(`2000-01-01T${hour}:00`, "Europe/Paris");

// Same as in orga-front
export const DEFAULT_COCKPIT_PREFERENCES: CockpitPreferences = {
  figures: ["notes", "registrations", "volunteering", "schedulingAndSupervision"],
  charts: {
    useful: ["presenceAndMealsStats", "tagsStats", "registrationStats"],
    answers: [],
  },
};

/**
 * SCHEMA
 */
const ProjectSchema = new Schema<ProjectD, Model<ProjectD> & ProjectQueryHelpers>(
  {
    name: {type: String, required: true},
    contactEmail: {type: String, required: false}, // TODO make it "required: true" when needed
    slug: {type: String, required: true},

    formComponents: Object,
    smsMessageTemplate: String,
    content: {type: Object, default: defaultWelcomePageContent, required: true},

    start: String,
    end: String,
    availabilitySlots: [AvailabilitySlotSchema],

    openingState: {
      type: String,
      enum: ["notOpened", "preRegisterOnly", "registerForStewardsOnly", "registerForAll"],
      default: "notOpened",
      required: true,
    },
    isPublic: {type: Boolean, default: false, required: true},
    blockSubscriptions: {type: Boolean, default: false, required: true},
    full: {type: Boolean, default: false, required: true},
    secretSchedule: {type: Boolean, default: false, required: true},

    usePlaces: {type: Boolean, default: true, required: true},
    useTeams: {type: Boolean, default: false, required: true},
    useAI: {type: Boolean, default: false, required: true},

    ticketingMode: {
      type: String,
      enum: [null, "helloAsso", "customTicketing", "tiBillet"],
      default: null,
    },
    helloAsso: HelloAssoConfigSchema,
    tiBillet: TiBilletConfigSchema,
    customTicketing: String,

    minMaxVolunteering: {type: [Number, Number], default: [60, 120]},
    blockVolunteeringUnsubscribeIfBeginsSoon: {type: Boolean, default: false},
    hideVolunteeringJaugeForParticipants: {type: Boolean, default: false},

    notes: String,

    notAllowOverlap: {type: Boolean, default: false},

    breakfastTime: {type: Date, default: defaultHour("08:30"), required: true},
    lunchTime: {type: Date, default: defaultHour("12:00"), required: true},
    dinnerTime: {type: Date, default: defaultHour("19:30"), required: true},

    cockpitPreferences: {
      type: Object,
      default: DEFAULT_COCKPIT_PREFERENCES,
    },

    // Customization
    theme: {
      type: new Schema({
        bg: {type: String, default: "#000a2e"},
        primary: {type: String, default: "#3775e1"},
        accent1: {type: String, default: "#000bff"},
        accent2: {type: String, default: "#7fffbb"},
        imageUrl: String,
      }),
      default: {bg: "#000a2e", primary: "#3775e1", accent1: "#000bff", accent2: "#7fffbb"},
    },
    sessionNameTemplate: String,

    webhooks: [WebhookSchema],

    deletedAt: Date,
  },
  {
    timestamps: true,
    toJSON: {virtuals: true, getters: true},
    toObject: {virtuals: true, getters: true},
  }
);

/**
 * POPULATED VIRTUALS
 */
// All the registrations linked to the project
ProjectSchema.virtual("registrations", {
  ref: "Registration",
  localField: "_id",
  foreignField: "project",
});
// All the registrations linked to the project
ProjectSchema.virtual("sessions", {
  ref: "Session",
  localField: "_id",
  foreignField: "project",
});

/**
 * QUERY HELPERS
 */
ProjectSchema.statics = {
  findByIdOrSlug(idOrSlug, ...args) {
    const conditions = isValidObjectId(idOrSlug)
      ? {$or: [{slug: idOrSlug}, {_id: idOrSlug}, {slug_history: idOrSlug}]}
      : {$or: [{slug: idOrSlug}, {slug_history: idOrSlug}]};
    return this.findOne(conditions, ...args);
  },
} as ProjectQueryHelpers;

/**
 * INSTANCE METHODS
 */
export const projectMethods = {
  // TODO test this with proper unit tests !
  async getHelloAssoAuthTokens(project: ProjectD): Promise<void> {
    if (project.helloAsso?.clientId && project.helloAsso?.clientSecret) {
      try {
        logger.debug(`Getting Hello Asso auth tokens for project ${project._id.toString()}`);
        const response = await fetch("https://api.helloasso.com/oauth2/token", {
          method: "POST",
          body: new URLSearchParams({
            grant_type: "client_credentials",
            client_id: project.helloAsso?.clientId,
            client_secret: project.helloAsso?.clientSecret,
          }).toString(),
          headers: {"content-type": "application/x-www-form-urlencoded"},
        });
        const authResult = await response.json();

        if (authResult.error) {
          throw Error(
            "Hello Asso Error (getHelloAssoAuthTokens)" + JSON.stringify(authResult || {})
          );
        }

        project.helloAsso.token = authResult.access_token;
        project.helloAsso.refreshToken = authResult.refresh_token;
        project.helloAsso.tokenExpirationDate = authResult.expires_in
          ? new Date(new Date().getTime() + authResult.expires_in * 1000)
          : undefined; // Set date at expiration time

        const organisationResponse = await fetch(
          "https://api.helloasso.com/v5/users/me/organizations",
          {headers: {Authorization: `Bearer ${authResult.access_token}`}}
        );
        if (organisationResponse.status === 200) {
          const organisationObject = await organisationResponse.json();
          project.helloAsso.organizationSlug = organisationObject[0].organizationSlug;
        } else {
          // If error, reset the Hello Asso stuff
          project.helloAsso.organizationSlug = undefined;
          project.helloAsso.selectedEvent = undefined;
          project.helloAsso.selectedEventCandidates = undefined;
        }
      } catch (error) {
        logger.error(error);
        Sentry.captureException(error);
      }
    }
  },

  // TODO test this with proper unit tests !
  async refreshHelloAssoAuthToken(project: ProjectD): Promise<void> {
    // Only refresh if client id and secret are present
    if (project.helloAsso?.clientId && project.helloAsso?.clientSecret) {
      // If some refresh token or expiration date is missing, force authentication reset with fresh credentials, and we're done
      if (
        !project.helloAsso.tokenExpirationDate || // If expiration date doesn't exist...
        moment().isAfter(project.helloAsso.tokenExpirationDate) || // ... or if it is past
        !project.helloAsso.refreshToken // or if the refresh token doesn't exist
      ) {
        logger.info("No valid token expiration or refresh token. Getting Hello Asso auth tokens.");
        await projectMethods.getHelloAssoAuthTokens(project);
        logger.info("Got them !");
        await project.save({timestamps: false, validateBeforeSave: false});
        return;
      }

      // Then after this check, if the token expiration date is over, refresh the auth token. No need otherwise.
      if (
        project.helloAsso?.tokenExpirationDate?.getTime() < new Date().getTime() // if token has expired only, refresh the token, or if the expiration date doesn't exist)
      ) {
        try {
          logger.debug(`Refreshing Hello Asso auth tokens for project ${project._id.toString()}`);
          const response = await fetch("https://api.helloasso.com/oauth2/token", {
            method: "POST",
            body: new URLSearchParams({
              grant_type: "refresh_token",
              client_id: project.helloAsso.clientId,
              refresh_token: project.helloAsso.refreshToken,
            }).toString(),
            headers: {"content-type": "application/x-www-form-urlencoded"},
          });
          const authResult = await response.json();

          // Check if the response body contains all needed stuff, otherwise throw an error
          if (!authResult.access_token || !authResult.refresh_token || !authResult.expires_in) {
            throw Error(
              "Hello Asso Error (refreshHelloAssoAuthToken) " + JSON.stringify(authResult || {})
            );
          }

          project.helloAsso.token = authResult.access_token;
          project.helloAsso.refreshToken = authResult.refresh_token;
          project.helloAsso.tokenExpirationDate = authResult.expires_in
            ? new Date(new Date().getTime() + authResult.expires_in * 1000)
            : undefined; // Set date at expiration time

          await project.save({timestamps: false, validateBeforeSave: false});
        } catch (error) {
          logger.error(error);
          Sentry.captureException(error);
        }
      }
    }
  },

  // This gets only called when the user is an organizer, and in orga frontend
  async populateHelloAssoEventCandidates(project: ProjectD): Promise<void> {
    if (project.helloAsso?.token && project.helloAsso?.organizationSlug) {
      try {
        const controller = new AbortController();
        const timeout = setTimeout(() => {
          controller.abort();
        }, 10000);

        logger.debug(`Populating Hello Asso events for project ${project._id.toString()}`);
        const response = await fetch(
          `https://api.helloasso.com/v5/organizations/${project.helloAsso.organizationSlug}/forms?formTypes=Event`,
          {
            headers: {Authorization: `Bearer ${project.helloAsso.token}`},
            signal: controller.signal as AbortSignal,
          }
        );
        const events = await response.json();

        clearTimeout(timeout);
        project.helloAsso.selectedEventCandidates = events.data;
      } catch (error) {
        logger.error(error);
        Sentry.captureException(error);
      }
    }
  },

  async hideSensitiveFields(project: ProjectD, userRegistration: RegistrationD): Promise<void> {
    if (!userRegistration?.role) project.customTicketing = undefined;

    if (project.helloAsso) {
      if (!userRegistration?.role) {
        project.helloAsso.clientSecret = undefined;
      }
      project.helloAsso.token = undefined;
      project.helloAsso.tokenExpirationDate = undefined;
      project.helloAsso.refreshToken = undefined;
    }
  },
};

/**
 * PLUGINS
 */
ProjectSchema.plugin(softDelete);
ProjectSchema.plugin(addDiffHistory, ["helloAsso", "registrations"]);
ProjectSchema.plugin(mongooseSlugPlugin, {tmpl: "<%=name%>"}); // Add autogenerated project slug
ProjectSchema.index({name: 1, deletedAt: 1}, {unique: true}); // Prevent projects with the same name

/**
 * MODEL
 */
export const Project = model<ProjectD, Model<ProjectD> & ProjectQueryHelpers>(
  "Project",
  ProjectSchema
);
