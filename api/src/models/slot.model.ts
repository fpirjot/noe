import {Schema, Document, model, SchemaTypes} from "mongoose";
import {StewardD} from "./steward.model";
import {PlaceD} from "./place.model";
import {SessionD} from "./session.model";
import {indexByIdAndProject, preventDuplicatesInProject, softDelete} from "../config/mongoose";

/**************************************************************
 *            TYPES
 **************************************************************/

export type SlotD = Document & {
  duration: number;
  start: Date;
  end: Date;
  stewardsSessionSynchro: boolean;
  placesSessionSynchro: boolean;
  session: SessionD;
  stewards: Array<StewardD>;
  places: Array<PlaceD>;

  deletedAt: Date;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const SlotSchema = new Schema<SlotD>(
  {
    duration: {type: Number, required: true},
    stewardsSessionSynchro: Boolean,
    placesSessionSynchro: Boolean,
    stewards: [{type: SchemaTypes.ObjectId, ref: "Steward"}],
    places: [{type: SchemaTypes.ObjectId, ref: "Place"}],

    start: {type: Date, required: true},
    end: {type: Date, required: true},

    deletedAt: Date,
  },
  {timestamps: true, toJSON: {virtuals: true}, toObject: {virtuals: true}}
);

/**
 * POPULATED VIRTUALS
 */
SlotSchema.virtual("session", {
  ref: "Session",
  localField: "_id",
  foreignField: "slots",
  justOne: true,
});

/**
 * PLUGINS
 */
SlotSchema.plugin(softDelete);
SlotSchema.plugin(indexByIdAndProject);
SlotSchema.plugin(preventDuplicatesInProject);

/**
 * MODEL
 */
export const Slot = model<SlotD>("Slot", SlotSchema);
