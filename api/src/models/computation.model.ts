import {Schema, Document, model, SchemaTypes} from "mongoose";
import {ProjectD} from "./project.model";
import {addDiffHistory} from "../config/mongoose";

/**************************************************************
 *            TYPES
 **************************************************************/

export enum ComputationState {
  WAITING = "WAITING",
  WORKING = "WORKING",
  SUCCESS = "SUCCESS",
  FAILED = "FAILED",
  CANCELLED = "CANCELLED",
}

export type ComputationD = Document & {
  project: ProjectD;
  state: ComputationState;
  startedAt: Date;
  result: any;
};

/**************************************************************
 *            IMPLEMENTATION
 **************************************************************/

/**
 * SCHEMA
 */
const ComputationSchema = new Schema<ComputationD>(
  {
    project: {type: SchemaTypes.ObjectId, ref: "Project", required: true},
    state: {type: String, required: true},
    startedAt: Date,
    result: SchemaTypes.Mixed,
  },
  {timestamps: true}
);

/**
 * PLUGINS
 */
ComputationSchema.plugin(addDiffHistory);

/**
 * MODEL
 */
export const Computation = model<ComputationD>("Computation", ComputationSchema);
