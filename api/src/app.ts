import express, {ErrorRequestHandler} from "express";
import passport from "passport";
import cors from "cors";
import mongoose from "mongoose";
import helmet from "helmet";
import expressValidator from "express-validator";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import {join} from "path";
import {UserRouter} from "./controllers/user.controller";
import {AuthRouter} from "./controllers/auth.controller";
import {SessionRouter} from "./controllers/session.controller";
import {TeamRouter} from "./controllers/team.controller";
import {ProjectRouter} from "./controllers/project.controller";
import {RegistrationRouter} from "./controllers/registration.controller";
import {ActivityRouter} from "./controllers/activity.controller";
import {StewardRouter} from "./controllers/steward.controller";
import {PlaceRouter} from "./controllers/place.controller";
import {CategoryRouter} from "./controllers/category.controller";
import {PdfRouter} from "./controllers/pdf.controller";
import {TicketingRouter} from "./controllers/ticketing.controller";
import {ComputingRouter} from "./controllers/computing.controller";
import {logger, loggerMiddleware} from "./services/logger";
import {globalErrorHandler, initSentry} from "./services/sentry";
import {initI18n} from "./config/i18n";
import config from "./config/config";
import {checkDatabaseMigrationStatus} from "./services/migrations";
import compression from "compression";
import * as Sentry from "@sentry/node";

export const app = express();

// Check database migration status
checkDatabaseMigrationStatus();

// Setup database
mongoose.set("strictQuery", false);
mongoose.connect(config.mongoose.uri).catch((error) => {
  logger.error(
    `MongoDB connection error to ${config.mongoose.uri}. Please make sure MongoDB is running.`,
    error
  );
  process.exit();
});

app.disable("x-powered-by");

// Enable GZip compression for all requests
app.use(compression());

// Initialize Sentry
if (config.env === "production") initSentry(app);

// Helmet: protection against well known vulnerabilities
// If getting some resources from /assets, allow cross origin requests
const helmetNoCrossOrigin = helmet({crossOriginResourcePolicy: {policy: "same-site"}});
const helmetAllowedCrossOrigin = helmet({crossOriginResourcePolicy: {policy: "cross-origin"}});
app.use((req, res, next) =>
  /^\/assets/.exec(req.path)
    ? helmetAllowedCrossOrigin(req, res, next)
    : helmetNoCrossOrigin(req, res, next)
);

// Cross-origin ressources sharing
app.use(cors({credentials: true, origin: true}));

// Parsing options
app.use(bodyParser.json({limit: "15mb"}));
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Request validation
app.use(expressValidator());

// Authentication
app.use(passport.initialize());

// Request logger
app.use(loggerMiddleware);

// // Internationalization
initI18n(app);

// Static assets
app.use("/assets", express.static(join("src", "assets")));

const router = express.Router();

// Then redirect to the appropriate sub-routers
app.use(router);
router.use("/users", UserRouter);
router.use("/projects", ProjectRouter);
router.use("/auth", AuthRouter);
router.use("/computing", ComputingRouter);
ProjectRouter.use("/:projectId/registrations", RegistrationRouter);
ProjectRouter.use("/:projectId/categories", CategoryRouter);
ProjectRouter.use("/:projectId/places", PlaceRouter);
ProjectRouter.use("/:projectId/stewards", StewardRouter);
ProjectRouter.use("/:projectId/activities", ActivityRouter);
ProjectRouter.use("/:projectId/sessions", SessionRouter);
ProjectRouter.use("/:projectId/teams", TeamRouter);
ProjectRouter.use("/:projectId/pdf", PdfRouter);
ProjectRouter.use("/:projectId/ticketing", TicketingRouter);

// All the rest: 404
app.use((req, res) => {
  res.status(404).send("Sorry, can't find that!");
});

// Global error Handler
app.use(globalErrorHandler as ErrorRequestHandler);

app.use(Sentry.Handlers.errorHandler());
