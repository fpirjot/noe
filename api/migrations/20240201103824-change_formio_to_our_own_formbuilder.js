const typeMigration = {
  textfield: "text",
  textarea: "longText",
  selectboxes: "checkboxGroup",
  radio: "radioGroup",
  htmlelement: "content",
  well: "panel",
};

const migrateConditional = ({show, when, eq} = {}) => {
  if ([show, when, eq].includes(null) || [show, when, eq].includes(undefined)) {
    return undefined;
  } else {
    return {
      show: show ? "show" : "doNotShow",
      when,
      eq,
    };
  }
};

const migrateOptions = (values, dataValues) => {
  const options = (values || dataValues)?.map(({label, value}) => ({label, value}));
  return options?.length > 0 ? options : undefined;
};

const migrateFormComponents = (formComponents, formMapping) => {
  return formComponents
    .map(
      ({
        input,
        label,
        title,
        placeholder,
        description,
        tooltip,
        disabled,
        data: {values: dataValues} = {},
        values,
        validate: {required} = {},
        key,
        type: formioType,
        html,
        content,
        conditional,
        components,
      }) => ({
        input,
        label: formioType === "panel" ? title : label,
        description: description || tooltip,
        disabled,
        required,
        key,
        placeholder,
        content: html || content,
        components:
          components?.length > 0 ? migrateFormComponents(components, formMapping) : undefined,

        // Legacy form mapping
        displayName: formMapping?.find(({formField}) => formField === key)?.columnName,
        inPdfExport: formMapping?.find(({formField}) => formField === key)?.inPdfExport,

        type: typeMigration[formioType] || formioType,

        conditional: migrateConditional(conditional),
        options: migrateOptions(values, dataValues),
      })
    )
    .map((formComp) => {
      // Remove undefined values from formComp
      return Object.fromEntries(
        Object.entries(formComp).filter((e) => {
          // For boolean values, we only keep the ones that are true
          if (["disabled", "required"].includes(e[0]) && e[1] === false) return false;

          // For the rest, we keep the ones that are not null or undefined
          return e[1] !== undefined && e[1] !== null && e[1] !== "";
        })
      );
    });
};

module.exports = {
  async up(db, client) {
    // BACKUP ALL PROJECTS
    //
    // First, backup the existing formComponents fields.
    // Only run this once, so if the formComponentsBackup already exists, don't touch it.

    const projectsToBackup = await db
      .collection("projects")
      .find({
        formComponents: {$exists: true},
        formComponentsBackup: {$exists: false},
      })
      .toArray();
    console.log(`Backing up ${projectsToBackup.length} projects`);

    for (const project of projectsToBackup) {
      await db.collection("projects").updateOne(
        {_id: project._id},
        {
          $set: {
            formComponentsBackup: project.formComponents,
            formMappingBackup: project.formMapping,
          },
        }
      );
    }

    // THEN, MIGRATE ALL PROJECTS based on the backups
    const projects = await db
      .collection("projects")
      .find({formComponentsBackup: {$exists: true}, deletedAt: {$exists: false}})
      .toArray();
    for (const {_id: projectId, formComponentsBackup, formMappingBackup} of projects) {
      const newFormComponents = migrateFormComponents(formComponentsBackup, formMappingBackup);

      await db.collection("projects").updateOne(
        {_id: projectId},
        {
          $set: {formComponents: newFormComponents},
          $unset: {formMapping: true},
        }
      );
    }
  },

  async down(db, client) {},
};
