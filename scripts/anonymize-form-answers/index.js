const {EVENT_FILE_PATH, FORM_ANSWERS_TO_ANONYMIZE} = require("./config");
const {fakerFR} = require("@faker-js/faker");
const fs = require("fs");

async function main() {
  const fullProjectData = await require(EVENT_FILE_PATH);

  // First, anonymize participants
  fullProjectData.registrations = fullProjectData.registrations.map((registration) => {
    return {
      ...registration,
      formAnswers: registration.formAnswers
        ? {
            ...registration.formAnswers,
            ...Object.fromEntries(
              Object.entries(registration.formAnswers).map(([key, answer]) => {
                const anonymizationRegExp = FORM_ANSWERS_TO_ANONYMIZE.find(
                  (answer) => answer.key === key
                )?.regExp;

                if (anonymizationRegExp) answer = fakerFR.helpers.fromRegExp(anonymizationRegExp);

                return [key, answer];
              })
            ),
          }
        : undefined,
    };
  });

  const newFilePath = EVENT_FILE_PATH.replace(/\.json/i, " - ANONYMIZED.json");
  await fs.writeFile(newFilePath, JSON.stringify(fullProjectData, null, 2), (err) => {
    if (err) throw err;
    else {
      console.log(
        "The anonymized version of your export is ready! You can find it here:",
        newFilePath
      );
    }
  });
}

main();
