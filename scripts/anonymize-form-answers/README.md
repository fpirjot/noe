# Anonymize form answers

Use this script to anonymize form answers in a NOÉ export file.

Change the config.js file with your own path to your event export file, 
and specify the form answers to anonymize, and how.

Then, install dependencies and launch the script:
```bash
# install 
npm install

# launch
node index.js
```