// Add some new expressions if you want
const socialSecurityNumberRegExp = "[12] [5-9][0-9] [01][0-9] [0-9]{2} [0-9]{3} [0-9]{3} [0-9]{2}";
const phoneNumberRegExp = "+33 [67] [0-9]{2} [0-9]{2} [0-9]{2} [0-9]{2}";
const nomBateau = "Simon Bidule";
const nomEtNumResponsableLegalRegExp = nomBateau + " " + phoneNumberRegExp;
const loremIpsum = "Lorem ipsum lorem ipsum lorem ipsum lorem ipsum";

///////////////////////////////////////////////////////////////
//                          CONFIG                           //
///////////////////////////////////////////////////////////////

// The absolute path to your event export file
const EVENT_FILE_PATH = "/home/< username >/Downloads/XXXX.json";

// Some form answers to anonymize for your participants:
// - key: the key of the form answer
// - regExp: a regular expression to tell how to replace it. Use commonRegExps.<yourType> to use already defined regexps.
const FORM_ANSWERS_TO_ANONYMIZE = [
  {
    key: "numeroDeSecuriteSociale",
    regExp: socialSecurityNumberRegExp,
  },
  {
    key: "numeroDeTelephone",
    regExp: phoneNumberRegExp,
  },
  {
    key: "nomDuRepresentantELegalE",
    regExp: nomBateau,
  },
  {
    key: "nomEtNumeroDeTelephoneDeLaOuDesPersonneSAContacterEnCasDurgence",
    regExp: nomEtNumResponsableLegalRegExp,
  },
  {
    key: "commentAsTuEntenduParlerDuCampClimat",
    regExp: loremIpsum,
  },
];

///////////////////////////////////////////////////////////////

module.exports = {EVENT_FILE_PATH, FORM_ANSWERS_TO_ANONYMIZE};
