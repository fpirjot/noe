CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
COMMIT=$(git log -1 --pretty=%H)
MESSAGE=$(git log -1 --pretty=%B)

echo "■■■■■■ You're currently on branch '$CURRENT_BRANCH' :"
echo $MESSAGE
echo $COMMIT
echo

while true; do
    read -p "I want to sync the branch... (default: '$CURRENT_BRANCH')  " TARGET_BRANCH
    case $TARGET_BRANCH in
        "" ) TARGET_BRANCH=$CURRENT_BRANCH; break ;;
        * ) break;;
    esac
done

while true; do
    read -p "...with the branch " DEPLOY_BRANCH
    case $DEPLOY_BRANCH in
        "" ) echo "Please give a branch name.";;
        * ) break;;
    esac
done

echo
echo "■■■■■■ You're about to reset the branch '$DEPLOY_BRANCH' to the branch '$TARGET_BRANCH'."

echo
while true; do
    read -p "Do you really want to hard reset '$DEPLOY_BRANCH' to the branch '$TARGET_BRANCH'? All unsaved changes will be lost.  " CONFIRM
    case $CONFIRM in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Please answer Y(yes) or N(no).";;
    esac
done

while true; do
    read -p "Push branch '$TARGET_BRANCH' before ?  " PUSH_BEFORE
    case $PUSH_BEFORE in
        [Yy]* ) echo; echo "Pushing '$TARGET_BRANCH'..."; git checkout $TARGET_BRANCH; git push ; break;;
        [Nn]* ) break;;
        * ) echo "Please answer Y(yes) or N(no).";;
    esac
done

echo
echo "■■■■■■ Deployment..."

echo
git checkout $DEPLOY_BRANCH

echo
echo "Resetting '$DEPLOY_BRANCH' to '$TARGET_BRANCH'..."
git reset --hard $TARGET_BRANCH

echo
echo "Pushing '$DEPLOY_BRANCH'..."
git push -f

echo
git checkout $CURRENT_BRANCH

echo
echo "■■■■■■ Success!"

