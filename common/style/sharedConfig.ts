/**
 * Please, do not change this URL. There should actually only be one instance of the NOE
 * website, and this should be at this URL. It helps also crediting our work ! Thanks :)
 */
export const WEBSITE_URL =
  process.env.NODE_ENV === "production"
    ? "https://get.noe-app.io"
    : process.env.REACT_APP_WEBSITE_URL;
