agenda:
  dateNavigator:
    beginning: Début
    goToBeginningOfEvent: Aller au commencement de l'événement
    goToToday: Aller à aujourd'hui
    today: Aujourd'hui
  filter: Filtrer
  menuDrawer:
    agendaDisplayHours:
      label: Heures par défaut d'affichage de l'agenda
    cellDisplayHeight:
      label: Densité d'affichage
      options:
        comfort: Confort
        compact: Compact
        large: Large
    exportAgendaView:
      buttonTitle: Exporter la vue agenda
      fileName: Vue planning
      formatOption: Exporter au format {{fileFormat}}
      moreOptions: Plus d'options
      warningMessage: |
        L'export est une tâche intensive : si le navigateur plante, diminuez le nombre de sessions
        visibles, ou choisissez l'export en SVG (qui est moins gourmand en ressources).
    slotsOnEachOther:
      label: Disposition des cartes
      options:
        "false": Les unes à côté des autres
        "true": Les unes au dessus des autres
  numberOfDaysSelector:
    allTheEvent: Tout l'événement
    daysMobile: "{{count}} J."
    days_one: "{{count}} Jour"
    days_other: "{{count}} Jours"
alerts:
  alreadyStewardOnOtherSession: Vous êtes déjà encadrant⋅e d'une autre session sur ces mêmes horaires.
  alreadySubscribedToOtherSession: Vous êtes déjà inscrit⋅e à une autre session sur ces mêmes horaires.
  belongsToTeam: Vous êtes inscrit⋅e à cette session car vous êtes membre de l'équipe.
  inConflictWithRegistrationDates: >-
    ⚠️ Vous êtes inscrit⋅e, mais vous n'êtes pas présent⋅e à l'événement sur ces horaires là. Corrigez vos dates de
    présence, ou désinscrivez-vous.
  isSteward: Vous êtes encadrant⋅e sur cette session, vous êtes inscrit⋅e par défaut.
  mustCompleteRegistration: Vous devez compléter votre inscription à l'événement pour vous inscrire aux sessions.
  participantIsNotAvailable: >-
    ⚠️ Cette session est hors des dates de présence que vous avez renseigné. Si vous voulez vous y inscrire, changez
    d'abord vos dates dans la page d'inscription.
  sessionIsFull: Mince! La session est déjà pleine.
changeViewButton:
  agenda: Agenda
  agendaView: Vue agenda
  list: Liste
  listView: Vue liste
filling:
  everybodyIsSubscribed: Inscrit·e d'office
  freeSubscription: Inscription libre
  label: Remplissage
inconsistenciesNotification:
  isOutOfPlaceSlots: est hors des plages d'ouverture de l'espace ({{entitiesInvolved}})
  isOutOfProjectSlots: est hors des plages d'ouverture de l'événement
  isOutOfStewardSlots: est hors des disponibilités de l'encadrant⋅e ({{entitiesInvolved}})
  placesConflictAtTheSameTime: rentre en conflit avec une autre plage horaire qui utilise le même espace au même moment ({{entitiesInvolved}})
  stewardConflictAtTheSameTime: rentre en conflit avec une autre plage horaire qui mobilise le même encadrant au même moment ({{entitiesInvolved}})
  theSlotNumberMessage: La plage horaire n°{{slotNumber}} {{message}}
  title: "Des incohérences ont été trouvées sur la session :"
label: Session
labelAll: Toutes les activités
labelSubscribed: Mon planning
label_clone: Cloner des sessions
label_create: Créer une session
label_createNew: Créer une nouvelle session
label_edit: Modifier une session
label_groupEdit: Édition groupée de sessions
label_other: Sessions
list:
  available: Disponibles
  past: Passées
  subscribedSplashScreen:
    subscribeToSessionsButton: Je m'inscris aux sessions
    subtitle: |
      <p>
        Ajoutez cette page à vos favoris: elle vous indiquera toujours la prochaine activité
        à venir pendant l'événement.
      </p>
      <p>Mais en attendant… allez découvrir le programme !</p>
    title: Ici, vous pourrez retrouver toutes les sessions auxquelles vous êtes inscrit⋅e.
  subtitleAll: Retrouvez ici le <strong>planning complet de l'événement</strong>, et inscrivez-vous à de nouvelles activités.
  subtitleSubscribed: Retrouvez ici toutes les activités auxquelles vous vous êtes <strong>inscrit⋅e</strong>.
  toSeePastSessionsClickOnButton: Pour voir les sessions déjà passées, cliquez sur <br/><button>Sessions Passées</button>
messages:
  cantUnsubscribe: Hu oh… Vous ne pouvez pas vous désinscrire…
  noMoreSpaceAvailable: Oups, quelqu'un a dû s'inscrire avant vous, il n'y a plus de place !
  subscriptionSuccessful: Inscription validée !
  unsubscribeSuccessful: Désinscription réussie !
pdf:
  explanationStewarding: Rempli de bleu = vous encadrez cette session
  explanationVolunteering: Entouré = compte comme du bénévolat
  planning: Planning
schema:
  description:
    label: Description
  name:
    label: Nom de la session
  subscriptionInfo:
    label: Inscription
    viaTeam: Via équipe
searchControls:
  availableFilter:
    tooltip: N'afficher que les sessions pour lesquelles l'inscription est possible.
  categoriesFilter:
    options:
      allTheRest: "- Tout le reste -"
      volunteering: "- Tout le bénévolat -"
    placeholder: Filtrer par catégorie…
  fromDateFilter:
    placeholder: À partir de…
  pdfPlanningButton:
    tooltip: Téléchargez votre programme en PDF pour l'avoir toujours sur vous!
  registeredFilter:
    options:
      "false": Non inscrit⋅e
      "null": Toutes
      "true": Inscrit⋅e
    placeholder: Inscription…
  searchBar:
    placeholder: Chercher une activité…
  showPastSessionsFilter:
    tooltip: Afficher aussi les sessions passées.
  stewardFilter:
    tooltip: N'afficher que les sessions que vous encadrez.
show:
  conflicts: Conflits d'inscription
  info: Informations
  otherTimeSlots: Autres horaires pour cette activité
  place: Lieu
  showFullPage: Voir les informations en pleine page
  thisSessionHasMultipleSlots: Cette session se déroule en {{count}} temps.
  timeSlot: Horaires
  youAreSubscribed: Vous êtes inscrit⋅e.
  youSeeThisInfoBecauseYouAre: Vous voyez ces informations car vous êtes {{role}}. Les participant⋅es n'y ont pas accès.
stillWantToRegisterConfirm: Voulez-vous vous inscrire quand même ?
subscribe: S'inscrire
subscribeUnsubscribeButton:
  confirmUnsubscribe: Valider votre désinscription ?
  volunteeringSoonWarning:
    confirmText: |
      <p>
        <strong>Cette session de bénévolat a lieu dans moins de 48 heures.</strong>
        <br />
        Se désinscrire au dernier moment d'un bénévolat, ce n'est pas très sympa pour les
        organisateur⋅ices qui devront se réadapter en fonction de votre désinscription.
      </p>
      Voulez-vous vous désinscrire quand même?
    tooltip: >-
      Cette session de bénévolat a lieu dans moins de 48 heures. Pour vous désinscrire, demandez directement aux
      organisateur⋅ices de l'événement de le faire pour vous.
unsubscribe: Se désinscrire
userTour:
  subscribedSessions:
    myPlanningAndAllSessions: >-
      <p>🗞️ Pour voir toutes les activités auxquelles vous êtes inscrit·e, allez sur "Mon Planning".</p> Allez sur
      "S'inscrire aux activités" pour voir le programme en entier.
    pdf: 💾 Une fois inscrit·e, téléchargez votre planning en PDF pour l'avoir toujours sur vous !
    phoneAlso: Vous pouvez aussi mettre NOÉ en favori sur votre smartphone. C'est vous qui voyez 😉
    sessionFilterAvailable: >-
      Utilisez les boutons pour ne voir que les activités auxquelles vous pouvez vous inscrire, ou pour afficher les
      activités déjà passées.
    sessionsFilters: >-
      <p>Bienvenue sur le planning de l'événement ! ☀</p> <p>Ici, filtrez les activités par nom, catégorie ou date.
      🔍</p>
    toggleListOrAgenda: >-
      <p>📅 Avez-vous testé la vue agenda ? Pratique pour s'inscrire facilement !</p> Vous pouvez basculer entre liste
      et agenda avec ce gros bouton.
volunteeringMajorationTag: Ce créneau de bénévolat compte {{volunteeringMajoration}} !
