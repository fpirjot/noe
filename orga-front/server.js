const express = require("express");
const i18next = require("i18next");
const i18nextMiddleware = require("i18next-http-middleware");
const fetch = require("node-fetch");
const path = require("path");
const PORT = process.env.PORT;
const fs = require("fs");
const helmet = require("helmet");
const {createProxyMiddleware} = require("http-proxy-middleware");

const app = express();

/*
  SECURITY
*/

app.use(helmet({contentSecurityPolicy: false}));
app.disable("x-powered-by");

/*
  I18N SETUP
*/

// Server type: either orga or inscription
const SERVER_TYPE = "orga";

const basePath = path.join(__dirname, "src", "app", "locales");

const i18nParams = {
  resources: {
    fr: require(path.join(basePath, "fr", "dynamicServer.json"))[SERVER_TYPE],
    en: require(path.join(basePath, "en", "dynamicServer.json"))[SERVER_TYPE],
  },
  fallbackLng: "fr",
  defaultNS: "common",
  preload: ["en", "fr"],
};
const defaultTags = i18nParams.resources[i18nParams.fallbackLng][i18nParams.defaultNS].default;

i18next.use(i18nextMiddleware.LanguageDetector).init(i18nParams);
app.use(i18nextMiddleware.handle(i18next));

/*
  SENTRY TUNNEL
*/

if (process.env.REACT_APP_SENTRY_DSN) {
  const {host: sentryHost, pathname} = new URL(process.env.REACT_APP_SENTRY_DSN);
  const sentryProjectId = pathname.slice(1);
  app.use(
    "/sentry-tunnel",
    createProxyMiddleware({
      target: `https://${sentryHost}/api/${sentryProjectId}/envelope/`,
      changeOrigin: true,
      pathRewrite: {"^/sentry-tunnel": "/"},
    })
  );
}

/*
  JS FILES SERVE
*/

const exclusions = ["service-worker.js"];

// Redirect all JS files to their compressed variant, so the download is quicker
app.get("*.js", function (req, res, next) {
  if (!exclusions.find((exclusion) => req.url.includes(exclusion))) {
    if (req.headers["accept-encoding"]?.includes("br")) {
      // Brotli is the most efficient compression
      req.url = req.url + ".br";
      res.set("Content-Encoding", "br");
    } else if (req.headers["accept-encoding"]?.includes("gzip")) {
      // Otherwise do Gzip
      req.url = req.url + ".gz";
      res.set("Content-Encoding", "gz");
    }

    // Force content mime type to JS for the browser https://stackoverflow.com/a/60759499
    res.set("Content-Type", "application/javascript");
  }

  next();
});

// Static resources should just be served as they are
app.use(express.static(path.resolve(__dirname, "build"), {maxAge: "30d"}));

// Don't know why but the app is still always querying this image no matter what. So just dont give it.
app.get("/images/react.png", (req, res) => res.sendStatus(200));

/*
  COMMON INDEX.HTML BUILD FUNCTION
*/
// Generic function to build the index files
const indexPath = path.resolve(__dirname, "build", "index.html");
const buildIndexFile = (req, res, type, values) => {
  fs.readFile(indexPath, "utf8", (err, htmlData) => {
    if (err) {
      console.error("Error during file reading", err);
      return res.sendStatus(404);
    }
    const tags = {
      title: req.t(`${type}.title`, values),
      description: req.t(`${type}.description`, values),
    };
    htmlData = htmlData
      .replace(`<title>${defaultTags.title}</title>`, `<title>${tags.title}</title>`)
      .replace(`"${defaultTags.title}"`, `"${tags.title}"`)
      .replace(new RegExp(defaultTags.description, "g"), tags.description);
    return res.send(htmlData);
  });
};

const getProject = async (id) => {
  try {
    return await fetch(`${process.env.REACT_APP_API_INTERNAL_URL}/projects/${id}/public`).then(
      (r) => r.json()
    );
  } catch {
    return null;
  }
};

/*
  DEFAULT INDEX SERVE
*/

const returnDefaultIndex = (req, res) => buildIndexFile(req, res, "default");

/*
  PROJECT INDEX SERVE
*/

const returnProjectIndex = async (req, res) => {
  const project = await getProject(req.params.projectId);
  return project
    ? buildIndexFile(req, res, "project", {projectName: project.name})
    : buildIndexFile(req, res, "default");
};

// /*
//   ELEMENT INDEX SERVE
// */
//
// // Copied from api service
// const personName = (person) =>
//   person
//     ? person.firstName && person.firstName.length > 0
//       ? person.lastName && person.lastName.length > 0
//         ? `${person.firstName} ${person.lastName}`
//         : person.firstName
//       : ""
//     : "";
//
// const customNaming = {
//   participants: (r) => personName(r.user),
//   stewards: personName,
//   sessions: (s) => s.activity.name,
// };
//
// const returnElementIndex = async (req, res) => {
//   const project = await getProject(req.params.projectId);
//
//   if (project.openingState === "registerForAll") {
//     const element = await fetch(
//       `${process.env.REACT_APP_API_INTERNAL_URL}/projects/${req.params.projectId}/${req.params.elementEndpoint}/${req.params.elementId}`
//     ).then((r) => r.json());
//     console.log(element);
//     const elementName =
//       (customNaming[req.params.elementEndpoint] &&
//         customNaming[req.params.elementEndpoint](element)) ||
//       element.name;
//     console.log(elementName);
//     return buildIndexFile(req, res, "element", {projectName: project.name, elementName});
//   } else {
//     return buildIndexFile(req, res, "project", {projectName: project.name});
//   }
// };

/*
  ROUTING
 */

// First, serve common routes we often land to with default index
app.get(["/", "/projects", "/public-projects", "/new", "/login", "/signup"], returnDefaultIndex);

// Then only, serve routes that begin with project ID directly.
// Elements are served before projects because the project URL has a wildcard
// app.get("/:projectId/:elementEndpoint/:elementId", returnElementIndex);
app.get(["/:projectId/*", "/:projectId"], returnProjectIndex);

/*
  THE REST
 */

// All the rest: 404
app.use((req, res) => {
  res.status(404).send("Sorry, can't find that!");
});

// Global error Handler
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke, oops!");
});

/*
  START SERVER
 */

// Set up the server to listen on the right port
app.listen(PORT, (error) => {
  if (error) return console.log("Error during app startup", error);
  console.log("Dynamic server listening on " + PORT + "...");
});
