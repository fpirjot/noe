// import {createSlice} from "@reduxjs/toolkit";
// import {fetchWithMessages} from "../utils/fetchWithMessages";
//
// // Frequency at which we refresh the computation status, in ms
// const AI_REFRESH_FREQUENCY = 3000;
//
// export const computingSlice = createSlice({
//   name: "computing",
//   initialState: {
//     project: {},
//     messageIA: undefined,
//     computationId: undefined,
//     queuePosition: 0,
//     queueLength: 10,
//     result: undefined,
//     interval: undefined,
//     computationState: "WAITING",
//   },
//   reducers: {
//     changeQueueDetails: (state, action) => {
//       state.queuePosition = action.payload.position;
//       state.computationState = action.payload.state;
//       state.result = action.payload.result;
//     },
//     changecomputationState: (state, action) => {
//       state.computationState = action.payload;
//     },
//     changeComputationID: (state, action) => {
//       state.computationId = action.payload;
//     },
//     changePeriodicRefreshChecker: (state, action) => {
//       clearInterval(state.interval);
//       if (action.payload !== undefined) {
//         state.interval = action.payload;
//       } else {
//         state.interval = undefined;
//       }
//     },
//   },
// });
//
// const asyncActions = {
//   runIA: (payload) => async (dispatch, getState) => {
//     const state = getState();
//
//     const refreshQueuePosition = async (computationId) => {
//       fetchWithMessages(`computing/${state.currentProject.project._id}/status/${computationId}`, {
//         method: "GET",
//       }).then((data) => {
//         if (data.state === "SUCCESS" || data.state === "FAILED") {
//           dispatch(computingActions.changePeriodicRefreshChecker(undefined));
//         }
//         dispatch(computingActions.changeQueueDetails(data));
//       });
//     };
//
//     fetchWithMessages(
//       `computing/${state.currentProject.project._id}`,
//       {
//         method: "GET",
//       },
//       {200: "Calcul de l'IA lancé."}
//     ).then((result) => {
//       const computationId = result._id;
//       dispatch(computingActions.changeComputationID(computationId));
//       dispatch(
//         computingActions.changePeriodicRefreshChecker(
//           setInterval(async () => {
//             await refreshQueuePosition(computationId);
//           }, AI_REFRESH_FREQUENCY)
//         )
//       );
//     });
//   },
// };
//
// export const computingReducer = computingSlice.reducer;
//
// export const computingActions = {
//   ...computingSlice.actions,
//   ...asyncActions,
// };
//
// export const computingSelectors = {
//   selectQueuePosition: (state) => state.computing.queuePosition,
//   selectQueueLength: (state) => state.computing.queueLength,
//   selectComputationState: (state) => state.computing.computationState,
//   selectComputationId: (state) => state.computing.computationId,
//   selectAIResult: (state) => state.computing.result,
// };
