import {Tag} from "antd";
import {ReactNode} from "react";

export const BetaTag = ({title = "beta", icon}: {title?: ReactNode, icon?: ReactNode}) => (
  <Tag
    size={"small"}
    style={{
      background: "linear-gradient(135deg, var(--noe-accent-1-90), 40%, var(--noe-accent-2))",
      color: "white",
      border: "none",
    }}>
    {icon}
    {title}
  </Tag>
);
