import {Drawer as AntdDrawer} from "antd";
import {DrawerProps} from "antd";

export const Drawer = ({children, setOpen, open, ...props}: DrawerProps & {setOpen: any}) => (
  <AntdDrawer placement="right" onClose={() => setOpen(false)} open={open} {...props}>
    {children}
  </AntdDrawer>
);
