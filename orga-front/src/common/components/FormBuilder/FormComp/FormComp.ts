import {FormCompInputType, FormCompLayoutType} from "../formCompCustomProps/FormCompInputType";

type FormCompBase = {
  isNew?: boolean;
  key: string;
  conditional?: {
    show: "show" | "doNotShow";
    when: string;
    eq: string;
  };
};

export type FormCompInput = FormCompBase & {
  input: true;
  label: string;
  type: FormCompInputType;
  description?: string;
  placeholder?: string;
  displayName?: string;
  disabled?: boolean;
  required?: boolean;
  selectedCount?: {
    min?: number;
    max?: number;
  };
  minMaxNumber?: {
    min?: number;
    max?: number;
  };
  options?: Array<{label: string; value: string}>;
};

export type FormCompLayout = FormCompBase & {
  input: false;
  label?: string;
  type: FormCompLayoutType;
  content?: string;
  components?: string;
};

export type FormComp = FormCompInput | FormCompLayout;

export type FormComps = Array<FormCompInput>;

export const initialInputFormCompValues: Partial<FormCompInput> = {
  type: "text",
  key: "", // We should put this to enable the autofill key feature
  input: true, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};

export const initialContentFormCompValues: Partial<FormCompLayout> = {
  type: "content",
  key: "", // We should put this to enable the autofill key feature
  input: false, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};

export const initialPanelFormCompValues: Partial<FormCompLayout> = {
  type: "panel",
  key: "", // We should put this to enable the autofill key feature
  input: false, // Needed to tell this is not a layout component, but an input component
  isNew: true,
};
