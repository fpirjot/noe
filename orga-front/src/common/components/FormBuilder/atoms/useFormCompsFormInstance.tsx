import {Form} from "antd";
import {FormCompsFormData} from "../FormBuilder";

export const useFormCompsFormInstance = () => Form.useFormInstance<FormCompsFormData>();
