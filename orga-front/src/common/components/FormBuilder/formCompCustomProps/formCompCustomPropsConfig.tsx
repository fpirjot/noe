import React from "react";
import {OptionsList} from "./OptionsList";
import {FormCompInputType, FormCompLayoutType} from "./FormCompInputType";
import {RichTextInput} from "../../../inputs/RichTextInput";
import {useTranslation} from "react-i18next";
import {TextAreaInput} from "../../../inputs/TextAreaInput";
import {NamePath} from "rc-field-form/es/interface";
import {FormBuilder} from "../FormBuilder";
import {NumberInput} from "../../../inputs/NumberInput";
import {Stack} from "../../../layout/Stack";
import {DisplayInput} from "../../../inputs/DisplayInput";
import {useFormCompsFormInstance} from "../atoms/useFormCompsFormInstance";
import {Form} from "antd";

type CustomPropsGroupProps = {
  rootName: NamePath;
  name: number;
  modifKeyBasedOnValue?: (value: string) => void;
};

export type CustomPropsGroup = React.FC<CustomPropsGroupProps>;
export type MainControlsCustomPropsGroup = React.FC<
  CustomPropsGroupProps & {isNewElement?: boolean}
>;

export type FormComponentCustomPropsConfig = {
  MainControls?: MainControlsCustomPropsGroup;
  generalTab?: CustomPropsGroup;
};

const ContentEditor: MainControlsCustomPropsGroup = ({name, modifKeyBasedOnValue}) => {
  const {t} = useTranslation();
  return (
    <RichTextInput
      label={t("common:formBuilder.schema.content.label")}
      placeholder={t("common:formBuilder.schema.content.placeholder")}
      name={[name, "content"]}
      onChange={modifKeyBasedOnValue}
      toolbarSupercharge={[[{header: [1, 2, 3, 4, 5, 6, false]}]]}
    />
  );
};

const PanelEditor: MainControlsCustomPropsGroup = ({
  rootName,
  name,
  modifKeyBasedOnValue,
  isNewElement,
}) => {
  const {t} = useTranslation();

  return (
    <>
      <TextAreaInput
        label={t("common:formBuilder.schema.title.label")}
        autoFocus={!!isNewElement} // Focus if new
        placeholder={t("common:formBuilder.schema.title.placeholder")}
        autoSize={{minRows: 1}}
        name={[name, "label"]}
        required
        onChange={(event) => {
          // If the key is not defined in the component value, it means it's a new component.
          // So pre-fill it unless the user touches the field
          modifKeyBasedOnValue?.(event.target.value);
        }}
      />
      <div
        style={{
          border: "2px dashed var(--colorBorderSecondary)",
          margin: "24px -4px",
          padding: 18,
          borderRadius: 12,
        }}>
        <FormBuilder
          nested
          name={[name, "components"]}
          rootName={[...rootName, name, "components"]}
        />
      </div>
    </>
  );
};

const MinMaxBase = ({
  label,
  name,
  required,
}: {
  label: string;
  name: NamePath;
  required?: boolean;
}) => {
  const {t} = useTranslation();
  return (
    <DisplayInput label={label}>
      <Stack row gap={1}>
        <NumberInput
          noStyle
          placeholder={t("common:formBuilder.schema.minMax.placeholderMin")}
          name={[...name, "min"]}
          min={0}
          disabled={!required}
          title={
            !required ? t("common:formBuilder.schema.minMax.minDisabledIfNotRequired") : undefined
          }
        />
        -
        <NumberInput
          noStyle
          placeholder={t("common:formBuilder.schema.minMax.placeholderMax")}
          name={[...name, "max"]}
          min={0}
        />
      </Stack>
    </DisplayInput>
  );
};

const MinMaxMultipleChoiceProps: CustomPropsGroup = ({rootName, name}) => {
  const {t} = useTranslation();
  const form = useFormCompsFormInstance();
  const requiredVal = Form.useWatch([...rootName, name, "required"], form);
  return (
    <MinMaxBase
      name={[name, "selectedCount"]}
      label={t("common:formBuilder.schema.minMax.labelSelectedCount")}
      required={requiredVal}
    />
  );
};

const MinMaxNumberProps: CustomPropsGroup = ({name}) => {
  const {t} = useTranslation();
  return (
    <MinMaxBase
      name={[name, "minMaxNumber"]}
      label={t("common:formBuilder.schema.minMax.labelNumber")}
    />
  );
};

export const formCompCustomPropsConfig: Partial<
  Record<FormCompInputType | FormCompLayoutType, FormComponentCustomPropsConfig>
> = {
  // Inputs
  number: {generalTab: MinMaxNumberProps},
  radioGroup: {MainControls: OptionsList, generalTab: MinMaxMultipleChoiceProps},
  select: {MainControls: OptionsList},
  checkboxGroup: {MainControls: OptionsList, generalTab: MinMaxMultipleChoiceProps},
  multiSelect: {MainControls: OptionsList, generalTab: MinMaxMultipleChoiceProps},

  // Layout comps
  content: {MainControls: ContentEditor},
  panel: {MainControls: PanelEditor},
};
