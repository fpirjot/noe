import {
  faCheck,
  faFont,
  faHashtag,
  faLink,
  faPhone,
  faQuoteLeft,
  faSquareCaretDown,
  faVectorSquare,
  faSquareCheck,
  faCalendarDay,
  faParagraph,
} from "@fortawesome/free-solid-svg-icons";
import {FormCompType} from "./FormCompInputType";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ReactNode} from "react";
import {
  faCircleDot,
  faSquareCaretDown as faSquareCaretDownRegular,
  faEnvelope,
  faClock,
} from "@fortawesome/free-regular-svg-icons";

export const formCompIcons: Record<FormCompType, ReactNode> = {
  text: <FontAwesomeIcon icon={faFont} />,
  longText: <FontAwesomeIcon icon={faParagraph} />,
  url: <FontAwesomeIcon icon={faLink} />,
  email: <FontAwesomeIcon icon={faEnvelope} />,
  phoneNumber: <FontAwesomeIcon icon={faPhone} />,
  number: <FontAwesomeIcon icon={faHashtag} />,
  checkbox: <FontAwesomeIcon icon={faCheck} />,
  radioGroup: <FontAwesomeIcon icon={faCircleDot} />,
  select: <FontAwesomeIcon icon={faSquareCaretDownRegular} />,
  checkboxGroup: <FontAwesomeIcon icon={faSquareCheck} />,
  multiSelect: <FontAwesomeIcon icon={faSquareCaretDown} />,
  datetime: <FontAwesomeIcon icon={faClock} />,
  day: <FontAwesomeIcon icon={faCalendarDay} />,
  panel: <FontAwesomeIcon icon={faVectorSquare} />,
  content: <FontAwesomeIcon icon={faQuoteLeft} />,
} as const;
