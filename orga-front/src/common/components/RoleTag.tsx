import {Tag} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";

type Role = "guest" | "contrib" | "admin";

export const ROLES_MAPPING: Array<{value: Role; color: string}> = [
  {value: "guest", color: "#6cdac5"},
  {value: "contrib", color: "#5f98ed"},
  {value: "admin", color: "#1330f5"},
];

export const RoleTag = React.memo(
  ({roleValue, small = false}: {roleValue: Role; small?: boolean}) => {
    const {t} = useTranslation();
    if (!roleValue) return null;
    const roleObject = ROLES_MAPPING.find((r) => r.value === roleValue);
    const roleLabel = t(`registrations:schema.role.options.${roleValue}.label`);
    return (
      <Tag color={roleObject?.color} title={small ? roleLabel : undefined}>
        {small ? roleLabel[0] : roleLabel}
      </Tag>
    );
  }
);
