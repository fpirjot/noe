import React from "react";

export const ColorDot = ({size = 26, color, style}) => (
  <div
    className="containerH"
    style={{
      height: size,
      width: size,
      outline: !color ? "1px solid var(--colorBorder)" : undefined,
      outlineOffset: -1,
      backgroundColor: color,
      borderRadius: "50%",
      ...style,
    }}
  />
);
