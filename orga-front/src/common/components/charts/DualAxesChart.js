import {DualAxes} from "@ant-design/plots";
import {ChartContainer} from "./ChartContainer";

export default function DualAxesChart({config}) {
  return <ChartContainer chart={DualAxes} config={config} />;
}
