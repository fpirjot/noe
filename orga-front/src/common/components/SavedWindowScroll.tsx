import {useDispatch, useSelector} from "react-redux";
import {viewActions, viewSelectors} from "../../features/view";
import {useDebounce} from "../hooks/useDebounce";
import {useEffect} from "react";
import {useViewEventListener} from "../hooks/useViewEventListener";

export const SavedWindowScroll = () => {
  const dispatch = useDispatch();
  const reduxScroll = useSelector(viewSelectors.selectScroll) || 0;

  const debouncedChangeScroll = useDebounce(() => {
    dispatch(viewActions.changeScroll(window.scrollY));
  }, 50);

  useViewEventListener("scroll", debouncedChangeScroll);

  useEffect(() => {
    window.scrollTo(0, reduxScroll);
  }, [window.location.pathname]);

  return null;
};
