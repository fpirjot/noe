import React from "react";
import Icon, {GitlabOutlined, MailOutlined} from "@ant-design/icons";
import {useTranslation} from "react-i18next";
import {ReactComponent as DiscordLogo} from "./../../app/images/discord-outlined.svg";
import {ReactComponent as NoeLogo} from "./../../app/images/logo-base.svg";
import {LinkContainer} from "./LinkContainer";
import {URLS} from "../../app/configuration";

export const DiscordLink = (props) => (
  <LinkContainer
    href="https://discord.gg/t6PwHhTUXP"
    icon={<Icon component={DiscordLogo} />}
    {...props}>
    Discord{" "}
  </LinkContainer>
);

export const GitlabLink = (props) => (
  <LinkContainer
    href="https://gitlab.com/alternatiba/noe/-/blob/master/CONTRIBUTING.md"
    icon={<GitlabOutlined />}
    {...props}>
    Gitlab
  </LinkContainer>
);

export const EmailLink = (props) => {
  const {t} = useTranslation();
  return (
    <LinkContainer
      href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=${t(
        "common:mailtoSubjectKnowMoreAboutNOE"
      )}`}
      target=""
      icon={<MailOutlined />}
      {...props}>
      {t("common:email")}
    </LinkContainer>
  );
};

export default function NOESocialIcons({expanded = false, linkClassname}) {
  const {t} = useTranslation();

  const iconStyle = {fontSize: 18};

  return (
    <div className="containerV" style={{alignItems: "center", gap: 8}}>
      {/* Website link */}
      <LinkContainer
        showTitle={false}
        href={URLS.WEBSITE}
        icon={
          <div
            className={"containerH"}
            style={{flexWrap: "wrap", justifyContent: "center", textAlign: "center", gap: 8}}>
            <Icon component={NoeLogo} />
            {new URL(URLS.WEBSITE).hostname}
          </div>
        }
        className={linkClassname}>
        {t("common:website")}
      </LinkContainer>

      {/* Social icons */}
      <div
        className={"containerH"}
        style={{gap: 16, justifyContent: "center", marginTop: 4, flexWrap: "wrap"}}>
        <DiscordLink style={iconStyle} className={linkClassname} showTitle={expanded} />
        <GitlabLink style={iconStyle} className={linkClassname} showTitle={expanded} />
        <EmailLink style={iconStyle} className={linkClassname} showTitle={expanded} />
      </div>
    </div>
  );
}
