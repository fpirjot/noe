import {FormItemProps} from "./FormItem";
import {SelectProps} from "antd";
import {SelectInput} from "./SelectInput";
import {timeFormatter} from "../../utils/formatters";
import React from "react";
import {useTranslation} from "react-i18next";

export const DurationSelectInput = (
  props: FormItemProps<Omit<SelectProps, "options" | "showSearch">>
) => {
  const {t} = useTranslation();
  return (
    <SelectInput
      label={t("common:schema.duration.label")}
      placeholder={t("common:schema.duration.placeholder")}
      options={Array.from(Array(287), (_, i) => ({
        value: (i + 1) * 5,
        label: timeFormatter.duration((i + 1) * 5),
      }))}
      showSearch
      {...props}
    />
  );
};
