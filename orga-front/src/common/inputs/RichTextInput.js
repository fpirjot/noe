import {PendingSuspense} from "../components/Pending";
import {FormItem, FormItemProps} from "./FormItem";
import React from "react";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import type {TextEditorProps} from "../components/TextEditor";
import type {PendingProps} from "../components/Pending";

const TextEditor = lazyWithRetry(() =>
  import(/* webpackPreload: true */ "../components/TextEditor")
);

export const RichTextInput = ({
  noFadeIn,
  ...props
}: FormItemProps<TextEditorProps> & {noFadeIn?: PendingProps["noFadeIn"]}) => (
  // Need to add the suspense around the form tag, because if in between the form data doesn't pass and it creates bugs
  <PendingSuspense animationDelay={"800ms"} minHeight={100} noFadeIn={noFadeIn}>
    <FormItem {...props}>
      <TextEditor />
    </FormItem>
  </PendingSuspense>
);
