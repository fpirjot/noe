import {Checkbox} from "antd";
import React, {ReactNode} from "react";
import {FormItem, FormItemProps} from "./FormItem";
import {CheckboxGroupProps as AntdCheckboxGroupProps} from "antd/es/checkbox";

type CheckboxGroupProps = Omit<AntdCheckboxGroupProps, "options"> & {
  options?: Array<{value: string | number; key: React.Key; label: ReactNode}>;
};

export const CheckboxGroupComponent = ({options, ...props}: CheckboxGroupProps) => {
  return (
    <Checkbox.Group {...props}>
      <div className={"containerV"}>
        {options?.map((opt) => (
          <Checkbox value={opt.value} key={opt.value}>
            {opt.label}
          </Checkbox>
        ))}
      </div>
    </Checkbox.Group>
  );
};

export const CheckboxGroupInput = (props: FormItemProps<CheckboxGroupProps>) => (
  <FormItem {...props}>
    <CheckboxGroupComponent />
  </FormItem>
);
