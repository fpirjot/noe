import {Unstable_Grid2 as MuiGrid, Grid2Props as MuiGridProps} from "@mui/material";

type GridProps = Omit<MuiGridProps, "gap">;
export default function Grid(props: GridProps) {
  return <MuiGrid columnSpacing={{md: 6, lg: 8}} rowSpacing={6} {...props} />;
}
