import React, {Suspense, useState} from "react";
import {ElementEditProps} from "../pages/EditPage";

export const useNewElementModal = <T extends {_id: string}>(
  ElementEdit?: React.FC<ElementEditProps<T>>
) => {
  const [showNewEntityModal, setShowNewEntityModal] = useState(false);

  // Return the CreateButton, and the Modal Component
  return [
    setShowNewEntityModal,
    ({onCreate}: Pick<ElementEditProps<T>, "onCreate">) =>
      showNewEntityModal && ElementEdit ? (
        <Suspense fallback={null}>
          <ElementEdit
            asModal
            modalOpen={showNewEntityModal}
            setModalOpen={setShowNewEntityModal}
            id="new"
            onCreate={onCreate}
          />
        </Suspense>
      ) : null,
  ] as const;
};
