// Helper to quickly implement javascript tweaks based on a window event
import {useEffect} from "react";

export const useViewEventListener = (
  eventType,
  listener,
  elementToWatch = window,
  initializeDirectly = false
) => {
  useEffect(() => {
    initializeDirectly && listener();
  }, []);

  useEffect(() => {
    elementToWatch.addEventListener(eventType, listener);
    return () => elementToWatch.removeEventListener(eventType, listener);
  });
};
