import useToken from "antd/es/theme/useToken";

export const useThemeToken = () => {
  const [_, themeToken] = useToken();
  return themeToken;
};
