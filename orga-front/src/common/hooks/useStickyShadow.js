import {debounce, useDebounce} from "./useDebounce";
import {useViewEventListener} from "./useViewEventListener";

// Allows to put a shadow on a sticky object when it begins to stick.
export const useStickyShadow = (querySelector = ".sticky-element", placement = "top") => {
  const debouncedUpdateStickyShadow = useDebounce(() => {
    const elem = document.querySelector(querySelector);
    // Once the element becomes sticky, add shadow
    if (elem) {
      if (
        (placement === "top" && window.scrollY === elem.offsetTop && window.scrollY !== 0) ||
        (placement === "bottom" &&
          window.scrollY + window.innerHeight === elem.offsetTop + elem.offsetHeight)
      ) {
        elem.classList.add(`sticking-shadow-${placement}`);
      } else {
        elem.classList.remove(`sticking-shadow-${placement}`);
      }
    }
  }, 17);

  useViewEventListener("scroll", debouncedUpdateStickyShadow, window, true);
};
