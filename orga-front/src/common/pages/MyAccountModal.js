import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {currentUserActions} from "../../features/currentUser.js";
import {usersActions, usersSelectors} from "../../features/users.js";
import {Alert, Button, Collapse, Form, App, Modal, Popconfirm} from "antd";
import {EditPage} from "./EditPage";
import {checkPasswordsAreSame, validatePassword} from "./AuthPage";
import {UserOutlined} from "@ant-design/icons";
import {CardElement} from "../components/CardElement";
import {DisplayOfflineModeFeatureSwitch} from "../../utils/offlineModeUtilities";
import {useTranslation} from "react-i18next";
import {TextInput, TextInputEmail, TextInputPassword} from "../inputs/TextInput";
import {SelectInput} from "../inputs/SelectInput";
import {DisplayInput} from "../inputs/DisplayInput";
import {SwitchInput} from "../inputs/SwitchInput";
import {BetaTag} from "../components/BetaTag";
import {viewActions, viewSelectors} from "../../features/view";

const {Panel} = Collapse;

const checkOldPasswordIsNotTheSameValidator = ({getFieldValue}) => ({
  validator(_, value) {
    const newPasswordGiven = getFieldValue("oldPassword");
    const oldPasswordGiven = value;

    return newPasswordGiven.length && !oldPasswordGiven.length
      ? Promise.reject(new Error("Vous devez saisir votre ancien mot de passe pour le changer."))
      : newPasswordGiven === oldPasswordGiven
      ? Promise.reject(new Error("Le nouveau mot de passe doit être différent du précédent."))
      : Promise.resolve();
  },
});

const DeleteAccountZone = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const [canDeleteAccount, setCanDeleteAccount] = useState(false);

  return (
    <CardElement
      style={{borderColor: "red", marginBottom: 0}}
      headStyle={{borderColor: "red"}}
      title={<strong style={{color: "red"}}>{t("users:edit.dangerZone.title")}</strong>}>
      <Alert
        style={{marginBottom: 26}}
        type="error"
        message={t("users:edit.dangerZone.accountDeletionAlert.message")}
        description={t("users:edit.dangerZone.accountDeletionAlert.description")}
      />

      <TextInput
        style={{marginBottom: 20}}
        label={t("users:edit.dangerZone.writeIWantToDeleteMyAccountToClick")}
        onChange={(event) =>
          setCanDeleteAccount(
            event.target.value === t("users:edit.dangerZone.iWantToDeleteMyAccount")
          )
        }
        placeholder={t("users:edit.dangerZone.iWantToDeleteMyAccount")}
      />
      <Popconfirm
        open={canDeleteAccount ? undefined : false}
        title={t("common:areYouSure")}
        onConfirm={() => dispatch(currentUserActions.deleteAccount())}
        okText={t("users:edit.dangerZone.yesDeleteMyAccount")}
        okButtonProps={{danger: true}}
        cancelText={t("common:cancel")}>
        <Button danger disabled={!canDeleteAccount} type="primary">
          {t("users:edit.dangerZone.deleteAccountButton")}
        </Button>
      </Popconfirm>
    </CardElement>
  );
};

export default function MyAccountModal({openState, extra}) {
  const {message} = App.useApp();
  const {i18n, t} = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const user = useSelector(usersSelectors.selectEditing);
  const darkMode = useSelector(viewSelectors.selectDarkMode);
  const [showModal, setShowModal] = openState;

  const onValidation = async (formData) => {
    dispatch(usersActions.persist({...user, ...formData}));
    form.resetFields(["oldPassword", "password", "confirmPassword"]);
  };

  return (
    <Modal
      open={showModal}
      width={"min(90vw, 1000pt)"}
      footer={false}
      onCancel={() => setShowModal(false)}>
      <div className="modal-page-content">
        <EditPage
          icon={<UserOutlined />}
          form={form}
          title={t("users:labelMyAccount")}
          goBackAfterAction={false}
          backButton={false}
          customButtons={
            <Button
              type="primary"
              danger
              onClick={() => {
                dispatch(currentUserActions.logOut());
                setShowModal(false);
                message.success(t("users:messages.logOutSuccessful"));
              }}>
              {t("users:edit.logOutButton")}
            </Button>
          }
          onValidation={onValidation}
          record={user}
          initialValues={user}>
          <CardElement title={t("users:edit.myInformation")}>
            <div className="container-grid">
              <div className="container-grid three-per-row">
                <TextInput i18nNs="users" name="firstName" />
                <TextInput i18nNs="users" name="lastName" />
                <TextInputEmail i18nNs="users" name="email" />
              </div>

              <Collapse>
                <Panel header={t("users:edit.changePassword.title")}>
                  <div className="container-grid">
                    <TextInputPassword
                      label={t("users:schema.password.labelCurrent")}
                      name="oldPassword"
                      placeholder={t("users:schema.password.placeholder")}
                      autoComplete="current-password"
                      required
                    />

                    <div className="container-grid two-per-row">
                      <TextInputPassword
                        label={t("users:schema.password.labelNew")}
                        name="password"
                        dependencies={["oldPassword"]}
                        placeholder={t("users:schema.password.placeholder")}
                        autoComplete="new-password"
                        rules={[
                          checkOldPasswordIsNotTheSameValidator,
                          {validator: validatePassword(form)},
                          {min: 8},
                        ]}
                      />

                      <TextInputPassword
                        label={t("users:schema.password.labelConfirmNew")}
                        name="confirmPassword"
                        placeholder={t("users:schema.password.placeholder")}
                        autoComplete="new-password"
                        dependencies={["password"]}
                        rules={[{validator: checkPasswordsAreSame(form)}]}
                      />
                    </div>
                  </div>
                </Panel>
              </Collapse>
            </div>
          </CardElement>

          <CardElement title={t("users:edit.myPreferences")}>
            <div className="container-grid two-per-row">
              <SelectInput
                i18nNs="users"
                name="locale"
                defaultValue={i18n.language}
                options={[
                  {value: "fr", label: t("users:schema.locale.options.french")},
                  {value: "en", label: t("users:schema.locale.options.english")},
                ]}
              />
              <SwitchInput
                label={t("users:schema.darkMode.label")}
                icon={<BetaTag title={"Experimental"} />}
                checked={darkMode}
                onChange={(val) => dispatch(viewActions.changeDarkMode(val))}
              />
              <DisplayInput i18nNs="users" name="resetUserTours">
                <Button
                  onClick={() =>
                    dispatch(
                      usersActions.persist({
                        _id: user._id,
                        shownTours: [],
                      })
                    )
                  }>
                  {t("users:schema.resetUserTours.buttonTitle")}
                </Button>
              </DisplayInput>
            </div>
          </CardElement>

          <Collapse style={{marginBottom: 26}}>
            <Panel header={t("common:offlineMode.title")}>
              <DisplayOfflineModeFeatureSwitch />
            </Panel>
          </Collapse>

          {extra}

          <Collapse style={{marginTop: 26}}>
            <Panel header={t("users:edit.accountManagement")}>
              <DeleteAccountZone />
            </Panel>
          </Collapse>
        </EditPage>
      </div>
    </Modal>
  );
}
