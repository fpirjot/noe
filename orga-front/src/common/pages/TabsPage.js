import React from "react";
import {Tabs} from "antd";
import {useLocation} from "react-router-dom";
import {PageHeading} from "../components/PageHeading";
import {useSelector} from "react-redux";
import {viewSelectors} from "../../features/view";

export function TabsPage({
  title,
  icon,
  defaultActiveKey,
  onValidation,
  customButtons,
  children,
  fullWidth = false,
  items,
  subtitle,
  ...otherProps
}) {
  // Move to the appropriate tab if there is a hash in the url
  const hash = useLocation().hash.slice(1);
  if (hash !== "") defaultActiveKey = hash;

  const Header = () => {
    const isModified = useSelector(viewSelectors.selectIsModified);

    return (
      <PageHeading
        className="tabs-page-header"
        icon={icon}
        title={title}
        buttonTitle={onValidation ? "Enregistrer" : undefined}
        customButtons={customButtons}
        buttonDisabled={!isModified}
        onButtonClick={onValidation}
      />
    );
  };

  return (
    <div className="page-container no-bottom-padding">
      <Header />
      {subtitle && <div>{subtitle}</div>}
      <div className={fullWidth ? "full-width-content" : undefined}>
        <Tabs
          defaultActiveKey={defaultActiveKey || 1}
          onChange={(activeKey) => (window.location.hash = `#${activeKey}`)}
          {...otherProps}
          renderTabBar={
            fullWidth &&
            ((props, TabBarComponent) => <TabBarComponent {...props} className="with-margins" />)
          }
          items={items}
        />
      </div>
    </div>
  );
}
