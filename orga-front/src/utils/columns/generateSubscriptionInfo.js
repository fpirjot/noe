import {t} from "i18next";
import {Link} from "react-router-dom";
import {dateFormatter} from "../formatters";
import {personName} from "../utilities";

export const generateSubscriptionInfo = (
  registration,
  sessionSubscription,
  registrations,
  showTeam
) => {
  if (!sessionSubscription) return "Inscription non enregistrée";
  let res;

  // added by who ?
  if (registrations) {
    const subscribedByRegistration = registrations.find(
      (r) => r.user._id === sessionSubscription.subscribedBy
    );

    if (subscribedByRegistration?._id === registration._id) res = "Manuelle";
    else if (subscribedByRegistration?.role) {
      const subscribedByName = ` ${personName(subscribedByRegistration.user)}`;

      res = (
        <>
          Par{" "}
          <Link to={`./../../participants/${subscribedByRegistration._id}`}>
            {subscribedByName}
          </Link>
        </>
      );
    }
  }

  // Added as a session or team subscription ?
  if (showTeam) {
    const sessionSubscriptionTeam = registration.teamsSubscriptions
      .map((ts) => ts.team)
      ?.find((t) => t._id === sessionSubscription.team);
    if (sessionSubscriptionTeam) {
      res = (
        <>
          {res}
          <br />
          {t("sessions:schema.subscriptionInfo.viaTeam")}{" "}
          <Link to={`./../../teams/${sessionSubscriptionTeam._id}`}>
            {sessionSubscriptionTeam.name}
          </Link>
        </>
      );
    }
  }

  // Add date of subscription
  res = (
    <>
      {res && (
        <>
          {res} <br />
        </>
      )}
      <span style={{color: "gray"}}>
        {dateFormatter.longDateTime(sessionSubscription.createdAt, true, false)}
      </span>
    </>
  );

  return res;
};
