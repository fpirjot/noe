import dayjs from "dayjs";
import {Link} from "react-router-dom";
import React from "react";
import {sorter} from "../sorters";

export const generateRegistrationDispoColumn = (path) => ({
  title: "Disponibilité",
  dataIndex: "dispo",
  render: (text, record) => {
    const nextOrCurrentSession = record.nextOrCurrentSession;
    if (nextOrCurrentSession) {
      const nextSessionIn = -dayjs().diff(nextOrCurrentSession.startDate, "minute");
      if (nextSessionIn <= 0) {
        return <Link to={`${path}/sessions/${nextOrCurrentSession.sessionId}`}>Occupé.e</Link>;
      } else if (nextSessionIn < 120) {
        return (
          <Link to={`${path}/sessions/${nextOrCurrentSession.sessionId}`}>Bientôt occupé.e</Link>
        );
      }
    }

    return "Libre";
  },
  sorter: (a, b) =>
    sorter.date(a.nextOrCurrentSession?.startDate, b.nextOrCurrentSession?.startDate),
});
