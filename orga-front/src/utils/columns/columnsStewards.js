import {sorter} from "../sorters";

export const columnsStewards = [
  {
    title: "Prénom",
    dataIndex: "firstName",
    sorter: (a, b) => sorter.text(a.firstName, b.firstName),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Nom",
    dataIndex: "lastName",
    sorter: (a, b) => sorter.text(a.lastName, b.lastName),
    searchable: true,
  },
];
