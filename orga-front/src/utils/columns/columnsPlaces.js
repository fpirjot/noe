import React from "react";
import {sorter} from "../sorters";

export const columnsPlaces = [
  {
    title: "Nom",
    dataIndex: "name",
    sorter: (a, b) => sorter.text(a.name, b.name),
    defaultSortOrder: "ascend",
    searchable: true,
  },
  {
    title: "Jauge max",
    dataIndex: "maxNumberOfParticipants",
    render: (text) =>
      text ? `jauge de ${text}` : <span style={{color: "gray"}}>pas de jauge max.</span>,
    sorter: (a, b) => sorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
  },
];
