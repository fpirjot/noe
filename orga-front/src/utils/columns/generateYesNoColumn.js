import {sorter} from "../sorters";

export const generateYesNoColumn = (
  key,
  render = (text, record) => (record[key] ? "✔️" : "❌")
) => ({
  dataIndex: key,
  sorter: (a, b) => sorter.number(a[key], b[key]),
  render,
  filters: [
    {text: "Oui", value: true},
    {text: "Non", value: false},
  ],
  onFilter: (value, record) => record[key] === value,
});
