import {useSelector} from "react-redux";
import {projectsSelectors} from "../../features/projects";
import {personName} from "../utilities";

export const useRegistrationTicketsColumns = () => {
  const project = useSelector(projectsSelectors.selectEditing);

  return [
    {
      title: "Numéro de billet",
      dataIndex: "id",
    },
    project.ticketingMode === "helloAsso" && {
      title: "Nom",
      dataIndex: "username",
      render: (text, record) => personName(record.user),
    },
    {
      title: "Article",
      dataIndex: "name",
    },
    {
      title: "Montant",
      dataIndex: "amount",
      render: (text, record) => `${record?.amount} €`,
    },
  ].filter((el) => !!el);
};
