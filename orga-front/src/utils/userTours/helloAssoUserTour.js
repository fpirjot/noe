import {Button} from "antd";
import {VideoCameraOutlined} from "@ant-design/icons";

export default () => [
  {
    content: (
      <>
        <p>
          Vous voulez connecter une billetterie Hello Asso avec NOÉ ? Parfait ! Nous avons réalisé
          une vidéo sur le sujet.
        </p>
        <Button
          type={"primary"}
          href={"https://youtu.be/f7eRko8HfJk"}
          target={"_blank"}
          icon={<VideoCameraOutlined />}>
          Voir la vidéo
        </Button>
      </>
    ),
    position: "center",
  },
];
