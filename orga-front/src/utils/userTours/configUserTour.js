import NOESocialIcons from "../../common/components/NOESocialIcons";
import {selectTab} from "./userTourUtilities";

export default (currentRegistration) => [
  {
    content: "👋 Bienvenue sur NOÉ ! Voici la page de configuration de votre événement.",
    position: "center",
  },

  /**
   * General
   */
  {
    ...selectTab("main"),
    content:
      "⚙️ Dans l'onglet Général, vous pouvez configurer les informations de base de votre événement.",
  },
  {
    ...selectTab("main", ".userTourProjectAvailabilities"),
    content:
      "Pensez à renseigner les dates de votre événement : NOÉ en a besoin pour vous aider à l'organiser au mieux.",
  },

  /**
   * Members
   */
  ["admin", "contrib"].includes(currentRegistration.role) && {
    ...selectTab("members"),
    content:
      "👥 Dans l'onglet Membres, vous pouvez voir et gérer les membres de votre équipe organisatrice.",
  },
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("members", ".userTourAddMembers"),
    content:
      "Vos co-équipiè·res ne sont pas encore sur NOÉ ? Envoyez-leur une invitation par email !",
  },

  /**
   * Welcome page
   */
  {
    ...selectTab("welcome-page"),
    content:
      "🏠 La page d'accueil de votre événement a un but à la fois promotionnel et informatif. C'est cette page que vous pourrez partager à vos participant·es pour qu'iels s'inscrivent. Personnalisez-la à votre image !",
  },

  /**
   * Form
   */
  {
    ...selectTab("form"),
    content:
      "📝 Construisez le formulaire d'inscription parfait pour vos participant·es, directement dans NOÉ. cette page est toute nouvelle, n'hésitez pas à nous faire des retours !",
  },
  {
    ...selectTab("form", ".userTourAddFormQuestion"),
    content: (
      <>
        <p>Ajoutez de nouvelles questions à votre formulaire d'inscription.</p>
        Choisissez parmis une multitude de types de questions, affichez les données partout dans
        NOÉ, et même dans les exports PDF.
      </>
    ),
  },
  {
    ...selectTab("form", ".userTourAddOtherComponents"),
    content:
      "Vous pouvez aussi ajouter du contenu libre pour communiquer des informations importantes, ou grouper vos questions.",
  },

  /**
   * Registrations
   */
  {
    ...selectTab("registrations"),
    content:
      "🕹 Gérez les modalités d'inscription des participant·es et vos communications avec elleux.",
  },
  {
    ...selectTab("registrations", ".userTourSmsMessageTemplate"),
    content:
      "Envie de pouvoir envoyer des SMS facilement à vos participant·es ? Configurez-les ici.",
  },

  /**
   * Ticketing
   */
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("ticketing"),
    content:
      "🎟️ Besoin d'intégrer une billetterie pour votre événement ? pas de problème ! NOÉ propose notamment une intégration avec Hello Asso.",
  },

  /**
   * Advanced
   */
  ["admin"].includes(currentRegistration.role) && {
    ...selectTab("advanced"),
    content:
      "🛠 Dans l'onglet Avancé, vous trouverez les fonctionnalités d'import et d'export, l'historique de modifications, et d'autres choses utiles.",
  },

  {
    content: (
      <>
        <p>Et voilà ! 🤗</p>
        <p>
          Dites-nous ce que vous avez pensé de ce tour. Clair, pas clair ? Trop court, trop long ?{" "}
          <strong>On veut tout savoir !</strong>
        </p>
        <p>
          <strong>Envoyez-nous un petit message en cliquant sur une des icônes 👇</strong>
        </p>
        <div className={"containerH"} style={{justifyContent: "center", marginTop: 20}}>
          <NOESocialIcons expanded />
        </div>
      </>
    ),
    position: "center",
  },
];
