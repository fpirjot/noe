import {personName} from "../utilities";

export const searchInActivityFields = (activity) => [
  activity.name,
  activity.category.name,
  ...(activity.secondaryCategories || []),
  ...(activity.stewards?.map(personName) || []),
  ...(activity.places?.map((p) => p.name) || []),
];
