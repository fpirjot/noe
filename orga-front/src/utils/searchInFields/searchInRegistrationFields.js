export const searchInRegistrationFields = (registration) => [
  registration.user.firstName,
  registration.user.lastName,
  registration.user.email,
  ...(registration.tags || []),
];
