import {useParams} from "react-router-dom";
import {ReactNode} from "react";

export const withFallBackOnUrlId = <Props>(
  Component: (props: Props) => ReactNode
): ((props: Props) => ReactNode) => {
  return ({id, ...props}) => {
    const {id: urlId} = useParams();
    return <Component id={id || urlId} {...props} />;
  };
};
