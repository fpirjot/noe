import {getSessionElementsFromListInSlot, slotNumberString} from "../agendaUtilities";
import {getFullSessionName} from "../sessionsUtilities";

describe("agendaUtilities", () => {
  describe("slotNumberString", () => {
    it("should return empty string if only 1 slot", () => {
      const slot = {
        session: {
          slots: [{}],
        },
      };
      expect(slotNumberString(slot)).toEqual("");
    });

    it("should return formatted slot number string if multiple slots", () => {
      const slot = {
        session: {
          slots: [{}, {_id: "123"}, {}],
        },
        _id: "123",
      };

      expect(slotNumberString(slot)).toEqual("(2/3) ");
    });
  });

  describe("getSessionName", () => {
    it("should format session name", () => {
      const session = {
        name: "Session 1",
        activity: {name: "Activity 1"},
        team: {name: "Team 1"},
      };
      const teams = [{_id: "abc", name: "Team 1"}];

      expect(getFullSessionName(session, teams)).toEqual("Session 1 - Activity 1 (Team 1)");
    });
  });

  describe("getSessionElementsFromListInSlot", () => {
    it("should get keys from slot if no session sync", () => {
      const slot = {
        stewards: [{_id: 1}],
        stewardsSessionSynchro: false,
        session: {stewards: [{_id: 1}, {_id: 2}]},
      };

      const getKey = (s) => s._id;

      expect(getSessionElementsFromListInSlot("stewards", slot, getKey)).toEqual([1]);
    });

    it("should get keys from session if session sync", () => {
      const slot = {
        stewards: [],
        stewardsSessionSynchro: true,
        session: {stewards: [{_id: 1}, {_id: 2}]},
      };

      const getKey = (s) => s._id;

      expect(getSessionElementsFromListInSlot("stewards", slot, getKey)).toEqual([1, 2]);
    });
  });
});
