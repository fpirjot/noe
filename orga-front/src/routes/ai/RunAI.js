import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Alert, Button, Card, List, Popconfirm, Progress} from "antd";
import {TabsPage} from "../../common/pages/TabsPage";
import {computingActions, computingSelectors} from "../../features/computing";
import {H3Title} from "../../common/layout/typography";

function AIDisplay() {
  const queueLength = useSelector(computingSelectors.selectQueueLength);
  const queuePosition = useSelector(computingSelectors.selectQueuePosition);
  const state = useSelector(computingSelectors.selectComputationState);
  const computationId = useSelector(computingSelectors.selectComputationId);
  let status = "active";
  if (state === "SUCCESS") {
    status = "success";
  } else if (state === "FAILED") {
    status = "exception";
  }

  if (computationId) {
    return (
      <div className="container-grid col-3">
        <span>État de votre calcul:</span>

        <Progress
          id="progress-bar"
          length={queueLength}
          percent={queuePosition}
          status={status}
          format={(percent) => `${state}: ${percent}%`}
        />
      </div>
    );
  }
  return null;
}

function AIResult(props) {
  const result = props.result;
  const nbIdSkipped = result?.skippedId?.length;
  if (nbIdSkipped === 0) {
    return <h4>result.conclusion</h4>;
  } else if (result.conclusion === "Solution complète trouvée" && nbIdSkipped > 0) {
    return (
      <>
        <h4>Le calcul a été effectué sans problème !</h4>
        <p>
          Mais {nbIdSkipped} sessions n'ont pas été placées. Voir l'onglet{" "}
          <i>Sessions non placées</i> pour plus de détails.
        </p>
      </>
    );
  } else {
    return null;
  }
}

function AISessionsNotPlaced(props) {
  const result = props.result;
  const data = [];
  if (result?.skippedId && result?.data.training) {
    for (const skipped of result.skippedId) {
      for (const training of result.data.training) {
        if (training.key === skipped.id.slice(0, -3)) {
          data.push({
            name: training.title,
            id: training.key,
            numberSession: skipped.id.slice(-2, -1),
            reason: skipped.reason,
          });
        }
      }
    }
  }

  if (data.length > 0) {
    return (
      <>
        <H3Title>Sessions non placées</H3Title>
        <List
          itemLayout="horizontal"
          dataSource={data}
          renderItem={(item) => (
            <List.Item>
              <List.Item.Meta
                title={'Activité "' + item.name + '" - Numéro de session : ' + item.numberSession}
                description={item.reason}
              />
            </List.Item>
          )}
        />
      </>
    );
  }
  return null;
}

function AIDetails(props) {
  return (
    <>
      <H3Title>Informations sur le calcul</H3Title>
      <Card style={{backgroundColor: "#eee"}}>
        <pre>{JSON.stringify(props.result, null, 2)}</pre>
      </Card>
    </>
  );
}

function RunAI() {
  const result = useSelector(computingSelectors.selectAIResult);
  const dispatch = useDispatch();

  return (
    <TabsPage
      title="Créations de sessions par IA"
      items={[
        {
          label: "Lancement",
          key: "run",
          children: (
            <>
              <Alert
                style={{flexGrow: 100, border: "none"}}
                message={"Attention, vous risquez de supprimer vos données."}
                description={
                  "Générer un planning implique de créer de nouvelles sessions basées sur " +
                  "les activités créées et les informations associées. Cela supprime également " +
                  "les sessions non figées. Si vous souhaitez générer un planning assurer de figer " +
                  "les sessions que vous voulez garder."
                }
                showIcon
                type="warning"
              />

              <div className="container-grid col-3">
                <Popconfirm
                  title="Re-générer un planning va effacer toutes les sessions existantes qui n'ont pas été figées."
                  onConfirm={() => dispatch(computingActions.runIA())}
                  okText="Oui, je veux générer le planning"
                  okButtonProps={{danger: true}}
                  cancelText="Annuler">
                  <Button danger>Générer le planning</Button>
                </Popconfirm>
              </div>
              <AIDisplay />
              {result && <AIResult result={result} />}
            </>
          ),
        },
        {
          label: "Sessions non placées",
          key: "unmatched-sessions",
          children: <AISessionsNotPlaced result={result} />,
        },
        {
          label: "Détails du calcul",
          key: "details",
          children: <AIDetails result={result} />,
        },
      ]}
    />
  );
}

export default RunAI;
