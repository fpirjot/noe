import {ElementSelectionModalProps} from "../../../common/components/ElementSelectionModal";
import {useSelector} from "react-redux";
import {placesSelectors} from "../../../features/places";
import {sessionsSelectors} from "../../../features/sessions";
import {activitiesSelectors} from "../../../features/activities";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Alert, Form} from "antd";
import {getMaxParticipantsBasedOnPlaces} from "../../../utils/sessionsUtilities";
import {ElementsTableWithModal} from "../../../common/components/ElementsTableWithModal";
import {EnvironmentOutlined, InfoCircleOutlined} from "@ant-design/icons";
import {columnsPlaces} from "../../../utils/columns/columnsPlaces";
import React from "react";
import {lazyWithRetry} from "../../../utils/lazyWithRetry";
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../../places/PlaceEdit"));

export const PlacesTableWithModal = ({
  onChange,
  sessionActivityVal,
}: Pick<ElementSelectionModalProps, "onChange"> & {sessionActivityVal: any}) => {
  const places = useSelector(placesSelectors.selectList);
  const session = useSelector(sessionsSelectors.selectEditing);
  const sessionMax = session.maxNumberOfParticipants;
  const activityMax = sessionActivityVal?.maxNumberOfParticipants;
  const sessionMaxIsDefined = sessionMax !== undefined && sessionMax !== null;
  const maxParticipantsBasedOnPlaces = getMaxParticipantsBasedOnPlaces(session);
  const numberOfParticipantsSetIsAbovePlacesCapacity =
    sessionMax > maxParticipantsBasedOnPlaces || activityMax > maxParticipantsBasedOnPlaces;

  return (
    <ElementsTableWithModal
      name={"places"}
      onChange={onChange}
      allElements={places}
      preselectedElements={sessionActivityVal?.places}
      title="Espaces"
      icon={<EnvironmentOutlined />}
      subtitle={
        numberOfParticipantsSetIsAbovePlacesCapacity && (
          <Alert
            message={`La jauge finale de la session est contrainte par la taille des espaces définis dans la session.
                 La jauge de participants définie dans ${
                   sessionMaxIsDefined ? "la session" : "l'activité"
                 } est contrainte, et la jauge finale sera de ${maxParticipantsBasedOnPlaces}.`}
            type="warning"
            showIcon
          />
        )
      }
      buttonTitle="Ajouter un espace"
      navigableRootPath="./../../places"
      columns={columnsPlaces}
      ElementEdit={PlaceEdit}
      elementSelectionModalProps={{
        title: "Ajouter les espaces utilisés pour cette session",
        subtitle:
          sessionActivityVal?.places?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les espaces éligibles de l'activité ont été pré-sélectionnés
              pour plus de facilités.
            </>
          ) : undefined,
        searchInFields: ["name"],
        createNewElementButtonTitle: "Créer un espace",
      }}
    />
  );
};
