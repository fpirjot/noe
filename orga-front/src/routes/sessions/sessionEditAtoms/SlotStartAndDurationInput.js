import {NamePath} from "rc-field-form/es/interface";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../../features/currentProject";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Form} from "antd";
import dayjs from "dayjs";
import {DateTimeInput} from "../../../common/inputs/DateTimeInput";
import {DurationSelectInput} from "../../../common/inputs/DurationSelectInput";
import {FormItem} from "../../../common/inputs/FormItem";
import React from "react";

export const SlotStartAndDurationInput = ({name}: {name: NamePath}) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const form = useFormInstance();
  const slotVal = Form.useWatch(name, form);

  const computeSlotEnd = (start, duration) => {
    if (start && duration) {
      form.setFieldValue([...name, "end"], dayjs(start).add(duration, "minute").format());
    }
  };

  return (
    <>
      <DateTimeInput
        label={"Début"}
        name={[...name, "start"]}
        disableDatesIfOutOfProject
        disableDatesBeforeNow={false}
        format={dayjs.localeData().longDateFormat("llll")}
        defaultPickerValue={dayjs(currentProject.start)}
        required
        onChange={(newStart) => computeSlotEnd(newStart, slotVal.duration)}
      />

      <DurationSelectInput
        name={[...name, "duration"]}
        required
        formItemProps={{style: {flexGrow: 1}}}
        onChange={(newDuration) => {
          computeSlotEnd(slotVal.start, newDuration);
        }}
      />

      {/* Hidden fields */}
      <FormItem name={[...name, "end"]} noStyle />
      <FormItem name={[...name, "_id"]} noStyle />
    </>
  );
};
