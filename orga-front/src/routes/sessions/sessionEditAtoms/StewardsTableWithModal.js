import {ElementSelectionModalProps} from "../../../common/components/ElementSelectionModal";
import {useSelector} from "react-redux";
import {stewardsSelectors} from "../../../features/stewards";
import {activitiesSelectors} from "../../../features/activities";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Form} from "antd";
import {ElementsTableWithModal} from "../../../common/components/ElementsTableWithModal";
import {InfoCircleOutlined, UserOutlined} from "@ant-design/icons";
import {columnsStewards} from "../../../utils/columns/columnsStewards";
import React from "react";
import {lazyWithRetry} from "../../../utils/lazyWithRetry";
const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../stewards/StewardEdit")
);
export const StewardsTableWithModal = ({
  onChange,
  sessionActivityVal,
}: Pick<ElementSelectionModalProps, "onChange"> & {sessionActivityVal: any}) => {
  const stewards = useSelector(stewardsSelectors.selectList);

  return (
    <ElementsTableWithModal
      name={"stewards"}
      onChange={onChange}
      allElements={stewards}
      title="Encadrant⋅es"
      icon={<UserOutlined />}
      preselectedElements={sessionActivityVal?.stewards}
      buttonTitle="Ajouter un⋅e encadrant⋅e"
      navigableRootPath="./../../stewards"
      columns={columnsStewards}
      ElementEdit={StewardEdit}
      elementSelectionModalProps={{
        title: "Ajouter des encadrant⋅es en charge de cette session",
        subtitle:
          sessionActivityVal?.stewards?.length > 0 ? (
            <>
              <InfoCircleOutlined /> Les encadrant⋅es éligibles de l'activité ont été
              pré-sélectionné⋅es pour plus de facilités.
            </>
          ) : undefined,
        searchInFields: ["firstName", "lastName"],
        createNewElementButtonTitle: "Créer un⋅e encadrant⋅e",
      }}
    />
  );
};
