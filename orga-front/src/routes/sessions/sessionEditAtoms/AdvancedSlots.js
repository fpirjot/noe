import {dateFormatter, timeFormatter} from "../../../utils/formatters";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../../features/currentProject";
import useFormInstance from "antd/es/form/hooks/useFormInstance";
import {Alert, Collapse, Form} from "antd";
import {CardElement} from "../../../common/components/CardElement";
import {Stack} from "../../../common/layout/Stack";
import {SwitchInputInline} from "../../../common/inputs/SwitchInput";
import {StewardsTableWithModal} from "./StewardsTableWithModal";
import {PlacesTableWithModal} from "./PlacesTableWithModal";
import {TableWithEditModal} from "../../../common/components/TableWithEditModal";
import {ClockCircleOutlined} from "@ant-design/icons";
import dayjs from "dayjs";
import React, {useRef, useState} from "react";
import {RecapSlot} from "./RecapSlot";
import {SlotStartAndDurationInput} from "./SlotStartAndDurationInput";
import {activitiesSelectors} from "../../../features/activities";
import {hasSameStartAndEnd, slotOverlapsAnOtherOne} from "../../../utils/slotsUtilities";
import {useTranslation} from "react-i18next";
import {FormHelperText} from "@mui/material";
import {ClosableAlert} from "../../../common/components/ClosableAlert";

const columnsSlots = [
  {
    dataIndex: "start",
    render: (text, record, index) =>
      dateFormatter.longDateTimeRange(record.start, record.end, true),
  },
  {
    dataIndex: "duration",
    render: timeFormatter.duration,
  },
];

export const AdvancedSlots = () => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const form = useFormInstance();
  const activitiesById = useSelector(activitiesSelectors.selectById);
  const sessionActivityVal = activitiesById[Form.useWatch("activity", form)];

  const ModalContent = () => {
    const slotForm = Form.useFormInstance();
    const sessionVal = Form.useWatch(undefined, form);
    const slotVal = Form.useWatch(undefined, slotForm);
    const {t} = useTranslation();

    const oldSlot = useRef();
    if (!oldSlot.current && slotVal) {
      oldSlot.current = form
        .getFieldValue("slots")
        .find((slot) => hasSameStartAndEnd(slot, slotVal));
    }

    const slotOverlapError =
      slotVal?.start &&
      slotVal?.duration &&
      slotOverlapsAnOtherOne(slotVal, sessionVal?.slots, oldSlot.current);

    const isNotSynchro = !slotVal?.stewardsSessionSynchro || !slotVal?.placesSessionSynchro;
    const updateEntitiesSessionSynchro = (entityName: string, activateSynchro: boolean) => {
      // If we deactivate the synchro, it means we wanna add the session's elements by default.
      // If we reactivate it, we wanna clear the arrays.
      slotForm.setFieldValue(entityName, !activateSynchro ? sessionVal[entityName] || [] : []);
    };

    return (
      <>
        <div className="container-grid two-thirds-one-third">
          <CardElement>
            <div className="container-grid two-per-row">
              <SlotStartAndDurationInput name={[]} />
            </div>
            {slotOverlapError && (
              <FormHelperText error>
                {t("common:availability.errors.slotOverlapsAnotherOne")}
              </FormHelperText>
            )}
          </CardElement>

          <RecapSlot name={[]} />
        </div>

        <Collapse
          defaultActiveKey={isNotSynchro ? "1" : undefined}
          items={[
            {
              key: "1",
              label: "Synchronisation des encadrant⋅es et espaces (avancé)",
              children: (
                <div className="container-grid two-per-row">
                  <Stack gap={2}>
                    <SwitchInputInline
                      label="Synchroniser les encadrant⋅es"
                      name="stewardsSessionSynchro"
                      onChange={(value) => updateEntitiesSessionSynchro("stewards", value)}
                    />

                    {!slotVal?.stewardsSessionSynchro && (
                      <StewardsTableWithModal sessionActivityVal={sessionActivityVal} />
                    )}
                  </Stack>

                  {currentProject.usePlaces && (
                    <Stack gap={2}>
                      <SwitchInputInline
                        label="Synchroniser les espaces"
                        name="placesSessionSynchro"
                        onChange={(value) => updateEntitiesSessionSynchro("places", value)}
                      />

                      {!slotVal?.placesSessionSynchro && (
                        <PlacesTableWithModal sessionActivityVal={sessionActivityVal} />
                      )}
                    </Stack>
                  )}
                </div>
              ),
            },
          ]}
        />

        {/*<div*/}
        {/*  style={{*/}
        {/*    position: "fixed",*/}
        {/*    bottom: 0,*/}
        {/*    maxHeight: 300,*/}
        {/*    overflow: "auto",*/}
        {/*    background: "white",*/}
        {/*    zIndex: 1000,*/}
        {/*  }}*/}
        {/*  className="container-grid two-per-row">*/}
        {/*  <pre>{JSON.stringify(sessionVal?.slots, null, 2)}</pre>*/}
        {/*  <pre>{JSON.stringify(slotVal, null, 2)}</pre>*/}
        {/*</div>*/}
      </>
    );
  };

  return (
    <TableWithEditModal
      name={"slots"}
      newElementValues={{
        stewardsSessionSynchro: true,
        placesSessionSynchro: true,
      }}
      title="Plages horaires"
      subtitle={
        <ClosableAlert
          localStorageKey={"warningMultipleSlotsSessions"}
          type={"info"}
          message={"Vous voulez peut-être créer plusieurs sessions ?"}
          description={
            <>
              <p>
                Une plage horaire est un temps pendant lequel se déroule la session. Souvent, il n'y
                en a qu'un seul, mais parfois il peut y en avoir plusieurs (par ex. une journée de
                journée de formation qui se déroule en deux temps le matin et l'après-midi).
              </p>
              Un·e participant·e qui s'inscrit à une session est forcément inscrit·e à toutes les
              plages horaires de la session (dans notre exemple, on s'inscrit donc d'un coup à la
              journée de formation entière, car c'est une seule session).
            </>
          }
        />
      }
      modalTitle="Éditer une plage horaire"
      icon={<ClockCircleOutlined />}
      buttonTitle="Ajouter une plage horaire"
      modalProps={{
        width: "min(90vw, 1200px)",
        zIndex: 100, // cause otherwise the ElementsSelectionModals are behind it
      }}
      columns={columnsSlots}>
      <ModalContent />
    </TableWithEditModal>
  );
};
