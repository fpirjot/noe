import {Select} from "antd";
import React from "react";
import {useSelector} from "react-redux";
import {categoriesSelectors} from "../../../features/categories";
import {useAgendaParams} from "../agenda/useAgendaParams";
import {categoriesOptionsWithColorDot} from "./categoriesOptionsWithColorDot";

/**
 * Component representing a select input for filtering sessions by categories.
 * The component fetches available categories from the Redux store and provides options for selecting
 * specific categories or special categories like "All the volunteering" and "All the rest".
 * @component
 * @returns {ReactNode} - Rendered component for filtering sessions by categories.
 */
export const CategoriesFilterSelect = () => {
  const categories = useSelector(categoriesSelectors.selectList);
  const [{categoriesFilter}, setAgendaParams] = useAgendaParams();

  // Check if categories are available and prepare options for the select input
  const categoriesReady = categories.length > 0;
  const categoriesOptions = [
    {key: "volunteering", value: "volunteering", label: <em>- Tout le bénévolat -</em>},
    {key: "allTheRest", value: "allTheRest", label: <em>- Tout le reste -</em>},
    ...categoriesOptionsWithColorDot(categories),
  ];

  /**
   * Handles the change event of the select input and updates the agenda parameters with selected categories.
   * @param {Array} value - The selected values from the select input.
   */
  const handleCategoriesFilterChange = (value) => {
    setAgendaParams({categoriesFilter: value});
  };

  return (
    <Select
      style={{minWidth: 300, maxWidth: 700, marginBottom: 0}}
      value={categoriesReady ? categoriesFilter : undefined}
      allowClear
      mode="multiple"
      placeholder="Filtrez par catégorie..."
      onChange={handleCategoriesFilterChange}
      options={categoriesOptions}
    />
  );
};
