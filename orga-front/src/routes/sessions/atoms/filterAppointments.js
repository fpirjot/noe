import {getSessionSubscription} from "../../../utils/registrationsUtilities";
import {searchInObjectsList} from "../../../utils/searchInObjectsList";
import {personName} from "../../../utils/utilities";
import {filterSessionsByCategories} from "./filterSessionsByCategories";

/**
 * Filters appointments based on search bar value, categories filter, and registrations.
 * @param {Array} slots - The array of slots to filter.
 * @param {Object} searchBarValue - The search bar value object containing text and scopes for filtering.
 * @param {Array} categoriesFilter - The array of categories to filter appointments.
 * @param {Array} registrations - The array of registrations to filter appointments by user.
 * @returns {Array} - The filtered array of appointments.
 */
export const filterAppointments = (slots, searchBarValue, categoriesFilter, registrations) => {
  const {text: searchBarText = "", scopes = []} = searchBarValue;

  // Extract search scopes
  const searchInScope = Object.fromEntries(scopes.map((scope) => [scope, true]));

  // Filter slots by categories
  slots = filterSessionsByCategories(slots, categoriesFilter);

  // Search in the list of appointments based on specified search scopes
  return searchInObjectsList(searchBarText, slots, (slot) => [
    searchInScope.session && slot.session.name,
    searchInScope.activity && slot.session.activity.name,
    searchInScope.category && slot.session.activity.category.name,
    ...((searchInScope.secondaryCategories && slot.session.activity.secondaryCategories) || []),
    ...((searchInScope.stewards && slot.stewards?.map(personName)) || []),
    ...((searchInScope.stewards && slot.session.stewards?.map(personName)) || []),
    ...((searchInScope.places && slot.places?.map((p) => p.name)) || []),
    ...((searchInScope.places && slot.session.places?.map((p) => p.name)) || []),
    ...((searchInScope.sessionTags && slot.sessionTags) || []),
    ...((searchInScope.activityTags && slot.activityTags) || []),
    ...((searchInScope.registrations &&
      registrations
        ?.filter((r) => getSessionSubscription(r, slot.session))
        .map((r) => personName(r.user))) ||
      []),
    searchInScope.team && slot.session.team?.name,
  ]);
};
