import {Tag} from "antd";
import React from "react";

export const getAppointmentTagsList = (tags) =>
  tags?.length > 0 &&
  tags.map((tag) => (
    <Tag
      style={{
        background: "rgba(255, 255, 255, 0.15)",
        color: "white",
        border: "none",
        fontSize: 11,
      }}>
      {tag}
    </Tag>
  ));
