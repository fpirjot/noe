import React from "react";
import {TextInput} from "../../../common/inputs/TextInput";
import {FillingInput} from "../../../common/inputs/FillingInput";

export const SessionFilling = ({
  computedMaxNumberOfParticipants,
  numberOfParticipants,
  showLabel = true,
  style,
  ...otherProps
}) => {
  // cf. getMaxParticipantsBasedOnPlaces() function in backend
  const noMaxNumberOfParticipants = computedMaxNumberOfParticipants === null;
  const noParticipantsAtAll = computedMaxNumberOfParticipants === 0;

  const fillingText =
    numberOfParticipants !== undefined && computedMaxNumberOfParticipants
      ? numberOfParticipants + "/" + computedMaxNumberOfParticipants
      : 0;

  const textIndic =
    (noMaxNumberOfParticipants || noParticipantsAtAll) &&
    `${noMaxNumberOfParticipants ? "Inscription libre" : "Fermé aux participant⋅es"} (${
      numberOfParticipants || 0
    } pers.)`;

  return textIndic ? (
    <TextInput
      label={showLabel ? "Remplissage" : undefined}
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0, ...style}}}
      title={textIndic}
      value={`${!showLabel ? (noMaxNumberOfParticipants ? "✔️" : "❌") : textIndic} (${
        numberOfParticipants || 0
      } pers.)`}
      readOnly
    />
  ) : (
    <FillingInput
      formItemProps={{style: {maxWidth: 250, marginBottom: showLabel ? undefined : 0, ...style}}}
      label={showLabel ? "Remplissage" : undefined}
      percent={(numberOfParticipants / computedMaxNumberOfParticipants) * 100}
      status={numberOfParticipants > computedMaxNumberOfParticipants ? "exception" : ""}
      text={fillingText}
      {...otherProps}
    />
  );
};
