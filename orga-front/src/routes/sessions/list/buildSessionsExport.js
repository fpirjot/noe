import dayjs from "dayjs";
import {personName} from "../../../utils/utilities";
import {getFullSessionName} from "../../../utils/sessionsUtilities";
import {dateFormatter, timeFormatter} from "../../../utils/formatters";
import {sorter} from "../../../utils/sorters";

/**
 * Builds and formats session data for export in CSV format.
 * @param {Array} sessions - The array of session objects to be exported.
 * @returns {Array} - An array of objects containing formatted session data for export.
 */
const buildSessionsExport = (sessions, sessionNameTemplate) => {
  const exportedData = sessions
    .map((session) => {
      return {
        "ID Session": session._id,
        "ID Activité": session.activity._id,
        Catégorie: session.activity.category.name,
        "Couleur de catégorie": session.activity.category.color,
        "Catégories secondaires": session.activity.secondaryCategories?.join(", "),
        "Nom de la session": getFullSessionName(session, sessionNameTemplate),
        Résumé: session.activity.summary,
        "Description détaillée": session.activity.description,
        "Début (heure)": timeFormatter.time(session.start),
        "Fin  (heure)": timeFormatter.time(session.end),
        "Début (jour + heure)": dateFormatter.longDateTime(session.start),
        "Fin  (jour + heure)": dateFormatter.longDateTime(session.end),
        "Début - Fin (heure)": timeFormatter.timeRange(session.start, session.end),
        "Début - Fin (jour + heure, court)": dateFormatter.longDateTimeRange(
          session.start,
          session.end,
          true
        ),
        "Début - Fin (jour + heure, long)": dateFormatter.longDateTimeRange(
          session.start,
          session.end,
          false
        ),
        "Début (Format ISO)": session.start,
        "Fin (Format ISO)": session.end,
        "Durée totale": session.slots
          ?.map((s) => timeFormatter.duration(dayjs(s.end).diff(dayjs(s.start), "minute"), true))
          .join(" + "),
        "Durée totale (brute)": session.slots
          ?.map((s) => dayjs(s.end).diff(dayjs(s.start), "minute"))
          .reduce((acc, duration) => acc + duration, 0),
        "Participant.es inscrit.es": session.numberParticipants,
        "Jauge max. de participant.es": session.computedMaxNumberOfParticipants,
        "Encadrant.es": session.stewards.map(personName).join(", "),
        Espaces: session.places.map((place) => place.name).join(", "),
      };
    })
    .sort((a, b) => sorter.date(a["Début (Format ISO)"], b["Début (Format ISO)"]));
  return exportedData;
};

export default buildSessionsExport;
