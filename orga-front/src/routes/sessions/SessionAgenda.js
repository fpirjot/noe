import React, {Suspense, useCallback, useEffect, useState} from "react";
import dayjs from "dayjs";
import {useDispatch, useSelector} from "react-redux";
import {Button, Drawer, App, Popover, Tooltip} from "antd";
import {
  CalendarOutlined,
  CloseOutlined,
  EnvironmentOutlined,
  ReloadOutlined,
  TagOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  EditingState,
  GroupingState,
  IntegratedEditing,
  IntegratedGrouping,
  ViewState,
} from "@devexpress/dx-react-scheduler";
import {
  AppointmentForm,
  Appointments,
  CurrentTimeIndicator,
  DateNavigator,
  DayView,
  DragDropProvider,
  GroupingPanel,
  Resources,
  Scheduler,
  Toolbar,
} from "@devexpress/dx-react-scheduler-material-ui";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {navbarHeight} from "../../utils/viewUtilities";
import {useWindowDimensions} from "../../common/hooks/useWindowDimensions";
import {CELL_DURATION_MINUTES, DEFAULT_AGENDA_PARAMS} from "../../utils/agendaUtilities";
import {NumberInput} from "../../common/inputs/NumberInput";
import {Pending} from "../../common/components/Pending";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import {slotOverlapsAnOtherOne} from "../../utils/slotsUtilities";
import {useAgendaParams} from "./agenda/useAgendaParams";
import {DatePickerComponent} from "../../common/inputs/DateTimeInput";
import {useTranslation} from "react-i18next";
import {NavButton} from "./agenda/NavButton";
import {useGetTimeScaleLabelComponent} from "./agenda/useGetTimeScaleLabelComponent";
import {AgendaControls} from "./agenda/AgendaControls";
import {AgendaMenuDrawer} from "./agenda/AgendaMenuDrawer";
import {useDetectHotKey} from "../../common/hooks/useDetectHotKey";
import {SelectInput} from "../../common/inputs/SelectInput";
import {ErrorBoundary} from "../../common/components/ErrorBoundary";
import {midnightDateManager} from "./atoms/midnightDateManager";
import {synchronizeTicksOnSideOfAgenda} from "./atoms/synchronizeTicksOnSideOfAgenda";
import {capitalize} from "../../utils/stringUtilities";
import {useAgendaDisplayHours} from "./agenda/useAgendaDisplayHours";
import {useNavigate} from "react-router-dom";
import {useLoadAgendaData} from "./atoms/useLoadAgendaData";
import {getAppointmentTagsList} from "./atoms/getAppointmentTagsList";
import {timeFormatter} from "../../utils/formatters";

const SessionShowSmall = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./atoms/SessionShowSmall")
);

function SessionAgenda() {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {isMobileView, windowHeight} = useWindowDimensions();

  const schedulerHeight = windowHeight - navbarHeight();

  const cloningFeatureActivated = useDetectHotKey("mod+shift");
  const [currentSessionPreview, setCurrentSessionPreview] = useState();

  // *********************** //
  // **** DATA FETCHING **** //
  // *********************** //

  const {
    allSlots,
    places,
    placesLoaded,
    stewards,
    stewardsLoaded,
    categories,
    categoriesLoaded,
    registrations,
    currentProject,
  } = useLoadAgendaData();

  // The appointments (ie. the slots from all the sessions, formatted in the way that the Agenda accepts it
  const filteredAppointments = useSelector(sessionsSelectors.selectAgendaFilteredAppointments);
  const resources = useSelector(sessionsSelectors.selectAgendaResources);

  const [
    {
      cellDisplayHeight,
      currentAgendaDate,
      slotsOnEachOther,
      groupByDaysFirst,
      defaultNewSlotDuration,
      showResourcesAvailabilities,
      resourcesListingsToDisplay,
      selectedResources,
      resourcesFilterSelections,
      numberOfDaysDisplayed,
      showResourcesListingOnAgendaCards,
      forcedDisplayHours,
    },
    setAgendaParams,
  ] = useAgendaParams();

  // ************************ //
  // **** MEMOIZED DATA **** //
  // ************************ //
  // everything that can easily be computed only once //

  const agendaDisplayHours = useAgendaDisplayHours(forcedDisplayHours);

  // ************************* //
  // ***** ASYNC EFFECTS ***** //
  // ************************* //

  // APPEARANCE TWEAKS

  useEffect(
    () => synchronizeTicksOnSideOfAgenda("timescale-cell", cellDisplayHeight),
    [cellDisplayHeight, agendaDisplayHours, currentAgendaDate]
  );

  // ************************* //
  // ** APPOINTMENTS BLOCKS ** //
  // ************************* //

  // Appointment popover when we hover the block
  const SessionPreview = ({session, fullContent}) => (
    <SessionShowSmall
      fullContent={fullContent}
      onClick={() => navigate(`./../${session._id}`)}
      style={{width: fullContent ? "100%" : 350}}
      session={session}
      registrations={registrations}
      onDelete={() => {
        dispatch(sessionsActions.remove(session._id));
        setCurrentSessionPreview(undefined);
      }}
    />
  );

  // Generic appointment component
  const CustomAppointment = useCallback(
    React.memo(
      ({style, children, component: Component, opacity = 1, data, ...restProps}) => {
        return (
          // Component variable must have an uppercase letter to be considered as a component by React
          <Component
            {...restProps}
            data={{...data}}
            className={restProps.className}
            style={{
              ...style,
              // Override default border color
              borderColor: "var(--colorBgContainer)",
              color: "white",
              backgroundColor: data.color,
              opacity: opacity,
              borderRadius: 8,
            }}>
            <div style={{marginLeft: 8, marginTop: 5}}>
              <div
                style={{
                  lineHeight: 1.5,
                  whiteSpace: "normal",
                  overflow: "hidden",
                  display: "-webkit-box",
                  WebkitLineClamp: "3",
                  WebkitBoxOrient: "vertical",
                  fontWeight: "bold",
                }}>
                {data.title}
              </div>
              <div style={{overflow: "hidden", whiteSpace: "nowrap", paddingTop: 1}}>
                {timeFormatter.timeRange(data.start, data.end)}
              </div>
              <div
                style={{marginTop: 4, lineHeight: 1.2, opacity: 0.8, gap: 5}}
                className="containerV">
                {((resourcesListingsToDisplay.activityTags && data?.activityTags?.length > 0) ||
                  (resourcesListingsToDisplay.sessionTags && data?.sessionTags?.length > 0)) && (
                  <div>
                    <TagOutlined />{" "}
                    {resourcesListingsToDisplay.activityTags &&
                      getAppointmentTagsList(data?.activityTags)}
                    {resourcesListingsToDisplay.sessionTags &&
                      getAppointmentTagsList(data?.sessionTags)}
                  </div>
                )}
                {currentProject.usePlaces &&
                  resourcesListingsToDisplay.places &&
                  data?.location.length > 0 && (
                    <div>
                      <EnvironmentOutlined /> {data?.location}
                    </div>
                  )}
                {resourcesListingsToDisplay.stewards && data?.stewardsNames.length > 0 && (
                  <div>
                    <UserOutlined /> {data?.stewardsNames}
                  </div>
                )}
                {((resourcesListingsToDisplay.registrations &&
                  data?.participantsNames.length > 0) ||
                  resourcesListingsToDisplay.maxNumberOfParticipants) && (
                  <div>
                    <TeamOutlined />{" "}
                    {resourcesListingsToDisplay.maxNumberOfParticipants &&
                      (data?.session.computedMaxNumberOfParticipants === null
                        ? `${data?.session.numberParticipants}`
                        : data?.session.computedMaxNumberOfParticipants === 0
                        ? `✖${data?.session.numberParticipants}`
                        : `${data?.session.numberParticipants}/${data?.session.computedMaxNumberOfParticipants}`)}{" "}
                    {resourcesListingsToDisplay.registrations && data?.participantsNames}
                  </div>
                )}
              </div>
            </div>
          </Component>
        );
      },
      (prevProps, nextProps) => JSON.stringify(prevProps.data) === JSON.stringify(nextProps.data)
    ),
    [currentProject, JSON.stringify(resourcesListingsToDisplay)]
  );

  // Renders the appointment block in the agenda view.
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and 30% after
  const Appointment = useCallback(
    React.memo(
      (props) => {
        const appointmentComp = (
          <CustomAppointment
            component={Appointments.Appointment}
            onClick={isMobileView && (() => setCurrentSessionPreview(props.data.session))}
            {...props}
          />
        );

        return isMobileView ? (
          appointmentComp
        ) : (
          // zIndex=100 to put the popover above the Sidebar, but below the SessionShow modal
          <Popover
            zIndex={100}
            content={
              <Suspense fallback={null}>
                <SessionPreview session={props.data.session} />
              </Suspense>
            }
            mouseEnterDelay={1}>
            {appointmentComp}
          </Popover>
        );
      },
      (prevProps, nextProps) =>
        JSON.stringify(prevProps.data.session) === JSON.stringify(nextProps.data.session)
    ),
    // Don't put the currentProject as a dependency cause it's not necessary and causes a whole re-render for
    // nothing on each session subscribe/unsubscribe
    [
      registrations,
      JSON.stringify(resourcesListingsToDisplay),
      JSON.stringify(resourcesFilterSelections),
    ]
  );

  // When we drag and drop, , renders the appointment block that is dragged
  const DraftAppointment = ({style, ...props}) => (
    <CustomAppointment
      component={DragDropProvider.DraftAppointment}
      {...props}
      style={
        // Only show cloning option if there is only one slot in the session
        props.data.session.slots.length === 1 && cloningFeatureActivated
          ? {
              boxShadow: "0 0 3px 3px green",
              outline: "1.6px solid var(--colorBgContainer)",
              zIndex: 1000,
              ...style,
            }
          : style
      }
    />
  );

  // When we drag and drop, renders the appointment at the original location
  const SourceAppointment = (props) => (
    <CustomAppointment
      component={DragDropProvider.SourceAppointment}
      // Only show cloning option if there is only one slot in the session
      opacity={props.data.session.slots.length === 1 && cloningFeatureActivated ? 1 : 0.5}
      {...props}
    />
  );

  // ************************* //
  // **** DATA DEFINITION **** //
  // ************************* //

  // Determine if grouping is enabled
  const groupByResourcesEnabled = selectedResources?.length > 0;

  // The resources and grouping data given to the agenda with just what it needs, not more
  const resourcesData = resources.filter((resource) =>
    selectedResources?.includes(resource.fieldName)
  );
  const groupingData = selectedResources?.map((fieldName) => ({resourceName: fieldName}));

  // Apply filter to resources
  const filteredResources = resourcesData.map((resource) => {
    const filterSelection = resourcesFilterSelections[resource.resourceName] || [];
    const filteredInstances = resource.instances.filter((instance) =>
      filterSelection.includes(instance.id)
    );
    return {...resource, instances: filteredInstances};
  });

  // Useful to calculate overlaps with different availabilities from the resources
  const filteredResourcesWithAvailabilities = filteredResources.filter(
    (resource) => resource.hasAvailabilities
  );

  // Tells if the Scheduler component can render or should wait
  const allSelectedResourcesAreAvailable = () => {
    if (selectedResources.includes("categoryId") && !categoriesLoaded) return false;
    if (selectedResources.includes("stewardId") && !stewardsLoaded) return false;
    if (selectedResources.includes("placesId") && !placesLoaded) return false;
    return true;
  };

  // ************************** //
  // ***** AGENDA ACTIONS ***** //
  // ************************** //

  // Action on update or deletion
  // Note: cloning is not available for multi-slots sessions
  const commitChanges = async ({deleted, changed}) => {
    // Actions if the slot is updated (by drag and drop for example)
    if (changed) {
      for (const [slotId, newSlotData] of Object.entries(changed)) {
        // Get the existing slot and create a new one
        const existingSlot = allSlots.find((slot) => slot._id === slotId);

        const newSlot = {...existingSlot};

        // Synchronize the category
        if (
          selectedResources.includes("categoryId") &&
          newSlotData.categoryId !== existingSlot.session.activity?.category?._id
        ) {
          message.info("Vous ne pouvez pas modifier la catégorie dans la vue agenda.");
        }

        // Synchronize places
        if (selectedResources.includes("placeId") && newSlotData.placeId) {
          newSlot.places = places.filter((place) => newSlotData.placeId.includes(place._id));
        }

        // Synchronize stewards
        if (selectedResources.includes("stewardId") && newSlotData.stewardId) {
          newSlot.stewards = stewards.filter((steward) =>
            newSlotData.stewardId.includes(steward._id)
          );
        }

        // Update dates
        newSlot.start = dayjs(newSlotData.startDate).second(0).format();
        newSlot.end = midnightDateManager
          .displayToData(existingSlot.endDate, newSlotData.endDate)
          .set("second", 0)
          .format();

        // Select the session and put it in editing mode, then update the slot from the session
        await dispatch(sessionsActions.selectEditingByIdFromList(existingSlot.session._id));

        // If the cloning key was pressed during the drop, clone the element instead of moving it.
        // Which means, create a new session first, and add the slot to it.
        // Else, just take the existing session and update the slot inside it.
        if (cloningFeatureActivated && existingSlot.session.slots.length === 1) {
          // If the slot is part of a session that has only one slot, clone the whole session
          await dispatch(
            sessionsActions.changeEditing({...existingSlot.session, _id: "new", slots: [newSlot]})
          );
          // Persist data and notify about successful creation
          // Optimistic cloning: create a element with "new" id in the list,
          // and when the element is really created, remove the "new" element from the list.
          await dispatch(sessionsActions.persist(undefined, true, true));
        } else {
          await dispatch(sessionsActions.updateSlotToEditing(newSlot));
          // Persist data silently
          await dispatch(sessionsActions.persist(undefined, true, true));
        }
        dispatch(sessionsActions.setEditing({}));
      }
    }
  };

  // Action on double click on appointment: navigates to the existing session
  // (also when drag and drop, but not supported yet)
  const onEditingAppointmentChange = (changes) => {
    if (changes && !["resize-end", "resize-start", "vertical"].includes(changes.type)) {
      navigate(`./../${changes.session._id}`);
    }
  };

  // Action on double click on an empty cell: navigates to a new session form
  const onAddedAppointmentChange = (appointmentData) => {
    const start = dayjs(appointmentData.startDate).set("second", 0).format();
    const end = dayjs(appointmentData.startDate).add(defaultNewSlotDuration, "minute").format();
    dispatch(
      sessionsActions.setEditing({
        _id: "new",
        slots: [
          {
            start,
            end,
            duration: defaultNewSlotDuration,
            stewardsSessionSynchro: true,
            placesSessionSynchro: true,
          },
        ],
        start,
        end,
        places: appointmentData.placeId?.map((id) => places.find((p) => p._id === id)) || [],
        stewards: appointmentData.stewardId?.map((id) => stewards.find((s) => s._id === id)) || [],
      })
    );
    navigate("./../new");
  };

  // ************************** //
  // ***** SUB-COMPONENTS ***** //
  // ************************** //

  // Renders a cell of the Agenda day view
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and cuts all the rendering after that
  const TimeTableCell = useCallback(
    React.memo((props) => {
      // Get the cell start and end hour
      const cellSlot = {start: props.startDate, end: props.endDate};
      const overlapsWithElements = [];

      // Check if the cell is out of project availabilities
      if (!slotOverlapsAnOtherOne(cellSlot, currentProject.availabilitySlots)) {
        overlapsWithElements.push("project");
      }

      // Check if the cell is out of the other entities availabilities (only for resources with availabilities
      for (const resourceInfo of filteredResourcesWithAvailabilities) {
        if (showResourcesAvailabilities[resourceInfo.fieldName]) {
          const resourceGrouping = props.groupingInfo?.find(
            (info) => info.fieldName === resourceInfo.fieldName
          );
          if (resourceGrouping) {
            const resource = resourceInfo.instances.find((res) => res.id === resourceGrouping.id);
            if (!slotOverlapsAnOtherOne(cellSlot, resource?.availabilitySlots)) {
              overlapsWithElements.push(resourceInfo.resourceName);
            }
          }
        }
      }

      // Add the appropriate classes to the cell, so it can be painted if there are some overlaps
      const className =
        overlapsWithElements.length > 0
          ? `${overlapsWithElements.join("-")}-disabled-date`
          : undefined;
      return (
        <DayView.TimeTableCell
          {...props}
          style={{height: cellDisplayHeight}}
          className={className}
        />
      );
    }),
    [
      JSON.stringify(showResourcesAvailabilities),
      JSON.stringify(filteredResourcesWithAvailabilities),
      cellDisplayHeight,
      currentProject.availabilitySlots,
    ]
  );

  const TimeScaleLabelComponent = useGetTimeScaleLabelComponent(cellDisplayHeight);

  const DayScaleCell = (props) => (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.getTime(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
    />
  );

  // Agenda date navigation arrows and selector
  const DateNavigatorRoot = useCallback(
    React.memo((props) => {
      const {isMobileView} = useWindowDimensions();
      const [datePickerOpen, setDatePickerOpen] = useState(false);
      const [{currentAgendaDate, numberOfDaysDisplayed}] = useAgendaParams();
      const currentAgendaDateObject = dayjs(currentAgendaDate);
      const goToProjectStartDate =
        dayjs(currentProject.start).isAfter(dayjs()) || dayjs(currentProject.end).isBefore(dayjs());

      const dateTextStart = capitalize(
        currentAgendaDateObject.format(isMobileView ? "MMM" : "MMMM")
      );
      const dateTextEnd = capitalize(
        currentAgendaDateObject
          .add(numberOfDaysDisplayed - 1, "day")
          .format(isMobileView ? "MMM" : "MMMM")
      );
      const dateText =
        isMobileView || dateTextStart === dateTextEnd
          ? dateTextStart
          : `${dateTextStart} – ${dateTextEnd}`;

      return (
        <div
          ref={props.rootRef}
          className="containerH buttons-container"
          style={{flexShrink: 0, alignItems: "center"}}>
          <div>
            <NavButton type="back" onClick={() => props.onNavigate("back")} />
            <NavButton type="forward" onClick={() => props.onNavigate("forward")} />
          </div>

          <div>
            <DatePickerComponent
              // Make the datepicker input invisible, only show the popup and control the picker with a button
              style={{visibility: "hidden", width: 0, paddingLeft: 0, paddingRight: 0}}
              value={currentAgendaDateObject}
              showTime={false}
              showToday={goToProjectStartDate}
              disableDatesIfOutOfProject
              popupClassName={isMobileView && "shift-date-picker-to-left"}
              disableDatesBeforeNow={false}
              open={datePickerOpen}
              onChange={(value) => {
                setDatePickerOpen(false);
                setAgendaParams({currentAgendaDate: value.unix() * 1000});
              }}
              onOpenChange={setDatePickerOpen}
            />
            <Button onClick={() => setDatePickerOpen(true)}>{dateText}</Button>
          </div>

          <Tooltip
            title={
              goToProjectStartDate
                ? t("sessions:agenda.dateNavigator.goToBeginningOfEvent")
                : t("sessions:agenda.dateNavigator.goToToday")
            }>
            <Button
              type="link"
              icon={<CalendarOutlined />}
              onClick={() =>
                setAgendaParams({
                  currentAgendaDate: goToProjectStartDate
                    ? dayjs(currentProject.start).unix() * 1000
                    : dayjs().unix() * 1000,
                })
              }>
              {!isMobileView &&
                (goToProjectStartDate
                  ? t("sessions:agenda.dateNavigator.beginning")
                  : t("sessions:agenda.dateNavigator.today"))}
            </Button>
          </Tooltip>
        </div>
      );
    }),
    []
  );

  // Where all the agenda controls are
  const AgendaControlsBar = useCallback(
    React.memo(() => (
      <AgendaControls
        settingsDrawer={
          <AgendaMenuDrawer buttonStyle={{flexGrow: 0}}>
            {/*Add info to the cards on the agenda*/}
            <SelectInput
              label="Informations affichées sur les cartes de l'agenda"
              defaultValue={showResourcesListingOnAgendaCards}
              mode="multiple"
              placeholder="sélectionnez des informations à afficher..."
              onChange={(value) => setAgendaParams({showResourcesListingOnAgendaCards: value})}
              options={[
                {value: "places", label: "Espaces"},
                {value: "stewards", label: "Encadrant⋅es"},
                {value: "registrations", label: "Participant⋅es"},
                {value: "maxNumberOfParticipants", label: "Jauge de participant⋅es"},
                {value: "activityTags", label: "Tags de l'activité"},
                {value: "sessionTags", label: "Tags de la session"},
              ]}
            />

            {/*Select if we want to filter by date then by entities, or the inverse. */}
            {/*Disabled if there is no selected grouping.*/}
            <SelectInput
              label="Méthode de groupage"
              onChange={(value) => setAgendaParams({groupByDaysFirst: value})}
              defaultValue={groupByDaysFirst}
              options={[
                {value: true, label: "Jours, puis Groupage d'élements"},
                {value: false, label: "Groupage d'élements, puis Jours"},
              ]}
              tooltip="Lorsque le groupage est activé, grouper d'abord par jour puis par entité, ou l'inverse."
            />

            <NumberInput
              label="Durée par défaut pour les nouvelles sessions"
              defaultValue={defaultNewSlotDuration}
              step={30}
              onChange={(value) => setAgendaParams({defaultNewSlotDuration: value})}
            />
          </AgendaMenuDrawer>
        }
      />
    )),
    [places, stewards, categories]
  );

  // ************************** //
  // **** RENDER FUNCTION ***** //
  // ************************** //

  // If everything isn't loaded yet, stop here, no need to go further. Display a pending spinner.
  return currentProject._id !== undefined &&
    filteredAppointments &&
    selectedResources &&
    numberOfDaysDisplayed &&
    allSelectedResourcesAreAvailable() ? (
    <div className="full-width-content scheduler-wrapper">
      {/*Error boundary to return back to normal state if it is buggy with slotsOnEachOther*/}
      <ErrorBoundary
        onError={() => setAgendaParams(DEFAULT_AGENDA_PARAMS)}
        extra={
          <Button
            icon={<ReloadOutlined />}
            onClick={() => {
              setAgendaParams(DEFAULT_AGENDA_PARAMS);
              window.location.reload();
            }}>
            {t("common:errorBoundary.reloadPage")}
          </Button>
        }>
        {/*The big agenda component*/}
        <Scheduler data={filteredAppointments} locale="fr-FR" height={schedulerHeight}>
          {/*State management*/}
          <ViewState
            defaultCurrentDate={currentAgendaDate}
            currentDate={currentAgendaDate}
            onCurrentDateChange={(date) => setAgendaParams({currentAgendaDate: date.getTime()})}
            defaultCurrentViewName="Day"
          />
          <EditingState
            onCommitChanges={commitChanges}
            onEditingAppointmentChange={onEditingAppointmentChange}
            onAddedAppointmentChange={onAddedAppointmentChange}
          />

          {groupByResourcesEnabled && (
            <GroupingState grouping={groupingData} groupByDate={() => groupByDaysFirst} />
          )}

          {/*Views management*/}
          <DayView
            timeTableCellComponent={TimeTableCell}
            timeScaleLabelComponent={TimeScaleLabelComponent}
            startDayHour={agendaDisplayHours.start}
            endDayHour={agendaDisplayHours.end}
            cellDuration={CELL_DURATION_MINUTES}
            intervalCount={numberOfDaysDisplayed}
            dayScaleCellComponent={DayScaleCell}
          />

          {/*Other tools*/}
          <Toolbar
            flexibleSpaceComponent={AgendaControlsBar} /*The toolbar with all the controls*/
          />
          <DateNavigator /*The controls to navigate to different days*/
            rootComponent={DateNavigatorRoot}
          />
          <Appointments
            appointmentComponent={Appointment} /*Displays the appointments on the agenda*/
            placeAppointmentsNextToEachOther={!slotsOnEachOther}
          />

          {groupByResourcesEnabled && (
            <Resources data={filteredResources} /*Infos about the resources*/ />
          )}
          <IntegratedEditing />
          {groupByResourcesEnabled && <IntegratedGrouping />}
          {groupByResourcesEnabled && <GroupingPanel />}
          <DragDropProvider
            sourceAppointmentComponent={SourceAppointment}
            draftAppointmentComponent={DraftAppointment}
          />
          <AppointmentForm visible={false} /*Makes double click possible everywhere*/ />

          <CurrentTimeIndicator />
        </Scheduler>

        {/* Session Preview drawer on mobile view */}
        {isMobileView && (
          <Drawer
            mask={false}
            placement="bottom"
            zIndex={10}
            height={300}
            styles={{
              body: {
                borderRadius: "12px 0",
                padding: 0,
                margin: 0,
                background: "var(--colorBgContainer)",
              },
              content: {
                background: "transparent",
                overflow: "visible",
              },
              header: {
                height: 0,
                padding: 0,
                margin: "15px 0 -15px 0",
                background: "transparent",
                border: "none",
                borderRadius: 12,
                zIndex: 1,
              },
            }}
            onClose={() => setCurrentSessionPreview(undefined)}
            closeIcon={
              <Button
                icon={<CloseOutlined />}
                style={{position: "relative", top: -15}}
                shape={"circle"}
              />
            }
            open={currentSessionPreview}>
            <Suspense fallback={null}>
              <SessionPreview session={currentSessionPreview} fullContent />
            </Suspense>
          </Drawer>
        )}
      </ErrorBoundary>
    </div>
  ) : (
    <Pending />
  );
}

export default SessionAgenda;
