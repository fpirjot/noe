import {useSelector} from "react-redux";
import {sessionsSelectors} from "../../../features/sessions";
import {currentProjectSelectors} from "../../../features/currentProject";
import {useMemo} from "react";
import {getAgendaDisplayHours} from "../atoms/getAgendaDisplayHours";

export const useAgendaDisplayHours = (forcedDisplayHours) => {
  const allSlots = useSelector(sessionsSelectors.selectSlotsListForAgenda);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  return useMemo(() => {
    const computedHours = getAgendaDisplayHours(allSlots, currentProject);
    computedHours.start = forcedDisplayHours?.start ?? computedHours.start;
    computedHours.end = forcedDisplayHours?.end ?? computedHours.end;
    return computedHours;
  }, [allSlots, currentProject, forcedDisplayHours?.start, forcedDisplayHours?.end]);
};
