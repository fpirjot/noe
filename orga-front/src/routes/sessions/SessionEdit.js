import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  displayInconsistencies,
  sessionsActions,
  sessionsSelectors,
} from "../../features/sessions.js";
import {stewardsActions} from "../../features/stewards.js";
import {placesActions} from "../../features/places.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {App, Button, Form} from "antd";
import {BookOutlined, ClockCircleOutlined, ScheduleOutlined, TeamOutlined} from "@ant-design/icons";
import dayjs from "dayjs";
import {CardElement} from "../../common/components/CardElement";
import {EditPage, ElementEditProps} from "../../common/pages/EditPage";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {teamsActions, teamsSelectors} from "../../features/teams";
import {pick} from "../../utils/utilities";
import {useColumnsBlacklistingSelect} from "../../common/hooks/useColumnsBlacklistingSelect";
import {TextInput} from "../../common/inputs/TextInput";
import {NumberInput} from "../../common/inputs/NumberInput";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import {getSessionSubscription} from "../../utils/registrationsUtilities";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {RichTextInput} from "../../common/inputs/RichTextInput";
import {SelectInput} from "../../common/inputs/SelectInput";
import {WithEntityLinks} from "../../common/components/WithEntityLinks";
import {TagsSelectInput} from "../../common/components/TagsSelectInput";
import {generateRegistrationDispoColumn} from "../../utils/columns/generateRegistrationDispoColumn";
import {generateRegistrationsColumns} from "../../utils/columns/generateRegistrationsColumns";
import {generateSubscriptionInfo} from "../../utils/columns/generateSubscriptionInfo";
import {searchInRegistrationFields} from "../../utils/searchInFields/searchInRegistrationFields";
import {GroupSmsButtons} from "../../common/components/buttons/GroupSmsButtons";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "../../utils/withFallbackOnUrlId";
import {generateSessionNoShowColumn} from "../../utils/columns/generateSessionNoShowColumn";
import {sorter} from "../../utils/sorters";
import {ElementsTableWithModal} from "../../common/components/ElementsTableWithModal";
import {Stack} from "../../common/layout/Stack";
import {getSlotsStartAndEnd} from "../../utils/slotsUtilities";
import {PlacesTableWithModal} from "./sessionEditAtoms/PlacesTableWithModal";
import {StewardsTableWithModal} from "./sessionEditAtoms/StewardsTableWithModal";
import {AdvancedSlots} from "./sessionEditAtoms/AdvancedSlots";
import {RecapSlot} from "./sessionEditAtoms/RecapSlot";
import {SessionSmallPreview} from "./sessionEditAtoms/SessionSmallPreview";
import {SlotStartAndDurationInput} from "./sessionEditAtoms/SlotStartAndDurationInput";
import {EditPageProps} from "../../common/pages/EditPage";

const ActivityEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../activities/ActivityEdit")
);

const TeamEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../teams/TeamEdit"));

const SessionGroupSmsButtons = () => {
  const form = Form.useFormInstance();
  const sessionVal = Form.useWatch(undefined, form);
  return <GroupSmsButtons registrations={sessionVal?.registrations} session={sessionVal} />;
};

export const hereditaryPlaceholder = (value: string | number, defaultPlaceholder: string) =>
  "↖ " + (value ? value : defaultPlaceholder);

function SessionEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const dispatch = useDispatch();
  const {message} = App.useApp();
  const location = useLocation();
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true});
  const [form] = Form.useForm();
  const [filterBlacklistedParticipantsColumns] = useColumnsBlacklistingSelect({
    endpoint: "participants",
  });

  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const activities = useSelector(activitiesSelectors.selectList);
  const activitiesById = useSelector(activitiesSelectors.selectById);
  const sessionActivityVal = activitiesById[Form.useWatch("activity", form)];
  const sessionEverybodyIsSubscribedVal = Form.useWatch("everybodyIsSubscribed", form);
  const teams = useSelector(teamsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.useTeams && dispatch(teamsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(registrationsActions.loadList());
      dispatch(sessionsActions.loadList()); // For tags
    },
    clonedElement
  );

  const onValuesChange: EditPageProps["onValuesChange"] = (changedValues) => {
    // Auto-update the default slots of the session when we change the activity
    if (Object.keys(changedValues).includes("activity")) {
      const newActivity = activitiesById[changedValues.activity];

      // Only auto-update slots if :
      if (
        session._id === "new" && // - the session is new
        !form.isFieldTouched("slots") && // - slots have not been touched yet
        newActivity?.slots?.length > 0 // and if the newly selected activity has some default slots
      ) {
        // Start building the new slots by adding basic stuff and activity duration
        const defaultSlots = newActivity.slots.map((s) => ({
          duration: s.duration,
          stewardsSessionSynchro: true,
          placesSessionSynchro: true,
        }));

        // Then we will add start and end dates :
        // Get timetrace starting from session start, or project start
        let timeTrace = dayjs(form.getFieldValue("start") || currentProject.start);
        // and compute start and end dates
        for (const slot of defaultSlots) {
          slot.start = timeTrace.format();
          timeTrace = timeTrace.add(slot.duration, "minute");
          slot.end = timeTrace.format();
        }

        // update form values
        form.setFieldValue("slots", defaultSlots);
        if (defaultSlots?.length > 1) setSimpleSlotsMode(false);
      }
    }

    if (Object.keys(changedValues).includes("slots")) {
      displayInconsistencies(form.getFieldsValue(), currentProject._id);
    }
  };

  // Initialize the slots mode, simple or advanced, depending on the initial number of slots
  // IF there is only one slot but stewardsSessionSynchro and placesSessionSynchro have been touched, switch to advanced
  const [simpleSlotsMode, setSimpleSlotsMode] = useState(true);
  useEffect(() => {
    if (session._id) {
      if (session.slots.length === 0) {
        setSimpleSlotsMode(true);
      } else if (session.slots.length === 1) {
        const firstSlot = session.slots[0];
        setSimpleSlotsMode(firstSlot.stewardsSessionSynchro && firstSlot.placesSessionSynchro);
      } else {
        setSimpleSlotsMode(false);
      }
    }
  }, [session._id, session.slots?.length]);

  const sessionRegistrationsColumns = [
    ...filterBlacklistedParticipantsColumns(
      generateRegistrationsColumns("./../..", currentProject)
    ),
    generateRegistrationDispoColumn("./../.."),
  ];
  const additionalRegistrationsColumns = [
    generateSessionNoShowColumn(session, registrations, dispatch),
    {
      title: "Inscription",
      render: (text, record) =>
        generateSubscriptionInfo(
          record,
          getSessionSubscription(record, session),
          registrations,
          true
        ),
      sorter: (a, b) =>
        sorter.date(
          getSessionSubscription(a, session).updatedAt,
          getSessionSubscription(b, session).updatedAt
        ),
    },
  ];

  // REGISTRATIONS STUFF

  const sessionRegistrations = registrations.filter((r) =>
    r.sessionsSubscriptions?.find((ss) => ss.session === session._id)
  );

  // Add registrations to the session (don't do this in cloning mode)
  useEffect(() => {
    if (!clonedElement && session._id) form.setFieldValue("registrations", sessionRegistrations);
  }, [JSON.stringify(sessionRegistrations), session._id, clonedElement]);

  // GLOBAL STUFF

  const onValidation = async (formData, fieldsKeysToUpdate) => {
    const payload = {...session, ...formData, ...getSlotsStartAndEnd(formData?.slots)};

    if (fieldsKeysToUpdate) {
      // Pick also start and end because they are together with the rest
      fieldsKeysToUpdate = [...fieldsKeysToUpdate, "start", "end"];
      const onlyFieldsToUpdate = pick(payload, fieldsKeysToUpdate);

      for (const entityId of groupEditing.elements) {
        await dispatch(sessionsActions.persist({...onlyFieldsToUpdate, _id: entityId}));
      }
    } else {
      if (payload.slots?.length > 0) {
        console.log("persist", payload);
        return dispatch(sessionsActions.persist(payload));
      } else {
        message.error("Vous devez créer au moins une plage horaire pour créer cette session.");
      }
    }
  };

  const sessionPostTransform = (values) => {
    if (values.start) values.start = values.start.format();
    if (values.end) values.end = values.end.format();

    // Set the session synchro if not done properly (workaround for now, but maybe we should delete this feature)
    for (const slot of values.slots) {
      if (slot.stewardsSessionSynchro === undefined) slot.stewardsSessionSynchro = true;
      if (slot.placesSessionSynchro === undefined) slot.placesSessionSynchro = true;
    }

    return values;
  };

  const SimpleSlot = () => (
    <CardElement
      title="Plage horaire"
      icon={<ClockCircleOutlined />}
      customButtons={
        <Button type={"link"} onClick={() => setSimpleSlotsMode(false)}>
          Plages multiples
        </Button>
      }>
      <div className="container-grid">
        <Stack row columnGap={5} rowGap={2} wrap>
          <SlotStartAndDurationInput name={["slots", 0]} />
        </Stack>

        <RecapSlot name={["slots", 0]} row style={{marginBottom: 0}} size={"small"} />
      </div>
    </CardElement>
  );

  return (
    <EditPage
      icon={<ScheduleOutlined />}
      i18nNs="sessions"
      form={form}
      onValidation={onValidation}
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      deletable
      onValuesChange={onValuesChange}
      elementsActions={sessionsActions}
      record={session}
      initialValues={{
        ...session,
        start: session.start && dayjs(session.start),
        end: session.end && dayjs(session.end),
        activity: session.activity?._id,
        team: session.team?._id,
      }}
      postTransform={sessionPostTransform}
      groupEditing={groupEditing}
      customButtons={<SessionGroupSmsButtons />}>
      <div className="container-grid two-thirds-one-third">
        <div className="container-grid">
          <CardElement>
            <div className="container-grid two-per-row">
              <WithEntityLinks
                endpoint="activities"
                name="activity"
                createButtonText="Créer une nouvelle activité"
                ElementEdit={ActivityEdit}>
                <SelectInput
                  label="Activité"
                  icon={<BookOutlined />}
                  options={activities.map((d) => ({value: d._id, label: d.name}))}
                  placeholder="sélectionnez une activité"
                  required
                  showSearch
                />
              </WithEntityLinks>

              {currentProject.useTeams && (
                <WithEntityLinks
                  endpoint="teams"
                  name="team"
                  createButtonText="Créer une nouvelle équipe"
                  ElementEdit={TeamEdit}>
                  <SelectInput
                    label="Équipe"
                    icon={<TeamOutlined />}
                    placeholder="sélectionnez une équipe"
                    options={[
                      {value: null, label: "- Pas d'équipe -"},
                      ...teams.map((d) => ({
                        value: d._id,
                        label: d.name + (d.activity?.name ? ` (${d.activity?.name})` : ""),
                      })),
                    ]}
                    showSearch
                  />
                </WithEntityLinks>
              )}

              <NumberInput
                label="Coefficient de bénévolat de la session"
                tooltip={
                  <>
                    ↖ Champ hérité de l'activité : Le coefficient de bénévolat de la session est
                    égal au coefficient de bénévolat de l'activité, mais vous pouvez le redéfinir si
                    besoin.
                  </>
                }
                name="volunteeringCoefficient"
                step={0.1}
                placeholder={hereditaryPlaceholder(
                  sessionActivityVal?.volunteeringCoefficient,
                  "coef"
                )}
                min={0}
                max={5}
              />

              <SwitchInput
                label="Inscrire tout le monde"
                name="everybodyIsSubscribed"
                tooltip={
                  "Rendre la session visible dans le planning de tous vos participants sans qu'iels s'y inscrivent."
                }
              />

              <NumberInput
                label="Nombre maximum de participant⋅es"
                name="maxNumberOfParticipants"
                disabled={sessionEverybodyIsSubscribedVal}
                tooltip={
                  <>
                    <p>
                      ↖ Champ hérité de l'activité : Le nombre maximum de participant⋅es de la
                      session est égal au nombre maximum de participant⋅es de l'activité, mais vous
                      pouvez le redéfinir si besoin.
                    </p>
                    Valeurs spéciales:
                    <ul>
                      <li>
                        Rien = valeur de l'activité (et si pas de valeur dans l'activité =
                        inscription libre)
                      </li>
                      <li>
                        0 = Session invisible pour les participant.es, seulement visible par les
                        orgas et les personnes inscrites
                      </li>
                    </ul>
                  </>
                }
                min={0}
                placeholder={hereditaryPlaceholder(
                  sessionActivityVal?.maxNumberOfParticipants,
                  "max"
                )}
              />
            </div>
          </CardElement>

          <CardElement>
            <div className="container-grid two-per-row">
              <RichTextInput i18nNs="sessions" name="notes" />

              <TagsSelectInput
                i18nNs="sessions"
                name="tags"
                elementsSelectors={sessionsSelectors}
              />
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <CardElement>
            <TextInput i18nNs="sessions" name="name" />
          </CardElement>

          <SessionSmallPreview />

          {currentProject.useAI && (
            <CardElement title="Créer avec l'IA">
              <SwitchInput label="Session figée" name="isScheduleFrozen" />
            </CardElement>
          )}
        </div>
      </div>

      <div
        className={
          "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
        }>
        <div className="containerV" style={{flexBasis: "33%"}}>
          {simpleSlotsMode ? <SimpleSlot /> : <AdvancedSlots />}
        </div>

        <div className="containerV" style={{flexBasis: "33%"}}>
          <StewardsTableWithModal
            onChange={() => {
              setIsModified(true);
              displayInconsistencies(form.getFieldsValue(), currentProject._id);
            }}
            sessionActivityVal={sessionActivityVal}
          />
        </div>

        {currentProject.usePlaces && (
          <div className="containerV" style={{flexBasis: "33%"}}>
            <PlacesTableWithModal
              onChange={() => {
                setIsModified(true);
                displayInconsistencies(form.getFieldsValue(), currentProject._id);
              }}
              sessionActivityVal={sessionActivityVal}
            />
          </div>
        )}
      </div>

      <ElementsTableWithModal
        name={"registrations"}
        onChange={() => setIsModified(true)}
        allElements={registrations}
        title="Participant⋅es"
        icon={<TeamOutlined />}
        subtitle="En rouge clair figurent les participant⋅es qui ne sont pas encore arrivé⋅es à l'événement."
        showHeader
        buttonTitle="Ajouter un⋅e participant⋅e"
        rowClassName={(record) => (record.hasCheckedIn ? "" : " ant-table-row-danger")}
        navigableRootPath="./../../participants"
        columns={[...sessionRegistrationsColumns, ...additionalRegistrationsColumns]}
        elementSelectionModalProps={{
          large: true,
          title: "Ajouter des participant⋅es à la session",
          searchInFields: searchInRegistrationFields,
          columns: sessionRegistrationsColumns,
        }}
      />
    </EditPage>
  );
}

export default withFallBackOnUrlId(SessionEdit);
