import {ScheduleOutlined} from "@ant-design/icons";
import dayjs from "dayjs";
import React, {useCallback} from "react";
import {useDispatch, useSelector} from "react-redux";
import CsvExportButton from "../../common/components/buttons/CsvExportButton";
import {useColumnsBlacklistingSelect} from "../../common/hooks/useColumnsBlacklistingSelect";
import {useLoadList} from "../../common/hooks/useLoadList";
import {ListPage} from "../../common/pages/ListPage";
import {categoriesActions} from "../../features/categories.js";
import {currentProjectSelectors} from "../../features/currentProject";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {useGenerateSessionsColumns} from "../../utils/columns/useGenerateSessionsColumns";
import {searchInSessionFields} from "../../utils/searchInFields/searchInSessionFields";
import {CategoriesFilterSelect} from "./atoms/CategoriesFilterSelect";
import buildSessionsExport from "./list/buildSessionsExport.js";
import {FilterPeriod} from "./list/FilterPeriod";

/**
 * Functional component representing custom action buttons for session list operations.
 * This component provides buttons for category filtering, filter period selection, and CSV export of sessions.
 * @component
 * @param {Object} props - The properties passed to the component.
 * @param {Function} props.dataExportFunction - The function responsible for exporting sessions to CSV format.
 * @param {Object} props.currentProject - The current project object used for naming the exported CSV file.
 * @returns {ReactNode} - Rendered component of custom action buttons for session list operations.
 */
const CustomButtons = ({dataExportFunction, currentProject}) => {
  return (
    <>
      <CategoriesFilterSelect />
      <FilterPeriod />
      <CsvExportButton
        tooltip="Exporter les sessions au format CSV"
        getExportName={() =>
          `Export des sessions - ${currentProject.name} - ${dayjs().format(
            "DD-MM-YYYY HH[h]mm"
          )}.csv`
        }
        dataExportFunction={dataExportFunction}>
        Exporter
      </CsvExportButton>
    </>
  );
};

/**
 * Functional component representing a list of sessions.
 * This component fetches and displays a list of sessions, allowing users to filter, search,
 * export data, and perform various actions on the sessions.
 * @component
 * @returns {ReactNode} - Rendered component of the session list.
 */
function SessionList() {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "sessions",
    defaultBlacklisting: [
      "everybodyIsSubscribed",
      "maxNumberOfParticipants",
      "volunteeringCoefficient",
    ],
  });
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);

  const dataExportFunction = useCallback(
    () => buildSessionsExport(sessions, currentProject.sessionNameTemplate),
    [sessions]
  );

  const filteredSessions = useSelector(sessionsSelectors.selectListFiltered);

  const columns = useGenerateSessionsColumns(
    "./..",
    currentProject.usePlaces,
    currentProject.useTeams,
    false,
    false,
    registrations,
    true
  );

  useLoadList(() => {
    dispatch(sessionsActions.loadList());
    dispatch(registrationsActions.loadList());
    dispatch(categoriesActions.loadList());
  });

  return (
    <ListPage
      icon={<ScheduleOutlined />}
      i18nNs="sessions"
      searchInFields={searchInSessionFields}
      elementsActions={sessionsActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      columns={filterBlacklistedColumns(columns)}
      customButtons={
        <CustomButtons dataExportFunction={dataExportFunction} currentProject={currentProject} />
      }
      dataSource={filteredSessions}
      groupEditable
      groupImportable
    />
  );
}

export default SessionList;
