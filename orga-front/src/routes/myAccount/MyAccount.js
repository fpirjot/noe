import MyAccountModal from "../../common/pages/MyAccountModal";
import {Alert, Collapse} from "antd";
import {useTranslation, Trans} from "react-i18next";
import {EyeInvisibleOutlined} from "@ant-design/icons";
import {DEFAULT_HEADERS} from "../../utils/api/fetchWithMessages";
import {TextInputPassword} from "../../common/inputs/TextInput";
import React from "react";

const {Panel} = Collapse;

export default function MyAccount({openState}) {
  const {t} = useTranslation();

  return (
    <MyAccountModal
      openState={openState}
      extra={
        // API Token information
        <Collapse style={{marginTop: 26}}>
          <Panel header={t("users:edit.apiToken.title")}>
            <Trans
              i18nKey="edit.apiToken.body"
              ns="users"
              components={{pre: <pre />, icon: <EyeInvisibleOutlined />}}
              values={{
                requestCode: JSON.stringify(
                  {...DEFAULT_HEADERS, Authorization: `JWT xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`},
                  null,
                  2
                ),
              }}
            />

            <Alert
              style={{marginBottom: 15}}
              type="warning"
              showIcon
              message={t("users:edit.apiToken.privacyAlert.message")}
              description={t("users:edit.apiToken.privacyAlert.description")}
            />
            <TextInputPassword
              label={t("users:edit.apiToken.label")}
              value={localStorage.getItem("token")}
            />
          </Panel>
        </Collapse>
      }
    />
  );
}
