import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {teamsActions, teamsSelectors} from "../../features/teams.js";
import {ListPage} from "../../common/pages/ListPage";
import {TeamOutlined} from "@ant-design/icons";
import {useLoadList} from "../../common/hooks/useLoadList";
import {useGenerateTeamsColumns} from "../../utils/columns/useGenerateTeamsColumns";

function TeamList() {
  const teams = useSelector(teamsSelectors.selectList);
  const dispatch = useDispatch();

  const columns = useGenerateTeamsColumns("./..");

  useLoadList(() => {
    dispatch(teamsActions.loadList());
  });

  return (
    <ListPage
      icon={<TeamOutlined />}
      i18nNs="teams"
      searchInFields={["name"]}
      elementsActions={teamsActions}
      columns={columns}
      dataSource={teams}
      groupEditable
      groupImportable
    />
  );
}

export default TeamList;
