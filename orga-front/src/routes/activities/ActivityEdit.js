import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {placesActions, placesSelectors} from "../../features/places.js";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {Alert, Collapse, Form} from "antd";
import {TableElement} from "../../common/components/TableElement";
import {CardElement} from "../../common/components/CardElement";
import {EditPage, ElementEditProps} from "../../common/pages/EditPage";
import {currentProjectSelectors} from "../../features/currentProject";
import {useLocation} from "react-router-dom";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {useLocalStorageState} from "../../utils/localStorageUtilities";
import {Trans, useTranslation} from "react-i18next";
import {
  BookOutlined,
  ClockCircleOutlined,
  QuestionCircleOutlined,
  ScheduleOutlined,
  TagOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {TextInput} from "../../common/inputs/TextInput";
import {NumberInput} from "../../common/inputs/NumberInput";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {useNewElementModal} from "../../common/hooks/useNewElementModal";
import {RichTextInput} from "../../common/inputs/RichTextInput";
import {TextAreaInput} from "../../common/inputs/TextAreaInput";
import {SelectInput} from "../../common/inputs/SelectInput";
import {WithEntityLinks} from "../../common/components/WithEntityLinks";
import {TagsSelectInput} from "../../common/components/TagsSelectInput";
import {columnsStewards} from "../../utils/columns/columnsStewards";
import {columnsPlaces} from "../../utils/columns/columnsPlaces";
import {useGenerateSessionsColumns} from "../../utils/columns/useGenerateSessionsColumns";
import {withFallBackOnUrlId} from "../../utils/withFallbackOnUrlId";
import {categoriesOptionsWithColorDot} from "../sessions/atoms/categoriesOptionsWithColorDot";
import {TableWithEditModal} from "../../common/components/TableWithEditModal";
import {ElementsTableWithModal} from "../../common/components/ElementsTableWithModal";
import {GroupEditionNavigationState} from "../../common/components/buttons/GroupEditionButton";
import {timeFormatter} from "../../utils/formatters";
import SessionEdit from "../sessions/SessionEdit";
import {DurationSelectInput} from "../../common/inputs/DurationSelectInput";

const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);
const CategoryEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../categories/CategoryEdit.js")
);
const PlaceEdit = lazyWithRetry(() => import(/* webpackPrefetch: true */ "../places/PlaceEdit.js"));

function ActivityEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const location = useLocation();
  const [form] = Form.useForm();
  const {t} = useTranslation();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const activity = useSelector(activitiesSelectors.selectEditing);
  const stewards = useSelector(stewardsSelectors.selectList);
  const places = useSelector(placesSelectors.selectList);
  const categories = useSelector(categoriesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const dispatch = useDispatch();
  const [setShowNewSessionModal, NewSessionModal] = useNewElementModal(SessionEdit);
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true});
  const [activitySessionsHelpCollapseKeys, setActivitySessionsHelpCollapseKeys] =
    useLocalStorageState("activitySessionsHelpCollapseKeys", false);

  const activitySessions = sessions.filter((s) => s.activity?._id === activity._id);

  const groupEditing: GroupEditionNavigationState = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    activitiesActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(categoriesActions.loadList());
      dispatch(sessionsActions.loadList());
      dispatch(activitiesActions.loadList()); // For tags
    },
    clonedElement
  );

  const sessionsColumns = useGenerateSessionsColumns(
    "./../..",
    currentProject.usePlaces,
    true,
    true
  );

  const columnsSlots = [{dataIndex: "duration", render: timeFormatter.duration}];

  const activityPostTransform = (values) => {
    const categoryIndex = categories.map((d) => d._id).indexOf(values.category);
    if (categoryIndex >= 0) {
      values.category = {...categories[categoryIndex]};
    }
    return values;
  };

  return (
    <>
      <EditPage
        icon={<BookOutlined />}
        i18nNs="activities"
        clonable
        form={form}
        clonedElement={clonedElement}
        asModal={asModal}
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        onCreate={onCreate}
        deletable
        elementsActions={activitiesActions}
        record={activity}
        initialValues={{
          ...activity,
          category: activity.category?._id,
          stewardVolunteeringCoefficient: activity.stewardVolunteeringCoefficient || 1.5,
        }}
        postTransform={activityPostTransform}
        groupEditing={groupEditing}>
        <div className="container-grid two-thirds-one-third">
          <CardElement>
            <div className="container-grid two-per-row">
              <TextInput
                i18nNs="activities"
                name="name"
                required
                rules={[
                  {
                    max: 75,
                    message: t("activities:schema.name.tooLong"),
                    warningOnly: true,
                  },
                ]}
              />

              <WithEntityLinks
                endpoint="categories"
                name="category"
                createButtonText={t("categories:label", {context: "createNew"})}
                ElementEdit={CategoryEdit}>
                <SelectInput
                  icon={<TagOutlined />}
                  label={t("categories:label")}
                  placeholder={t("categories:label").toLowerCase()}
                  required
                  options={categoriesOptionsWithColorDot(categories)}
                  showSearch
                />
              </WithEntityLinks>

              <TagsSelectInput
                i18nNs="activities"
                name="secondaryCategories"
                elementsSelectors={activitiesSelectors}
              />

              <SwitchInput i18nNs="activities" name="allowSameTime" />
            </div>
          </CardElement>

          <CardElement>
            <div className="container-grid">
              <NumberInput
                i18nNs="activities"
                tooltip={"↘ Champ re-définissable dans la session."}
                name="volunteeringCoefficient"
                step={0.1}
                min={0}
                max={5}
              />

              <NumberInput
                i18nNs="activities"
                name="stewardVolunteeringCoefficient"
                step={0.1}
                min={0}
                max={5}
              />

              <NumberInput
                i18nNs="activities"
                name="maxNumberOfParticipants"
                tooltip={
                  <>
                    <p>↘ Champ re-définissable dans la session.</p>
                    Valeurs spéciales:
                    <ul>
                      <li>Rien = inscription libre</li>
                      <li>
                        0 = Les sessions de l'activité seront invisibles pour les participant.es,
                        seulement visibles par les orgas et les personnes inscrites
                      </li>
                    </ul>
                  </>
                }
                min={0}
              />
            </div>
          </CardElement>
        </div>

        <CardElement>
          <div className="container-grid two-thirds-one-third">
            <RichTextInput i18nNs="activities" name="description" />

            <TextAreaInput
              i18nNs="activities"
              name="summary"
              rules={[
                {max: 250, message: <Trans i18nKey="schema.summary.tooLong" ns="activities" />},
              ]}
            />

            <RichTextInput i18nNs="activities" name="notes" />

            <TagsSelectInput
              i18nNs="activities"
              name="tags"
              elementsSelectors={activitiesSelectors}
            />
          </div>
        </CardElement>

        <Collapse
          style={{marginBottom: 26}}
          onChange={setActivitySessionsHelpCollapseKeys}
          activeKey={currentProject.useAI ? ["sessions-help"] : activitySessionsHelpCollapseKeys}>
          <Collapse.Panel
            header={t("activities:helpForSessionsCreation.title")}
            key="sessions-help">
            <Alert
              message={
                <>
                  <Trans
                    ns="activities"
                    i18nKey="helpForSessionsCreation.information"
                    components={{questionMarkIcon: <QuestionCircleOutlined />}}
                  />
                  {currentProject.useAI && t("activities:helpForSessionsCreation.informationUseAI")}
                </>
              }
              style={{marginBottom: 20}}
            />

            <div
              style={{marginBottom: -26}}
              className={
                "container-grid " + (currentProject.usePlaces ? "three-per-row" : "two-per-row")
              }>
              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <TableWithEditModal
                  name={"slots"}
                  title={t("activities:schema.slots.label")}
                  modalTitle={t("activities:schema.slots.editModal.title")}
                  icon={<ClockCircleOutlined />}
                  buttonTitle={t("activities:schema.slots.buttonTitle")}
                  tooltip={t("activities:schema.slots.tooltip")}
                  columns={columnsSlots}>
                  <DurationSelectInput name={"duration"} required />
                </TableWithEditModal>
              </div>

              <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                <ElementsTableWithModal
                  name={"stewards"}
                  onChange={() => setIsModified(true)}
                  allElements={stewards}
                  title={t("activities:schema.stewards.label")}
                  icon={<UserOutlined />}
                  buttonTitle={t("activities:schema.stewards.buttonTitle")}
                  tooltip={t("activities:schema.stewards.tooltip")}
                  navigableRootPath="./../../stewards"
                  columns={columnsStewards}
                  ElementEdit={StewardEdit}
                  elementSelectionModalProps={{
                    title: t("activities:schema.slots.editModal.stewards.label"),
                    searchInFields: ["firstName", "lastName"],
                    createNewElementButtonTitle: t(
                      "activities:schema.slots.editModal.stewards.buttonTitle"
                    ),
                  }}
                />
              </div>

              {currentProject.usePlaces && (
                <div className="containerV" style={{flexGrow: 1, flexShrink: 1, flexBasis: "33%"}}>
                  <ElementsTableWithModal
                    name={"places"}
                    onChange={() => setIsModified(true)}
                    allElements={places}
                    title={t("activities:schema.places.label")}
                    icon={<UserOutlined />}
                    buttonTitle={t("activities:schema.places.buttonTitle")}
                    tooltip={t("activities:schema.places.tooltip")}
                    navigableRootPath="./../../places"
                    columns={columnsPlaces}
                    ElementEdit={PlaceEdit}
                    elementSelectionModalProps={{
                      title: t("activities:schema.slots.editModal.places.label"),
                      searchInFields: ["name"],
                      createNewElementButtonTitle: t(
                        "activities:schema.slots.editModal.places.buttonTitle"
                      ),
                    }}
                  />
                </div>
              )}
            </div>
          </Collapse.Panel>
        </Collapse>

        {activity._id !== "new" && (
          <TableElement.WithTitle
            title={t("activities:schema.sessions.label")}
            icon={<ScheduleOutlined />}
            showHeader
            buttonTitle={t("activities:schema.sessions.buttonTitle")}
            onClickButton={() => {
              dispatch(
                sessionsActions.changeEditing({
                  _id: "new",
                  activity,
                  places: [],
                  stewards: [],
                  slots: [],
                })
              );
              setShowNewSessionModal(true);
            }}
            navigableRootPath="./../../sessions"
            columns={sessionsColumns}
            dataSource={activitySessions}
            pagination
          />
        )}
      </EditPage>

      <NewSessionModal />
    </>
  );
}

export default withFallBackOnUrlId(ActivityEdit);
