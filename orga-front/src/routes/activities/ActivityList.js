import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {activitiesActions, activitiesSelectors} from "../../features/activities.js";
import {ListPage} from "../../common/pages/ListPage";
import {Tag} from "antd";
import {listOfClickableElements} from "../../utils/listOfClickableElements";
import {currentProjectSelectors} from "../../features/currentProject";
import {Link} from "react-router-dom";
import {personName} from "../../utils/utilities";
import {useColumnsBlacklistingSelect} from "../../common/hooks/useColumnsBlacklistingSelect";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {useTranslation} from "react-i18next";
import {BookOutlined} from "@ant-design/icons";
import {useLoadList} from "../../common/hooks/useLoadList";
import {editableTagsColumn} from "../../utils/columns/editableTagsColumn";
import {searchInActivityFields} from "../../utils/searchInFields/searchInActivityFields";
import {sorter} from "../../utils/sorters";
import {editableCellColumn} from "../../common/components/EditableCell";
import {editableEntitiesColumn} from "../../utils/columns/editableEntitiesColumn";
import PlaceEdit from "../places/PlaceEdit";
import {placesActions, placesSelectors} from "../../features/places";
import {stewardsActions, stewardsSelectors} from "../../features/stewards";
import StewardEdit from "../stewards/StewardEdit";

function ActivityList() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const activities = useSelector(activitiesSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "activities",
    defaultBlacklisting: ["summary", "volunteeringCoefficient", "maxNumberOfParticipants"],
  });

  const getNumberOfSessionsForActivity = (activity) =>
    sessions.filter((session) => session.activity._id === activity._id).length;

  const getActivityCategoryTitle = (record) => record.category?.name;
  const columns = [
    {
      title: t("categories:label"),
      dataIndex: "category",
      sorter: (a, b) => sorter.text(getActivityCategoryTitle(a), getActivityCategoryTitle(b)),
      render: (text, record) => (
        <Link to={`./../categories/${record.category?._id}`}>
          <Tag
            style={{
              cursor: "pointer",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              overflow: "hidden",
              maxWidth: 125,
            }}
            title={getActivityCategoryTitle(record)}
            color={record.category?.color}>
            {getActivityCategoryTitle(record)}
          </Tag>
        </Link>
      ),
      searchable: true,
      width: 140,
      searchText: getActivityCategoryTitle,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.name.label"),
        dataIndex: "name",
        type: "longText",
        placeholder: t("common:schema.name.placeholder"),
        elementsActions: activitiesActions,
      }),
      sorter: (a, b) => sorter.text(a.name, b.name),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: t("common:schema.summary.label"),
        dataIndex: "summary",
        type: "longText",
        placeholder: t("common:schema.summary.placeholder"),
        elementsActions: activitiesActions,
      }),
      sorter: (a, b) => sorter.text(a.summary, b.summary),
      searchable: true,
    },
    editableEntitiesColumn({
      title: t("activities:schema.stewards.label"),
      dataIndex: "stewards",
      mode: "multiple",
      getEntityLabel: personName,
      elementsActions: activitiesActions,
      entityElementsActions: stewardsActions,
      entityElementsSelectors: stewardsSelectors,
      ElementEdit: StewardEdit,
    }),
    currentProject.usePlaces &&
      editableEntitiesColumn({
        title: t("activities:schema.places.label"),
        dataIndex: "places",
        mode: "multiple",
        getEntityLabel: (el) => el.name,
        elementsActions: activitiesActions,
        entityElementsActions: placesActions,
        entityElementsSelectors: placesSelectors,
        ElementEdit: PlaceEdit,
      }),
    editableTagsColumn({
      title: t("common:schema.tags.label"),
      dataIndex: "tags",
      elementsActions: activitiesActions,
      elementsSelectors: activitiesSelectors,
    }),
    editableTagsColumn({
      title: t("activities:schema.secondaryCategories.label"),
      dataIndex: "secondaryCategories",
      elementsActions: activitiesActions,
      elementsSelectors: activitiesSelectors,
    }),
    currentProject.usePlaces &&
      currentProject.useAI && {
        title: t("activities:schema.nonBlockingActivity.antiLabel"),
        dataIndex: "nonBlockingActivity",
        sorter: (a, b) => sorter.number(a.nonBlockingActivity, b.nonBlockingActivity),
        searchable: true,
        render: (text, record) =>
          record.nonBlockingActivity
            ? t("activities:schema.nonBlockingActivity.options.true")
            : t("activities:schema.nonBlockingActivity.options.false"),
        width: 180,
      },
    {
      ...editableCellColumn({
        title: t("activities:schema.maxNumberOfParticipants.label_short"),
        dataIndex: "maxNumberOfParticipants",
        type: "number",
        placeholder: t("activities:schema.maxNumberOfParticipants.placeholder"),
        elementsActions: activitiesActions,
      }),
      sorter: (a, b) => sorter.number(a.maxNumberOfParticipants, b.maxNumberOfParticipants),
      width: 165,
    },
    {
      ...editableCellColumn({
        title: t("activities:schema.volunteeringCoefficient.label"),
        dataIndex: "volunteeringCoefficient",
        type: "number",
        placeholder: t("activities:schema.volunteeringCoefficient.placeholder"),
        elementsActions: activitiesActions,
      }),
      sorter: (a, b) => sorter.number(a.volunteeringCoefficient, b.volunteeringCoefficient),
      width: 200,
    },
    {
      title: t("activities:schema.numberOfSessions.label_short"),
      render: (text, record) => getNumberOfSessionsForActivity(record) || "",
      sorter: (a, b) =>
        sorter.number(getNumberOfSessionsForActivity(a), getNumberOfSessionsForActivity(b)),
      width: 125,
    },
  ].filter((el) => !!el);

  useLoadList(() => {
    dispatch(activitiesActions.loadList()).then(() =>
      dispatch(sessionsActions.loadList({silent: true}))
    );
  });

  return (
    <ListPage
      icon={<BookOutlined />}
      i18nNs="activities"
      searchInFields={searchInActivityFields}
      elementsActions={activitiesActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      columns={filterBlacklistedColumns(columns)}
      dataSource={activities}
      groupEditable
      groupImportable
    />
  );
}

export default ActivityList;
