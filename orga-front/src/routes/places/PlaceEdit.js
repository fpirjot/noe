import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {placesActions, placesSelectors} from "../../features/places.js";
import {AvailabilitySlotsTable} from "../../common/components/AvailabilitySlotsTable.js";
import {EditPage, ElementEditProps} from "../../common/pages/EditPage";
import {CardElement} from "../../common/components/CardElement";
import {EnvironmentOutlined} from "@ant-design/icons";
import {TextInput} from "../../common/inputs/TextInput";
import {NumberInput} from "../../common/inputs/NumberInput";
import {ImportProjectAvailabilitiesButton} from "../../common/components/buttons/ImportProjectAvailabilitiesButton";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {RichTextInput} from "../../common/inputs/RichTextInput";
import {TextAreaInput} from "../../common/inputs/TextAreaInput";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "../../utils/withFallbackOnUrlId";

function PlaceEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const location = useLocation();
  const dispatch = useDispatch();
  const place = useSelector(placesSelectors.selectEditing);
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true});

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(placesActions, id, undefined, clonedElement);

  const changeAvailabilitySlots = (newSlots) => {
    dispatch(placesActions.changeEditing({availabilitySlots: newSlots}));
    setIsModified(true);
  };

  return (
    <EditPage
      i18nNs="places"
      icon={<EnvironmentOutlined />}
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      deletable
      elementsActions={placesActions}
      record={place}
      initialValues={place}
      outerChildren={
        <AvailabilitySlotsTable
          title="Disponibilités"
          disableDatesIfOutOfProject // We grey out all the dates out of the project scope, but we allow to select outside of them
          onChange={changeAvailabilitySlots}
          customButtons={
            <ImportProjectAvailabilitiesButton changeAvailabilitySlots={changeAvailabilitySlots} />
          }
          availabilitySlots={place.availabilitySlots}
        />
      }
      groupEditing={groupEditing}>
      <CardElement>
        <div className="container-grid two-per-row">
          <TextInput label="Nom de l'espace" name="name" placeholder="nom" required />

          <NumberInput
            label="Nombre maximum de personnes"
            name="maxNumberOfParticipants"
            min={1}
            placeholder="max"
          />
        </div>
      </CardElement>

      <CardElement>
        <div className="container-grid">
          <TextAreaInput label="Informations" name="summary" placeholder="informations" />

          <RichTextInput
            label="Notes privées pour les orgas"
            name="notes"
            placeholder="notes privées"
            tooltip="Ces notes ne sont pas affichées aux participant⋅es, elles ne sont disponibles que pour les organisateur⋅ices de l'événement"
          />
        </div>
      </CardElement>
    </EditPage>
  );
}

export default withFallBackOnUrlId(PlaceEdit);
