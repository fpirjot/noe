import React, {Suspense} from "react";
import dayjs from "dayjs";
import {personName, removeDuplicates} from "../../../utils/utilities";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../../features/projects";
import {registrationsSelectors} from "../../../features/registrations";
import {teamsSelectors} from "../../../features/teams";
import {sessionsSelectors} from "../../../features/sessions";
import {BetaTag} from "../../../common/components/BetaTag";
import {lazyWithRetry} from "../../../utils/lazyWithRetry";
import {useWindowDimensions} from "../../../common/hooks/useWindowDimensions";
import {useTranslation} from "react-i18next";
import {cleanAnswer} from "../../../utils/columns/cleanAnswer";
import {getSessionsPlanningForRegistration} from "./getSessionsPlanningForRegistration";
import {Alert} from "antd";
import {Link} from "react-router-dom";
import {dateFormatter} from "../../../utils/formatters";

const CsvGroupImportButton = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../../common/components/buttons/CsvGroupImportButton")
);
const CsvExportButton = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../../common/components/buttons/CsvExportButton")
);

export const buildParticipantExport = (project, registrations, teams, allSlots) => {
  const ticketsFieldName = project.ticketingMode && `${project.ticketingMode}Tickets`;

  const baseColumns = [
    "Prénom",
    "Nom",
    "Email",
    "Dates OK",
    "Formulaire OK",
    "Billetterie OK",
    "Inscription commencée",
    "Arrivé.e",
    `Détail billets (${project.ticketingMode || " - "})`,
    "Montant total payé",
    "Encadrant⋅e lié⋅e",
    "Équipes",
    "Date d'arrivée",
    "Date de départ",
    "Dates avancées",
    "Nombre de jours comptabilisés",
    "Nombre de jours total",
    "Jauge de bénévolat (minutes/jour)",
    "Rôle",
  ];

  let allFormColumns = [],
    allPlanningColumns = [];

  const exportedData = registrations
    .map((registration) => {
      // Compute min and max arrival and departure date
      const earliestArrivalDate = registration.availabilitySlots.reduce(
        (accumulator, currentValue) => {
          const start = dayjs(currentValue.start);
          return !accumulator || start < accumulator ? start : accumulator;
        },
        undefined
      );
      const latestLeavingDate = registration.availabilitySlots.reduce(
        (accumulator, currentValue) => {
          const end = dayjs(currentValue.end);
          return !accumulator || end > accumulator ? end : accumulator;
        },
        undefined
      );

      const sessionsPlanning = getSessionsPlanningForRegistration(
        registration,
        allSlots,
        teams,
        project.sessionNameTemplate,
        allPlanningColumns
      );

      // Format it to a single total string
      const sessionPlanningTotal = sessionsPlanning.map((slotInfo) => slotInfo.value).join(" | ");
      // And also group it by day
      const sessionPlanningByDate = sessionsPlanning.reduce((groupByDayAcc, {startDate, value}) => {
        const formattedDate = dateFormatter.longDate(startDate, true);
        return {
          ...groupByDayAcc,
          [formattedDate]:
            groupByDayAcc[formattedDate]?.length > 0
              ? groupByDayAcc[formattedDate] + " | " + value
              : value,
        };
      }, {});

      // Clean the form answers
      const cleanedFormAnswers =
        registration.formAnswers &&
        Object.entries(registration.formAnswers).reduce((acc, [key, value]) => {
          return {...acc, [key]: cleanAnswer(value, ";")};
        }, {});
      // Add the fields to the form columns
      cleanedFormAnswers && allFormColumns.push(...Object.keys(cleanedFormAnswers));

      // Return all the registration data
      return {
        createdAt: dayjs(registration.createdAt),
        INFOS: ">>",
        Prénom: registration.user?.firstName,
        Nom: registration.user?.lastName,
        Email: registration.user?.email,
        "Dates OK": registration.firstSlotIsOk,
        "Formulaire OK": registration.formIsOk,
        "Billetterie OK": registration.ticketingIsOk,
        "Inscription commencée": registration.booked,
        "Arrivé.e": registration.hasCheckedIn,
        [`Détail billets (${project.ticketingMode || " - "})`]:
          project.ticketingMode &&
          registration[ticketsFieldName]?.map((ticket) => ticket.id).join(", "),
        "Montant total payé":
          project.ticketingMode &&
          registration[ticketsFieldName]?.reduce((acc, ticket) => acc + ticket.amount, 0),
        "Encadrant⋅e lié⋅e": personName(registration.steward),
        Équipes: registration.teamsSubscriptionsNames,
        "Date d'arrivée": earliestArrivalDate?.format("LLL"),
        "Date de départ": latestLeavingDate?.format("LLL"),
        "Dates avancées": registration.availabilitySlots?.length > 1,
        "Nombre de jours comptabilisés": registration.numberOfDaysOfPresence,
        "Nombre de jours total": registration.daysOfPresence.length,
        Rôle: registration.role,
        "Jauge de bénévolat (minutes/jour)": Math.round(registration.voluntaryCounter),
        PLANNING: ">>",
        "Tout l'événement": sessionPlanningTotal,
        ...sessionPlanningByDate,
        "FORMULAIRE D'INSCRIPTION": ">>",
        ...cleanedFormAnswers,
      };
    })
    .sort((a, b) => a.createdAt.diff(b.createdAt)); // Sort it by creation date

  // Compute the final columns layout of the CSV file
  allPlanningColumns = allPlanningColumns
    .sort((a, b) => a.diff(b))
    .map((startDate) => dateFormatter.longDate(startDate, true));

  const columnsLayout = [
    "createdAt",
    "INFOS",
    ...baseColumns,
    "PLANNING",
    "Tout l'événement",
    ...removeDuplicates(allPlanningColumns),
    "FORMULAIRE D'INSCRIPTION",
    ...removeDuplicates(allFormColumns),
  ];

  // Return the data with the column layout
  return [columnsLayout, ...exportedData];
};

export const ParticipantImportExportButtons = () => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const project = useSelector(projectsSelectors.selectEditing);
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
  const teams = useSelector(teamsSelectors.selectList);
  const allSlots = useSelector(sessionsSelectors.selectSlotsList);
  const {isMobileView} = useWindowDimensions();

  const validateImport = (entitiesToImport, allowedFields) => {
    const errors = [];

    entitiesToImport.forEach(
      ({firstName, lastName, email, arrivalDateTime, departureDateTime, ...row}, index) => {
        // User must be defined
        if (!firstName || !lastName || !email) {
          errors.push({
            row: index,
            message: "Il manque des informations sur l'utilisateur (nom, prénom, ou email).",
          });
        }

        // Availability slots must be correct dates
        const arrivalDate = arrivalDateTime && dayjs(arrivalDateTime, "L LT");
        const departureDate = departureDateTime && dayjs(departureDateTime, "L LT");
        if (!(arrivalDate?.isValid() && departureDate?.isValid())) {
          errors.push({
            row: index,
            message:
              `Les dates de départ ou d'arrivée ne sont pas bien formatées. ` +
              `Formatez de cette manière: ${dayjs().format(
                "L LT"
              )} pour la date d'aujourd'hui, par exemple.`,
          });
        }
      }
    );
    return errors;
  };

  const handleRegistrationsGroupImport = (entitiesToImport, allowedFields) => {
    // Prepare the imported registrations
    const preparedRegistrations = entitiesToImport
      .map(({firstName, lastName, email, arrivalDateTime, departureDateTime, ...row}, index) => {
        // Dispatch the values to registration data or to form data based on if it is allowed or not
        const registrationData = {};
        const formData = {};
        Object.keys(row).forEach((key) => {
          if (allowedFields.includes(key)) registrationData[key] = row[key];
          else formData[key] = row[key];
        });

        // Availability slots must be correct dates
        const arrivalDate = arrivalDateTime && dayjs(arrivalDateTime, "L LT");
        const departureDate = departureDateTime && dayjs(departureDateTime, "L LT");
        if (arrivalDate?.isValid() && departureDate?.isValid()) {
          registrationData.availabilitySlots = [{start: arrivalDate, end: departureDate}];
        }

        console.log({
          ...registrationData,
          formAnswers: formData,
          user: {firstName, lastName, email},
        });
        return {
          ...registrationData,
          formAnswers: formData,
          user: {firstName, lastName, email},
        };
      })
      .filter((el) => !!el);

    // Import them !
    dispatch(projectsActions.import({registrations: preparedRegistrations}, true, true));
  };

  return (
    <Suspense fallback={null}>
      <CsvExportButton
        tooltip="Exporter les participant⋅es au format CSV"
        getExportName={() =>
          `Export des participant⋅es - ${project.name} - ${dayjs().format(
            "DD-MM-YYYY HH[h]mm"
          )}.csv`
        }
        withFields
        dataExportFunction={() => buildParticipantExport(project, registrations, teams, allSlots)}>
        Exporter
      </CsvExportButton>
      <CsvGroupImportButton
        title={
          <>
            {!isMobileView && "Importer"} <BetaTag />
          </>
        }
        keepUnknownEntries
        onOk={handleRegistrationsGroupImport}
        validate={validateImport}
        elementsName={t("registrations:label_other")}
        forceEndpoint={"registrations"}
        forceSchema={[
          {key: "firstName", label: t("users:schema.firstName.label")},
          {key: "lastName", label: t("users:schema.lastName.label")},
          {key: "email", label: t("users:schema.email.label")},
          {key: "arrivalDateTime", label: t("registrations:schema.arrivalDateTime.label")},
          {key: "departureDateTime", label: t("registrations:schema.departureDateTime.label")},
          {key: "steward", label: t("stewards:label")},
          {key: "tags", label: t("registrations:schema.tags.label"), isArray: true},
        ]}
        customExampleImportFileFunction={() => [
          {
            firstName: "John",
            lastName: "Doe",
            email: "johndoe@example.com",
            arrivalDateTime: dayjs().format("L LT"),
            departureDateTime: dayjs().add(3, "day").format("L LT"),
            steward: undefined,
            tags: ["PSC1", "Vegan"],
          },
          {
            firstName: "Sarah",
            lastName: "Dean",
            email: "sarahdean@example.com",
            arrivalDateTime: dayjs().format("L LT"),
            departureDateTime: dayjs().add(3, "day").format("L LT"),
            steward: "< l'ID d'un·e encadrant·e >",
            tags: undefined,
          },
        ]}
        customInfo={
          <Alert
            message={"Importez aussi les réponses au formulaire d'inscription"}
            description={
              <>
                <p>
                  Dans l'import, après les colonnes existantes, vous pouvez ajouter des colonnes
                  pour remplir les réponses aux questions du formulaire d'inscription.
                </p>
                <p>
                  Pour cela, il vous suffit de saisir comme nom de colonne la clé de la question de
                  formulaire que vous souhaitez remplir (voir dans l'
                  <Link to={"../config#form"}>onglet Aide du formulaire d'inscription</Link> pour
                  savoir où trouver la "clé"), et de donner une valeur de réponse.
                </p>
                Attention cependant au format de ces réponses: il varie en fonction du type de
                question (choix multiple, case à cocher, texte simple, etc.). En cas de doute,
                contactez l'équipe NOÉ.
              </>
            }
            style={{marginBottom: 26}}
          />
        }
      />
    </Suspense>
  );
};
