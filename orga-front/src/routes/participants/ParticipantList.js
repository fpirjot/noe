import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {TeamOutlined} from "@ant-design/icons";
import {Checkbox} from "antd";
import {ListPage} from "../../common/pages/ListPage";
import {CardElement} from "../../common/components/CardElement";
import VolunteeringGauge from "../../common/components/VolunteeringGauge";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {projectsSelectors} from "../../features/projects.js";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {sessionsActions} from "../../features/sessions";
import {teamsActions} from "../../features/teams";
import {currentUserSelectors} from "../../features/currentUser";
import {FormElement} from "../../common/inputs/FormElement";
import {useColumnsBlacklistingSelect} from "../../common/hooks/useColumnsBlacklistingSelect";
import {GetPdfPlanningButton} from "../../common/components/buttons/GetPdfPlanningButton";
import {personName} from "../../utils/utilities";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import {UnregisterButton} from "./atoms/UnregisterButton";
import {ChangeIdentityButton} from "./atoms/ChangeIdentityButton";
import {useLoadList} from "../../common/hooks/useLoadList";
import {useExpandableRegistrationInfo} from "./atoms/useExpandableRegistrationInfo";
import {ParticipantImportExportButtons} from "./atoms/ParticipantImportExportButtons";
import {generateRegistrationDispoColumn} from "../../utils/columns/generateRegistrationDispoColumn";
import {generateRegistrationsColumns} from "../../utils/columns/generateRegistrationsColumns";
import {generateYesNoColumn} from "../../utils/columns/generateYesNoColumn";
import {searchInRegistrationFields} from "../../utils/searchInFields/searchInRegistrationFields";
import {dateFormatter} from "../../utils/formatters";
import {sorter} from "../../utils/sorters";
import {editableEntitiesColumn} from "../../utils/columns/editableEntitiesColumn";

const StewardEdit = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../stewards/StewardEdit")
);

function ParticipantList() {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  useLoadList(() => {
    dispatch(registrationsActions.loadList()).then(() => {
      // Load registrations first, because it's critical, then the rest can be loaded
      dispatch(sessionsActions.loadList());
      dispatch(teamsActions.loadList());
      dispatch(stewardsActions.loadList());
    });
  });

  const project = useSelector(projectsSelectors.selectEditing);
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "participants",
  });
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);
  const [advancedMode, setAdvancedMode] = useState(false);
  const [displayUnbookedUsers, setDisplayUnbookedUsers] = useState(false);

  const registrationsFinal = displayUnbookedUsers
    ? registrations
    : registrations.filter((r) => r.availabilitySlots?.length > 0);

  const modifyFormAnswerField = async (record) =>
    await dispatch(registrationsActions.persist(record));

  const modifyBooleanFieldFn = (registrationId, field) => (e) => {
    const registrationToUpdate = {
      _id: registrationId,
      [field]: e.target.checked,
    };
    dispatch(registrationsActions.persist(registrationToUpdate));
  };

  const getTicketsListString = (record) => {
    const tickets = record[`${project.ticketingMode}Tickets`];
    return tickets?.length > 0 ? tickets.map((ticket) => ticket.id).join(", ") : undefined;
  };

  const getExpandableRegistrationInfo = useExpandableRegistrationInfo();

  const columns = generateRegistrationsColumns(
    "./..",
    project,
    {
      start: [
        {
          width: 48,
          render: (text, record) => (
            <GetPdfPlanningButton
              customFileName={() => personName(record.user)}
              elementsActions={registrationsActions}
              id={record._id}
              noText
              noTooltip
            />
          ),
        },
        {
          width: 65,
          title: "Date",
          dataIndex: "createdAt",
          sorter: (a, b) => sorter.date(a.createdAt, b.createdAt),
          render: (text, record) => dateFormatter.shortDate(record.createdAt),
        },
      ],
      middle: [
        editableEntitiesColumn({
          title: t("registrations:schema.steward.label"),
          dataIndex: "steward",
          getEntityLabel: personName,
          deselectEntityLabel: "Pas d'encadrant⋅e",
          elementsActions: registrationsActions,
          entityElementsActions: stewardsActions,
          entityElementsSelectors: stewardsSelectors,
          ElementEdit: StewardEdit,
        }),
      ],
      end: [
        {
          title: t("registrations:schema.hasCheckedIn.label"),
          ...generateYesNoColumn("hasCheckedIn", (text, record) => (
            <Checkbox
              checked={record.hasCheckedIn}
              onChange={modifyBooleanFieldFn(record._id, "hasCheckedIn")}
            />
          )),
          width: 110,
        },
        {
          title: t("registrations:schema.arrivalDateTime.label"),
          dataIndex: "arrivalDateTime",
          render: (text, record) => {
            // If arrived, green. If not arrived and date is past, red. Else, no color
            const color = record.hasCheckedIn
              ? "green"
              : dayjs().isAfter(record.arrivalDateTime) && "red";
            return (
              <span style={{color}}>
                {dateFormatter.longDateTime(record.arrivalDateTime, true)}
              </span>
            );
          },
          sorter: (a, b) => sorter.date(a.arrivalDateTime, b.arrivalDateTime),
          searchable: true,
          searchText: (record) => dateFormatter.longDateTime(record.arrivalDateTime, true),
          width: 155,
        },
        {
          title: t("registrations:schema.departureDateTime.label"),
          dataIndex: "departureDateTime",
          render: (text, record) => {
            // If arrived, green. If not arrived and date is past, red. Else, no color
            const color =
              record.hasCheckedIn && dayjs().isAfter(record.departureDateTime) && "green";
            return (
              <span style={{color}}>
                {dateFormatter.longDateTime(record.departureDateTime, true)}
              </span>
            );
          },
          sorter: (a, b) => sorter.date(a.departureDateTime, b.departureDateTime),
          searchable: true,
          searchText: (record) => dateFormatter.longDateTime(record.departureDateTime, true),
          width: 155,
        },
        advancedMode && {
          title: t("registrations:schema.hidden.label"),
          ...generateYesNoColumn("hidden", (text, record) => (
            <Checkbox
              checked={record.hidden}
              onChange={modifyBooleanFieldFn(record._id, "hidden")}
            />
          )),
          width: 95,
        },
        {
          title: t("registrations:schema.everythingIsOk.label"),
          ...generateYesNoColumn("everythingIsOk"),
          width: 130,
        },
        project.ticketingMode && {
          title: t("registrations:schema.tickets.label"),
          dataIndex: `${project.ticketingMode}Tickets`,
          render: (text, record) => getTicketsListString(record),
          sorter: (a, b) => sorter.text(getTicketsListString(a), getTicketsListString(b)),
          searchable: true,
          searchText: getTicketsListString,
          width: 115,
        },
        {
          title: t("registrations:schema.voluntaryCounter.label"),
          dataIndex: "volunteering",
          render: (text, record) => <VolunteeringGauge registration={record} noTooltip />,
          sorter: (a, b) => sorter.number(a.voluntaryCounter, b.voluntaryCounter),
        },
        generateRegistrationDispoColumn("./.."),
      ],
    },
    modifyFormAnswerField
  );

  const stickyChangeIdentityColumn = {
    fixed: "right",
    render: (text, record) => (
      <div style={{margin: 4, textAlign: "right"}}>
        {record.user._id !== currentUser?._id && (
          <ChangeIdentityButton registration={record} noTooltip />
        )}
      </div>
    ),
    width: 56,
  };

  return (
    <ListPage
      i18nNs="registrations"
      icon={<TeamOutlined />}
      creatable={false}
      customButtons={<ParticipantImportExportButtons />}
      groupEditable
      forceEndpoint={"registrations"}
      deletable={false}
      columns={[...filterBlacklistedColumns(columns), stickyChangeIdentityColumn]}
      dataSource={registrationsFinal}
      searchInFields={
        project.ticketingMode
          ? (registration) => [
              getTicketsListString(registration),
              ...searchInRegistrationFields(registration),
            ]
          : searchInRegistrationFields
      }
      rowClassName={(record) =>
        record.invitationToken
          ? "ant-table-row-selected"
          : record.everythingIsOk
          ? ""
          : "ant-table-row-danger"
      }
      multipleActionsButtons={({selectedRowKeys, setSelectedRowKeys}) => (
        <>
          <UnregisterButton
            title={
              <>
                Cela désinscrira également les personnes de <br />
                leurs sessions et équipes si elles en ont.
              </>
            }
            entityIds={selectedRowKeys}
            onClick={() => setSelectedRowKeys([])}
          />

          <GetPdfPlanningButton
            customFileName={() => "Sélection de participant·es"}
            elementsActions={registrationsActions}
            id={selectedRowKeys}
            noText
            onClick={() => setSelectedRowKeys([])}
            tooltip={"Exporter les plannings des participant·es sélectionné·es en une seule fois."}
          />
        </>
      )}
      expandable={{
        expandedRowRender: getExpandableRegistrationInfo,
      }}
      settingsDrawerContent={
        <FormElement>
          <ColumnsBlacklistingSelector columns={columns} />
          <CardElement title="Affichage de la liste">
            <div className="container-grid">
              <SwitchInput
                label="Afficher les personnes désinscrit⋅es"
                checked={displayUnbookedUsers}
                onChange={setDisplayUnbookedUsers}
              />
              <SwitchInput label="Mode avancé" checked={advancedMode} onChange={setAdvancedMode} />
            </div>
          </CardElement>
        </FormElement>
      }
    />
  );
}

export default ParticipantList;
