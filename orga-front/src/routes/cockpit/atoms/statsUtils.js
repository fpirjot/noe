import groupBy from "lodash/groupBy";
import {useSelector} from "react-redux";
import {registrationsSelectors} from "../../../features/registrations";
import {useMemo} from "react";
import {useTranslation} from "react-i18next";
import {getVolunteeringCoefficient} from "../../../utils/sessionsUtilities";
import {sessionsSelectors} from "../../../features/sessions";
import {activitiesSelectors} from "../../../features/activities";

/**
 * Custom React hook that provides data about registrations with days of presence.
 *
 * @returns {Object} Object containing registrations with days of presence and the average number of days of presence.
 * @property {Array<Object>} registrationsWithDaysOfPresence - Registrations with days of presence.
 * @property {number} averageNumberOfDaysOfPresence - Average number of days of presence across registrations.
 * @example
 * const { registrationsWithDaysOfPresence, averageNumberOfDaysOfPresence } = useRegistrationsDaysOfPresenceData();
 */
export const useRegistrationsDaysOfPresenceData = () => {
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

  return useMemo(() => {
    const registrationsWithDaysOfPresence = registrations.filter(
      (registration) => registration.availabilitySlots?.length > 0
    );

    const averageNumberOfDaysOfPresence =
      registrationsWithDaysOfPresence
        .map((el) => el.numberOfDaysOfPresence)
        .reduce((acc, el) => acc + el, 0) / registrationsWithDaysOfPresence.length;

    return {registrationsWithDaysOfPresence, averageNumberOfDaysOfPresence};
  }, [registrations]);
};

export const useVolunteeringData = () => {
  const sessions = useSelector(sessionsSelectors.selectList);
  const activities = useSelector(activitiesSelectors.selectList);

  return useMemo(() => {
    const volunteeringSessionsWithMetadata = sessions
      .map((el) => ({
        ...el,
        duration: el.slots.reduce((acc, slot) => acc + slot.duration, 0),
        volunteeringCoefficient: getVolunteeringCoefficient(el, activities),
      }))
      .filter((el) => el.volunteeringCoefficient > 0);

    const totalAmountOfVolunteering = volunteeringSessionsWithMetadata.reduce(
      (acc, el) => acc + el.duration * el.computedMaxNumberOfParticipants,
      0
    );
    return {volunteeringSessionsWithMetadata, totalAmountOfVolunteering};
  }, [activities, sessions]);
};

export const useRegistrationsGroupedByTypeAndRole = () => {
  const {t} = useTranslation();
  const registrations = useSelector(registrationsSelectors.selectListWithMetadata);

  return useMemo(() => {
    return Object.entries(
      groupBy(registrations, (el) =>
        el.invitationToken
          ? t("cockpit:registrations.invitations")
          : !el.everythingIsOk
          ? el.booked === false
            ? t("cockpit:registrations.unregistrations")
            : t("cockpit:registrations.incomplete")
          : t("cockpit:registrations.complete")
      )
    ).map(([name, value]) => ({
      name,
      value: value.length,
      children: Object.entries(
        groupBy(value, (el) =>
          el.role ? t("cockpit:registrations.orga") : t("cockpit:registrations.nonOrga")
        )
      ).map(([name, value]) => ({name, value: value.length, children: null})),
    }));
  }, [registrations, t]);
};
