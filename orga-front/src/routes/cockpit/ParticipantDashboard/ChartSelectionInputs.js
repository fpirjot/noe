import {Alert} from "antd";
import React from "react";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {CardElement} from "../../../common/components/CardElement";
import {projectsSelectors} from "../../../features/projects";
import {Link} from "react-router-dom";
import {SelectInput} from "../../../common/inputs/SelectInput";
import {DEFAULT_COCKPIT_PREFERENCES} from "../CockpitConfigDrawer";

const ALLOWED_FORM_FIELD_TYPES = [
  "radioGroup",
  "select",
  "checkboxGroup",
  "multiSelect",
  "checkbox",
];

const ChartSelectionInputs = () => {
  const {t} = useTranslation();

  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);

  // Get only questions with a type that can be displayed in a chart view
  const formAnswersOptions = flatFormComponents
    .filter((component) => ALLOWED_FORM_FIELD_TYPES.includes(component.type))
    .map((answer) => ({label: answer.label, value: answer.key}));

  const GraphMultiSelect = ({label, type, options}) => {
    return (
      <SelectInput
        label={label}
        mode="multiple"
        name={["charts", type]}
        placeholder="Choisissez un ou plusieurs graphique(s) à afficher"
        options={options}
      />
    );
  };

  return (
    <CardElement title={t("cockpit:dashboard.participantsDataTabLabel")}>
      <GraphMultiSelect
        label={"Graphiques utiles"}
        type="useful"
        options={DEFAULT_COCKPIT_PREFERENCES.charts.useful.map((chartName) => ({
          label: t(`cockpit:${chartName}.label`),
          value: chartName,
        }))}
      />

      {formAnswersOptions?.length > 0 ? (
        <GraphMultiSelect
          label={"Graphiques des réponses au formulaire d'inscription"}
          type="answers"
          options={formAnswersOptions}
        />
      ) : (
        <Alert
          message={
            <>
              Une fois que vous aurez créé des questions dans votre{" "}
              <Link to="../config#form">formulaire d'inscription</Link>, vous pourrez aussi choisir
              d'afficher des graphiques correspondant à vos réponses ici.
            </>
          }
        />
      )}
    </CardElement>
  );
};

export default ChartSelectionInputs;
