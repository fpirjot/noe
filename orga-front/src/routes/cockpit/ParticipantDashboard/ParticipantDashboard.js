import {Alert} from "antd";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {Pending} from "../../../common/components/Pending";
import {useLoadList} from "../../../common/hooks/useLoadList";
import {projectsSelectors} from "../../../features/projects";
import {registrationsActions, registrationsSelectors} from "../../../features/registrations";
import {useRegistrationsDaysOfPresenceData} from "../atoms/statsUtils";
import PresenceAndMealsChart from "./charts/PresenceAndMealsChart";
import TagsPerDayChart from "./charts/TagsPerDayChart";
import RegistrationsChart from "./charts/RegistrationsChart";
import FormAnswerChartContainer from "./charts/FormAnswerChart/FormAnswerChartContainer";

const usefulChartsMapping = {
  presenceAndMealsStats: PresenceAndMealsChart,
  tagsStats: TagsPerDayChart,
  registrationStats: RegistrationsChart,
};

export default function ParticipantDashboard() {
  const dispatch = useDispatch();

  // Very narrow selectors to avoid re-render on charts prefes changes
  const usefulCharts = useSelector(
    (state) => projectsSelectors.selectEditing(state).cockpitPreferences?.charts?.useful
  );
  const answersCharts = useSelector(
    (state) => projectsSelectors.selectEditing(state).cockpitPreferences?.charts?.answers
  );

  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);

  const {registrationsWithDaysOfPresence: registrationsBooked} =
    useRegistrationsDaysOfPresenceData();

  useLoadList(() => {
    dispatch(registrationsActions.loadList());
  });

  // If loading, return the spinner
  if (!registrationsLoaded) return <Pending />;

  // If no participants at all, show  alert at the top
  if (registrationsBooked.length === 0)
    return (
      <Alert
        message="Vous n'avez pas encore de personnes inscrites à votre événement."
        description="Dès qu'une personne s'inscrira, vos données de participation s'afficheront ici."
      />
    );

  return (
    <>
      <Alert
        description="Ces données prennent en compte à la fois les inscriptions complètes et incomplètes."
        style={{marginBottom: 26}}
      />

      {usefulCharts?.map((chartName) => {
        const UsefulChartComp = usefulChartsMapping[chartName];
        return <UsefulChartComp key={chartName} />;
      })}

      {answersCharts?.map((answer) => (
        <FormAnswerChartContainer
          key={answer.key}
          field={answer.key}
          displayMode={answer.displayMode}
        />
      ))}
    </>
  );
}
