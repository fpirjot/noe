import React from "react";
import {CardElement} from "../../../../common/components/CardElement";
import {Pending, PendingSuspense} from "../../../../common/components/Pending";
import {lazyWithRetry} from "../../../../utils/lazyWithRetry";
import {addKeyToItemsOfList} from "../../../../utils/tableUtilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {useRegistrationsStats} from "./utils/useRegistrationsStats";
import {useTranslation} from "react-i18next";
import {sorter} from "../../../../utils/sorters";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../../../common/components/charts/DualAxesChart")
);

const columnsRegistrationsStats = [
  {
    title: "Jour",
    dataIndex: "date",
    sorter: (a, b) => sorter.date(a.dayjsDate, b.dayjsDate),
  },
  {
    title: "Nouvelles inscriptions",
    dataIndex: "newRegistrations",
    sorter: (a, b) => sorter.number(a.newRegistrations, b.newRegistrations),
  },
  {
    title: "Inscriptions complètes",
    dataIndex: "completedRegistrations",
    sorter: (a, b) => sorter.number(a.completedRegistrations, b.completedRegistrations),
  },
  {
    title: "Nouvelles inscriptions (Total)",
    dataIndex: "newRegistrationsTotal",
    sorter: (a, b) => sorter.number(a.newRegistrations, b.newRegistrations),
  },
  {
    title: "Inscriptions complétées (Total)",
    dataIndex: "completedRegistrationsTotal",
    sorter: (a, b) => sorter.number(a.completedRegistrations, b.completedRegistrations),
  },
];

export default function RegistrationsChart() {
  const {t} = useTranslation();
  const registrationsStats = useRegistrationsStats();

  if (!registrationsStats) return <Pending />;

  // Create the stats object for the Ant design plot
  const plottedRegistrationsStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {dateShort: stat.dateShort, count: stat.newRegistrations, name: "Nouvelles inscriptions"},
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrations,
        name: "Inscriptions complètes",
      },
    ],
    []
  );

  const plottedRegistrationsTotalStats = registrationsStats.reduce(
    (acc, stat) => [
      ...acc,
      {
        dateShort: stat.dateShort,
        count: stat.newRegistrationsTotal,
        name: "Nouvelles inscriptions (Total)",
      },
      {
        dateShort: stat.dateShort,
        count: stat.completedRegistrationsTotal,
        name: "Inscriptions complètes (Total)",
      },
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:registrationStats.label")}>
        <PendingSuspense>
          <DualAxesChart
            config={{
              data: [plottedRegistrationsStats, plottedRegistrationsTotalStats],
              height: 280,
              xField: "dateShort",
              yField: ["count", "count"],
              geometryOptions: [
                {
                  geometry: "line",
                  smooth: true,
                  seriesField: "name",
                },
                {
                  geometry: "column",
                  color: ["#B0ADFF", "#B5FFF6"],
                  seriesField: "name",
                },
              ],
              slider: {start: 0, end: 1},
            }}
          />
        </PendingSuspense>
        <RawDataCollapsibleTable
          exportTitle="Suivi des inscriptions"
          columns={columnsRegistrationsStats}
          dataSource={addKeyToItemsOfList(registrationsStats)}
        />
      </CardElement>
    </>
  );
}
