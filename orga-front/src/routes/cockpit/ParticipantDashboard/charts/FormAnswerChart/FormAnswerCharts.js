import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../../../features/projects";
import {useMealsAndFreqStats, useMealsAndFreqStatsAbsolute} from "../utils/useMealsAndFreqStats";
import CommonChart from "./CommonChart";
import {useMemo} from "react";
import {
  getFormComponentsMap,
  getLabelAnswer,
  getPlottedStats,
  getUniqueKeys,
} from "../utils/formAnswersFunctions";
import {Pending} from "../../../../../common/components/Pending";
import {addKeyToItemsOfList} from "../../../../../utils/tableUtilities";
import {truncate} from "../../../../../utils/stringUtilities";
import {sorter} from "../../../../../utils/sorters";

export const AbsoluteChart = ({field, answerLabel}) => {
  const data = useMealsAndFreqStatsAbsolute(field);
  const columns = [
    {title: "Réponse", dataIndex: "name", key: "name"},
    {title: "Total", dataIndex: "count", key: "count"},
  ];

  const dataWithTruncatedLabels = data.map((v) => ({
    ...v,
    name: truncate(v.name, 40),
    fullName: v.name,
  }));

  return (
    <CommonChart
      data={dataWithTruncatedLabels}
      columns={columns}
      answerLabel={answerLabel}
      displayMode="absolute"
      dataSourceTable={data}
    />
  );
};

export const PerDayChart = ({field, answerLabel}) => {
  const {mealsAndFreqStats, daysOfPresenceSliderDates} = useMealsAndFreqStats(field) || {};
  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);
  const formComponentsMap = useMemo(
    () => getFormComponentsMap(flatFormComponents),
    [flatFormComponents]
  );
  const plottedStats = useMemo(
    () => getPlottedStats(mealsAndFreqStats, field, formComponentsMap),
    [mealsAndFreqStats, field, formComponentsMap]
  );
  const uniqueKeys = useMemo(
    () => getUniqueKeys(mealsAndFreqStats, field),
    [mealsAndFreqStats, field]
  );
  const columns = useMemo(
    () => [
      {
        title: "Day",
        dataIndex: "date",
        sorter: (a, b) => sorter.date(a.dayjsDate, b.dayjsDate),
      },
      ...uniqueKeys.map((key) => ({
        dataIndex: [field, key],
        title: getLabelAnswer(key, formComponentsMap),
      })),
    ],
    [field, formComponentsMap, uniqueKeys]
  );

  if (!mealsAndFreqStats || !daysOfPresenceSliderDates) {
    return <Pending />;
  }
  return (
    <CommonChart
      data={plottedStats}
      columns={columns}
      answerLabel={answerLabel}
      displayMode="perDay"
      sliderDates={daysOfPresenceSliderDates}
      mealsAndFreqStats={mealsAndFreqStats}
      dataSourceTable={addKeyToItemsOfList(mealsAndFreqStats)}
    />
  );
};
