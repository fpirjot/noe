import {Alert} from "antd";
import {PendingSuspense} from "../../../../../common/components/Pending";
import ColumnChartWithData from "../ColumnChartWithData";
import RawDataCollapsibleTable from "../utils/RawDataCollapsibleTable";

const CommonChart = ({
  data,
  columns,
  dataSourceTable,
  answerLabel,
  displayMode,
  ...additionalProps
}) => {
  if (!data || data.length === 0) {
    return (
      <Alert
        message="Vous n'avez pas encore de réponses à cette question."
        description="Dès qu'un·e participant·e y répondra, les données s'afficheront ici."
      />
    );
  }

  return (
    <>
      <PendingSuspense>
        <ColumnChartWithData data={data} displayMode={displayMode} {...additionalProps} />
      </PendingSuspense>
      <RawDataCollapsibleTable
        exportTitle={`Réponse formulaire : ${answerLabel}`}
        columns={columns}
        dataSource={dataSourceTable}
      />
    </>
  );
};

export default CommonChart;
