import {Tag} from "antd";
import React, {useMemo, useState} from "react";
import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {CardElement} from "../../../../../common/components/CardElement";
import {SelectInput} from "../../../../../common/inputs/SelectInput";
import {projectsActions, projectsSelectors} from "../../../../../features/projects";
import {AbsoluteChart, PerDayChart} from "./FormAnswerCharts";

const DISPLAY_MODE_OPTIONS = ["perDay", "absolute"];

const FormAnswerChartContainer = ({
  field,
  displayMode,
}: {
  field: string,
  displayMode: "perDay" | "absolute",
}) => {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);
  const answerLabel = useMemo(() => {
    const foundComponent = flatFormComponents.find(({key}) => key === field);
    return foundComponent?.label || field;
  }, [flatFormComponents, field]);

  const cockpitPreferences = useSelector(
    (s) => projectsSelectors.selectEditing(s).cockpitPreferences
  );

  const handleDisplayModeChange = (value) => {
    const updatedFormAnswers = cockpitPreferences.charts.answers.map((answer) =>
      answer.key === field ? {...answer, displayMode: value} : answer
    );

    dispatch(
      projectsActions.persist({
        cockpitPreferences: {
          ...cockpitPreferences,
          charts: {...cockpitPreferences.charts, answers: updatedFormAnswers},
        },
      })
    );
  };

  return (
    <CardElement
      icon={
        <Tag style={{background: "var(--noe-bg)", color: "white"}} bordered={false}>
          {t("cockpit:formGraph.formAnswer")}
        </Tag>
      }
      customButtons={
        <SelectInput
          defaultValue={displayMode}
          onChange={handleDisplayModeChange}
          options={DISPLAY_MODE_OPTIONS.map((displayMode) => ({
            label: t(`cockpit:formGraph.${displayMode}`),
            value: displayMode,
          }))}
        />
      }
      title={answerLabel}>
      {displayMode === "absolute" && <AbsoluteChart field={field} answerLabel={answerLabel} />}
      {displayMode === "perDay" && <PerDayChart field={field} answerLabel={answerLabel} />}
    </CardElement>
  );
};

export default FormAnswerChartContainer;
