import {removeDuplicates} from "../../../../../utils/utilities";

/**
 * Retrieves the number of participants for a given date from meal and frequency statistics.
 *
 * @param {string} dateShort - The date in short format (e.g., '2023-10-25').
 * @param {Array<Object>} mealsAndFreqStats - The meal and frequency statistics as an array of objects.
 * @returns {number} The number of participants for the given date. Returns 0 if no statistics are found for the date.
 */
export const getNumberOfParticipantsForDay = (dateShort, mealsAndFreqStats) => {
  const stat = mealsAndFreqStats.find((stat) => stat.dateShort === dateShort);
  return stat ? stat.participantsOnSite : 0;
};

/**
 * Maps flat form components to their corresponding values and labels.
 *
 * @param {Array<Object>} flatFormComponents - The flat form components as an array of objects.
 * @returns {Object} An object mapping values to their corresponding labels.
 */
export const getFormComponentsMap = (flatFormComponents) => {
  return flatFormComponents.reduce((acc, field) => {
    const values = field.values || field.data?.values;
    if (values && values.length) {
      values.forEach((value) => {
        acc[value.value] = value.label;
      });
    }
    return acc;
  }, {});
};

/**
 * Retrieves the label for a given answer option using the form components map.
 *
 * @param {string} name - The answer option's identifier.
 * @param {Object} formComponentsMap - The mapping of values to labels.
 * @returns {string} The corresponding label for the answer option.
 */
export const getLabelAnswer = (name, formComponentsMap) => {
  if (name === "true") return "Oui";
  if (name === "false") return "Non";
  return formComponentsMap[name] || name;
};

/**
 * Retrieves plotted statistics for a specific field from meal and frequency statistics.
 *
 * @param {Array<Object>} mealsAndFreqStats - The meal and frequency statistics as an array of objects.
 * @param {string} field - The key of specific field object to retrieve statistics for.
 * @param {Object} formComponentsMap - The mapping of values to labels.
 * @returns {Array<Object>} An array of objects containing plotted statistics for the specified field.
 */
export const getPlottedStats = (mealsAndFreqStats, field, formComponentsMap) => {
  return mealsAndFreqStats.reduce((acc, stat) => {
    const statsArray = Object.entries(stat[field]).map(([name, count]) => ({
      dateShort: stat.dateShort,
      count,
      name: getLabelAnswer(name, formComponentsMap),
      participantsOnSite: stat.participantsOnSite,
    }));
    return [...acc, ...statsArray];
  }, []);
};

/**
 * Retrieves unique keys for a specific field from meal and frequency statistics.
 *
 * @param {Array<Object>} mealsAndFreqStats - The meal and frequency statistics as an array of objects.
 * @param {string} field - The key of specific field object to retrieve keys for.
 * @returns {Array<string>} An array of unique keys for the specified field.
 */
export const getUniqueKeys = (mealsAndFreqStats, field) => {
  return removeDuplicates(
    mealsAndFreqStats.reduce((acc, stat) => [...acc, ...Object.keys(stat[field])], [])
  );
};
