import {dateFormatter} from "../../../../../utils/formatters";

/**
 * Initializes dates for the graph based on registrations data and specified date-related functions.
 *
 * @param {Array<Object>} registrations - Array of registered participants' data.
 * @param {Function} getStartDate - Function to extract start date from a registration object.
 * @param {Function} getEndDate - Function to extract end date from a registration object.
 * @param {Function} initializeStats - Function to initialize statistics entry based on the provided date rendering information.
 * @returns {Array<Object>} An array of date objects with formatted date information for graph display.
 */
export const initializeGraphDates = (registrations, getStartDate, getEndDate, initializeStats) => {
  let minStartDate;
  let maxEndDate;

  // Compute the minimum and maximum dates from registrations data
  registrations.forEach((r) => {
    const startDate = getStartDate(r);
    const endDate = getEndDate(r);

    if (
      (startDate && (minStartDate === undefined || startDate.isBefore(minStartDate))) ||
      minStartDate === undefined
    ) {
      minStartDate = startDate.clone();
    }
    if (
      (endDate && (maxEndDate === undefined || endDate.isAfter(maxEndDate))) ||
      maxEndDate === undefined
    ) {
      maxEndDate = endDate.clone();
    }
  });

  const arrayOfDates = [];

  // Calculate the total number of days to display on the graph
  const totalNumberOfDays = maxEndDate.startOf("day").diff(minStartDate.startOf("day"), "day") + 1;

  // Generate date objects and initialize statistics entry for each day
  for (let i = 0; i < totalNumberOfDays; i++) {
    arrayOfDates.push(minStartDate.add(i, "day"));
  }

  // Initialize data for each date object
  arrayOfDates.forEach((dateToDisplay) => {
    const dateRender = {
      dayjsDate: dateToDisplay,
      dateShort: dateToDisplay.format("D MMM YYYY"),
      date: dateFormatter.longDate(dateToDisplay, false, true),
    };
    initializeStats(dateRender);
  });

  return arrayOfDates;
};
