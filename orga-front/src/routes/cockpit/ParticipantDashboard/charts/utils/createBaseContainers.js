import {removeDuplicates} from "../../../../../utils/utilities";

/**
 * Creates base containers for specific keys based on registrations data, initializing counts for each unique value.
 *
 * @param {string} key - Specific key for which base containers need to be created.
 * @param {Array<Object>} registrationsBooked - Array of registered participants' data.
 * @returns {Object} Base containers for the specified key, initialized with counts for each unique value.
 *
 * @example
 * const key = "specificKey";
 * const baseContainers = createBaseContainers(key, registrationsBooked);
 */
export const createBaseContainers = (key, registrationsBooked) => {
  /**
   * Array of options for the specified key extracted from registrations data.
   *
   * @type {Array<*>}
   */
  const optionsArray = registrationsBooked.reduce((acc, a) => {
    if (a.formAnswers && a.formAnswers[key] !== undefined) {
      const valuesArray = Array.isArray(a.formAnswers[key])
        ? a.formAnswers[key]
        : [a.formAnswers[key]];
      return [...valuesArray, ...acc];
    }
    return acc;
  }, []);

  /**
   * Unique options with initialized counts.
   *
   * @type {Array<*>}
   */
  const options = removeDuplicates(
    optionsArray.filter(
      (option) => option !== undefined && (Array.isArray(option) ? option.length > 0 : true)
    )
  );

  /**
   * Base containers object with unique options as keys and initial counts set to 0.
   *
   * @type {Object}
   */
  const optionsWithCount = Object.fromEntries(options.map((option) => [option, 0]));
  const baseContainers = {};
  baseContainers[key] = optionsWithCount;

  return baseContainers;
};

/**
 * Creates a base tags container based on registrations data, initializing counts for each unique tag.
 *
 * @param {Array<Object>} registrationsBooked - Array of registered participants' data.
 * @returns {Object} Base tags container initialized with counts for each unique tag.
 *
 * @example
 * const baseTagsContainer = createBaseTagsContainer(registrationsBooked);
 */
export const createBaseTagsContainer = (registrationsBooked) => {
  /**
   * Unique tags extracted from registrations data.
   *
   * @type {Array<string>}
   */
  const tagsOptions = removeDuplicates(
    registrationsBooked.reduce((acc, a) => (a.tags ? [...a.tags, ...acc] : acc), [])
  );

  /**
   * Base tags container object with unique tags as keys and initial counts set to 0.
   *
   * @type {Object}
   */
  const tagsContainer = Object.fromEntries(tagsOptions.map((tag) => [tag, 0]));

  return tagsContainer;
};
