import {Collapse} from "antd";
import dayjs from "dayjs";
import React from "react";
import {TableElement} from "../../../../../common/components/TableElement";
import CsvExportButton from "../../../../../common/components/buttons/CsvExportButton";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../../../features/projects";

const {Panel} = Collapse;

const RawDataCollapsibleTable = ({dataSource, columns, exportTitle}) => {
  const project = useSelector(projectsSelectors.selectEditing);

  return (
    <Collapse style={{marginTop: 26}}>
      <Panel
        header={
          <div className="containerH" style={{justifyContent: "space-between"}}>
            <span>Données brutes</span>
            <CsvExportButton
              size="small"
              dataExportFunction={() => dataSource}
              getExportName={() =>
                `Données de participant·es [${exportTitle}] - ${project.name} - ${dayjs().format(
                  "DD-MM-YYYY HH[h]mm"
                )}.csv`
              }>
              Exporter
            </CsvExportButton>
          </div>
        }>
        <TableElement.Simple
          showHeader
          columns={columns}
          dataSource={dataSource}
          scroll={{y: 700}}
        />
      </Panel>
    </Collapse>
  );
};

export default RawDataCollapsibleTable;
