import dayjs from "dayjs";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../../../features/projects";
import {registrationsSelectors} from "../../../../../features/registrations";
import {useRegistrationsDaysOfPresenceData} from "../../../atoms/statsUtils";
import {calculateMealsAndFreqStat} from "./calculateMealsAndFreqStat";
import {createBaseContainers, createBaseTagsContainer} from "./createBaseContainers";
import {initializeGraphDates} from "./initializeGraphDates";
import {useMemo} from "react";
import {getFormComponentsMap, getLabelAnswer} from "./formAnswersFunctions";

/**
 * Initializes a statistics entry object based on the provided date rendering information and optional keys.
 *
 * @param {Object} dateRender - Date rendering information (usually containing 'dayjsDate', 'date', etc.).
 * @param {string} [key] - Optional key for specific statistics entry (e.g., specific container or category).
 * @param {Object} baseTagsContainer - Base structure for tags statistics.
 * @param {Object} baseContainers - Base structure for other statistics (e.g., participants, meals).
 * @returns {Array<Object>} An array containing the initialized statistics entry object(s).
 */
const initializeStatsEntry = (dateRender, key, baseTagsContainer, baseContainers) => {
  /**
   * Creates a new statistics entry object with the provided key (if available) and base structures.
   *
   * @param {string} [key] - Optional key for specific statistics entry.
   * @returns {Object} The initialized statistics entry object.
   * @private
   */
  const createStatsEntry = (key) => ({
    ...dateRender,
    participantsOnSite: 0,
    breakfast: 0,
    lunch: 0,
    dinner: 0,
    tags: {...baseTagsContainer},
    ...(key && {[key]: {...baseContainers[key]}}),
  });

  // If a specific key is provided, create an array with a single entry for that key;
  // otherwise, create a single entry without a key.
  return key ? [createStatsEntry(key)] : [createStatsEntry()];
};

const countAbsoluteValues = (registrationsBooked, fieldKey, formComponentsMap) => {
  const answersForField = registrationsBooked
    .map((registration) => registration.formAnswers[fieldKey])
    .flat()
    .filter((field) => !!field);

  const countValues = answersForField.reduce((acc, box) => {
    acc[box] = (acc[box] || 0) + 1;
    return acc;
  }, {});

  return Object.keys(countValues).map((fieldKey) => ({
    key: fieldKey,
    name: getLabelAnswer(fieldKey, formComponentsMap),
    count: countValues[fieldKey],
  }));
};

export const useMealsAndFreqStatsAbsolute = (fieldKey) => {
  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);
  const {registrationsWithDaysOfPresence: registrationsBooked} =
    useRegistrationsDaysOfPresenceData();

  const flatFormComponents = useSelector(projectsSelectors.selectFlatFormComponents);

  const formComponentsMap = useMemo(
    () => getFormComponentsMap(flatFormComponents),
    [flatFormComponents]
  );

  if (!registrationsLoaded || registrationsBooked.length === 0)
    return {mealsAndFreqStats: undefined, daysOfPresenceSliderDates: undefined};

  return countAbsoluteValues(registrationsBooked, fieldKey, formComponentsMap);
};

/**
 * Custom React hook for calculating meals and frequency statistics based on registration data.
 *
 * @param {string} [fieldKey] - Optional key for specific statistics entry (e.g., specific container or category).
 * @returns {Object} Object containing calculated meals and frequency statistics and slider dates.
 * @property {Array<Object>} mealsAndFreqStats - Array of objects representing meals and frequency statistics for each day.
 * @property {Object} daysOfPresenceSliderDates - Object containing start and end positions for slider based on project dates.
 */
export const useMealsAndFreqStats = (fieldKey) => {
  const registrationsLoaded = useSelector(registrationsSelectors.selectIsLoaded);
  const project = useSelector(projectsSelectors.selectEditing);
  const {registrationsWithDaysOfPresence: registrationsBooked} =
    useRegistrationsDaysOfPresenceData();

  const {mealsAndFreqStats, daysOfPresenceSliderDates} = useMemo(() => {
    if (!registrationsLoaded || registrationsBooked.length === 0)
      return {mealsAndFreqStats: undefined, daysOfPresenceSliderDates: undefined};

    const mealsAndFreqStats = [];

    // Create base containers for tags and specified key.
    const baseTagsContainer = createBaseTagsContainer(registrationsBooked);
    const baseContainers = createBaseContainers(fieldKey, registrationsBooked);

    // If a key is specified and no base containers are created, return undefined for stats and slider dates.
    if (fieldKey && !baseContainers)
      return {mealsAndFreqStats: undefined, daysOfPresenceSliderDates: undefined};

    // Initialize graph dates and calculate stats.
    initializeGraphDates(
      registrationsBooked,
      // Get the minimum start date from days of presence.
      (registration) =>
        registration.daysOfPresence.reduce((accumulator, day) => {
          const start = dayjs(day.start);
          return accumulator ? dayjs.min(start, accumulator) : start;
        }, undefined),
      // Get the maximum end date from days of presence.
      (registration) =>
        registration.daysOfPresence.reduce((accumulator, day) => {
          const end = dayjs(day.end);
          return accumulator ? dayjs.max(end, accumulator) : end;
        }, undefined),
      // Initialize stats entry for each date render.
      (dateRender) => {
        const statsEntries = initializeStatsEntry(
          dateRender,
          fieldKey,
          baseTagsContainer,
          baseContainers
        );
        mealsAndFreqStats.push(...statsEntries);
      }
    );

    // Calculate meals and frequency statistics.
    calculateMealsAndFreqStat(registrationsBooked, fieldKey, mealsAndFreqStats);

    // Calculate slider start and end positions based on project dates.
    const daysOfPresenceSliderDates = {
      start: Math.max(
        0,
        (mealsAndFreqStats.findIndex((stat) => stat.dayjsDate.isSame(dayjs(project.start), "day")) -
          10) /
          mealsAndFreqStats.length
      ),
      end: Math.min(
        1,
        (mealsAndFreqStats.findIndex((stat) => stat.dayjsDate.isSame(dayjs(project.end), "day")) +
          10) /
          mealsAndFreqStats.length
      ),
    };

    return {mealsAndFreqStats, daysOfPresenceSliderDates};
  }, [registrationsLoaded, registrationsBooked, fieldKey, project]);

  return {mealsAndFreqStats, daysOfPresenceSliderDates};
};
