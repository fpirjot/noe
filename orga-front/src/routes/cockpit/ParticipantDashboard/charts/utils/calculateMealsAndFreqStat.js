import dayjs from "dayjs";
import {meals} from "./meals";

/**
 * Updates the count of specific keys for a particular registration in the meals and frequency statistics.
 *
 * @param {string} key - The specific key to be updated.
 * @param {Object} registration - The registration object containing specific keys.
 * @param {Object} mealsAndFreqStatForDay - The statistics object for the specific day.
 */
const updateFormAnswerKeysCount = (key, registration, mealsAndFreqStatForDay) => {
  const formAnswer = registration.formAnswers?.[key];

  if (formAnswer) {
    if (Array.isArray(formAnswer)) {
      formAnswer.forEach((k) => {
        mealsAndFreqStatForDay[key][k] += 1;
      });
    } else {
      mealsAndFreqStatForDay[key][formAnswer] += 1;
    }
  }
};

/**
 * Calculates meals and frequency statistics for a specific day based on a registration object.
 *
 * @param {Object} registration - The registration object containing meals, tags, and specific keys information.
 * @param {string} key - The specific key (if any) to consider while calculating statistics.
 * @param {Array<Object>} mealsAndFreqStats - The array containing meals and frequency statistics objects.
 */
const calculateMealsAndFreqStatForDay = (registration, key, mealsAndFreqStats) => {
  // Iterate through each day of presence in the registration object
  registration.daysOfPresence.forEach((dayOfPresence) => {
    // Extract the current day from the day of presence information
    const currentDay = dayjs(dayOfPresence.start);

    // Find the corresponding statistics object for the current day
    const mealsAndFreqStatForDay = mealsAndFreqStats.find((st) =>
      st.dayjsDate.isSame(currentDay, "day")
    );

    // Increment participants on site count
    mealsAndFreqStatForDay.participantsOnSite += 1;

    // Increment meal counts if the participant had those meals on the current day
    meals.forEach((meal) => {
      if (dayOfPresence[meal.field]) {
        mealsAndFreqStatForDay[meal.field] += 1;
      }
    });

    // Increment tag counts for the current day
    registration.tags?.forEach((tag) => {
      mealsAndFreqStatForDay.tags[tag] += 1;
    });

    // Update specific key counts (if provided)
    if (key) {
      updateFormAnswerKeysCount(key, registration, mealsAndFreqStatForDay);
    }
  });
};

/**
 * Calculates meals and frequency statistics for all registrations based on provided parameters.
 *
 * @param {Array<Object>} registrationsBooked - Array of booked registrations.
 * @param {string} key - The specific key (if any) to consider while calculating statistics.
 * @param {Array<Object>} mealsAndFreqStats - The array containing meals and frequency statistics objects.
 */
export const calculateMealsAndFreqStat = (registrationsBooked, key, mealsAndFreqStats) => {
  registrationsBooked.forEach((registration) => {
    calculateMealsAndFreqStatForDay(registration, key, mealsAndFreqStats);
  });
};
