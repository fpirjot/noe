import React from "react";
import {Pending, PendingSuspense} from "../../../../common/components/Pending";
import {lazyWithRetry} from "../../../../utils/lazyWithRetry";
import {addKeyToItemsOfList} from "../../../../utils/tableUtilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {CardElement} from "../../../../common/components/CardElement";
import {useMealsAndFreqStats} from "./utils/useMealsAndFreqStats";
import {useTranslation} from "react-i18next";
import {sorter} from "../../../../utils/sorters";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../../../common/components/charts/DualAxesChart")
);

const columnsDOPMealStats = [
  {
    title: "Jour",
    dataIndex: "date",
    sorter: (a, b) => sorter.date(a.dayjsDate, b.dayjsDate),
  },
  {
    title: "Participant⋅es sur place",
    dataIndex: "participantsOnSite",
    sorter: (a, b) => sorter.number(a.participantsOnSite, b.participantsOnSite),
  },
  {
    title: "Participant⋅es mangeant le matin",
    dataIndex: "breakfast",
    sorter: (a, b) => sorter.number(a.breakfast, b.breakfast),
  },
  {
    title: "Participant⋅es mangeant le midi",
    dataIndex: "lunch",
    sorter: (a, b) => sorter.number(a.lunch, b.lunch),
  },
  {
    title: "Participant⋅es mangeant le soir",
    dataIndex: "dinner",
    sorter: (a, b) => sorter.number(a.dinner, b.dinner),
  },
];

export default function PresenceAndMealsChart() {
  const {t} = useTranslation();
  const {mealsAndFreqStats, daysOfPresenceSliderDates} = useMealsAndFreqStats();

  if (!mealsAndFreqStats || !daysOfPresenceSliderDates) return <Pending />;

  // Create the stats objects for the Ant design plot
  const plottedFreqStats = mealsAndFreqStats.map((stat) => ({
    dateShort: stat.dateShort,
    participantsOnSite: stat.participantsOnSite,
  }));

  // Create the stats objects for the Ant design plot
  const plottedMealsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      {dateShort: stat.dateShort, count: stat.breakfast, name: "Petits-déjeuners à prévoir"},
      {
        dateShort: stat.dateShort,
        count: stat.lunch,
        name: "Déjeuners à prévoir",
      },
      {dateShort: stat.dateShort, count: stat.dinner, name: "Dîners à prévoir"},
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:presenceAndMealsStats.label")}>
        <PendingSuspense>
          <DualAxesChart
            config={{
              data: [plottedFreqStats, plottedMealsStats],
              height: 280,
              xField: "dateShort",
              yField: ["participantsOnSite", "count"],
              geometryOptions: [
                {geometry: "column"},
                {geometry: "line", smooth: true, seriesField: "name"},
              ],
              meta: {
                // Use sync to synchronize both axis
                participantsOnSite: {alias: "Participant⋅es sur place", sync: "count"},
                count: {sync: true},
              },
              // Don't display the second axis, as they are sync
              yAxis: {count: false, min: 10},
              slider: daysOfPresenceSliderDates,
            }}
          />
        </PendingSuspense>
        <RawDataCollapsibleTable
          exportTitle="Jours de présence et repas"
          columns={columnsDOPMealStats}
          dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
        />
      </CardElement>
    </>
  );
}
