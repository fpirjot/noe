import React from "react";
import {CardElement} from "../../../../common/components/CardElement";
import {Pending, PendingSuspense} from "../../../../common/components/Pending";
import {lazyWithRetry} from "../../../../utils/lazyWithRetry";
import {addKeyToItemsOfList} from "../../../../utils/tableUtilities";
import {removeDuplicates} from "../../../../utils/utilities";
import RawDataCollapsibleTable from "./utils/RawDataCollapsibleTable";
import {useMealsAndFreqStats} from "./utils/useMealsAndFreqStats";
import {useTranslation} from "react-i18next";
import {sorter} from "../../../../utils/sorters";

const DualAxesChart = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "../../../../common/components/charts/DualAxesChart")
);

export default function TagsPerDayChart() {
  const {t} = useTranslation();
  const {mealsAndFreqStats, daysOfPresenceSliderDates} = useMealsAndFreqStats();

  if (!mealsAndFreqStats || !daysOfPresenceSliderDates) return <Pending />;

  const columnsTagsStats = [
    {
      title: "Jour",
      dataIndex: "date",
      sorter: (a, b) => sorter.date(a.dayjsDate, b.dayjsDate),
    },
    ...removeDuplicates(
      mealsAndFreqStats.reduce((acc, stat) => [...acc, ...Object.keys(stat.tags)], [])
    ).map((tag) => ({dataIndex: ["tags", tag], title: tag})),
  ];

  // Create the stats objects for the Ant design plot
  const plottedFreqStats = mealsAndFreqStats.map((stat) => ({
    dateShort: stat.dateShort,
    participantsOnSite: stat.participantsOnSite,
  }));

  // Create the stats objects for the Ant design plot
  const plottedTagsStats = mealsAndFreqStats.reduce(
    (acc, stat) => [
      ...acc,
      ...Object.entries(stat.tags).map(([tagName, count]) => ({
        dateShort: stat.dateShort,
        count,
        name: tagName,
      })),
    ],
    []
  );

  return (
    <>
      <CardElement title={t("cockpit:tagsStats.label")}>
        <PendingSuspense>
          <DualAxesChart
            config={{
              data: [plottedFreqStats, plottedTagsStats],
              height: 280,
              xField: "dateShort",
              yField: ["participantsOnSite", "count"],
              yAxis: [{min: 0}, {min: 0}],
              geometryOptions: [
                {geometry: "column"},
                {geometry: "line", smooth: true, seriesField: "name"},
              ],
              meta: {
                // Use sync to synchronize both axis
                participantsOnSite: {alias: "Participant⋅es sur place"},
              },
              slider: daysOfPresenceSliderDates,
            }}
          />
        </PendingSuspense>
        <RawDataCollapsibleTable
          exportTitle="Comptabilisation des tags par jour"
          columns={columnsTagsStats}
          dataSource={addKeyToItemsOfList(mealsAndFreqStats)}
        />
      </CardElement>
    </>
  );
}
