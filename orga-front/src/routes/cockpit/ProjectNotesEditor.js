import {useTranslation} from "react-i18next";
import {useDispatch, useSelector} from "react-redux";
import {App, Button, Form, Result} from "antd";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {CardElement} from "../../common/components/CardElement";
import {FormElement} from "../../common/inputs/FormElement";
import {RichTextInput} from "../../common/inputs/RichTextInput";
import React, {useEffect, useState} from "react";
import {viewSelectors} from "../../features/view";
import {EditPage} from "../../common/pages/EditPage";
import {EditOutlined} from "@ant-design/icons";
import TextDisplayer from "../../common/components/TextDisplayer";
import {ReactComponent as SvgAddNotes} from "../../app/images/undraw/undraw_add_notes.svg";
import {SvgImageContainer} from "../../common/components/SvgImageContainer";
import {ButtonProps} from "antd";

export const ProjectNotesEditor = () => {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const isModified = useSelector(viewSelectors.selectIsModified);
  const [isInEditMode, setIsInEditMode] = useState(isModified);
  const notes = useSelector((state) => projectsSelectors.selectEditing(state)?.notes);
  const setIsModified = EditPage.useSetIsModified();

  useEffect(() => {
    if (isInEditMode) {
      return () => {
        dispatch(projectsActions.changeEditing({notes: form.getFieldValue("notes")}));
        message.warning(t("cockpit:schema.notes.notSavedWarning"));
      };
    }
  }, [isInEditMode]);

  const saveNotes = async ({notes}) => {
    await dispatch(projectsActions.persist({notes}));
    setIsModified(false);
    setIsInEditMode(false);
  };

  const EditButton = (props: ButtonProps) => (
    <Button
      type={"link"}
      icon={<EditOutlined />}
      onClick={() => {
        setIsInEditMode(true);
        setTimeout(() => document.querySelector(".ql-editor").focus(), 50); // Then select the input automatically
      }}
      {...props}
    />
  );

  const SaveButton = () => {
    return isModified ? (
      <Button
        type={"primary"}
        htmlType={"submit"}
        className={"bounce-in"}
        onClick={() => form.submit()}>
        {t("common:editPage.buttonTitle.edit")}
      </Button>
    ) : !isInEditMode ? (
      <EditButton className={"reveal-on-hover-item"} title={t("cockpit:schema.notes.editButton")} />
    ) : null;
  };

  return (
    <CardElement
      title={t("cockpit:notes.label")}
      readOnly
      customButtons={<SaveButton />}
      className={"reveal-on-hover-container"}>
      {isInEditMode ? (
        <FormElement
          form={form}
          initialValues={{notes}}
          onFinish={saveNotes}
          onValuesChange={() => setIsModified(true)}>
          <RichTextInput
            name={"notes"}
            placeholder={t("cockpit:schema.notes.placeholder")}
            noFadeIn
          />
        </FormElement>
      ) : notes ? (
        <TextDisplayer value={notes} />
      ) : (
        <Result
          className={"fade-in"}
          icon={<SvgImageContainer svg={SvgAddNotes} width={"70%"} />}
          title={t("cockpit:schema.notes.fullPlaceholderTitle")}
          subTitle={t("cockpit:schema.notes.fullPlaceholderSubtitle")}
          extra={<EditButton>{t("cockpit:schema.notes.editButton")}</EditButton>}
        />
      )}
    </CardElement>
  );
};
