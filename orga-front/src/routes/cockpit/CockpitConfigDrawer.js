import {SettingOutlined} from "@ant-design/icons";
import {Button} from "antd";
import React, {useState} from "react";

import {Form} from "antd";
import {useDispatch, useSelector} from "react-redux";
import {Drawer} from "../../common/components/Drawer";
import {useWindowDimensions} from "../../common/hooks/useWindowDimensions";
import {FormElement} from "../../common/inputs/FormElement";
import {projectsActions, projectsSelectors} from "../../features/projects";
import ChartSelectionInputs from "./ParticipantDashboard/ChartSelectionInputs";
import FiguresSelect from "./ParticipantDashboard/FiguresSelect";

// Same as in backend project.model.ts
export const DEFAULT_COCKPIT_PREFERENCES = {
  figures: ["notes", "registrations", "volunteering", "schedulingAndSupervision"],
  charts: {
    useful: ["presenceAndMealsStats", "tagsStats", "registrationStats"],
    answers: [],
  },
};

/**
 * CockpitConfigDrawer component manages the configuration drawer for the chart view.
 * It allows users to select specific charts to be displayed.
 *
 * @returns {ReactNode} The configuration drawer component for the chart view.
 */
const CockpitConfigDrawer = () => {
  const dispatch = useDispatch();
  const {isMobileView} = useWindowDimensions();
  const [cockpitPreferencesForm] = Form.useForm();
  const cockpitPreferences = useSelector(
    (s) => projectsSelectors.selectEditing(s).cockpitPreferences
  );
  const [settingsDrawerOpen, setSettingsDrawerOpen] = useState(false);

  /**
   * Handles saving the selected configuration and updates the Redux store.
   */
  const saveCockpitConfig = () => {
    const fieldsValues = cockpitPreferencesForm.getFieldsValue();

    // GO find the existing answer config if it already set, otherwise initialize it with the key
    const savedAnswers = fieldsValues?.charts?.answers.map(
      (answerKey) =>
        cockpitPreferences.charts?.answers.find((a) => a.key === answerKey) || {
          key: answerKey,
          displayMode: "perDay",
        }
    );

    dispatch(
      projectsActions.persist({
        cockpitPreferences: {
          ...fieldsValues,
          charts: {...fieldsValues.charts, answers: savedAnswers},
        },
      })
    );

    setSettingsDrawerOpen(false);
  };

  /**
   * Reset cockpit
   */
  const resetCockpitConfig = () => {
    dispatch(projectsActions.persist({cockpitPreferences: undefined}));
    setSettingsDrawerOpen(false);
  };

  return (
    <>
      <Button icon={<SettingOutlined />} onClick={() => setSettingsDrawerOpen(true)}>
        {!isMobileView && "Personnaliser"}
      </Button>

      <Drawer
        title="Personnaliser le cockpit"
        width={isMobileView ? undefined : "700px"}
        open={settingsDrawerOpen}
        setOpen={setSettingsDrawerOpen}
        extra={
          <Button onClick={saveCockpitConfig} type="primary">
            Enregistrer
          </Button>
        }>
        <div style={{padding: 16}}>
          <p style={{color: "gray", marginBottom: 26}}>
            Sélectionnez les élements que vous souhaitez afficher en cliquant sur les tags
            correspondants. Cliquez sur 'Enregistrer' pour appliquer les filtres sélectionnés et
            afficher les élements correspondants.
          </p>
          <FormElement
            form={cockpitPreferencesForm}
            onValidate={saveCockpitConfig}
            initialValues={cockpitPreferences}>
            <FiguresSelect />
            <ChartSelectionInputs />
            <Button onClick={resetCockpitConfig}>Réinitialiser le cockpit</Button>
          </FormElement>
        </div>
      </Drawer>
    </>
  );
};

export default CockpitConfigDrawer;
