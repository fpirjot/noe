import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {EditPage} from "../../common/pages/EditPage";
import {AvailabilitySlotsTable} from "../../common/components/AvailabilitySlotsTable";
import {Alert} from "antd";
import {CardElement} from "../../common/components/CardElement";
import {AdditionalFeatures} from "../config/advanced/FeaturesFormContent";
import {TextInput, TextInputEmail} from "../../common/inputs/TextInput";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {getSlotsStartAndEnd} from "../../utils/slotsUtilities";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {useNavigate} from "react-router-dom";

function ProjectNew() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const setIsModified = EditPage.useSetIsModified({resetOnUnmount: true});

  useLoadEditing(projectsActions, "new");

  const toggle = (key) => {
    setIsModified(true);
    dispatch(projectsActions.changeEditing({[key]: !project[key]}));
  };

  const changeAvailabilitySlots = (newSlots) => {
    dispatch(
      projectsActions.changeEditing({availabilitySlots: newSlots, ...getSlotsStartAndEnd(newSlots)})
    );
    setIsModified(true);
  };

  return (
    <EditPage
      i18nNs="projects"
      record={project}
      initialValues={project}
      elementsActions={projectsActions}
      goBackAfterAction={false}
      onCreate={(projectSlug) => navigate(`/${projectSlug}/config`)}
      createAndStayButton={false}>
      <Alert
        type="success"
        style={{marginBottom: 26}}
        message="Bienvenue sur NOÉ !"
        description="Cette page vous permet de créer votre événement en renseignant les informations de base."
      />
      <CardElement>
        <div className="container-grid three-per-row">
          <TextInput label="Nom de l'événement" name="name" placeholder="nom" required />

          <TextInputEmail
            label="Email de contact"
            name="contactEmail"
            placeholder="email"
            tooltip={"C'est l'email auquel pourront écrire vos participant·es en cas de problème."}
            required
          />

          <SwitchInput
            label="Rendre l'événement public"
            tooltip="L'événement sera visible dans la liste de tous les événements publics de la plateforme, côté participant⋅e."
            name="isPublic"
          />
        </div>
      </CardElement>
      <div className="container-grid">
        <AvailabilitySlotsTable
          title="Plages d'ouverture de l'événement"
          onChange={changeAvailabilitySlots}
          subtitle={
            "Ce sont les heures d'ouverture de votre événement. Vous pourrez revenir les modifier dans la page Configuration de votre événement."
          }
          availabilitySlots={project.availabilitySlots}
        />
      </div>

      <AdditionalFeatures
        subtitle={
          <div style={{marginTop: -14, marginBottom: 26}}>
            Vous pouvez configurer NOÉ selon vos besoins, en activant ou en désactivant certaines
            fonctionnalités de la plateforme. Retrouvez ces contrôles dans les onglets "Avancé" >
            "Fonctionnalités" de la page Configuration.
          </div>
        }
        project={project}
        toggle={toggle}
        noWarning
      />
    </EditPage>
  );
}

export default ProjectNew;
