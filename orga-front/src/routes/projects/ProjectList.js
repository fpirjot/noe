import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects.js";
import {ListPage} from "../../common/pages/ListPage";
import {currentProjectActions, currentProjectSelectors} from "../../features/currentProject";
import {
  CalendarOutlined,
  PlaySquareOutlined,
  SaveOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {Button, Tooltip} from "antd";
import {usersSelectors} from "../../features/users.js";
import {registrationsActions} from "../../features/registrations";
import {currentUserActions} from "../../features/currentUser";
import {URLS} from "../../app/configuration";
import {useTranslation} from "react-i18next";
import {SupportNOEModal} from "../../common/components/SupportNOEModal";
import {BookmarkedProjectButton} from "../../common/components/buttons/BookmarkedProjectButton";
import {RoleTag} from "../../common/components/RoleTag";
import {useNavigate} from "react-router-dom";
import {dateFormatter} from "../../utils/formatters";
import {sorter} from "../../utils/sorters";

export default function ProjectList({userCanCreateProject}) {
  const navigate = useNavigate();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const projects = useSelector(projectsSelectors.selectList);
  const alreadyLoadedProject = useSelector(currentProjectSelectors.selectProject);
  const currentUser = useSelector(usersSelectors.selectEditing);
  const [supportModalOpen, setSupportModalOpen] = useState();

  useEffect(() => {
    dispatch(projectsActions.loadList());
  }, []);

  const projectsWithRegistrationRoles = projects.map((project) => ({
    ...project,
    registrationRole: currentUser.registrations.find((r) => r.project === project._id)?.role,
  }));

  const columns = [
    {
      title: "Nom",
      dataIndex: "name",
      sorter: (a, b) => sorter.text(a.name, b.name),
      render: (text, record) => (
        <>
          {text}
          {alreadyLoadedProject._id === record._id && (
            <Tooltip
              title={
                "Cette icône indique que certaines données de cet événement sont déjà chargées dans votre navigateur. " +
                "Si vous revenez sur cet événement, vous les retrouverez telles quelles sans avoir à attendre à nouveau qu'elles chargent. " +
                "Si vous décidez d'accéder à un autre événement, les données seront remplacées par celles du nouvel événement."
              }>
              <SaveOutlined style={{marginLeft: 8}} />
            </Tooltip>
          )}
        </>
      ),
      searchable: true,
    },
    {
      title: "Accès interfaces",
      key: "access",
      render: (text, record) => (
        <>
          <Button
            icon={<SettingOutlined />}
            type="link"
            style={{marginRight: 8}}
            onClick={() => cleanStateAndNavigate(record.slug || record._id)}>
            Orga
          </Button>
          <Button
            type="link"
            href={`${URLS.INSCRIPTION_FRONT}/${record._id}`}
            icon={<PlaySquareOutlined />}>
            Participant⋅e
          </Button>
        </>
      ),
    },
    {
      title: "Début – Fin",
      dataIndex: "start",
      sorter: (a, b) => sorter.date(a.start, b.start),
      render: (text, record) => dateFormatter.longDateRange(record.start, record.end, true, true),
      searchable: true,
      searchText: (record) => dateFormatter.longDateRange(record.start, record.end, true, true),
    },
    {
      title: "Rôle",
      dataIndex: "role",
      render: (text, record) =>
        record.registrationRole ? (
          <RoleTag roleValue={record.registrationRole} />
        ) : (
          <Button onClick={() => onRequireAdmin(record)}>Devenir administrateur⋅ice</Button>
        ),
      sorter: (a, b) => sorter.text(a.registrationRole, b.registrationRole),
    },
    {
      title: "Stats",
      dataIndex: "stats",
      render: (_, {stats}) =>
        stats && (
          <div
            title={`${stats.participants} participant·es + ${stats.orgas} orgas, et ${stats.sessions} sessions`}>
            {stats.participants} px.{" "}
            <span style={{color: "grey"}}>
              | {stats.orgas} o. | {stats.sessions} s.
            </span>
          </div>
        ),
      ellipsis: true,
      sorter: (a, b) => sorter.number(a.stats?.participants, b.stats?.participants),
      width: 180,
    },
    {
      dataIndex: "starred",
      width: 60,
      render: (text, record) => <BookmarkedProjectButton projectId={record._id} />,
    },
  ];

  const onRequireAdmin = async (project) => {
    try {
      const existingRegistration = currentUser.registrations.find(
        (registration) => registration.project === project._id
      );
      await dispatch(
        registrationsActions.persist({
          _id: existingRegistration?._id || "new",
          user: currentUser._id,
          project: project._id,
          role: "admin",
        })
      );
      dispatch(currentUserActions.refreshAuthTokens()); // Reload the user and all its registrations
    } catch {
      /* nothing */
    }
  };

  const cleanStateAndNavigate = async (projectId) => {
    const shouldReload =
      alreadyLoadedProject._id &&
      alreadyLoadedProject._id !== projectId &&
      alreadyLoadedProject.slug !== projectId;

    // If there is already a loaded project, and that it's not the same as the requested project, clean everything. Otherwise, keep the data
    if (shouldReload) await dispatch(currentProjectActions.cleanProject());

    if (projectId === "new") {
      navigate("/new");
    } else {
      const projectToNavigateTo = projects.find(
        (project) => project.slug === projectId || project._id === projectId
      );

      // Then navigate only after cleaning to the cockpit
      navigate(`/${projectToNavigateTo.slug || projectToNavigateTo._id}/cockpit`);
    }
  };

  // If a project has been bookmarked, then load the page directly. Only redirect if there is no "no-redir" in the URL
  useEffect(() => {
    projects?.length > 0 &&
      currentUser?.bookmarkedProject &&
      new URLSearchParams(window.location.search).get("no-redir") === null &&
      cleanStateAndNavigate(currentUser.bookmarkedProject);
  }, [projects, currentUser.bookmarkedProject]);

  return (
    <>
      <ListPage
        icon={<CalendarOutlined />}
        i18nNs="projects"
        title={t("projects:labelMyProjects")}
        elementsActions={projectsActions}
        searchInFields={["name"]}
        onNavigate={(projectId) =>
          projectId === "new" && !userCanCreateProject
            ? setSupportModalOpen(true)
            : cleanStateAndNavigate(projectId)
        }
        customButtons={
          <Button type={"link"} onClick={() => setSupportModalOpen(true)}>
            {t("common:supportNOE.supportTheProject")}
          </Button>
        }
        noActionIcons
        columns={columns}
        dataSource={projectsWithRegistrationRoles}
      />
      <SupportNOEModal
        open={supportModalOpen}
        setOpen={setSupportModalOpen}
        userCanCreateProject={userCanCreateProject}
      />
    </>
  );
}
