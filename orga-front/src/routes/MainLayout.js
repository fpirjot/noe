import React from "react";
import {useSelector} from "react-redux";
import {CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserSelectors} from "../features/currentUser.js";
import {LayoutStructure} from "../common/layout/LayoutStructure";
import {useRedirectToProjectIfOffline} from "../utils/offlineModeUtilities";
import {instanceName, URLS} from "../app/configuration";
import {useTranslation} from "react-i18next";
import {MenuLayout} from "../common/components/Menu";
import {useBrowserTabTitle} from "../common/hooks/useBrowserTabTitle";
import {Outlet} from "react-router";
import {setAppTheme} from "../common/layout/DynamicProjectThemeProvider";

export default function MainLayout({page}) {
  const {t} = useTranslation();

  const currentUser = useSelector(currentUserSelectors.selectUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(instanceName, page, {projects: "Mes événements"}, "ORGA");

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  setAppTheme(undefined);

  // ****** SIDE MENU ******

  const menu = {
    top: (
      <MenuLayout
        rootNavUrl={"/"}
        selectedItem={page}
        items={[
          // User events
          {
            label: t("projects:labelMyProjects"),
            key: "projects",
            icon: <CalendarOutlined />,
          },
        ]}
      />
    ),
    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Inscription front
          {
            label: t("common:pagesNavigation.inscriptionFront"),
            url: `${URLS.INSCRIPTION_FRONT}/projects?no-redir`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure
      title={instanceName}
      ribbon={t("common:orgaRibbon")}
      menu={menu}
      profileUser={currentUser}>
      <Outlet />
    </LayoutStructure>
  );
}
