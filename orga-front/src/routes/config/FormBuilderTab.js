import {useSelector} from "react-redux";
import {FormBuilder} from "../../common/components/FormBuilder/FormBuilder";
import {FormBuilderLivePreview} from "../../common/components/FormBuilder/FormBuilderLivePreview";
import React from "react";
import {projectsSelectors} from "../../features/projects";
import {EditPage} from "../../common/pages/EditPage";
import {FormElement} from "../../common/inputs/FormElement";

export const FormBuilderTab = ({formComponentsForm}) => {
  const formComponents = useSelector(
    (state) => projectsSelectors.selectEditing(state)?.formComponents
  );
  const setIsModified = EditPage.useSetIsModified();

  return (
    <div className="container-grid two-per-row" style={{rowGap: 40}}>
      <FormElement
        requiredMark
        form={formComponentsForm}
        initialValues={{formComponents}}
        onValuesChange={() => setIsModified(true)}>
        <FormBuilder name={"formComponents"} />
      </FormElement>

      <FormBuilderLivePreview formComponentsForm={formComponentsForm} />
    </div>
  );
};
