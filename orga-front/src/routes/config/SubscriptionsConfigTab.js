import {CardElement} from "../../common/components/CardElement";
import {FormElement} from "../../common/inputs/FormElement";
import {flattenFormComponents} from "../../utils/registrationsUtilities";
import React from "react";
import dayjs from "dayjs";
import {useDispatch, useSelector} from "react-redux";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useTranslation} from "react-i18next";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {TextAreaInput} from "../../common/inputs/TextAreaInput";
import {SliderInput} from "../../common/inputs/SliderInput";
import {Link} from "react-router-dom";
import {clickOnTab} from "../../utils/userTours/userTourUtilities";
import {dateFormatter, timeFormatter} from "../../utils/formatters";
import {EditPage} from "../../common/pages/EditPage";

export const SubscriptionsConfigTab = () => {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);

  const smsMessageTags = {
    "{{sessionName}}": t("sessions:schema.name.label"),
    "{{startDate}} / {{endDate}}": dateFormatter.longDate(dayjs()),
    "{{startDateTime}} / {{endDateTime}}": dateFormatter.longDateTime(dayjs()),
    "{{startTime}} / {{endTime}}": timeFormatter.time(dayjs()),
  };

  const changeEditing = (changedFields, values) => {
    dispatch(projectsActions.changeEditing(values));
    setIsModified(true);
  };

  return (
    <FormElement onValuesChange={changeEditing} initialValues={project}>
      <div className={"container-grid two-per-row"}>
        <div>
          <CardElement title={t("projects:registrations.subscriptionsManagement")}>
            <div className="container-grid">
              <SwitchInput i18nNs={"projects"} name="notAllowOverlap" />

              <SwitchInput i18nNs={"projects"} name="blockSubscriptions" />

              <SwitchInput
                i18nNs={"projects"}
                disabled={project.blockSubscriptions}
                name="blockVolunteeringUnsubscribeIfBeginsSoon"
              />

              <SliderInput
                i18nNs={"projects"}
                name="minMaxVolunteering"
                range
                inputProps={{
                  tooltip: {formatter: (val) => timeFormatter.duration(val, true)},
                }}
                step={15}
                max={720}
                marks={{
                  0: "0h",
                  60: "1h",
                  120: "2h",
                  180: "3h",
                  240: "4h",
                  300: "5h",
                  360: "6h",
                  420: "7h",
                  480: "8h",
                  540: "9h",
                  600: "10h",
                  660: "11h",
                  720: "12h",
                }}
              />
            </div>
          </CardElement>
        </div>

        <div>
          <CardElement
            title={t("projects:registrations.bulkSmsMessages")}
            subtitle={t("projects:schema.smsMessageTemplate.description")}
            className={"userTourSmsMessageTemplate"}>
            {flattenFormComponents(project.formComponents).find(
              (formComp) => formComp.type === "phoneNumber"
            ) ? (
              <TextAreaInput
                label={t("projects:schema.smsMessageTemplate.label")}
                formItemProps={{style: {marginTop: 26, marginBottom: 0}}}
                placeholder={t("projects:schema.smsMessageTemplate.defaultValue")}
                tooltip={
                  <>
                    {t("projects:schema.smsMessageTemplate.tooltip")}
                    <ul>
                      {Object.entries(smsMessageTags).map(([key, val]) => (
                        <li key={key}>
                          <strong>{key} :</strong> {val}
                        </li>
                      ))}
                    </ul>
                  </>
                }
                name={"smsMessageTemplate"}
              />
            ) : (
              <>
                <p style={{color: "grey", marginTop: 16}}>
                  {t("projects:schema.smsMessageTemplate.addPhoneNumberFormComp")}
                </p>
                <Link to={"#form"} onClick={() => clickOnTab("form")}>
                  {t("projects:schema.smsMessageTemplate.goToForm")}
                </Link>
              </>
            )}
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
};
