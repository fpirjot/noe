import React, {useMemo} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, App, Space, Steps} from "antd";
import {FormElement} from "../../common/inputs/FormElement";
import dayjs from "dayjs";
import {OpeningState, projectsActions, projectsSelectors} from "../../features/projects.js";
import {CardElement} from "../../common/components/CardElement";
import {TimeInput} from "../../common/inputs/TimeInput";
import {TextInput, TextInputEmail} from "../../common/inputs/TextInput";
import {SwitchInput} from "../../common/inputs/SwitchInput";
import {AvailabilitySlotsTable} from "../../common/components/AvailabilitySlotsTable";
import {ColorInput} from "../../common/inputs/ColorInput";
import {getSlotsStartAndEnd} from "../../utils/slotsUtilities";
import {Trans, useTranslation} from "react-i18next";
import {DisplayInput} from "../../common/inputs/DisplayInput";
import {EditPage} from "../../common/pages/EditPage";

export function GeneralConfigTab({generalConfigForm}) {
  const {message} = App.useApp();
  const setIsModified = EditPage.useSetIsModified();
  const dispatch = useDispatch();
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const projectIsNotOpened = project.openingState === "notOpened";

  const onValuesChange = (changedFields, allFields) => {
    dispatch(projectsActions.changeEditing(allFields));
    setIsModified(true);
  };

  const onOpeningStateChange = (newStateIndex) => {
    const newState = Object.keys(OpeningState).find((key) => OpeningState[key] === newStateIndex);

    // TODO: IA legacy
    const projectData =
      newStateIndex >= 2 ? {openingState: newState, useAI: false} : {openingState: newState};
    // if (newStateIndex >= 2 && project.useAI) {
    //   message.warning(
    //     "Vous lancez vos inscriptions : par sécurité, nous désactivons automatiquement l'IA. Vous pouvez la réactiver dans les paramètres avancés.",
    //     15
    //   );
    // }

    if (newStateIndex >= 3 && !project.isPublic) {
      message.info(
        <span>
          {t("projects:wantToMakeYourProjectPublicMessage.content")}{" "}
          <Button
            type={"primary"}
            style={{marginLeft: 8}}
            target="_blank"
            onClick={() => {
              dispatch(projectsActions.changeEditing({isPublic: true}));
              message.destroy();
            }}
            rel="noreferrer">
            {t("projects:wantToMakeYourProjectPublicMessage.buttonTitle")}
          </Button>
        </span>
      );
    }
    dispatch(projectsActions.changeEditing(projectData));
    setIsModified(true);
  };

  const sessionNameTemplateTags = {
    "{{sessionName}}": t("sessions:schema.name.label"),
    "{{activityName}}": t("activities:schema.name.label"),
  };

  const changeAvailabilitySlots = (newSlots) => {
    dispatch(
      projectsActions.changeEditing({availabilitySlots: newSlots, ...getSlotsStartAndEnd(newSlots)})
    );
    setIsModified(true);
  };

  // We have to memoize this because with the current system, selectors were rerendered each time we click, which was annoying.
  const memoizedAccentuationColorsSelectors = useMemo(
    () => (
      <DisplayInput label={t("projects:schema.theme.colors.accentuation")}>
        <Space>
          <ColorInput name={["theme", "accent1"]} noStyle />
          <ColorInput name={["theme", "accent2"]} noStyle />
        </Space>
      </DisplayInput>
    ),
    []
  );

  return (
    <FormElement
      form={generalConfigForm}
      initialValues={{
        ...project,
        breakfastTime: dayjs(project.breakfastTime),
        lunchTime: dayjs(project.lunchTime),
        dinnerTime: dayjs(project.dinnerTime),
      }}
      onValuesChange={onValuesChange}>
      <div className="container-grid two-per-row" style={{alignItems: "start"}}>
        <div className="container-grid">
          <CardElement>
            <TextInput i18nNs={"projects"} name="name" required />

            <TextInputEmail i18nNs={"projects"} name="contactEmail" required />
          </CardElement>

          <CardElement title={t("projects:generalConfig.eventManagement")}>
            <div className="container-grid">
              <DisplayInput i18nNs={"projects"} name={"openingState"}>
                <Steps
                  current={OpeningState[project.openingState]}
                  direction={"vertical"}
                  onChange={onOpeningStateChange}
                  items={[
                    {
                      title: t("projects:schema.openingState.options.closed.title"),
                      key: "0",
                      description: t("projects:schema.openingState.options.closed.description"),
                    },
                    {
                      title: t("projects:schema.openingState.options.preRegisterOnly.title"),
                      key: "1",
                      description: t(
                        "projects:schema.openingState.options.preRegisterOnly.description"
                      ),
                    },
                    {
                      title: t(
                        "projects:schema.openingState.options.registerForStewardsOnly.title"
                      ),
                      key: "2",
                      description: t(
                        "projects:schema.openingState.options.registerForStewardsOnly.description"
                      ),
                    },
                    {
                      title: t("projects:schema.openingState.options.registerForAll.title"),
                      key: "3",
                      description: t(
                        "projects:schema.openingState.options.registerForAll.description"
                      ),
                    },
                  ]}
                />
              </DisplayInput>

              <SwitchInput i18nNs={"projects"} name="full" disabled={projectIsNotOpened} />

              <SwitchInput i18nNs={"projects"} name="isPublic" />

              <SwitchInput i18nNs={"projects"} name="secretSchedule" />
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <div className="userTourProjectAvailabilities containerV">
            <AvailabilitySlotsTable
              title={t("projects:schema.availabilitySlots.label")}
              availabilitySlots={project.availabilitySlots}
              onChange={changeAvailabilitySlots}
            />
          </div>

          <CardElement title={t("projects:generalConfig.mealsTimes")}>
            <div className="container-grid">
              <p style={{color: "gray"}}>
                <Trans i18nKey="generalConfig.mealsTimesDescription" ns="projects" />
              </p>
              <div className="container-grid three-per-row">
                <TimeInput i18nNs={"projects"} name="breakfastTime" required />
                <TimeInput i18nNs={"projects"} name="lunchTime" required />
                <TimeInput i18nNs={"projects"} name="dinnerTime" required />
              </div>
            </div>
          </CardElement>

          <CardElement title={t("projects:generalConfig.themeAndAppearance")}>
            <div className="container-grid">
              <div className="container-grid three-per-row">
                <ColorInput
                  label={t("projects:schema.theme.colors.primary")}
                  name={["theme", "primary"]}
                />
                <ColorInput label={t("projects:schema.theme.colors.bg")} name={["theme", "bg"]} />
                {memoizedAccentuationColorsSelectors}
              </div>

              <TextInput
                i18nNs="projects"
                name="sessionNameTemplate"
                placeholder={t("projects:schema.sessionNameTemplate.defaultValue")}
                tooltip={
                  <>
                    {t("projects:schema.sessionNameTemplate.tooltip")}
                    <ul>
                      {Object.entries(sessionNameTemplateTags).map(([key, val]) => (
                        <li key={key}>
                          <strong>{key} :</strong> {val}
                        </li>
                      ))}
                    </ul>
                  </>
                }
                allowClear
              />

              <SwitchInput i18nNs={"projects"} name="hideVolunteeringJaugeForParticipants" />
            </div>
          </CardElement>
        </div>
      </div>
    </FormElement>
  );
}
