import {useDispatch, useSelector} from "react-redux";
import {Form, App} from "antd";
import {safeValidateFields} from "../../common/inputs/FormElement";
import {TabsPage} from "../../common/pages/TabsPage";
import {ExperimentOutlined, ToolOutlined} from "@ant-design/icons";
import {projectsActions} from "../../features/projects.js";
import {MembersConfigTab} from "./MembersConfigTab";
import {registrationsSelectors} from "../../features/registrations";
import {GeneralConfigTab} from "./GeneralConfigTab";
import {TicketingConfigTab} from "./TicketingConfigTab";
import {SubscriptionsConfigTab} from "./SubscriptionsConfigTab";
import {WelcomePageConfigTab} from "./WelcomePageConfigTab";
import {PendingSuspense} from "../../common/components/Pending";
import {useUserTour} from "../../utils/userTours/userTourUtilities";
import configUserTour from "../../utils/userTours/configUserTour";
import {lazyWithRetry} from "../../utils/lazyWithRetry";
import {useTranslation} from "react-i18next";
import {FormBuilderTab} from "./FormBuilderTab";
import {BetaTag} from "../../common/components/BetaTag";
import {EditPage} from "../../common/pages/EditPage";

const AdvancedConfig = lazyWithRetry(() =>
  import(/* webpackPrefetch: true */ "./AdvancedConfigTab")
);

export function ConfigEdit() {
  const {message} = App.useApp();
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const setIsModified = EditPage.useSetIsModified();
  const isUserAdminOnThisProject = currentRegistration.role === "admin";

  useUserTour("config", configUserTour(currentRegistration));

  const [formComponentsForm] = Form.useForm();
  const [generalConfigForm] = Form.useForm();
  const [webhooksForm] = Form.useForm();

  const onConfigSaveButtonClick = () => {
    // Validate general config...
    safeValidateFields(
      generalConfigForm,
      () => {
        // Then validate form components...
        safeValidateFields(
          formComponentsForm,
          () => {
            // Then validate webhooks...
            safeValidateFields(
              webhooksForm,
              () => {
                // Then save the project with the new form components
                dispatch(
                  projectsActions.persist({
                    ...formComponentsForm.getFieldsValue(),
                    ...webhooksForm.getFieldsValue(),
                  })
                );
                setIsModified(false);
              },
              () =>
                message.error(
                  "Des champs ne sont pas corrects dans l'onglet Avancé > Webhooks. Complétez-les et ré-enregistrez.",
                  8
                )
            );
          },
          (error) => {
            console.error("formComponentsForm error", error, formComponentsForm.getFieldsError());
            message.error(
              "Des renseignements sont manquants dans un ou plusieurs champs du Formulaire d'inscription. Complétez-le et ré-enregistrez.",
              8
            );
          }
        );
      },
      (error) => {
        console.error("generalConfigForm error", error, generalConfigForm.getFieldsError());
        message.error(
          "Des champs ne sont pas corrects dans l'onglet Général. Complétez-les et ré-enregistrez.",
          8
        );
      }
    );
  };

  return (
    <TabsPage
      title={t("projects:eventConfiguration.title")}
      icon={<ToolOutlined />}
      fullWidth
      onValidation={onConfigSaveButtonClick}
      items={[
        {
          label: t("projects:eventConfiguration.tabs.general"),
          key: "main",
          className: "with-margins",
          children: <GeneralConfigTab generalConfigForm={generalConfigForm} />,
        },
        {
          label: t("projects:eventConfiguration.tabs.members"),
          key: "members",
          className: "with-margins",
          children: <MembersConfigTab isAdmin={isUserAdminOnThisProject} />,
        },
        {
          label: t("projects:schema.content.label"),
          key: "welcome-page",
          destroyInactiveTabPane: true, // Prevent the react-page module to trigger fake isModified updates
          children: <WelcomePageConfigTab />,
        },
        {
          label: t("projects:eventConfiguration.tabs.form"),
          key: "form",
          className: "with-margins",
          children: <FormBuilderTab formComponentsForm={formComponentsForm} />,
        },
        {
          label: t("projects:eventConfiguration.tabs.registrations"),
          key: "registrations",
          className: "with-margins",
          children: <SubscriptionsConfigTab />,
        },
        isUserAdminOnThisProject && {
          label: t("projects:eventConfiguration.tabs.ticketing"),
          key: "ticketing",
          className: "with-margins",
          children: <TicketingConfigTab />,
        },
        isUserAdminOnThisProject && {
          label: t("projects:eventConfiguration.tabs.advanced"),
          key: "advanced",
          className: "with-margins",
          children: (
            <PendingSuspense>
              <AdvancedConfig webhooksForm={webhooksForm} />
            </PendingSuspense>
          ),
        },
      ]}
    />
  );
}
