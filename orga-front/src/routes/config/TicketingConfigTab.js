import React, {useMemo} from "react";
import {Alert} from "antd";
import {projectsActions, projectsSelectors} from "../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {FormElement} from "../../common/inputs/FormElement";
import {addKeyToItemsOfList} from "../../utils/tableUtilities";
import {TableElement} from "../../common/components/TableElement";
import dayjs from "dayjs";
import Paragraph from "antd/es/typography/Paragraph";
import {CardElement} from "../../common/components/CardElement";
import {URLS} from "../../app/configuration";
import {TextInput, TextInputPassword} from "../../common/inputs/TextInput";
import {useUserTour} from "../../utils/userTours/userTourUtilities";
import helloAssoUserTour from "../../utils/userTours/helloAssoUserTour";
import {useTranslation} from "react-i18next";
import {SelectInput} from "../../common/inputs/SelectInput";
import {sorter} from "../../utils/sorters";
import {EditPage} from "../../common/pages/EditPage";
import {viewSelectors} from "../../features/view";

const useHelloAssoTicketingColumns = () => {
  const {t} = useTranslation();
  return [
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.title"),
      dataIndex: "title",
      sorter: (a, b) => sorter.text(a.title, b.title),
      defaultSortOrder: "descend",
      width: 500,
    },
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.startDate"),
      dataIndex: "startDate",
      sorter: (a, b) => sorter.date(a.startDate, b.startDate),
      render: (text, record) => (text !== undefined ? dayjs(text).format("LL") : text),
      width: 170,
    },
    {
      title: t("projects:ticketing.helloAssoTicketingColumns.description"),
      dataIndex: "description",
      width: 1000,
    },
  ];
};

export function TicketingConfigTab() {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const isModified = useSelector(viewSelectors.selectIsModified);
  const dispatch = useDispatch();
  const project = useSelector(projectsSelectors.selectEditing);
  const ticketingIsSetUp = {
    helloAsso:
      project.helloAsso?.selectedEvent &&
      project.helloAsso?.clientId &&
      project.helloAsso?.clientSecret,
    tiBillet: project.tiBillet?.serverUrl && project.tiBillet?.apiKey,
    customTicketing: project.customTicketing?.length > 0,
  };
  const ticketingIsOperational = project.ticketingMode && ticketingIsSetUp[project.ticketingMode];

  const helloAssoTicketingColumns = useHelloAssoTicketingColumns();

  useUserTour("helloAsso", helloAssoUserTour(), {
    shouldShowNow: () =>
      project.ticketingMode === "helloAsso" && window.location.hash === "#ticketing",
    deps: [project.ticketingMode, window.location.hash],
  });

  const setHelloAssoEvent = (selectedRowObject) => {
    dispatch(
      projectsActions.changeEditing({
        helloAsso: {...project.helloAsso, selectedEvent: selectedRowObject[0].key},
      })
    );
    setIsModified(true);
  };

  const onTicketingValuesConfigChange = (_, allValues) => {
    dispatch(projectsActions.changeEditing(allValues));
    setIsModified(true);
  };

  const rowSelectionHelloAssoEvent = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setHelloAssoEvent(selectedRowObject);
    },
    type: "radio",
  };

  const TicketingCardElement = useMemo(
    () =>
      ({ticketingMode, children, ...props}) =>
        project.ticketingMode === ticketingMode ? (
          <CardElement {...props} className="fade-in" style={{marginBottom: 0, marginTop: 16}}>
            <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
              {children}
            </FormElement>
          </CardElement>
        ) : null,
    [project?.ticketingMode]
  );

  return (
    <>
      {ticketingIsOperational && !isModified && (
        <Alert
          style={{marginBottom: 26}}
          message={t("projects:ticketing.ticketingIsActivated")}
          showIcon
          type="success"
        />
      )}

      <CardElement>
        <FormElement initialValues={project} onValuesChange={onTicketingValuesConfigChange}>
          <SelectInput
            className={"userTourTicketingOptions"}
            i18nNs={"projects"}
            name="ticketingMode"
            options={[
              {value: null, label: "- Aucune -"},
              {value: "tiBillet", label: t("projects:schema.tiBillet.label")},
              {value: "helloAsso", label: t("projects:schema.helloAsso.label")},
              {value: "customTicketing", label: t("projects:schema.customTicketing.label")},
            ]}
          />
        </FormElement>
      </CardElement>
      <TicketingCardElement title="TiBillet" ticketingMode="tiBillet">
        <div className="container-grid three-per-row">
          <TextInput
            label="URL de la billetterie TiBillet"
            name={["tiBillet", "serverUrl"]}
            placeholder="https://url-de-la-billetterie-tibillet.org"
            rules={[{type: "url"}]}
          />
          <TextInput
            label="Slug de l'événement"
            name={["tiBillet", "eventSlug"]}
            placeholder="mon-evenemement-102123-xxxx"
          />
          <TextInputPassword label="Clé API" name={["tiBillet", "apiKey"]} placeholder="clé api" />
        </div>
      </TicketingCardElement>

      <TicketingCardElement
        title={t("projects:schema.customTicketing.label")}
        ticketingMode="customTicketing">
        <Alert
          style={{marginBottom: 26}}
          message="Vous pouvez valider n'importe quel type de numéro de billet avec NOÉ."
          description={
            <>
              <p>
                NOÉ peut appeler une URL particulière pour valider des numéros de billets en tous
                genres rentrés par les participant⋅es.
              </p>
              <Paragraph>
                <p>La requête envoyée sera la suivante :</p>
                <ul>
                  <li>
                    <strong>Type :</strong> POST
                  </li>
                  <li>
                    <strong>URL de destination :</strong> {project.customTicketing || " -"}
                  </li>
                  <li>
                    <strong>Corps de la requête :</strong>
                    <pre>{'{ ticketId: "XXXXXXX" }'}</pre>
                  </li>
                </ul>
                <p>
                  Toute réponse avec un <strong>code d'état HTTP égal à 200</strong> sera interprété
                  comme une validation du numéro de billet. Toute code d'état autre que 200 sera
                  interprétée comme une invalidation.
                </p>
                <p>Le corps de la réponse peut contenir les éléments suivants :</p>
                <pre>
                  {"{"}
                  <br />
                  {"  "}name: <i>string</i>, # nom de l'article acheté
                  <br />
                  {"  "}amount: <i>number</i>, # montant
                  <br />
                  {"  "}lastName: <i>number</i>, # nom de la personne, facultatif
                  <br />
                  {"  "}firstName: <i>number</i> # prénom de la personne, facultatif
                  <br />
                  {"}"}
                </pre>
                <p>
                  Vous pouvez faire des tests avec l'URL suivante, qui validera n'importe quel
                  numéro de billet, sauf si l'indentifiant du billet vaut "
                  <span style={{fontFamily: "monospace"}}>fail</span>" :
                </p>
                <a href={`${URLS.API}/projects/${project._id}/ticketing/test`}>
                  {URLS.API}/projects/{project._id}/ticketing/test
                </a>
              </Paragraph>
            </>
          }
          type="info"
        />
        <TextInput
          label="URL de validation des billets"
          name="customTicketing"
          placeholder="https://mon-url-de-validation-de-ticket.com"
          rules={[{type: "url"}]}
        />
      </TicketingCardElement>

      <TicketingCardElement title={t("projects:schema.helloAsso.label")} ticketingMode="helloAsso">
        {!project.helloAsso?.selectedEvent &&
          project.helloAsso?.organizationSlug &&
          !isModified && (
            <Alert
              type="warning"
              showIcon
              style={{marginBottom: 26}}
              className="bounce-in"
              message="Plus qu'une étape pour finir la configuration !"
              description="Veillez sélectionner une billetterie Hello Asso avec laquelle lier votre événement, puis Enregistrer les modifications."
            />
          )}
        {project.helloAsso?.organizationSlug && (
          <Alert
            showIcon
            style={{marginBottom: 26}}
            className="bounce-in"
            message="Une dernière étape pour intégrer Hello Asso !"
            description={
              <>
                <p>
                  <strong>
                    NOÉ a besoin que Hello Asso l'avertisse lorsqu'un paiement est réalisé.
                  </strong>
                  Cela permettra à NOÉ de rentrer automatiquement les numéros de billet de vos·es
                  lorsqu'iels paient sur NOÉ (pratique !). En plus, cela permet aussi à NOÉ
                  d'envoyer un email d'invitation ou de rappel aux particpant·es avec les
                  informations sur leurs billets.
                </p>
                <p>
                  Pour cela,{" "}
                  <strong>
                    accédez à votre espace Hello Asso, puis allez dans l'onglet "Mon Compte" >
                    "Intégrations et API"
                  </strong>
                  .
                </p>
                <p>Enfin, copiez-collez l'URL ci-dessous dans la partie "Notifications" :</p>
                <Paragraph
                  copyable
                  style={{
                    color: "var(--noe-primary)",
                    backgroundColor: "white",
                    width: "fit-content",
                    borderRadius: 8,
                    padding: 8,
                    marginBottom: 0,
                  }}>
                  {`${URLS.API}/projects/${project._id}/ticketing/onHelloAssoOrder`}
                </Paragraph>
              </>
            }
          />
        )}
        <div className="container-grid two-per-row">
          <TextInputPassword
            label="Client Id"
            name={["helloAsso", "clientId"]}
            placeholder="client id"
          />
          <TextInputPassword
            label="Client Secret"
            name={["helloAsso", "clientSecret"]}
            placeholder="client secret"
          />
        </div>
        {project.helloAsso?.organizationSlug && (
          <>
            <div style={{marginBottom: 26}}>
              <strong>Organisation trouvée :</strong> {project.helloAsso?.organizationSlug}
            </div>
            <TableElement.Simple
              showHeader
              scroll={{x: (helloAssoTicketingColumns.length - 1) * 160 + 70}}
              rowSelection={rowSelectionHelloAssoEvent}
              selectedRowKeys={[{key: project.helloAsso?.selectedEvent}]}
              columns={helloAssoTicketingColumns}
              rowKey="formSlug"
              dataSource={addKeyToItemsOfList(
                project.helloAsso?.selectedEventCandidates,
                "formSlug"
              )}
            />
          </>
        )}
      </TicketingCardElement>
    </>
  );
}
