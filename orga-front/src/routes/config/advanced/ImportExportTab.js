import React, {useRef, useState} from "react";
import {Alert, Button, InputNumber, App, Modal, Space, Upload} from "antd";
import {CalendarOutlined, ExportOutlined, ImportOutlined} from "@ant-design/icons";
import {projectsActions, projectsSelectors} from "../../../features/projects";
import {useDispatch, useSelector} from "react-redux";
import {FormElement} from "../../../common/inputs/FormElement";
import {BetaTag} from "../../../common/components/BetaTag";
import {CardElement} from "../../../common/components/CardElement";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {SwitchInput} from "../../../common/inputs/SwitchInput";
import {ElementSelectionModal} from "../../../common/components/ElementSelectionModal";
import {DisplayInput} from "../../../common/inputs/DisplayInput";
import {normalize} from "../../../utils/stringUtilities";
import {extractImportExportFields} from "./atoms/extractImportExportFields";
import {getSelectionWithDependencies} from "./atoms/getSelectionWithDependencies";
import {pickSelectedImportExportData} from "./atoms/pickSelectedImportExportData";
import {shiftImportDates} from "./atoms/shiftImportDates";
import {useNavigate} from "react-router-dom";
import {dateFormatter} from "../../../utils/formatters";
import {sorter} from "../../../utils/sorters";

export function ImportExportTab() {
  const {message} = App.useApp();
  const navigate = useNavigate();
  const {t} = useTranslation();
  const project = useSelector(projectsSelectors.selectEditing);
  const dispatch = useDispatch();
  const downloadButton = useRef();

  // Can ben either "import" or "export"
  const [operationType, setOperationType] = useState();

  // Import data & options
  const [dataToImportExport, setDataToImportExport] = useState();
  const [isAdditiveImport, setIsAdditiveImport] = useState(true);

  // Date shifting
  const [dateShift, setDateShift] = useState(0);

  // Export options
  const [exportWithRegistrations, setExportWithRegistrations] = useState(false);
  const [anonymizedExport, setAnonymizedExport] = useState(false);
  const [keepStewardNames, setKeepStewardNames] = useState(false);

  // Open modals: can be "globalConfig", "projectConfig" or "dateShifting", or undefined if none are open
  const [modalOpen, setModalOpen] = useState(undefined);

  // Fields to import
  const {globalFields, projectFields} = extractImportExportFields(
    dataToImportExport,
    operationType
  );
  const [globalFieldsSelection, setGlobalFieldsSelection] = useState([]);
  const [projectFieldsSelection, setProjectFieldsSelection] = useState([]);

  const safelySetGlobalFieldsSelection = (fields) => {
    const valuesKeys = fields.map((v) => v.key);
    const valueKeysWithDependencies = getSelectionWithDependencies(valuesKeys, dataToImportExport);
    const newFields = globalFields.filter((f) => valueKeysWithDependencies.includes(f.key));
    setGlobalFieldsSelection(newFields);
  };

  const importModalColumns = [
    {
      title: operationType === "import" ? "Donnée à importer" : "Donnée à exporter",
      dataIndex: "label",
      defaultSortOrder: "ascend",
      sorter: (a, b) => sorter.text(a.label, b.label),
    },
  ];

  /**
   * UTILS
   */

  const initializeImportExport = (opType, dataToImportExport) => {
    // Initialize the type of operation
    setOperationType(opType);

    // Store the import/export data
    setDataToImportExport(dataToImportExport);

    // Open the first modal
    setModalOpen("globalConfig");
  };

  const closeModals = () => setModalOpen(undefined);

  /**
   * IMPORT / EXPORT PREPARATION
   */

  const prepareExport = async () => {
    try {
      const data = await dispatch(
        projectsActions.export(exportWithRegistrations, anonymizedExport, keepStewardNames)
      );
      initializeImportExport("export", data);
    } catch (e) {
      message.error("Le projet n'a pas pu être exporté.");
    }
  };

  // Parse the imported file, and set the data to import in the state
  const prepareImport = (file) => {
    let reader = new FileReader();
    reader.onload = function (e) {
      try {
        let data = JSON.parse(e.target.result);
        initializeImportExport("import", data);
      } catch (e) {
        message.error("Oups, il semblerait que votre fichier d'import soit malformé !");
      }
    };
    reader.readAsText(file);
  };

  /**
   * IMPORT / EXPORT VALIDATION
   */

  const validateExport = () => {
    // Build the export file name
    const exportName = normalize(
      `Export du projet - ${project.name} - ${dayjs().format("HH-mm DD-MM-YYYY")}`,
      true
    ).replace(/[^\w ]/g, "-");

    // Pick the data to export
    const pickedExportData = pickSelectedImportExportData(
      dataToImportExport,
      globalFieldsSelection,
      projectFieldsSelection
    );

    // Create the export file
    const file = new File([JSON.stringify(pickedExportData, null, 2)], exportName, {
      type: "application/json",
    });

    // Download the file
    let exportUrl = URL.createObjectURL(file);
    downloadButton.current.setAttribute("href", exportUrl);
    downloadButton.current.setAttribute("download", exportName);
    downloadButton.current.click();

    // Close modals
    closeModals();
  };

  // Finally, process and send the import data to backend
  const validateImport = async () => {
    // Pick the data to import
    const pickedImportData = pickSelectedImportExportData(
      dataToImportExport,
      globalFieldsSelection,
      projectFieldsSelection
    );

    // Shift the data's dates according to the date shift
    const shiftedImportData = shiftImportDates(pickedImportData, dateShift);

    // Import the data into database
    dispatch(
      projectsActions.import(shiftedImportData, isAdditiveImport, !!shiftedImportData.registrations)
    ).then(() => message.success("Import réussi !"));

    // Close modals
    closeModals();
  };

  const validateOperation = operationType === "import" ? validateImport : validateExport;

  /**
   * ELEMENTS SELECTION MODAL CONFIGURATION
   */

  const globalConfigRowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      safelySetGlobalFieldsSelection(selectedRowObject);
    },
  };
  const projectConfigRowSelection = {
    onChange: (selectedRowKeys, selectedRowObject) => {
      setProjectFieldsSelection(selectedRowObject);
    },
  };

  const globalConfigModalParams = {
    title: t(`projects:advanced.importExport.globalModal.${operationType}.title`),
    subtitle: (
      <>
        {t(`projects:advanced.importExport.globalModal.${operationType}.subtitle`)}{" "}
        <Alert
          style={{marginTop: 10}}
          showIcon
          message={
            <>
              Certains éléments sont interdépendants : par exemple les sessions ont besoin des
              activités, qui ont elles-mêmes besoin des catégories. Ceux-ci sont donc{" "}
              <strong>automatiquement pré-selectionnés ensemble</strong> pour que vous ne puissiez
              pas les oublier.
            </>
          }
        />
        {globalFieldsSelection.find((field) => field.key === "registrations") && (
          <Alert
            style={{marginTop: 10}}
            type={"warning"}
            showIcon
            message={
              (operationType === "import"
                ? "Attention, vous importez des inscriptions de participant·es."
                : "Attention, vous exportez des inscriptions de participant·es.") +
              " En êtes-vous sûr·e?"
            }
          />
        )}
      </>
    ),
    okText:
      !globalFieldsSelection.find((field) => field.key === "project") &&
      operationType === "export" &&
      t("projects:advanced.importExport.export"),
    onOk: () => {
      if (globalFieldsSelection.find((field) => field.key === "project")) {
        // If the project config is selected, then go to the project config
        setModalOpen("projectConfig");
      } else if (operationType === "import") {
        // If the project config is NOT selected, and we are importing stuff, then go to the date shifting modal
        setModalOpen("dateShifting");
      } else {
        // Else, validate
        validateOperation();
      }
    },
    rowSelection: globalConfigRowSelection,
    selectedRowKeys: globalFieldsSelection,
    setSelectedRowKeys: safelySetGlobalFieldsSelection,
    columns: importModalColumns,
    dataSource: globalFields,
  };

  const projectConfigModalParams = {
    title: t(`projects:advanced.importExport.projectModal.${operationType}.title`),
    subtitle: t(`projects:advanced.importExport.projectModal.${operationType}.subtitle`),
    onOk: () => {
      if (operationType === "import") {
        setModalOpen("dateShifting");
      } else {
        validateOperation();
      }
    },
    okText: operationType === "export" && t("projects:advanced.importExport.export"),
    rowSelection: projectConfigRowSelection,
    selectedRowKeys: projectFieldsSelection,
    setSelectedRowKeys: setProjectFieldsSelection,
    columns: importModalColumns,
    dataSource: projectFields,
  };

  const importedProjectStart =
    dataToImportExport?.project?.start ||
    dataToImportExport?.project?.availabilitySlots?.[0]?.start;

  return (
    <>
      <a ref={downloadButton} style={{display: "none"}} />

      <div className="container-grid two-per-row">
        <div className="container-grid">
          <CardElement icon={<ExportOutlined />} title={"Exporter l'événement"}>
            <p>
              Tout est exporté (catégories, espaces,activités, encadrant⋅es, équipes, sessions, et
              configuration générale de l'événement).
            </p>
            <CardElement
              size={"small"}
              style={{marginBottom: 26}}
              title={"Export des participant·es"}>
              <p>
                Vous pouvez choisir d'exporter aussi les participant·es et utilisateur·ices qui sont
                liées à votre projet, avec leurs données personelles. Attention toutefois aux enjeux
                de vie privée de vos participant·es : vous êtes responsable des manipulations que
                vous faites avec ces données et devez vous assurer du consentement des personnes.
              </p>
              <FormElement>
                <SwitchInput
                  formItemProps={{style: {marginBottom: 0}}}
                  onChange={() => setExportWithRegistrations(!exportWithRegistrations)}
                  checked={exportWithRegistrations}
                  label="Exporter aussi les participant·es ?"
                />
              </FormElement>
            </CardElement>

            <CardElement
              size={"small"}
              style={{marginBottom: 26}}
              title={"Anonymiser les données personnelles"}>
              <p>
                Anonymisez votre export si vous souhaitez conserver votre planning, vos participants
                et les statistiques d'inscriptions de chaque participant aux activités de votre
                événement, mais sans conserver les données individuelles de chacun·e.
              </p>
              <p>
                Les noms, prénoms, emails des participant·es seront remplacés par des faux. Les
                noms, prénoms et numéros de téléphone des encadrant·es également.
              </p>
              <FormElement>
                <SwitchInput
                  onChange={() => setAnonymizedExport(!anonymizedExport)}
                  checked={anonymizedExport}
                  label={"Anonymiser les données personnelles"}
                />
                {anonymizedExport && (
                  <SwitchInput
                    onChange={() => setKeepStewardNames(!keepStewardNames)}
                    checked={keepStewardNames}
                    tooltip={"Ne pas anonymiser le nom des encadrant·es"}
                    label={"Garder le nom des encadrant·es"}
                  />
                )}
              </FormElement>
              {anonymizedExport && (
                <Alert
                  type={"warning"}
                  showIcon
                  message={
                    "Attention, pour l'instant, les réponses des participant·es au formulaire d'inscription " +
                    "ainsi que les paiements de billetterie ne sont pas anonymisées."
                  }
                />
              )}
            </CardElement>

            <Button onClick={prepareExport} icon={<ExportOutlined />} type={"primary"}>
              Exporter {exportWithRegistrations && "avec les participant·es"}
            </Button>
          </CardElement>

          <CardElement icon={<ExportOutlined />} title={"Autres exports en format CSV"}>
            <p>
              D'autres exports sont disponibles sur d'autres pages (bouton <ExportOutlined /> en
              haut à droite) :
            </p>
            <div className={"containerH"} style={{gap: "4pt", flexWrap: "wrap"}}>
              <Button
                type="link"
                onClick={() => navigate("./../participants")}
                icon={<ExportOutlined />}>
                Participant⋅es & stats
              </Button>
              <Button
                type="link"
                onClick={() => navigate("./../sessions")}
                icon={<ExportOutlined />}>
                Sessions
              </Button>
            </div>
          </CardElement>
        </div>

        <div className="container-grid">
          <div>
            <CardElement
              icon={<ImportOutlined style={{color: !isAdditiveImport && "red"}} />}
              title={
                <span style={{color: !isAdditiveImport && "red"}}>
                  Importer à partir d'un fichier
                </span>
              }
              style={{borderColor: !isAdditiveImport && "red"}}
              headStyle={{borderColor: !isAdditiveImport && "red"}}>
              <p>
                La fonctionnalité d'import{" "}
                <strong>ajoute les données du fichier importé aux données existantes</strong> de
                votre projet (sessions, activités, etc.).{" "}
                <strong>Seule la Configuration du projet est remplacée</strong> (formulaire
                d'inscription, page d'accueil, paramètres généraux, etc...). Le nom du projet et les
                droits des membres restent inchangés.
              </p>
              <CardElement
                size={"small"}
                style={{marginBottom: 26}}
                title={"Importer et tout remplacer"}>
                <p>
                  Vous pouvez aussi effacer tout votre projet pour recommencer de zéro en activant
                  l'option "Tout remplacer".
                </p>
                <p>
                  Cela efface absolument tout votre projet pour le remplacer avec les données
                  importées. Le nom du projet et les droits des membres restent inchangés.
                  <strong>Aucun retour en arrière ne sera possible.</strong>
                </p>
                <FormElement>
                  <SwitchInput
                    style={{background: !isAdditiveImport && "red"}}
                    formItemProps={{style: {marginBottom: 0}}}
                    onChange={(value) => setIsAdditiveImport(!value)}
                    checked={!isAdditiveImport}
                    label="Tout remplacer ?"
                  />
                </FormElement>
              </CardElement>

              <p></p>
              <Upload
                beforeUpload={(file) => {
                  prepareImport(file);
                  return false;
                }}
                accept="application/json"
                showUploadList={false}>
                <Button
                  icon={<ImportOutlined />}
                  danger={!isAdditiveImport}
                  type={!isAdditiveImport ? "primary" : "default"}>
                  Importer {isAdditiveImport ? "et ajouter à l'existant" : "et tout remplacer"}
                </Button>
              </Upload>
            </CardElement>

            <CardElement icon={<ImportOutlined />} title={"Autres imports en format CSV"}>
              <p>
                Il est possible d'importer des données en format CSV sur d'autres pages (bouton{" "}
                <ImportOutlined /> en haut à droite) :
              </p>
              <div className={"containerH"} style={{gap: "4pt", flexWrap: "wrap"}}>
                <Button
                  type="link"
                  onClick={() => navigate("./../participants")}
                  icon={<ImportOutlined />}>
                  Participant⋅es
                </Button>
                <Button
                  type="link"
                  onClick={() => navigate("./../categories")}
                  icon={<ImportOutlined />}>
                  {t("categories:label_other")}
                </Button>
                {project.usePlaces && (
                  <Button
                    type="link"
                    onClick={() => navigate("./../places")}
                    icon={<ImportOutlined />}>
                    {t("places:label_other")}
                  </Button>
                )}
                <Button
                  type="link"
                  onClick={() => navigate("./../stewards")}
                  icon={<ImportOutlined />}>
                  {t("stewards:label_other")}
                </Button>
                {project.useTeams && (
                  <Button
                    type="link"
                    onClick={() => navigate("./../teams")}
                    icon={<ImportOutlined />}>
                    {t("teams:label_other")}
                  </Button>
                )}
                <Button
                  type="link"
                  onClick={() => navigate("./../activities")}
                  icon={<ImportOutlined />}>
                  {t("activities:label_other")}
                </Button>
                <Button
                  type="link"
                  onClick={() => navigate("./../sessions")}
                  icon={<ImportOutlined />}>
                  {t("sessions:label_other")}
                </Button>
              </div>
            </CardElement>
          </div>
        </div>

        <ElementSelectionModal
          open={modalOpen === "globalConfig"}
          {...globalConfigModalParams}
          rowKey="key"
          size="small"
          onCancel={closeModals}
        />
        <ElementSelectionModal
          open={modalOpen === "projectConfig"}
          {...projectConfigModalParams}
          rowKey="key"
          size="small"
          onCancel={closeModals}
        />
        <Modal
          open={modalOpen === "dateShifting"}
          title={
            <>
              {t(`projects:advanced.importExport.dateShiftingModal.title`)}{" "}
              <BetaTag title={"new"} />
            </>
          }
          centered
          onOk={validateOperation}
          okText={
            operationType === "import"
              ? t("projects:advanced.importExport.launchImport")
              : t("projects:advanced.importExport.export")
          }
          okButtonProps={{danger: true, type: "default"}}
          onCancel={closeModals}>
          <FormElement onValidate={validateOperation}>
            <div className="container-grid">
              <div>{t(`projects:advanced.importExport.dateShiftingModal.subtitle`)}</div>

              <DisplayInput
                label={t("projects:advanced.importExport.dateShiftingModal.dateShift.label")}
                name="dateShift"
                formItemProps={{help: !dateShift && "Laissez vide pour ne rien faire."}}>
                <Space.Compact>
                  <InputNumber
                    keyboard
                    value={dateShift === 0 ? undefined : dateShift}
                    onChange={setDateShift}
                    placeholder={0}
                    min={0}
                    rules={[{type: "number"}]}
                  />
                  <Button icon={<CalendarOutlined />} onClick={() => setDateShift(dateShift + 365)}>
                    + 1 année
                  </Button>
                </Space.Compact>
              </DisplayInput>

              {importedProjectStart && (
                <div>
                  <p>
                    L'événement importé commençait initialement le{" "}
                    <strong>{dateFormatter.longDate(importedProjectStart, false, true)}</strong>.
                  </p>
                  {!dateShift ? (
                    "Avez zéro jours de décalage, l'événement sera importé tel quel."
                  ) : (
                    <>
                      Avec un décalage de {dateShift} jours, les données seront donc importées comme
                      si votre événement importé commençait le{" "}
                      <strong>
                        {dateFormatter.longDate(
                          dayjs(importedProjectStart).add(dateShift, "day").format(),
                          false,
                          true
                        )}
                      </strong>
                      .
                    </>
                  )}
                </div>
              )}
            </div>
          </FormElement>
        </Modal>
      </div>
    </>
  );
}
