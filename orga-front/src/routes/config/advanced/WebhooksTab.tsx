import React from "react";
import {SimpleFormList} from "../../../common/inputs/SimpleFormList";
import {TextInput} from "../../../common/inputs/TextInput";
import {SelectInput} from "../../../common/inputs/SelectInput";
import {FormElement} from "../../../common/inputs/FormElement";
import {useSelector} from "react-redux";
import {projectsSelectors} from "../../../features/projects";
import {EditPage} from "../../../common/pages/EditPage";
import {Trans, useTranslation} from "react-i18next";
import {Alert, Button, FormInstance} from "antd";
import {CardElement} from "../../../common/components/CardElement";
import {URLS} from "../../../app/configuration";
import {ReadOutlined} from "@ant-design/icons";
import {Stack} from "../../../common/layout/Stack";

export const WebhooksTab = ({webhooksForm}: {webhooksForm: FormInstance}) => {
  const {t} = useTranslation();
  const setIsModified = EditPage.useSetIsModified();
  const webhooks = useSelector((s) => projectsSelectors.selectEditing(s).webhooks);
  return (
    <div>
      <Alert
        message={t("projects:schema.webhooks.explanationAlert.message")}
        description={
          <>
            <div>
              <Trans i18nKey="schema.webhooks.explanationAlert.description" ns="projects" />
            </div>
            <Button
              type={"link"}
              icon={<ReadOutlined />}
              style={{padding: 0}}
              href={`${URLS.WEBSITE}/docs/webhooks`}>
              {t("projects:schema.webhooks.explanationAlert.readWebhooksDocumentationButton")}
            </Button>
          </>
        }
        style={{marginBottom: 26}}
      />

      <Alert
        type={"warning"}
        message={t("projects:schema.webhooks.securityAlert.message")}
        description={
          <Trans
            i18nKey="schema.webhooks.securityAlert.description"
            ns="projects"
            components={{
              linkToIpFindWebsite: (
                <a
                  href={"https://www.site24x7.com/tools/find-ip-address-of-web-site.html"}
                  target={"_blank"}
                  rel="noreferrer"
                />
              ),
            }}
            values={{apiUrl: new URL(URLS.API).hostname}}
          />
        }
        style={{marginBottom: 26}}
      />

      <CardElement>
        <FormElement
          form={webhooksForm}
          initialValues={{webhooks}}
          onValuesChange={() => setIsModified(true)}>
          <SimpleFormList
            name={"webhooks"}
            addButtonText={t("projects:schema.webhooks.addAWebhook")}
            noItemText={t("projects:schema.webhooks.noWebhookConfigured")}
            getDefaultValue={() => ({apiKey: crypto.randomUUID()})}>
            {(name) => (
              <Stack
                row
                width={"100%"}
                justifySpaceBetween
                rowGap={2}
                columnGap={4}
                sx={{
                  flexWrap: {xs: "wrap", lg: "nowrap"},
                  "> *": {flexGrow: 1, flexBasis: {xs: "100%", md: "45%"}},
                }}>
                <TextInput
                  name={[name, "url"]}
                  label={t("projects:schema.webhooks.url.label")}
                  placeholder={t("projects:schema.webhooks.url.placeholder")}
                  required
                  rules={[{type: "url"}]}
                />
                <SelectInput
                  name={[name, "endpoints"]}
                  label={t("projects:schema.webhooks.endpoints.label")}
                  placeholder={t("projects:schema.webhooks.endpoints.placeholder")}
                  mode={"multiple"}
                  required
                  options={[
                    "categories",
                    "places",
                    "stewards",
                    "activities",
                    "teams",
                    "sessions",
                  ].map((endpoint) => ({label: t(`${endpoint}:label_other`), value: endpoint}))}
                />
                <SelectInput
                  name={[name, "actions"]}
                  label={t("projects:schema.webhooks.actions.label")}
                  placeholder={t("projects:schema.webhooks.actions.placeholder")}
                  mode={"multiple"}
                  required
                  options={[
                    {
                      label: t("projects:schema.webhooks.actions.options.create"),
                      value: "create",
                    },
                    {
                      label: t("projects:schema.webhooks.actions.options.update"),
                      value: "update",
                    },
                    {
                      label: t("projects:schema.webhooks.actions.options.delete"),
                      value: "delete",
                    },
                  ]}
                />
                <TextInput
                  name={[name, "apiKey"]}
                  label={t("projects:schema.webhooks.apiKey.label")}
                  placeholder={t("projects:schema.webhooks.apiKey.placeholder")}
                />
              </Stack>
            )}
          </SimpleFormList>
        </FormElement>
      </CardElement>
    </div>
  );
};
