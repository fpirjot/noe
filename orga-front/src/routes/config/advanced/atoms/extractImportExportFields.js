import {t} from "i18next";
import React from "react";

const getFieldsOptions = (obj, getLabel) =>
  Object.keys(obj || {}).map((key) => ({
    key,
    label: getLabel(key),
  }));
export const extractImportExportFields = (dataToImportExport, operationType) => {
  // Extract and format fields from the data at global and project level
  const projectFields = getFieldsOptions(dataToImportExport?.project, (key) =>
    t(`projects:schema.${key}.label`, key)
  ).filter((field) => !["start", "end"].includes(field.key)); // Don't include start and end in th choice selection, but import them automatically when the availability slots are asked by the user

  const globalFields = getFieldsOptions(dataToImportExport, (key) =>
    key === "project" ? (
      <>
        Configuration du projet
        <span style={{color: "gray"}}>
          {" "}
          - vous pourrez sélectionner plus tard ce que vous souhaitez{" "}
          {operationType === "import" ? "importer" : "exporter"}
        </span>
      </>
    ) : (
      <>
        {t(`${key}:label_other`, key)}{" "}
        <span style={{color: "gray"}}> - {dataToImportExport[key]?.length} éléments</span>
      </>
    )
  );

  return {projectFields, globalFields};
};
