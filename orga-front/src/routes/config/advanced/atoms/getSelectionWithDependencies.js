export const getSelectionWithDependencies = (valuesKeys, dataToImportExport) => {
  if (valuesKeys.includes("registrations")) {
    if (!valuesKeys.includes("sessions") && dataToImportExport.sessions) {
      valuesKeys.push("sessions");
    }
  }
  if (valuesKeys.includes("sessions")) {
    if (!valuesKeys.includes("activities") && dataToImportExport.activities) {
      valuesKeys.push("activities");
    }
    if (!valuesKeys.includes("teams") && dataToImportExport.teams) {
      valuesKeys.push("teams");
    }
  }

  if (valuesKeys.includes("teams")) {
    if (!valuesKeys.includes("activities") && dataToImportExport.activities) {
      valuesKeys.push("activities");
    }
  }

  if (valuesKeys.includes("activities")) {
    if (!valuesKeys.includes("categories") && dataToImportExport.categories) {
      valuesKeys.push("categories");
    }
    if (!valuesKeys.includes("stewards") && dataToImportExport.stewards) {
      valuesKeys.push("stewards");
    }
    if (!valuesKeys.includes("places") && dataToImportExport.places) {
      valuesKeys.push("places");
    }
  }
  return valuesKeys;
};
