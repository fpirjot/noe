import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {categoriesActions, categoriesSelectors} from "../../features/categories.js";
import {SwatchesPicker} from "react-color";
import {EditPage, ElementEditProps} from "../../common/pages/EditPage";
import {Alert, Form, Tag} from "antd";
import {CardElement} from "../../common/components/CardElement";
import {useTranslation} from "react-i18next";
import {TagOutlined} from "@ant-design/icons";
import {ColorDot} from "../../common/components/ColorDot";
import {TextInput} from "../../common/inputs/TextInput";
import {contrastColor} from "contrast-color";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {TextAreaInput} from "../../common/inputs/TextAreaInput";
import {useLocation} from "react-router-dom";
import {withFallBackOnUrlId} from "../../utils/withFallbackOnUrlId";
import FormItem from "antd/es/form/FormItem";

function CategoryEdit({id, asModal, modalOpen, setModalOpen, onCreate}: ElementEditProps) {
  const location = useLocation();
  const [form] = Form.useForm();
  const {t} = useTranslation();
  const category = useSelector(categoriesSelectors.selectEditing);
  const categories = useSelector(categoriesSelectors.selectList);
  const dispatch = useDispatch();

  const groupEditing = location?.state?.groupEditing;
  const clonedElement = location?.state?.clonedElement;

  useLoadEditing(
    categoriesActions,
    id,
    () => {
      dispatch(categoriesActions.loadList());
    },
    clonedElement
  );

  const ExampleCurrentCategory = () => {
    const nameVal = Form.useWatch("name", form);
    const colorVal = Form.useWatch("color", form);

    return (
      <div className="containerH" style={{alignItems: "center"}}>
        <ColorDot color={colorVal} style={{marginRight: 8, transition: "all .3s"}} />
        <Tag style={{textOverflow: "ellipsis", overflow: "hidden", maxWidth: 125}} color={colorVal}>
          {nameVal}
        </Tag>
      </div>
    );
  };

  const ColorPicker = ({value, onChange}) => {
    const colorIsTooLight = value && contrastColor({bgColor: value, threshold: 150}) === "#000000";

    return (
      <>
        {colorIsTooLight && (
          <Alert
            type="warning"
            style={{marginBottom: 15}}
            message={t("categories:schema.color.colorTooLight")}
          />
        )}
        <SwatchesPicker
          height="auto"
          width="100%"
          color={value}
          onChangeComplete={(val) => onChange(val.hex)}
        />
      </>
    );
  };

  return (
    <EditPage
      icon={<TagOutlined />}
      i18nNs="categories"
      clonable
      clonedElement={clonedElement}
      asModal={asModal}
      modalOpen={modalOpen}
      setModalOpen={setModalOpen}
      onCreate={onCreate}
      form={form}
      deletable
      elementsActions={categoriesActions}
      record={category}
      initialValues={category}
      groupEditing={groupEditing}>
      <div className="container-grid two-per-row">
        <CardElement>
          <div className="container-grid">
            <TextInput
              i18nNs="categories"
              name="name"
              required
              rules={[
                {
                  validator: (_, value) => {
                    return 30 <= value.length && value.length <= 45
                      ? Promise.reject(new Error(t("categories:schema.name.tooLong")))
                      : Promise.resolve();
                  },
                  warningOnly: true,
                },
                {max: 45, message: t("categories:schema.name.reallyTooLong")},
              ]}
            />

            <TextAreaInput i18nNs="categories" name="summary" />
          </div>
        </CardElement>

        <CardElement>
          <FormItem
            label={t("categories:schema.color.label")}
            name="color"
            rules={[{required: true, message: t("categories:schema.color.pleaseChooseAColor")}]}>
            <ColorPicker />
          </FormItem>

          <div style={{marginTop: 26}}>
            <p>{t("categories:schema.color.lookHowThisColorFitsInThePalette")}</p>
            <div className="containerH" style={{gap: 8, flexWrap: "wrap"}}>
              {categories
                .filter((c) => category._id !== c._id)
                .map((c) => (
                  <ColorDot color={c.color} key={c._id} />
                ))}
              <ExampleCurrentCategory />
            </div>
          </div>
        </CardElement>
      </div>
    </EditPage>
  );
}

export default withFallBackOnUrlId(CategoryEdit);
