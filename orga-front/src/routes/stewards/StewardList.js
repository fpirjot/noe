import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {stewardsActions, stewardsSelectors} from "../../features/stewards.js";
import {ListPage} from "../../common/pages/ListPage";
import {listOfClickableElements} from "../../utils/listOfClickableElements";
import {GetPdfPlanningButton} from "../../common/components/buttons/GetPdfPlanningButton";
import {
  getRegistrationsLinkedToSteward,
  getStewardAndLinkedUsersNames,
} from "./atoms/stewardsRegistrationsUtils";
import {Link} from "react-router-dom";
import {personName} from "../../utils/utilities";
import {registrationsActions, registrationsSelectors} from "../../features/registrations";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {UserOutlined} from "@ant-design/icons";
import {useColumnsBlacklistingSelect} from "../../common/hooks/useColumnsBlacklistingSelect";
import {currentProjectSelectors} from "../../features/currentProject";
import {useLoadList} from "../../common/hooks/useLoadList";
import {WaitingInvitationTag} from "../../common/components/WaitingInvitationTag";
import {getParticipantsCustomFieldsColumns} from "../../utils/columns/getParticipantsCustomFieldsColumns";
import {sorter} from "../../utils/sorters";
import {editableCellColumn} from "../../common/components/EditableCell";

function StewardList() {
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const stewards = useSelector(stewardsSelectors.selectList);
  const sessions = useSelector(sessionsSelectors.selectList);
  const registrations = useSelector(registrationsSelectors.selectList);
  const [filterBlacklistedColumns, ColumnsBlacklistingSelector] = useColumnsBlacklistingSelect({
    endpoint: "stewards",
  });

  // Not counting sessions that are desynchronized
  const getNumberOfSessionsForSteward = (steward) =>
    sessions.filter((session) => session.stewards.find((s) => s._id === steward._id)).length;

  const columns = [
    {
      width: 48,
      render: (text, record) => (
        <GetPdfPlanningButton
          customFileName={() => getStewardAndLinkedUsersNames(record, registrations)}
          elementsActions={stewardsActions}
          id={record._id}
          noText
          noTooltip
        />
      ),
    },
    {
      ...editableCellColumn({
        title: "Prénom",
        dataIndex: "firstName",
        type: "text",
        placeholder: "prénom",
        required: true,
        elementsActions: stewardsActions,
      }),
      sorter: (a, b) => sorter.text(a.firstName, b.firstName),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: "Nom",
        dataIndex: "lastName",
        type: "text",
        placeholder: "nom",
        elementsActions: stewardsActions,
      }),
      sorter: (a, b) => sorter.text(a.lastName, b.lastName),
      searchable: true,
    },
    {
      ...editableCellColumn({
        title: "Téléphone",
        dataIndex: "phoneNumber",
        type: "phoneNumber",
        elementsActions: stewardsActions,
      }),
      searchable: true,
    },
    {
      title: "Participant⋅e lié⋅e",
      dataIndex: "registrationsLinkedToSteward",
      sorter: (a, b) =>
        sorter.text(
          listOfClickableElements(getRegistrationsLinkedToSteward(a, registrations), (r) =>
            personName(r.user)
          ),
          listOfClickableElements(getRegistrationsLinkedToSteward(b, registrations), (r) =>
            personName(r.user)
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => personName(r.user))
          .join(" "),
      render: (text, record) =>
        listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => (
            <Link to={`./../participants/${el._id}`} key={index}>
              {el.invitationToken && <WaitingInvitationTag />}
              {personName(el.user)}
            </Link>
          )
        ),
    },
    {
      title: "Email participant⋅e",
      dataIndex: "registrationsLinkedToStewardEmail",
      sorter: (a, b) =>
        sorter.text(
          listOfClickableElements(
            getRegistrationsLinkedToSteward(a, registrations),
            (r) => r.user.email
          ),
          listOfClickableElements(
            getRegistrationsLinkedToSteward(b, registrations),
            (r) => r.user.email
          )
        ),
      searchable: true,
      searchText: (record) =>
        getRegistrationsLinkedToSteward(record, registrations)
          .map((r) => r.user.email)
          .join(" "),
      render: (text, record) =>
        listOfClickableElements(
          getRegistrationsLinkedToSteward(record, registrations),
          (el, index) => el.user.email
        ),
    },
    ...getParticipantsCustomFieldsColumns({
      project: currentProject,
      nameSuffix: "participant·e",
      getAllFormAnswers: (record) =>
        getRegistrationsLinkedToSteward(record, registrations).map((r) => r.formAnswers),
    }),
    {
      title: "Nb. sessions",
      render: (text, record) => getNumberOfSessionsForSteward(record) || "",
      sorter: (a, b) =>
        sorter.number(getNumberOfSessionsForSteward(a), getNumberOfSessionsForSteward(b)),
      width: 125,
    },
  ];

  useLoadList(() => {
    dispatch(stewardsActions.loadList());
    dispatch(registrationsActions.loadList());
    dispatch(sessionsActions.loadList());
  });

  return (
    <ListPage
      i18nNs="stewards"
      icon={<UserOutlined />}
      elementsActions={stewardsActions}
      settingsDrawerContent={<ColumnsBlacklistingSelector columns={columns} />}
      searchInFields={["firstName", "lastName"]}
      customButtons={
        <GetPdfPlanningButton
          customFileName={() => "Tout·es les encadrant·es"}
          elementsActions={stewardsActions}
          id="all"
          tooltip={"Exporter tous les plannings d'encadrant·es en une seule fois."}
        />
      }
      multipleActionsButtons={({selectedRowKeys, setSelectedRowKeys}) => (
        <GetPdfPlanningButton
          customFileName={() => "Sélection d'encadrant·es"}
          elementsActions={stewardsActions}
          id={selectedRowKeys}
          noText
          onClick={() => setSelectedRowKeys([])}
          tooltip={"Exporter les plannings des encadrant·es sléectionné·es en une seule fois."}
        />
      )}
      columns={filterBlacklistedColumns(columns)}
      dataSource={stewards}
      groupEditable
      groupImportable
    />
  );
}

export default StewardList;
