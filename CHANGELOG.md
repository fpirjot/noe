![NOÉ](.gitlab/images/readme-banner.jpg)

# CHANGELOG

Il peut être trouvé ici : [CHANGELOG NOÉ](https://bit.ly/changelog-noeappio-gitlab)

# MODULE UPGRADES HISTORY

## Ant Design (antd)

### **12/07/2023**

**Rollback from 5.5.2 to 5.1.7** only in inscription-front because of an issue with the subscribe button
when the subscribe button is in warning mode 