import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {BrowserRouter} from "react-router-dom";
import {App, ConfigProvider} from "antd";
import frFR from "antd/es/locale/fr_FR";

import "./app/services/i18n";

import {persistor, store} from "./app/store";

import "./style/App.less";

import NoeApp from "./App";
import {OFFLINE_MODE, useInitializeOfflineModeOnStart} from "./utils/offlineModeUtilities";
import {OnlineStatusProvider} from "./common/hooks/useOnlineStatus";
import {WindowDimensionsProvider} from "./common/hooks/useWindowDimensions";
import {initSentry} from "./app/services/sentry";
import {useTranslation} from "react-i18next";
import {UserTourProvider} from "./utils/userTours/userTourUtilities";
import {useBrowserUpdate} from "./app/services/browserUpdate";
import {useTheme} from "./common/hooks/useTheme";

initSentry();

const Root: React.FC = () => {
  const {i18n} = useTranslation();
  const theme = useTheme();
  useInitializeOfflineModeOnStart();
  useBrowserUpdate();

  return (
    <Suspense fallback={null}>
      <BrowserRouter>
        <WindowDimensionsProvider>
          <OnlineStatusProvider>
            <ConfigProvider locale={i18n.language === "fr" ? frFR : undefined} theme={theme}>
              <App>
                <UserTourProvider>
                  <NoeApp />
                </UserTourProvider>
              </App>
            </ConfigProvider>
          </OnlineStatusProvider>
        </WindowDimensionsProvider>
      </BrowserRouter>
    </Suspense>
  );
};

ReactDOM.render(
  <Provider store={store}>
    {OFFLINE_MODE ? (
      <PersistGate loading={null} persistor={persistor}>
        <Root />
      </PersistGate>
    ) : (
      <Root />
    )}
  </Provider>,
  document.getElementById("root")
);
