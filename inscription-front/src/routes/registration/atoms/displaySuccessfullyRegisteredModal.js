import {Button, Modal, Result} from "antd";
import {useNavigate} from "react-router-dom";
import {useTranslation} from "react-i18next";
import React from "react";
import {registrationsActions, registrationsSelectors} from "../../../features/registrations";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectSelectors} from "../../../features/currentProject";
import {SvgImageContainer} from "../../../common/components/SvgImageContainer";
import {ReactComponent as SvgPartying} from "../../../app/images/undraw/undraw_partying.svg";

export default function RegistrationSuccessfulModal() {
  const {t} = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
  const registrationSuccessfulModalOpen = useSelector(
    registrationsSelectors.selectRegistrationSuccessfulModalOpen
  );

  const closeModal = () => dispatch(registrationsActions.setRegistrationSuccessfulModalOpen(false));

  // Same code as in ProjectLayout
  const openingState = currentProject?.openingState;
  const userIsLinkedToSteward = currentRegistration?.steward !== undefined;
  const openingStateAllowsAccessToSessions =
    (userIsLinkedToSteward && openingState === "registerForStewardsOnly") ||
    openingState === "registerForAll";
  const userHasAccessToSecretSchedule = !currentProject.secretSchedule;

  const giveAccessToSessionsPage =
    userHasAccessToSecretSchedule && openingStateAllowsAccessToSessions;

  return (
    <Modal open={registrationSuccessfulModalOpen} footer={null} onCancel={closeModal} maskClosable>
      <Result
        status="success"
        icon={<SvgImageContainer svg={SvgPartying} width={"90%"} />}
        title={t("registrations:messages.bravoYouAreRegistered.title")}
        subTitle={
          <div className={"containerV"} style={{marginTop: 26, gap: 20, alignItems: "center"}}>
            {giveAccessToSessionsPage ? (
              <>
                {t("registrations:messages.bravoYouAreRegistered.subTitle")}
                <Button
                  type={"primary"}
                  onClick={() => {
                    navigate("./../sessions/all");
                    closeModal();
                  }}>
                  {t("sessions:list.subscribedSplashScreen.subscribeToSessionsButton")}
                </Button>
              </>
            ) : (
              <>
                {t("registrations:messages.bravoYouAreRegistered.comeBackWhenSubscriptionsAreOpen")}
                <Button type={"primary"} onClick={closeModal}>
                  {t("common:ok")}
                </Button>
              </>
            )}
          </div>
        }
      />
    </Modal>
  );
}
