import {Trans} from "react-i18next";
import {textLogo} from "../../../app/configuration";
import {DiscordLink, EmailLink} from "../../../common/components/NOESocialIcons";
import React from "react";

export const NoeOpenSourceParagraph = () => (
  <div style={{marginTop: 14, opacity: 0.6}}>
    <Trans
      ns="common"
      i18nKey="noeOpenSourceParagraph"
      components={{
        noeTextLogo: (
          <img src={textLogo} height="18" style={{display: "inline-block", marginBottom: 4}} />
        ),
        discordLink: <DiscordLink showTitle />,
        mailToLink: <EmailLink showTitle />,
      }}
    />
  </div>
);
