import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {CalendarFilled, CalendarOutlined, PlaySquareOutlined} from "@ant-design/icons";
import {currentUserActions, currentUserSelectors} from "../features/currentUser.js";
import {LayoutStructure} from "../common/layout/LayoutStructure";
import {useRedirectToProjectIfOffline} from "../utils/offlineModeUtilities";
import {instanceName, URLS} from "../app/configuration";
import {useTranslation} from "react-i18next";
import {MenuLayout} from "../common/components/Menu";
import {useBrowserTabTitle} from "../common/hooks/useBrowserTabTitle";
import {Outlet} from "react-router";
import {setAppTheme} from "../common/layout/DynamicProjectThemeProvider";

export default function MainLayout({page}) {
  const {t} = useTranslation();
  const dispatch = useDispatch();

  const authenticatedUser = useSelector(currentUserSelectors.selectAuthenticatedUser);

  useRedirectToProjectIfOffline();

  // ****** TAB TITLE ******

  useBrowserTabTitle(instanceName, page, {
    projects: t("projects:labelMyProjects"),
    "public-projects": t("projects:labelPublicProjects"),
  });

  // ***** RESET STUFF ******

  // On Main layout, reset app theme
  setAppTheme(undefined);

  // If we are on the main page, reset the connection as another user automatically
  useEffect(() => {
    if (authenticatedUser._id) dispatch(currentUserActions.changeConnectedAsUser(undefined));
  }, [authenticatedUser, dispatch]);

  // ****** SIDE MENU ******

  const menu = {
    top: (
      <MenuLayout
        rootNavUrl={"/"}
        className={"userTourProjectsLists"}
        selectedItem={page}
        items={[
          // User events
          authenticatedUser._id && {
            label: t("projects:labelMyProjects"),
            key: "projects",
            icon: <CalendarOutlined />,
          },

          // Public events
          {
            label: t("projects:labelPublicProjects"),
            key: "public-projects",
            icon: <CalendarFilled />,
          },
        ]}
      />
    ),
    footer: (
      <MenuLayout
        selectedItem={page}
        items={[
          // Orga front
          authenticatedUser.superAdmin && {
            label: t("common:pagesNavigation.orgaFront"),
            url: `${URLS.ORGA_FRONT}/projects?no-redir`,
            icon: <PlaySquareOutlined />,
          },
        ]}
      />
    ),
  };

  return (
    <LayoutStructure title={instanceName} menu={menu} profileUser={authenticatedUser}>
      <Outlet />
    </LayoutStructure>
  );
}
