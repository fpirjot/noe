import React, {Suspense, useCallback, useEffect, useMemo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Modal, Popover, Tooltip} from "antd";
import {CalendarOutlined, EnvironmentOutlined, ReloadOutlined} from "@ant-design/icons";
import {ViewState} from "@devexpress/dx-react-scheduler";
import {
  Appointments,
  CurrentTimeIndicator,
  DateNavigator,
  DayView,
  Scheduler,
  Toolbar,
} from "@devexpress/dx-react-scheduler-material-ui";
import SessionShow from "./SessionShow";
import SessionShowSmall from "./atoms/SessionShowSmall";
import {sessionsActions, sessionsSelectors} from "../../features/sessions.js";
import {currentProjectSelectors} from "../../features/currentProject.js";
import {slotOverlapsAnOtherOne} from "../../utils/slotsUtilities";
import {navbarHeight} from "../../utils/viewUtilities";
import {useWindowDimensions} from "../../common/hooks/useWindowDimensions";
import {
  CELL_DURATION_MINUTES,
  DEFAULT_AGENDA_PARAMS,
  getSessionElementsFromListInSlot,
  slotNumberString,
} from "../../utils/agendaUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {viewActions} from "../../features/view";
import dayjs from "dayjs";
import {useTranslation} from "react-i18next";
import {Pending} from "../../common/components/Pending";
import {useAgendaParams} from "./agenda/useAgendaParams";
import {DatePickerComponent} from "../../common/inputs/DateTimeInput";
import {NavButton} from "./agenda/NavButton";
import {useGetTimeScaleLabelComponent} from "./agenda/useGetTimeScaleLabelComponent";
import {AgendaControls} from "./agenda/AgendaControls";
import {AgendaMenuDrawer} from "./agenda/AgendaMenuDrawer";
import {useSessionsViewUrl} from "./atoms/useSessionsViewUrl";
import {useLoadSessionsView} from "./atoms/useLoadSessionsView";
import {ChangeSessionViewButtons} from "./atoms/ChangeSessionViewButtons";
import {SessionFilterControls} from "./atoms/SessionFilterControls";
import {sessionShouldBeShown} from "./atoms/sessionShouldBeShown";
import {ErrorBoundary} from "../../common/components/ErrorBoundary";
import {midnightDateManager} from "./atoms/midnightDateManager";
import {synchronizeTicksOnSideOfAgenda} from "./atoms/synchronizeTicksOnSideOfAgenda";
import {getSessionMetadata} from "../../utils/getSessionMetadata";
import {capitalize} from "../../utils/stringUtilities";
import {useAgendaDisplayHours} from "./agenda/useAgendaDisplayHours";

import {getFullSessionName} from "../../utils/sessionsUtilities";
import {timeFormatter} from "../../utils/formatters";

function SessionAgenda() {
  const {t} = useTranslation();
  const dispatch = useDispatch();
  const viewUrl = useSessionsViewUrl();

  // This component is responsive with three layouts:
  // - large screens: all the filter controls are in the agenda toolbar
  // - medium screens: the controls are above the agenda toolbar
  // - mobile screens: the filter controls are located in a drawer that can be opened with a button in the agenda toolbar
  const {isMobileView, windowWidth, windowHeight} = useWindowDimensions();
  const isLargeScreen = windowWidth > 1350;

  // Substract the height from the navbar and the session filter toolbar if it is displayed (for medium screens)
  const schedulerHeight =
    windowHeight -
    navbarHeight() -
    (document.querySelector(".session-filter-toolbar")?.offsetHeight || 0);

  // *********************** //
  // **** DATA FETCHING **** //
  // *********************** //

  const slotsFiltered = useSelector(sessionsSelectors.selectSlotsListFiltered);

  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  useLoadSessionsView(viewUrl, "agenda", dispatch);

  const [
    {
      cellDisplayHeight,
      numberOfDaysDisplayed,
      currentAgendaDate,
      slotsOnEachOther,
      forcedDisplayHours,
    },
  ] = useAgendaParams();

  const [sessionShowModalOpen, setSessionShowModalOpen] = useState(false);

  const setAgendaParams = (params) =>
    dispatch(viewActions.changeAgendaParams({projectId: currentProject._id, params}));

  // ************************ //
  // **** MEMOIZED DATA **** //
  // ************************ //
  // everything that can easily be computed only once //

  const agendaDisplayHours = useAgendaDisplayHours(forcedDisplayHours);

  // The appointments (ie. the slots from all the sessions, formatted in the way that the Agenda accepts it
  const appointments = useMemo(() => {
    // Only show the slots that should be shown
    const slotsShown = slotsFiltered?.filter((s) =>
      sessionShouldBeShown(viewUrl, s.session, currentRegistration)
    );
    // Build metadata
    return slotsShown?.map((s) =>
      midnightDateManager.dataToDisplay({
        ...s,
        id: s._id,
        title:
          slotNumberString(s) + getFullSessionName(s.session, currentProject.sessionNameTemplate),
        startDate: s.start,
        endDate: s.end,
        location: getSessionElementsFromListInSlot("places", s, (el) => el.name).join(", "),
        placeId: getSessionElementsFromListInSlot("places", s, (el) => el._id),
        color: s.session.activity?.category?.color,
      })
    );
  }, [slotsFiltered, viewUrl, currentRegistration]);

  // ************************* //
  // ***** ASYNC EFFECTS ***** //
  // ************************* //

  // APPEARANCE TWEAKS

  useEffect(
    () => synchronizeTicksOnSideOfAgenda("timescale-cell", cellDisplayHeight),
    [cellDisplayHeight, agendaDisplayHours, currentAgendaDate]
  );

  // ************************* //
  // ** APPOINTMENTS BLOCKS ** //
  // ************************* //

  // Appointment popover when we hover the block
  const SessionPreview = ({session, onClick}) => (
    <SessionShowSmall
      onClick={onClick}
      style={{width: 350}}
      session={session}
      onUnsubscribe={() => dispatch(sessionsActions.unsubscribe(session._id))}
      onSubscribe={() => dispatch(sessionsActions.subscribe(session._id))}
    />
  );

  // Generic appointment component
  const CustomAppointment = ({
    style,
    children,
    component: Component,
    opacity,
    data,
    ...restProps
  }) => (
    // Component variable must have an uppercase letter to be considered as a component by React
    <Component
      {...restProps}
      data={data}
      className={` ${restProps.className}`}
      style={{
        ...style,
        color: "white",
        backgroundColor: "var(--colorBgContainer)",
        borderRadius: 8,
        padding: 3,
        transition: "border 0.4s ease-in-out, outline 0.4s ease-in-out",
      }}>
      <div
        style={{
          backgroundColor: data.color,
          height: "calc(100% + 20px)",
          width: "calc(100% + 20px)",
          opacity,
          transition: "opacity 0.4s ease-in-out",
          transitionDelay: "0.4s",
          margin: -10,
          padding: 10,
        }}
        className={"max-opacity-on-hover"}>
        <div style={{marginLeft: 8, marginTop: 5}}>
          <div
            style={{
              lineHeight: 1.5,
              whiteSpace: "normal",
              overflow: "hidden",
              display: "-webkit-box",
              WebkitLineClamp: "3",
              WebkitBoxOrient: "vertical",
              fontWeight: "bold",
            }}>
            {data.title}
          </div>
          <div style={{overflow: "hidden", whiteSpace: "nowrap", paddingTop: 1}}>
            {timeFormatter.timeRange(data.start, data.end)}
          </div>

          {currentProject.usePlaces && data?.location.length > 0 && (
            <div style={{marginTop: 4, lineHeight: 1.2, opacity: 0.8}}>
              <EnvironmentOutlined /> {data?.location}
            </div>
          )}
        </div>
      </div>
    </Component>
  );

  // Renders the appointment block in the agenda view.
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and 30% after
  const Appointment = useCallback(
    React.memo(
      ({style, ...props}) => {
        const session = props.data.session;

        const {
          shouldBeDimmed,
          inConflictWithRegistrationDates,
          shouldBeAddedAutomagicallyInUsersPlanning,
        } = getSessionMetadata(session, currentRegistration);

        if (inConflictWithRegistrationDates) props.data.title = "⚠️ " + props.data.title;

        const openSessionShowModal = () => {
          // Load editing state before displaying SessionShow component
          dispatch(sessionsActions.loadEditing(session._id)).then(() =>
            setSessionShowModalOpen(true)
          );
        };

        // Add outline for sessions where we are steward or subscribed
        if (session.isSteward || session.subscribed || shouldBeAddedAutomagicallyInUsersPlanning) {
          // Blue for session as a steward, Green for sessions subscribed
          const borderColor = session.isSteward
            ? "rgb(75, 104, 252)"
            : session.subscribed
            ? "rgb(82, 196, 26)"
            : shouldBeAddedAutomagicallyInUsersPlanning && "rgb(150,150,150)";
          style = {
            ...style,
            border: "5px solid " + borderColor,
            outline: "1.6px solid var(--colorBgContainer)",
            outlineOffset: -6,
          };
        }

        const appointmentComp = (
          <CustomAppointment
            component={Appointments.Appointment}
            style={{
              // Override default border color
              borderColor: "var(--colorBgContainer)",
              ...style,
            }}
            opacity={shouldBeDimmed ? 0.4 : 1}
            onClick={openSessionShowModal}
            {...props}
          />
        );

        return isMobileView ? (
          appointmentComp
        ) : (
          // zIndex=100 to put the popover above the Sidebar, but below the SessionShow modal
          <Popover
            zIndex={100}
            content={
              <Suspense fallback={null}>
                <SessionPreview session={session} onClick={openSessionShowModal} />
              </Suspense>
            }
            mouseEnterDelay={0.6}>
            {appointmentComp}
          </Popover>
        );
      },
      (prevProps, nextProps) =>
        JSON.stringify(prevProps.data.session) === JSON.stringify(nextProps.data.session)
    ),
    // Don't put the currentProject as a dependency cause it's not necessary and causes a whole re-render for
    // nothing on each session subscribe/unsubscribe
    []
  );

  // ************************** //
  // ***** SUB-COMPONENTS ***** //
  // ************************** //

  // Renders a cell of the Agenda day view
  // useCallback(React.memo()) helps avoiding approx 300% of re-rendering on start and cuts all the rendering after that
  const TimeTableCell = useCallback(
    React.memo((props) => {
      // Get the cell start and end hour
      const cellSlot = {start: props.startDate, end: props.endDate};
      const overlapsWithElements = [];

      // Check if the cell is out of project availabilities
      if (!slotOverlapsAnOtherOne(cellSlot, currentProject.availabilitySlots)) {
        overlapsWithElements.push("project");
      }

      // Add the appropriate classes to the cell, so it can be painted if there are some overlaps
      const className =
        overlapsWithElements.length > 0
          ? `${overlapsWithElements.join("-")}-disabled-date`
          : undefined;
      return (
        <DayView.TimeTableCell
          {...props}
          style={{height: cellDisplayHeight}}
          className={className}
        />
      );
    }),
    [currentProject.availabilitySlots, cellDisplayHeight]
  );

  const TimeScaleLabelComponent = useGetTimeScaleLabelComponent(cellDisplayHeight);

  const DayScaleCell = (props) => (
    <DayView.DayScaleCell
      style={{cursor: "pointer"}}
      onDoubleClick={() =>
        setAgendaParams({
          currentAgendaDate: props.startDate.getTime(),
          numberOfDaysDisplayed: 1,
        })
      }
      {...props}
    />
  );

  // Agenda date navigation arrows and selector
  const DateNavigatorRoot = useCallback(
    React.memo((props) => {
      const {isMobileView} = useWindowDimensions();
      const [datePickerOpen, setDatePickerOpen] = useState(false);
      const [{currentAgendaDate, numberOfDaysDisplayed}] = useAgendaParams();
      const currentAgendaDateObject = dayjs(currentAgendaDate);
      const goToProjectStartDate =
        dayjs(currentProject.start).isAfter(dayjs()) || dayjs(currentProject.end).isBefore(dayjs());

      const dateTextStart = capitalize(
        currentAgendaDateObject.format(isMobileView ? "MMM" : "MMMM")
      );
      const dateTextEnd = capitalize(
        currentAgendaDateObject
          .add(numberOfDaysDisplayed - 1, "day")
          .format(isMobileView ? "MMM" : "MMMM")
      );
      const dateText =
        isMobileView || dateTextStart === dateTextEnd
          ? dateTextStart
          : `${dateTextStart} – ${dateTextEnd}`;

      return (
        <div
          ref={props.rootRef}
          className="containerH buttons-container"
          style={{flexShrink: 0, alignItems: "center"}}>
          <div>
            <NavButton type="back" onClick={() => props.onNavigate("back")} />
            <NavButton type="forward" onClick={() => props.onNavigate("forward")} />
          </div>

          <div>
            <DatePickerComponent
              // Make the datepicker input invisible, only show the popup and control the picker with a button
              style={{visibility: "hidden", width: 0, paddingLeft: 0, paddingRight: 0}}
              value={currentAgendaDateObject}
              showTime={false}
              showToday={goToProjectStartDate}
              disableDatesIfOutOfProject
              popupClassName={isMobileView && "shift-date-picker-to-left"}
              disableDatesBeforeNow={false}
              open={datePickerOpen}
              onChange={(value) => {
                setDatePickerOpen(false);
                setAgendaParams({currentAgendaDate: value.unix() * 1000});
              }}
              onOpenChange={setDatePickerOpen}
            />
            <Button onClick={() => setDatePickerOpen(true)}>{dateText}</Button>
          </div>

          <Tooltip
            title={
              goToProjectStartDate
                ? t("sessions:agenda.dateNavigator.goToBeginningOfEvent")
                : t("sessions:agenda.dateNavigator.goToToday")
            }>
            <Button
              type="link"
              icon={<CalendarOutlined />}
              onClick={() =>
                setAgendaParams({
                  currentAgendaDate: goToProjectStartDate
                    ? dayjs(currentProject.start).unix() * 1000
                    : dayjs().unix() * 1000,
                })
              }>
              {!isMobileView &&
                (goToProjectStartDate
                  ? t("sessions:agenda.dateNavigator.beginning")
                  : t("sessions:agenda.dateNavigator.today"))}
            </Button>
          </Tooltip>
        </div>
      );
    }),
    []
  );

  // Where all the agenda controls are
  const AgendaControlsBar = useCallback(
    React.memo(() => (
      <AgendaControls
        settingsDrawer={<AgendaMenuDrawer buttonStyle={{flexGrow: 0}} />}
        isLargeScreen={isLargeScreen}
      />
    )),
    [isLargeScreen]
  );

  // The modal is always visible but hidden with a {sessionShowModal && ...} condition in the render.
  // it improves by 50% the amount of re-renderings.
  const SessionShowModal = useCallback(
    React.memo(() => (
      <Modal
        open
        onCancel={() => {
          dispatch(sessionsActions.setEditing({}));
          setSessionShowModalOpen(false);
        }}
        width={"min(90vw, 1200px)"}
        footer={false}
        centered>
        <div className="modal-page-content">
          <SessionShow asModal navigatePathRoot={"./../../"} />
        </div>
      </Modal>
    )),
    []
  );

  // ************************** //
  // **** RENDER FUNCTION ***** //
  // ************************** //

  // If everything isn't loaded yet, stop here, no need to go further. Display a pending spinner.
  return currentProject._id !== undefined && slotsFiltered !== undefined ? (
    <div className="full-width-content scheduler-wrapper">
      {/*Session filter above the agenda toolbar for medium screens*/}
      {!isLargeScreen && !isMobileView && (
        <div className="session-filter-toolbar" style={{marginLeft: 20, marginRight: 20}}>
          <SessionFilterControls agendaView style={{margin: 0, padding: 0}} />
        </div>
      )}
      {/*Error boundary to return back to normal state if it is buggy with slotsOnEachOther*/}
      <ErrorBoundary
        onError={() => setAgendaParams(DEFAULT_AGENDA_PARAMS)}
        extra={
          <Button
            icon={<ReloadOutlined />}
            onClick={() => {
              setAgendaParams(DEFAULT_AGENDA_PARAMS);
              window.location.reload();
            }}>
            {t("common:errorBoundary.reloadPage")}
          </Button>
        }>
        {/*The big agenda component*/}
        <Scheduler data={appointments} locale="fr-FR" height={schedulerHeight}>
          {/*State management*/}
          <ViewState
            defaultCurrentDate={currentAgendaDate}
            currentDate={currentAgendaDate}
            onCurrentDateChange={(date) => setAgendaParams({currentAgendaDate: date.getTime()})}
            defaultCurrentViewName="Day"
          />

          {/*Views management*/}
          <DayView
            timeTableCellComponent={TimeTableCell}
            timeScaleLabelComponent={TimeScaleLabelComponent}
            startDayHour={agendaDisplayHours.start}
            endDayHour={agendaDisplayHours.end}
            cellDuration={CELL_DURATION_MINUTES}
            intervalCount={numberOfDaysDisplayed}
            dayScaleCellComponent={DayScaleCell}
          />

          {/*Other tools*/}
          <Toolbar
            flexibleSpaceComponent={AgendaControlsBar} /*The toolbar with all the controls*/
          />
          <DateNavigator
            rootComponent={DateNavigatorRoot} /*The controls to navigate to different days*/
          />
          <Appointments
            appointmentComponent={Appointment} /*Displays the appointments on the agenda*/
            placeAppointmentsNextToEachOther={!slotsOnEachOther}
          />

          <CurrentTimeIndicator />
        </Scheduler>
      </ErrorBoundary>

      {/*The SessionShow modal*/}
      {sessionShowModalOpen && <SessionShowModal />}

      <ChangeSessionViewButtons viewMode="agenda" />
    </div>
  ) : (
    <Pending />
  );
}

export default SessionAgenda;
