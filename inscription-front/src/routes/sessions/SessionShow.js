import {Alert, Button, Card, List, Tag, Tooltip} from "antd";
import React from "react";
import {CardElement} from "../../common/components/CardElement";
import {FormElement} from "../../common/inputs/FormElement";
import {TableElement} from "../../common/components/TableElement";
import {useDispatch, useSelector} from "react-redux";
import {currentProjectSelectors} from "../../features/currentProject";
import {sessionsActions, sessionsSelectors} from "../../features/sessions";
import {stewardsActions} from "../../features/stewards";
import {activitiesActions} from "../../features/activities";
import {placesActions} from "../../features/places";
import {
  ClockCircleOutlined,
  EnvironmentOutlined,
  ExclamationCircleOutlined,
  FullscreenOutlined,
  TeamOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {getFullSessionName, getVolunteeringCoefficient} from "../../utils/sessionsUtilities";
import {registrationsSelectors} from "../../features/registrations";
import {personName} from "../../utils/utilities";
import TextDisplayer from "../../common/components/TextDisplayer";
import {t} from "i18next";
import {URLS} from "../../app/configuration";
import {PageHeading} from "../../common/components/PageHeading";
import {getSessionSubscription} from "../../utils/registrationsUtilities";
import {getSessionInfoAlert} from "./atoms/getSessionInfoAlert";
import {SubscribeUnsubscribeButton} from "./atoms/SubscribeUnsubscribeButton";
import {CategoryTagWithVolunteeringMajoration} from "./atoms/CategoryTagWithVolunteeringMajoration";
import {SessionFilling} from "./atoms/SessionFilling";
import {useLoadEditing} from "../../common/hooks/useLoadEditing";
import {DisplayInput} from "../../common/inputs/DisplayInput";
import {getSessionMetadata} from "../../utils/getSessionMetadata";
import {generateRegistrationsColumns} from "../../utils/columns/generateRegistrationsColumns";
import {generateSubscriptionInfo} from "../../utils/columns/generateSubscriptionInfo";
import {GroupSmsButtons} from "../../common/components/buttons/GroupSmsButtons";
import {Link, useNavigate, useParams} from "react-router-dom";
import {sorter} from "../../utils/sorters";
import {dateFormatter, timeFormatter} from "../../utils/formatters";
import {generateSessionNoShowColumn} from "../../utils/columns/generateSessionNoShowColumn";

function SessionShow({navigatePathRoot, asModal = false}) {
  const navigate = useNavigate();
  const {id} = useParams();
  const dispatch = useDispatch();
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  const session = useSelector(sessionsSelectors.selectEditing);
  const sessions = useSelector(sessionsSelectors.selectList);
  const currentRegistration = useSelector(registrationsSelectors.selectCurrent);

  useLoadEditing(
    sessionsActions,
    id,
    () => {
      dispatch(stewardsActions.loadList());
      dispatch(activitiesActions.loadList());
      currentProject.usePlaces && dispatch(placesActions.loadList());
      dispatch(sessionsActions.loadList());
    },
    undefined,
    asModal // Don't clean if displayed as a modal so there is no glitch when making the page full screen
  );

  // Prevents rendering as long as we don't have crucial info to display
  if (!session._id) return null;

  const sessionSubscription = getSessionSubscription(currentRegistration, session);

  const sameTimeSessionsRegistered = session.sameTimeSessions?.filter(
    (sts) => sts.sameTimeSessionRegistered
  );

  const otherSessionsOfSameActivityRegisterable = sessions.filter((otherSession) => {
    const notCurrentSession = session._id !== otherSession._id;
    const hasSameActivity = session.activity?._id === otherSession.activity._id;
    return notCurrentSession && hasSameActivity;
  });

  const subscriptionInfoColumn = {
    title: t("sessions:schema.subscriptionInfo.label"),
    render: (text, record) =>
      generateSubscriptionInfo(
        record,
        getSessionSubscription(record, session),
        currentProject.slug || currentProject._id
      ),
    sorter: (a, b) =>
      sorter.date(
        getSessionSubscription(a, session).updatedAt,
        getSessionSubscription(b, session).updatedAt
      ),
  };

  const {
    isSteward,
    sessionIsFull,
    participantIsNotAvailable,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    registrationIncomplete,
    volunteeringBeginsSoon,
    inConflictWithRegistrationDates,
    shouldBeAddedAutomagicallyInUsersPlanning,
  } = getSessionMetadata(session, currentRegistration);

  const infoAlert = getSessionInfoAlert(
    sessionSubscription,
    isSteward,
    currentProject.blockSubscriptions,
    registrationIncomplete,
    participantIsNotAvailable,
    sessionIsFull,
    alreadyStewardOnOtherSession,
    alreadySubscribedToOtherSession,
    inConflictWithRegistrationDates
  );

  const onSubscribe = () => dispatch(sessionsActions.subscribe());
  const onUnsubscribe = () => dispatch(sessionsActions.unsubscribe());

  return (
    <div className="page-container containerV fade-in">
      <PageHeading
        backButton={!asModal}
        className="page-header"
        title={getFullSessionName(session, currentProject.sessionNameTemplate)}
        customButtons={
          <>
            {asModal && (
              <Tooltip title={t("sessions:show.showFullPage")}>
                <Button
                  type="link"
                  style={{flexGrow: 0}}
                  onClick={() => navigate(`${navigatePathRoot}${session._id}`)}
                  icon={<FullscreenOutlined />}
                />
              </Tooltip>
            )}
            {session.participants && (
              <GroupSmsButtons registrations={session.participants} session={session} />
            )}
            <SubscribeUnsubscribeButton
              onUnsubscribe={onUnsubscribe}
              onSubscribe={onSubscribe}
              sessionIsFull={sessionIsFull}
              isSteward={isSteward}
              sessionSubscription={sessionSubscription}
              registrationIncomplete={registrationIncomplete}
              tooltipMessage={infoAlert?.message}
              shouldBeAddedAutomagicallyInUsersPlanning={shouldBeAddedAutomagicallyInUsersPlanning}
              alreadySubscribedToOtherSession={alreadySubscribedToOtherSession}
              alreadyStewardOnOtherSession={alreadyStewardOnOtherSession}
              participantIsNotAvailable={participantIsNotAvailable}
              currentProject={currentProject}
              volunteeringBeginsSoon={volunteeringBeginsSoon}
            />
          </>
        }
      />

      <FormElement>
        <div className="summary">
          <div
            className="containerH"
            style={{
              alignItems: "baseline",
              flexWrap: "wrap",
              marginTop: 15,
              rowGap: 8,
              marginBottom: 26,
            }}>
            <CategoryTagWithVolunteeringMajoration
              volunteeringCoefficient={getVolunteeringCoefficient(session)}
              category={session.activity?.category}>
              {session.activity?.category.name}
            </CategoryTagWithVolunteeringMajoration>
            {session.activity?.secondaryCategories?.map((tagName, index) => (
              <Tag key={index}>{tagName}</Tag>
            ))}
          </div>

          {!infoAlert && sessionSubscription && (
            <Alert
              type="success"
              showIcon
              style={{marginBottom: 26}}
              message={t("sessions:show.youAreSubscribed")}
            />
          )}
          {infoAlert && <Alert style={{marginBottom: 26}} {...infoAlert} />}

          {session.activity?.summary?.length > 0 && (
            <CardElement>{session.activity?.summary}</CardElement>
          )}
        </div>
        <div className="container-grid two-thirds-one-third">
          <div style={{overflowY: "auto"}}>
            {session.activity?.description?.length > 0 && (
              <CardElement title={t("sessions:schema.description.label")}>
                <TextDisplayer value={session.activity?.description} />
              </CardElement>
            )}
            {session.participants && (
              <TableElement.WithTitle
                showHeader
                icon={<TeamOutlined />}
                title={t("registrations:label", {count: Infinity})}
                subtitle={t("sessions:show.youSeeThisInfoBecauseYouAre", {
                  role: currentRegistration.role
                    ? t("registrations:roles.orgaOfTheEvent")
                    : t("registrations:roles.stewardOnThisSession"),
                })}
                columns={[
                  ...generateRegistrationsColumns(currentProject),
                  generateSessionNoShowColumn(session, session.participants, dispatch),
                  subscriptionInfoColumn,
                ]}
                onRow={(record) => ({
                  onDoubleClick:
                    currentRegistration.role &&
                    (() => {
                      window.location.href = `${URLS.ORGA_FRONT}/${
                        currentProject.slug || currentProject._id
                      }/participants/${record._id}`;
                    }),
                })}
                dataSource={session.participants}
                pagination
              />
            )}
          </div>
          <div className="container-grid" style={{height: "fit-content"}}>
            <CardElement title={t("sessions:show.info")}>
              <div className="containerV container-grid">
                <DisplayInput icon={<ClockCircleOutlined />} label={t("sessions:show.timeSlot")}>
                  {session.slots.length > 1 && (
                    <Alert
                      message={t("sessions:show.thisSessionHasMultipleSlots", {
                        count: session.slots.length,
                      })}
                      style={{marginBottom: 12}}
                    />
                  )}
                  <List
                    rowKey="_id"
                    dataSource={session.slots}
                    renderItem={(slot) => (
                      <div>{dateFormatter.longDateTimeRange(slot.start, slot.end)}</div>
                    )}
                  />
                </DisplayInput>

                {session.stewards.length > 0 && (
                  <DisplayInput
                    icon={<UserOutlined />}
                    label={t("stewards:label", {count: Infinity})}>
                    <List
                      dataSource={session.stewards}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{personName(item)}</Tooltip>
                        </div>
                      )}
                    />
                  </DisplayInput>
                )}

                {currentProject.usePlaces && session.places.length > 0 && (
                  <DisplayInput label={t("sessions:show.place")} icon={<EnvironmentOutlined />}>
                    <List
                      dataSource={session.places}
                      renderItem={(item) => (
                        <div>
                          <Tooltip title={item.summary}>{item.name}</Tooltip>
                        </div>
                      )}
                    />
                  </DisplayInput>
                )}

                <SessionFilling
                  style={{marginBottom: 0}}
                  computedMaxNumberOfParticipants={session.computedMaxNumberOfParticipants}
                  numberOfParticipants={session.numberParticipants}
                />
              </div>
            </CardElement>

            {sameTimeSessionsRegistered?.length > 0 && (
              <CardElement
                icon={<ExclamationCircleOutlined />}
                title={t("sessions:show.conflicts")}
                style={{borderColor: "orange"}}>
                <p>
                  Ce sont les sessions auxquelles vous êtes déjà inscrit⋅e, et qui entrent en
                  conflit car elles se déroulent sur les mêmes heures.
                </p>
                <List
                  dataSource={sameTimeSessionsRegistered}
                  renderItem={(stSession) => (
                    <Link
                      to={
                        asModal
                          ? `../../${stSession.sameTimeSession._id}`
                          : `../${stSession.sameTimeSession._id}`
                      }>
                      <Card style={{marginTop: 10, borderColor: "orange"}} hoverable>
                        <strong>{stSession.sameTimeSession.activity.name}</strong>
                        <List
                          rowKey="_id"
                          dataSource={stSession.sameTimeSlots}
                          renderItem={(slot) => (
                            <div>
                              {dateFormatter.longDateTimeRange(
                                slot.sameTimeSlot.start,
                                slot.sameTimeSlot.end
                              )}
                            </div>
                          )}
                        />
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
            {otherSessionsOfSameActivityRegisterable?.length > 0 && (
              <CardElement icon={<ClockCircleOutlined />} title={t("sessions:show.otherTimeSlots")}>
                <List
                  grid
                  style={{margin: -4}}
                  dataSource={otherSessionsOfSameActivityRegisterable}
                  renderItem={(session) => (
                    <Link to={asModal ? `./../../${session._id}` : `./../${session._id}`}>
                      <Card style={{margin: 4}} hoverable>
                        {dateFormatter.longDate(session.start)}
                        <br />
                        {timeFormatter.timeRange(session.start, session.end)}
                      </Card>
                    </Link>
                  )}
                />
              </CardElement>
            )}
          </div>
        </div>
      </FormElement>
    </div>
  );
}

export default SessionShow;
