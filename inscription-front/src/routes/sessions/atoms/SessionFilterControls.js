import {useDispatch, useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../../features/sessions";
import {currentProjectSelectors} from "../../../features/currentProject";
import React, {useEffect, useState} from "react";
import {Input, Select, Tag, Tooltip} from "antd";
import dayjs from "dayjs";
import {registrationsSelectors} from "../../../features/registrations";
import {useTranslation} from "react-i18next";
import {useUserTour} from "../../../utils/userTours/userTourUtilities";
import subscribedSessionsUserTour from "../../../utils/userTours/subscribedSessionsUserTour";
import {DatePickerComponent} from "../../../common/inputs/DateTimeInput";
import {useDebounce} from "../../../common/hooks/useDebounce";
import {useSessionsViewUrl} from "./useSessionsViewUrl";
import {useNavigate} from "react-router-dom";
import {CategoriesFilterSelect} from "./CategoriesFilterSelect";

const {Search} = Input;
const {CheckableTag} = Tag;

export const SessionFilterControls = React.memo(
  ({agendaView = false, isMobileView, className, style, customButtons}) => {
    const navigate = useNavigate();
    const {t} = useTranslation();
    const dispatch = useDispatch();
    const viewUrl = useSessionsViewUrl();

    const viewOnlySubscribed = viewUrl === "subscribed";

    const categories = useSelector(sessionsSelectors.selectCategoriesList);
    const sessions = useSelector(sessionsSelectors.selectList);
    const sessionsFilter = useSelector(sessionsSelectors.selectListFilter);
    const currentProject = useSelector(currentProjectSelectors.selectProject);
    const currentRegistration = useSelector(registrationsSelectors.selectCurrent);
    const participantIsLinkedWithASteward = currentRegistration.steward;

    ///////////////////////////////////////////
    // Filter functions
    ///////////////////////////////////////////

    const updateFilterFn = (name: string) => (value: any) =>
      dispatch(sessionsActions.updateFilteredList({[name]: value}));

    // Cause with debounce, we can't directly synchronize the search bar with the redux state, we need a parallel state like this
    const [searchBarText, setSearchBarText] = useState(sessionsFilter.searchBar);

    const debouncedSearch = useDebounce(updateFilterFn("searchBar"));

    const onChangeSearch = (e) => {
      setSearchBarText(e.target.value); // Set the instant searchtext state
      debouncedSearch(e.target.value); // Then fire the debounced function to update Redux
    };

    ///////////////////////////////////////////
    // Async stuff
    ///////////////////////////////////////////

    useEffect(() => {
      // Deactivate date filtering for agenda view
      if (agendaView && sessionsFilter.fromDateFilter) updateFilterFn("fromDateFilter")(undefined);

      if (viewOnlySubscribed) {
        // Deactivate the subscribed not subscribed select on subscribed viewUrl
        if (sessionsFilter.registeredFilter !== undefined)
          updateFilterFn("registeredFilter")(undefined);

        // Deactivate the available or not filter
        if (sessionsFilter.availableFilter) updateFilterFn("availableFilter")(false);
      }
    }, [agendaView, viewOnlySubscribed]);

    useUserTour("subscribingToSessions", subscribedSessionsUserTour(isMobileView, navigate), {
      shouldShowNow: () => !viewOnlySubscribed && !agendaView && sessions.length > 0,
      deps: [sessions.length],
      delayBeforeShow: 1000,
    });

    return (
      <div
        className={`containerH buttons-container ${className} userTourSubscribedSessionsFilterBar`}
        style={{alignItems: "center", marginBottom: 0, marginTop: 0, ...style}}>
        <Search
          placeholder={t("sessions:searchControls.searchBar.placeholder")}
          value={searchBarText}
          style={{minWidth: 200, flexGrow: 4, flexBasis: 3}}
          onChange={onChangeSearch}
          enterButton
        />

        <CategoriesFilterSelect
          style={{minWidth: 200, flexGrow: 4, flexBasis: 5}}
          categories={categories}
          categoriesFilter={sessionsFilter.categoriesFilter}
          onChange={updateFilterFn("categoriesFilter")}
        />

        {customButtons}

        {!agendaView && (
          <DatePickerComponent
            bordered
            style={{flexBasis: 0, minWidth: 130}}
            value={sessionsFilter.fromDateFilter}
            defaultPickerValue={dayjs(currentProject.start)}
            format={`ddd ${dayjs.localeData().longDateFormat("LL")}`}
            placeholder={t("sessions:searchControls.fromDateFilter.placeholder")}
            showTime={false}
            useMinutes={false}
            disableDatesIfOutOfProject
            disableDatesBeforeNow={false}
            onChange={updateFilterFn("fromDateFilter")}
          />
        )}

        <div className={"userTourSubscribedSessionsFilterBarAvailable"} style={{flexGrow: 0}}>
          {!agendaView && !dayjs().isAfter(dayjs(currentProject.end)) && (
            <Tooltip title={t("sessions:searchControls.showPastSessionsFilter.tooltip")}>
              <CheckableTag
                checked={sessionsFilter.showPastSessionsFilter}
                onChange={updateFilterFn("showPastSessionsFilter")}>
                {t("sessions:list.past")}
              </CheckableTag>
            </Tooltip>
          )}

          {!viewOnlySubscribed && (
            <Tooltip title={t("sessions:searchControls.availableFilter.tooltip")}>
              <CheckableTag
                checked={sessionsFilter.availableFilter}
                onChange={updateFilterFn("availableFilter")}>
                {t("sessions:list.available")}
              </CheckableTag>
            </Tooltip>
          )}

          {participantIsLinkedWithASteward && (
            <Tooltip title={t("sessions:searchControls.stewardFilter.tooltip")}>
              <CheckableTag
                checked={sessionsFilter.stewardFilter}
                onChange={updateFilterFn("stewardFilter")}>
                Encadré.es
              </CheckableTag>
            </Tooltip>
          )}
        </div>
      </div>
    );
  },
  () => true
);
