// Returns the hour of the day based on hours and minutes
import dayjs from "dayjs";

const hoursOfDay = (dayjsDate) => dayjsDate.minute() / 60.0 + dayjsDate.hour();

// Tells if a slot is located on two different days
const slotEndsAfterMidnight = (slot) => {
  const start = dayjs(slot.start);
  const end = dayjs(slot.end);
  const length = end.diff(start, "minute");
  return start.hour() * 60 + start.minute() + length > 1439;
};

// Get the min and max hour of all slots
const getSlotsMinMaxHours = (slots) => {
  // If no slots, return
  if (!slots || !(slots?.length > 0)) return;

  let minStart, maxEnd;
  for (const slot of slots) {
    if (slotEndsAfterMidnight(slot)) {
      // If any of the slots ends after midnight, display all the hours
      return {start: 0, end: 24};
    } else {
      // Else, compute dates depending on the
      const slotStart = hoursOfDay(dayjs(slot.start));
      const slotEnd = hoursOfDay(dayjs(slot.end));
      minStart = minStart && minStart < slotStart ? minStart : slotStart;
      maxEnd = maxEnd && maxEnd > slotEnd ? maxEnd : slotEnd;
    }
  }

  return {start: minStart, end: maxEnd};
};

// Get the min and max hour of the project availability slots
const getProjectMinMaxHours = (project) => {
  // If there is no availability slots, don't return anything, we can't calculate.
  if (!(project?.availabilitySlots?.length > 0)) return;

  const projectSlotThatEndsAfterMidnight = project.availabilitySlots.find((slot) =>
    slotEndsAfterMidnight(slot)
  );
  // If some slots pass through midnight, we have to display everything.
  if (projectSlotThatEndsAfterMidnight) return;

  // If not, then just take the event's slots as reference
  return {
    start: hoursOfDay(dayjs(project.start)),
    end: hoursOfDay(dayjs(project.end)),
  };
};

const MIN_START_DISPLAY_HOUR = 8.5;
const MIN_END_DISPLAY_HOUR = 22.5;

const halfRound = (funcName: "floor" | "ceil" | "round", val) => Math[funcName](val * 2) / 2;

// Get the display parameters to give to the Agenda view
export const getAgendaDisplayHours = (slots, project) => {
  let params;
  const projectMinMaxHours = getProjectMinMaxHours(project);
  const slotsMinMaxHours = getSlotsMinMaxHours(slots);

  if (projectMinMaxHours && slotsMinMaxHours) {
    // If there are both slots and project hours, calculate based on both

    const slotsExistBeforeProjectHours = slotsMinMaxHours.start < projectMinMaxHours.start;
    const slotsExistAfterProjectHours = slotsMinMaxHours.end > projectMinMaxHours.end;

    params = {
      start: halfRound(
        "floor",
        slotsExistBeforeProjectHours ? slotsMinMaxHours.start : projectMinMaxHours.start
      ),
      end: halfRound(
        "ceil",
        slotsExistAfterProjectHours ? slotsMinMaxHours.end : projectMinMaxHours.end
      ),
    };

    // Set up the scroll diff
    params.hoursDiff =
      slotsMinMaxHours && slotsExistBeforeProjectHours
        ? halfRound("floor", projectMinMaxHours.start) - params.start
        : 0;
  } else if (slotsMinMaxHours || projectMinMaxHours) {
    // If there are only one of them, just use the ones we have
    params = {
      start: halfRound(
        "floor",
        slotsMinMaxHours?.start !== undefined ? slotsMinMaxHours?.start : projectMinMaxHours?.start
      ),
      end: halfRound(
        "ceil",
        slotsMinMaxHours?.end !== undefined ? slotsMinMaxHours?.end : projectMinMaxHours?.end
      ),
    };
  } else {
    // if there are none, fail safe
    params = {start: MIN_START_DISPLAY_HOUR, end: MIN_END_DISPLAY_HOUR};
  }

  params.start = Math.max(params.start - 1, 0); // Display one hour before display start, but do not go below 0 hours
  params.end = Math.min(params.end + 1, 24); // Display one hour after display end, but do not go after 24 hours

  // We should at least display from MIN to MAX default display hour
  params.start = Math.min(MIN_START_DISPLAY_HOUR, params.start);
  params.end = Math.max(MIN_END_DISPLAY_HOUR, params.end);

  return params;
};
