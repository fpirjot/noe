import {useSelector} from "react-redux";
import {sessionsActions, sessionsSelectors} from "../../../features/sessions";
import {currentProjectSelectors} from "../../../features/currentProject";
import {useEffect} from "react";
import dayjs from "dayjs";
import {teamsActions} from "../../../features/teams";

export const useLoadSessionsView = (viewUrl, viewMode, dispatch) => {
  const sessionFilter = useSelector(sessionsSelectors.selectListFilter);
  const currentProject = useSelector(currentProjectSelectors.selectProject);

  useEffect(() => {
    const isAgendaView = viewMode === "agenda";
    const eventIsFinished = dayjs().isAfter(dayjs(currentProject.end));

    // If we are on the agenda view, or if the past sessions filter is activated, or if the event is already finished,
    // Then load all sessions (= no dateToLoadFrom). Else, only load from now date.
    const dateToLoadFrom =
      isAgendaView || sessionFilter.showPastSessionsFilter || eventIsFinished
        ? undefined
        : Date.now();

    dispatch(sessionsActions.loadList({type: viewUrl, fromDate: dateToLoadFrom}));

    // In the agenda view, always activate past sessions.
    // If we are after the end of the event, also systematically load past sessions.
    // TODO optimize this, as updateFilteredList is also called in the loadList function
    if (isAgendaView || eventIsFinished)
      dispatch(sessionsActions.updateFilteredList({showPastSessionsFilter: true}));

    dispatch(sessionsActions.loadCategoriesList());
    dispatch(teamsActions.loadList());
  }, [viewUrl, sessionFilter.showPastSessionsFilter]);
};
