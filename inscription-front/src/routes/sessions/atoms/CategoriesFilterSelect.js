import {Select} from "antd";
import React from "react";
import {categoriesOptionsWithColorDot} from "./categoriesOptionsWithColorDot";

/**
 * Component representing a select input for filtering sessions by categories.
 * @component
 * @returns {ReactNode} - Rendered component for filtering sessions by categories.
 */
export const CategoriesFilterSelect = ({categories, categoriesFilter, onChange, style}) => {
  // Check if categories are available and prepare options for the select input
  const categoriesReady = categories.length > 0;
  const categoriesOptions = [
    {key: "volunteering", value: "volunteering", label: <em>- Tout le bénévolat -</em>},
    {key: "allTheRest", value: "allTheRest", label: <em>- Tout le reste -</em>},
    ...categoriesOptionsWithColorDot(categories),
  ];

  return (
    <Select
      style={style}
      value={categoriesReady ? categoriesFilter : undefined}
      allowClear
      mode="multiple"
      placeholder="Filtrez par catégorie..."
      onChange={onChange}
      options={categoriesOptions}
    />
  );
};
