import {useDispatch} from "react-redux";
import {useEffect} from "react";

export const useLoadEditing = (
  elementsActions,
  idInUrl,
  additionalActions,
  clonedElement,
  dontClean
) => {
  const dispatch = useDispatch();

  useEffect(() => {
    additionalActions && additionalActions();
    if (idInUrl) {
      if (idInUrl === "clone") dispatch(elementsActions.changeEditing(clonedElement));
      else if (idInUrl === "groupedit") dispatch(elementsActions.loadEditing("new"));
      else dispatch(elementsActions.loadEditing(idInUrl));
    }

    if (!dontClean) return () => dispatch(elementsActions.setEditing({}));
  }, [idInUrl]);
};
