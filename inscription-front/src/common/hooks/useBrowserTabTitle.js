import {useEffect} from "react";

/**
 * Browser tab title changer
 * @param baseName root name that appears all the time
 * @param currentPageRoot the variable containing the current page uri
 * @param titlesForPages key/value pairs of page titles
 * @param suffix end name that appears all the time
 */
export const useBrowserTabTitle = (baseName, currentPageRoot, titlesForPages, suffix) => {
  useEffect(() => {
    if (baseName && currentPageRoot) {
      const title = `${baseName ? baseName : ""}${
        titlesForPages[currentPageRoot] ? " - " + titlesForPages[currentPageRoot] : ""
      }${suffix ? " - " + suffix : ""}`;
      document.title = title;
    }
  }, [baseName, currentPageRoot]);
};
