import {Button, Popover, PopoverProps} from "antd";
import {CSSProperties, useState} from "react";
import {currentUserSelectors} from "../../features/currentUser";
import {useDispatch, useSelector} from "react-redux";
import {useTranslation} from "react-i18next";
import {usersActions} from "../../features/users";

export const UserNudgeDotPopover = ({
  tourKey,
  size = 14,
  dotStyle,
  content,
  ...popoverProps
}: {tourKey: string; dotStyle?: CSSProperties; size?: number} & Omit<
  PopoverProps,
  "onOpenChange"
>) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(currentUserSelectors.selectUser);
  const shouldShowTour = !currentUser.shownTours?.includes(tourKey);
  const [open, setOpen] = useState(false);
  const {t} = useTranslation();

  const dontDisplayAnymore = () => {
    setOpen(false);
    dispatch(
      usersActions.persist(
        {
          _id: currentUser._id,
          shownTours: [...(currentUser.shownTours || []), tourKey],
        },
        false
      )
    );
  };

  return shouldShowTour ? (
    <Popover
      open={open}
      content={
        <div className={"containerV"}>
          {content}
          <Button type={"link"} style={{padding: 0, alignSelf: "end"}} onClick={dontDisplayAnymore}>
            {t("common:close")}
          </Button>
        </div>
      }
      {...popoverProps}>
      <div
        onClick={() => setOpen(true)}
        style={{
          cursor: "pointer",
          backgroundImage: "var(--noe-gradient)",
          borderRadius: "50%",
          margin: size / 2,
          height: size,
          width: size,
          position: "absolute",
          zIndex: 1000,
          ...dotStyle,
        }}
        className={"pulse"}
      />
    </Popover>
  ) : null;
};
