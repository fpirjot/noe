import {getSessionSubscription} from "../../utils/registrationsUtilities";
import {registrationsActions} from "../../features/registrations";
import {Checkbox, Tooltip} from "antd";
import {Link} from "react-router-dom";
import {personName} from "../../utils/utilities";
import React from "react";

export const SessionNoShowCheckbox = ({
  session,
  registration,
  registrations,
  disabled,
  dispatch,
}) => {
  const sessionSubscription = getSessionSubscription(registration, session);

  const checkedBy =
    sessionSubscription?.hasNotShownUp &&
    registrations.find((r) => r.user._id === sessionSubscription.hasNotShownUp);

  const toggle =
    !disabled &&
    ((event) => {
      dispatch(
        registrationsActions.hasNotShownUpInSession(
          registration._id,
          session._id,
          event.target.checked
        )
      );
    });

  if (!sessionSubscription) return null;
  if (disabled) return sessionSubscription?.hasNotShownUp ? "❗" : null;

  return (
    <Tooltip
      title={
        checkedBy && (
          <>
            Coché par{" "}
            <Link
              to={`./../../participants/${checkedBy._id}`}
              style={{color: "white", textDecoration: "underline"}}>
              {personName(checkedBy.user)}
            </Link>
          </>
        )
      }>
      <Checkbox checked={sessionSubscription?.hasNotShownUp} onClick={toggle} disabled={disabled} />
    </Tooltip>
  );
};
