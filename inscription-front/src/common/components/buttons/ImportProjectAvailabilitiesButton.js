import {Button, Popconfirm} from "antd";
import {useSelector} from "react-redux";
import {currentProjectSelectors} from "../../../features/currentProject";

export const ImportProjectAvailabilitiesButton = ({changeAvailabilitySlots}) => {
  const currentProject = useSelector(currentProjectSelectors.selectProject);
  return (
    <Popconfirm
      title="Les disponibilités existantes seront écrasées."
      okText="Importer quand même"
      okButtonProps={{danger: true}}
      cancelText="Annuler"
      onConfirm={() =>
        changeAvailabilitySlots(
          currentProject.availabilitySlots.map((slot) => {
            const {_id, ...slotWithoutId} = slot;
            return slotWithoutId;
          })
        )
      }>
      <Button type="link">Importer les plages de l'événement</Button>
    </Popconfirm>
  );
};
