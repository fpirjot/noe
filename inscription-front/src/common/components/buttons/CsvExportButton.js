import React, {useRef} from "react";
import {Parser} from "@json2csv/plainjs";
import {Button, Tooltip} from "antd";
import {ExportOutlined} from "@ant-design/icons";
import {normalize} from "../../../utils/stringUtilities";

export default function CsvExportButton({
  dataExportFunction,
  withFields,
  children,
  tooltip,
  getExportName,
  ...props
}) {
  const downloadButton = useRef();

  const exportData = () => {
    const exportName = normalize(getExportName(), true).replace(/[^\w ]/g, "-");

    try {
      const exportedData = dataExportFunction();
      const fields = withFields ? exportedData[0] : undefined;
      const parser = new Parser({fields});
      const csv = parser.parse(withFields ? exportedData.slice(1) : exportedData);

      let file = new File([csv], exportName, {
        type: "text/csv",
      });
      let exportUrl = URL.createObjectURL(file);

      downloadButton.current.setAttribute("href", exportUrl);
      downloadButton.current.setAttribute("download", exportName);
      downloadButton.current.click();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <>
      <Tooltip title={tooltip}>
        <Button
          onClick={(e) => {
            exportData();
          }}
          icon={<ExportOutlined />}
          type="link"
          {...props}>
          {children}
        </Button>
      </Tooltip>
      <a ref={downloadButton} style={{display: "none"}} />
    </>
  );
}
