import {Button, Form} from "antd";
import {FormElement} from "../inputs/FormElement";
import FormRenderer from "./FormRenderer";
import {CheckOutlined, EditOutlined} from "@ant-design/icons";
import React, {ReactNode, useRef, useState} from "react";
import {cleanAnswer} from "../../utils/columns/cleanAnswer";
import {FormCompInput} from "./FormBuilder/FormComp/FormComp";
import {MakeOptional} from "@mui/x-date-pickers/internals/models/helpers";
import {ColumnType} from "antd/es/table";
import {store} from "../../app/store";
import {useTranslation} from "react-i18next";
import {useHover} from "usehooks-ts";
import {UserNudgeDotPopover} from "./UserNudgeDotPopover";

const editableCellStyle = {
  base: {
    // Set a minimum height of 32 (22 + 2*5) to be exactly lie the height if the edit button
    minHeight: 32,
    // Add radius and transition timings
    borderRadius: 12,
    transition: "background 0.5s",
  },
  baseDisplay: {
    overflow: "hidden",
    padding: "5px 8px 5px 5px",
    margin: "-2px -8px -2px -5px",
  },
  baseEdit: {
    overflow: "unset",
    paddingRight: 8,
    marginRight: -8,
  },
  hover: {
    background: "var(--colorBgSelected)",
  },
  buttonBase: {
    background: "var(--colorBgContainer)",
    // On hover, add a border so we think that the edit button is inside the edit cell
    border: "2px solid var(--colorBgContainer)",
  },
  buttonHover: {
    border: "2px solid var(--colorBgSelected)",
  },
};

type EditableCellInnerFormProps = {
  formComp: Omit<FormCompInput, "input">; // A definition of the form component to display
  value: any;
  editCell?: ReactNode; // A replacement of the default editable cell
  onChange: (value: any) => Promise<void>;
};

const EditableCellInnerForm = ({
  formComp,
  editCell,
  value,
  onChange,
}: EditableCellInnerFormProps) => {
  const [form] = Form.useForm();
  return (
    <FormElement
      form={form}
      onFinish={(values) => onChange?.(values[formComp.key])}
      initialValues={{[formComp.key]: value}}>
      <div
        className={"containerH flex-space reveal-on-hover-container"}
        style={{
          flexWrap: "wrap",
          flexDirection: "row-reverse",
          alignItems: "start",
          columnGap: 0,
          rowGap: 2,
          ...editableCellStyle.base,
          ...editableCellStyle.baseEdit,
          ...editableCellStyle.hover,
        }}>
        {/* And the button to validate the modification*/}
        <div
          style={{
            flexGrow: 0,
            marginLeft: "auto",
            translate: 8,
          }}>
          <UserNudgeDotPopover
            tourKey={"editableCellShortcut"}
            title={"Les raccourcis, c'est la vie."}
            content={"Utilisez Ctrl+S pour sauver vos modifications au sein d'une cellule."}
            dotStyle={{top: -7, right: -7}}
          />
          <Button
            icon={<CheckOutlined />}
            type={"link"}
            onClick={form.submit}
            style={{
              ...editableCellStyle.buttonBase,
              ...editableCellStyle.buttonHover,
            }}
          />
        </div>

        {/* Render the field for the field, in minimalist mode */}
        {editCell || (
          <FormRenderer
            fields={[{...formComp, autoFocus: true, input: true}]}
            form={form}
            minimalist
          />
        )}
      </div>
    </FormElement>
  );
};

export type EditableCellProps = MakeOptional<EditableCellInnerFormProps, "onChange"> & {
  simpleDisplayCell?: ReactNode; // A replacement of the default cell in not-editable mode
};

export const EditableCell = ({
  formComp,
  value,
  onChange,
  simpleDisplayCell = cleanAnswer(value, undefined, formComp, "html"),
  editCell,
}: EditableCellProps) => {
  const {t} = useTranslation();
  const [isEditing, setIsEditing] = useState(false);

  const DisplayCellWithEditButton = () => {
    const editButtonRef = useRef(null);
    const isHover = useHover(editButtonRef);
    return (
      // Main container to reveal the edit button
      <div
        className={"containerH flex-space reveal-on-hover-container"}
        style={{
          overflow: "unset",
          // Align content at the center
          flexWrap: "nowrap",
          alignItems: "center",
          justifyContent: "center",
          // Extend the area in which the edit button reveal will work
          minHeight: 52,
          marginTop: -8,
          marginBottom: -8,
        }}>
        <div
          style={{
            ...editableCellStyle.base,
            ...editableCellStyle.baseDisplay,
            ...(isHover && editableCellStyle.hover),
          }}>
          {simpleDisplayCell}
        </div>

        <Button
          ref={editButtonRef}
          title={t("common:editableCell.editFieldX", {
            fieldName: formComp.displayName || formComp.label,
          })}
          icon={<EditOutlined />}
          className={"reveal-on-hover-item"}
          type={"link"}
          onClick={() => setIsEditing(true)}
          style={{
            position: "absolute",
            right: 0,
            flexGrow: 0,
            flexShrink: 0,
            ...editableCellStyle.buttonBase,
            ...(isHover && editableCellStyle.buttonHover),
          }}
        />
      </div>
    );
  };

  return isEditing ? (
    // If the user is editing, show the editing inner form
    <div
      onDoubleClick={
        // Prevent double click from triggering some action outside the cell
        (e) => e.stopPropagation()
      }>
      <EditableCellInnerForm
        formComp={formComp}
        editCell={editCell}
        value={value}
        onChange={async (value) => {
          await onChange?.(value);
          setIsEditing(false);
        }}
      />
    </div>
  ) : // Else, if there is an onChange function, show the answer with the edit button...
  onChange ? (
    <DisplayCellWithEditButton />
  ) : (
    // ...and if no onChange, just display the answer alone (null because we should always return something to render)
    <>{simpleDisplayCell}</>
  );
};

export type EditableCellColumnOptions = {
  title: string;
  editableValueTitle: string;
  dataIndex: string;
  name?: string;
  beforeChange: (newRecord: any, value: any | Array<any>, currentRecord: any) => Promise<void>;
  elementsActions: {persist: (fieldsToUpdate?: any) => any};
} & Omit<EditableCellProps["formComp"], "key" | "label">;

export type EditableCellComponentsRenders = {
  simpleDisplayCellRender?: (record: any) => EditableCellProps["simpleDisplayCell"];
  editCellRender?: (record: any) => EditableCellProps["editCell"];
};

export const editableCellColumn = (
  {
    title,
    dataIndex,
    editableValueTitle = title,
    name = dataIndex,
    beforeChange,
    elementsActions,
    ...formComp
  }: EditableCellColumnOptions,
  {
    simpleDisplayCellRender, // The cell in not-editable mode
    editCellRender, // The cell in editable mode
  }: EditableCellComponentsRenders = {}
): Pick<ColumnType<any>, "title" | "dataIndex" | "render"> => ({
  title,
  dataIndex,
  render: (text, record) => {
    return (
      <EditableCell
        value={record[name]}
        formComp={{...formComp, key: name, label: editableValueTitle}}
        onChange={async (value) => {
          if (beforeChange) {
            const fullPayload = {...record, [name]: value};
            await beforeChange?.(fullPayload, value, record);
          }

          return store
            .dispatch(
              elementsActions.persist({
                _id: record._id,
                [name]: value,
              })
            )
            .catch(() => {
              /* do nothing */
            });
        }}
        simpleDisplayCell={simpleDisplayCellRender?.(record)}
        editCell={editCellRender?.(record)}
      />
    );
  },
});
