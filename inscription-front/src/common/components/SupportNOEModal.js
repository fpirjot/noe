import {Trans, useTranslation} from "react-i18next";
import {Alert, Button, Modal} from "antd";
import {CardElement} from "./CardElement";
import React from "react";
import {useNavigate} from "react-router-dom";
import {DiscordLink, EmailLink} from "./NOESocialIcons";
import {PlusOutlined, QuestionCircleOutlined, ReadOutlined} from "@ant-design/icons";
import {LinkContainer} from "./LinkContainer";
import {URLS} from "../../app/configuration";

export const SupportNOEModal = ({open, setOpen}) => {
  const navigate = useNavigate();
  const {t} = useTranslation();
  return (
    <Modal open={open} width={"min(90vw, 1000pt)"} onCancel={() => setOpen(false)} footer={null}>
      <div className={"container-grid"}>
        <Alert message={t("common:supportNOE.description")} style={{marginBottom: 26}} />
        <CardElement
          title={t("common:supportNOE.useNOEForMyEvent")}
          style={{background: "color-mix(in srgb, var(--noe-primary) 2%, white)"}}>
          <div className="container-grid three-per-row">
            <CardElement size={"small"} icon={"☎️"} title={t("common:supportNOE.contactUs.title")}>
              <Trans
                i18nKey="supportNOE.contactUs.description"
                ns="common"
                components={{
                  contactLinks: (
                    <div className="containerH" style={{gap: 26}}>
                      <DiscordLink />
                      <EmailLink />
                    </div>
                  ),
                }}
              />
            </CardElement>

            <CardElement size={"small"} icon={"🖥️"} title={t("common:supportNOE.test.title")}>
              {process.env.REACT_APP_ORGA_FRONT_DEMO_URL && (
                <>
                  <p>{t("common:supportNOE.test.createDemo.description")}</p>
                  <p>
                    {process.env.REACT_APP_MODE === "DEMO" ? (
                      <Button icon={<PlusOutlined />} onClick={() => navigate("/new")}>
                        {t("common:supportNOE.test.createDemo.button")}
                      </Button>
                    ) : (
                      <LinkContainer
                        icon={<PlusOutlined />}
                        href={`${process.env.REACT_APP_ORGA_FRONT_DEMO_URL}/new`}>
                        {t("common:supportNOE.test.createDemo.button")}
                      </LinkContainer>
                    )}
                  </p>
                </>
              )}
            </CardElement>

            <CardElement size={"small"} icon={"📚"} title={t("common:supportNOE.learn.title")}>
              <p>{t("common:supportNOE.learn.description")}</p>
              <div className="containerH" style={{gap: 26}}>
                <LinkContainer
                  href={`${URLS.WEBSITE}/community/help-center`}
                  icon={<QuestionCircleOutlined />}>
                  {t("common:supportNOE.learn.helpCenterButton")}
                </LinkContainer>
                <LinkContainer href={`${URLS.WEBSITE}/docs`} icon={<ReadOutlined />}>
                  {t("common:supportNOE.learn.docsButton")}
                </LinkContainer>
              </div>
            </CardElement>
          </div>
        </CardElement>

        <CardElement title={t("common:supportNOE.supportTheProject")}>
          <div className="container-grid two-per-row">
            <CardElement size={"small"} icon={"💸"} title={t("common:supportNOE.donate.title")}>
              <p>{t("common:supportNOE.donate.description")}</p>
              <LinkContainer href={process.env.REACT_APP_DONATE_URL}>
                {t("common:supportNOE.donate.button")}
              </LinkContainer>
            </CardElement>

            <CardElement
              size={"small"}
              icon={"🌐️"}
              title={t("common:supportNOE.helpTranslate.title")}>
              <p>{t("common:supportNOE.helpTranslate.description")}</p>
              <LinkContainer href={`${URLS.WEBSITE}/community/translate`}>
                {t("common:supportNOE.helpTranslate.button")}
              </LinkContainer>
            </CardElement>
          </div>
        </CardElement>
      </div>
    </Modal>
  );
};
