import React from "react";

export const ChartContainer = React.memo(
  ({chart: Chart, config}) => {
    config = Object.assign(
      {interactions: [{type: "element-selected"}, {type: "element-active"}]},
      config
    );

    return <Chart {...config} />;
  },
  (pp, np) => JSON.stringify(pp) === JSON.stringify(np)
);
