import {useLocalStorageState} from "../../utils/localStorageUtilities";
import {Alert} from "antd";
import React from "react";
import {AlertProps} from "antd";

export const ClosableAlert = ({
  localStorageKey,
  ...props
}: {localStorageKey: string} & AlertProps) => {
  if (!localStorageKey) throw Error("Need key to display ClosableAlert");
  const [open, setOpen] = useLocalStorageState("closable-alert-" + localStorageKey, true);
  return open ? <Alert closable afterClose={() => setOpen(false)} {...props} /> : null;
};
