import React, {useState} from "react";
import {Button, ConfigProvider, Form, Input, App, Modal} from "antd";
import dayjs from "dayjs";
import {TableElement} from "./TableElement";
import {FormElement, safeValidateFields} from "../inputs/FormElement";
import {useTranslation} from "react-i18next";
import {slotEndIsBeforeBeginning, slotOverlapsAnOtherOne} from "../../utils/slotsUtilities";
import {DateTimeRangeInput} from "../inputs/DateTimeRangeInput";
import {H3Title} from "../layout/typography";
import {sorter} from "../../utils/sorters";
import {dateFormatter} from "../../utils/formatters";

export function AvailabilitySlotsTable({
  title,
  customButtons,
  defaultPickerValue, // Doesn't work with rangePicker, but hopefully a new version of AntDesign will solve the issue
  disableDatesIfOutOfProject, // True: We grey out all the dates out of the project scope / False: only dates in the past
  blockDisabledDates, // We prevent from choosing grayed out dates
  disabled,
  useMinutes,
  onChange,
  availabilitySlots,
}) {
  const {t} = useTranslation();
  const {message} = App.useApp();

  const [showAddSlotInput, setShowAddSlotInput] = useState(false);
  const [showUpdateSlotModal, setShowUpdateSlotModal] = useState(false);
  const [addSlotForm] = Form.useForm();
  const [updateSlotForm] = Form.useForm();

  const validateSlotFn =
    (ignoreThisSlot = undefined) =>
    (_, value) => {
      // If null, OK
      if (value == null) return Promise.resolve();

      const slot = formatSlotFromFormValue(value);

      // Make various checks
      if (slotOverlapsAnOtherOne(slot, availabilitySlots, ignoreThisSlot)) {
        return Promise.reject(t("common:availability.errors.slotOverlapsAnotherOne"));
      }
      if (slotEndIsBeforeBeginning(slot)) {
        return Promise.reject(t("common:availability.errors.slotEndIsBeforeBeginning"));
      }

      // If it passes, it's ok
      return Promise.resolve();
    };

  const formatSlotFromFormValue = (fields) => ({
    start: dayjs(fields[0]).format(),
    end: dayjs(fields[1]).format(),
  });

  const updateSlot = () =>
    safeValidateFields(
      updateSlotForm,
      () => {
        const updatedSlotFormData = updateSlotForm.getFieldValue("slot");
        const updatedSlot = formatSlotFromFormValue(updatedSlotFormData);

        const oldSlot = updateSlotForm.getFieldValue("oldSlot");
        let newData = [...(availabilitySlots || [])];
        const slotIndexToModify = availabilitySlots.findIndex(
          (s) => s.start === oldSlot.start && s.end === oldSlot.end
        );
        newData[slotIndexToModify] = updatedSlot;
        updateSlotForm.resetFields();
        onChange?.(newData);

        setShowUpdateSlotModal(false);
      },
      () => message.error(t("common:availability.errors.oopsDateIncorrect"))
    );

  const addSlot = () =>
    safeValidateFields(
      addSlotForm,
      () => {
        const slotFormData = addSlotForm.getFieldValue("slot");
        const slot = formatSlotFromFormValue(slotFormData);

        const newData = [...(availabilitySlots || []), slot];
        addSlotForm.resetFields();
        onChange?.(newData);
      },
      () => message.error(t("common:availability.errors.oopsDateIncorrect"))
    );

  const onEditSlot = (record) => {
    updateSlotForm.setFieldsValue({
      slot: [dayjs(record.start), dayjs(record.end)],
      oldSlot: record,
    });
    setShowUpdateSlotModal(true);
  };

  const onDeleteSlot = (removedSlot) => {
    const newData = availabilitySlots.filter(
      (slot) => !(slot.start === removedSlot.start && slot.end === removedSlot.end)
    );
    onChange?.(newData);
  };

  const columns = [
    {
      title: t("common:availability.startEnd"),
      key: "dateRange",
      sorter: (a, b) => sorter.date(a.start, b.start),
      defaultSortOrder: "ascend",
      render: (text, record) =>
        dateFormatter.longDateTimeRange(record.start, record.end, false, true),
    },
  ];

  const datePickerProps = {
    useMinutes,
    defaultPickerValue,
    disableDatesIfOutOfProject,
    blockDisabledDates,
  };

  return (
    <>
      <div>
        {(title || !disabled) && (
          <div className="header-space-between list-element-header">
            <H3Title>{title}</H3Title>
            <div className="containerH buttons-container">
              {customButtons}
              {!disabled && (
                <FormElement form={addSlotForm} style={{flexGrow: 1}}>
                  <div className="containerH buttons-container">
                    {showAddSlotInput && (
                      <>
                        <DateTimeRangeInput
                          autoFocus
                          name="slot"
                          {...datePickerProps}
                          required
                          rules={[{validator: validateSlotFn()}]}
                        />

                        <Button onClick={addSlot}>{t("common:add")}</Button>
                      </>
                    )}
                    {availabilitySlots?.length > 0 && !showAddSlotInput && (
                      <Button onClick={() => setShowAddSlotInput(true)}>
                        {t("common:availability.addNewSlots")}
                      </Button>
                    )}
                  </div>
                </FormElement>
              )}
            </div>
          </div>
        )}

        <ConfigProvider
          renderEmpty={
            !showAddSlotInput &&
            !disabled &&
            (() => (
              <Button onClick={() => setShowAddSlotInput(true)}>
                {t("common:availability.addNewSlots")}
              </Button>
            ))
          }>
          <TableElement.Simple
            columns={columns}
            rowKey="start"
            scroll={{x: 300, y: "50vh"}}
            dataSource={availabilitySlots}
            onEdit={!disabled && onEditSlot}
            onDelete={!disabled && onDeleteSlot}
          />
        </ConfigProvider>
      </div>

      <Modal
        title={t("common:availability.modifyASlot")}
        open={showUpdateSlotModal}
        onOk={updateSlot}
        onCancel={() => setShowUpdateSlotModal(false)}>
        <FormElement form={updateSlotForm}>
          <div className="containerH" style={{flexWrap: "wrap"}}>
            <DateTimeRangeInput
              label={t("common:availability.slot")}
              name="slot"
              {...datePickerProps}
              required
              rules={[{validator: validateSlotFn(updateSlotForm.getFieldValue("oldSlot"))}]}
            />
            <div style={{display: "none"}}>
              <Form.Item hidden name="oldSlot">
                <Input />
              </Form.Item>
            </div>
          </div>
        </FormElement>
      </Modal>
    </>
  );
}
