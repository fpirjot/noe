import React from "react";
import {Tooltip} from "antd";
import {ReactElement} from "react";
import {TooltipProps} from "antd";

/**
 * For performance concerns (especially in big lists), we wanna deactivate tooltips as they are consuming a lot of computing power
 */
export const WithTooltipOrNot = ({
  children,
  noTooltip,
  title,
  ...tooltipProps
}: {children: ReactElement; noTooltip: boolean} & TooltipProps) =>
  noTooltip ? (
    React.cloneElement(children, {title})
  ) : (
    <Tooltip title={title} {...tooltipProps}>
      {children}
    </Tooltip>
  );
