import {Button, Carousel as AntdCarousel, Image} from "antd";
import {ArrowLeftOutlined, ArrowRightOutlined} from "@ant-design/icons";
import React from "react";

const Carousel = ({children, ...carouselProps}) => {
  const CarouselArrow = (props) => (
    <Button
      size="large"
      shape="circle"
      className={`${props.className} carousel-arrow-button`}
      onClick={props.onClick}
      icon={props.icon}
    />
  );

  return (
    <AntdCarousel
      style={{margin: "0 25px"}}
      arrows
      {...carouselProps}
      nextArrow={<CarouselArrow icon={<ArrowRightOutlined />} />}
      prevArrow={<CarouselArrow icon={<ArrowLeftOutlined />} />}>
      {children}
    </AntdCarousel>
  );
};

export const HelpCarousel = ({content, baseUrl}) => {
  const mappedContent = content.map(({title, description, src}, index) => {
    return (
      <div>
        <div className="carousel-container">
          <div
            style={{
              flexGrow: 4,
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              width: "400px",
            }}>
            {title && (
              <h4>
                {index + 1}. {title}
              </h4>
            )}
            {description && <p>{description}</p>}
          </div>
          {src && (
            <div style={{flexGrow: 6, width: "500px"}}>
              <Image src={`${baseUrl}/${src}`} />
            </div>
          )}
        </div>
      </div>
    );
  });

  return <Carousel dots={false}>{mappedContent}</Carousel>;
};
