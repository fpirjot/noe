import {Form} from "antd";
import {addKeyToItemsOfList} from "../../utils/tableUtilities";
import {TableElement, TableWithTitleProps} from "./TableElement";
import React from "react";

/**
 * A Table, plugged to a form field
 * @param name the form field name
 * @param props some props for the table
 * @constructor
 */
export type WithKey<T> = T & {key: number};

export type FormTableProps<T> = {name: string} & TableWithTitleProps<WithKey<T>>;

export function FormTable<T>({name, rowKey, ...props}: FormTableProps<T>) {
  const form = Form.useFormInstance();
  const dataSource = Form.useWatch(name, form) || [];
  const dataSourceWithKey = addKeyToItemsOfList(dataSource);

  return <TableElement.WithTitle {...props} dataSource={dataSourceWithKey} rowKey={rowKey} />;
}
