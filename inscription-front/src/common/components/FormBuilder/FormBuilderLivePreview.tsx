import {Form, FormInstance} from "antd";
import {FormComp} from "./FormComp/FormComp";
import {FormCompsFormData} from "./FormBuilder";
import RegistrationForm from "../RegistrationForm";
import {useEffect, useMemo, useState} from "react";
import {useDebounce} from "../../hooks/useDebounce";
import {useTranslation} from "react-i18next";
import {ClosableAlert} from "../ClosableAlert";

export const FormBuilderLivePreview = ({
  formComponentsForm,
}: {
  formComponentsForm: FormInstance<FormCompsFormData>;
}) => {
  const {t} = useTranslation();
  const formCompsWatched = Form.useWatch("formComponents", formComponentsForm);
  const [previewForm] = Form.useForm();

  const [formComps, setFormComps] = useState<Array<FormComp>>();
  const debouncedSetFormComps = useDebounce(setFormComps);

  useEffect(() => {
    const displayableFormComponents = formCompsWatched?.filter((comp: FormComp) => comp.key !== "");

    // If values are undefined, initialize directly without debounce, else, change with debounce
    if (formComps === undefined) setFormComps(displayableFormComponents);
    else debouncedSetFormComps(displayableFormComponents);
  }, [formCompsWatched]);

  const memoizedFormPreview = useMemo(() => {
    return <RegistrationForm formComponents={formComps || []} form={previewForm} />;
  }, [formComps, previewForm]);

  return (
    <div>
      <ClosableAlert
        localStorageKey={"formBuilderPreview"}
        message={t("common:formBuilder.livePreviewNotice")}
        style={{marginBottom: 26}}
      />

      {memoizedFormPreview}
    </div>
  );
};
