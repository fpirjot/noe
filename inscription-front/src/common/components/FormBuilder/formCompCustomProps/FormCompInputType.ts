export const formCompInputTypes = [
  "text",
  "longText",
  "phoneNumber",
  "checkbox",
  "radioGroup",
  "select",
  "checkboxGroup",
  "multiSelect",
  "url",
  "email",
  "number",
  "datetime",
  "day",
] as const;

export const formCompLayoutTypes = ["content", "panel"] as const;

export type FormCompInputType = (typeof formCompInputTypes)[number];
export type FormCompLayoutType = (typeof formCompLayoutTypes)[number];
export type FormCompType = FormCompInputType | FormCompLayoutType;
