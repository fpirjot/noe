import {NamePath} from "rc-field-form/es/interface";
import {useTranslation} from "react-i18next";
import {Button, ButtonProps, Dropdown, Form} from "antd";
import React from "react";
import {
  FormComps,
  initialContentFormCompValues,
  initialInputFormCompValues,
  initialPanelFormCompValues,
} from "./FormComp/FormComp";
import {PlusOutlined} from "@ant-design/icons";
import {FormCompConfig} from "./FormComp/FormCompConfig";
import {formCompIcons} from "./formCompCustomProps/formCompIcons";
import {Stack} from "../../layout/Stack";

export type FormCompsFormData = {formComponents: FormComps};

export const FormBuilder = ({
  rootName,
  name,
  nested = false,
}: {
  rootName?: NamePath;
  name: NamePath;
  nested?: boolean;
}) => {
  const {t} = useTranslation();

  // If rootName is not given, defaults to the name
  rootName ||= Array.isArray(name) ? name : [name];

  const NewCompButton = (props: ButtonProps) => (
    <Button {...props} style={{height: nested ? 52 : 64}} type="dashed" block />
  );

  return (
    <Form.List name={name}>
      {(fields, {add, remove, move}) => (
        <>
          {fields.map(({name, key}, index) => (
            <React.Fragment key={index}>
              {/* Add button in the middle of two questions */}
              {index !== 0 && (
                <div
                  style={{margin: "2px auto", justifyContent: "center"}}
                  className={"containerH reveal-on-hover-container"}>
                  <Dropdown
                    menu={{
                      items: [
                        {
                          label: t("common:formBuilder.addAQuestion"),
                          key: "formInputComp",
                          onClick: () => add(initialInputFormCompValues, index),
                        },
                        {
                          label: t("common:formBuilder.addAFreeContent"),
                          key: "formContentComp",
                          onClick: () => add(initialContentFormCompValues, index),
                        },
                        ...((!nested && [
                          {
                            label: t("common:formBuilder.addAPanel"),
                            key: "panelComp",
                            onClick: () => add(initialPanelFormCompValues, index),
                          },
                        ]) ||
                          []),
                      ],
                    }}
                    trigger={["click"]}>
                    <Button
                      type={"text"}
                      size={"small"}
                      className={"reveal-on-hover-item"}
                      icon={<PlusOutlined />}
                    />
                  </Dropdown>
                </div>
              )}

              {/* Question config component */}
              <FormCompConfig
                rootName={rootName}
                name={name}
                nested={nested}
                remove={remove}
                moveUp={index !== 0 ? () => move(index, index - 1) : undefined}
                moveDown={index !== fields.length - 1 ? () => move(index, index + 1) : undefined}
              />
            </React.Fragment>
          ))}

          <Stack gap={2} mt={fields.length > 0 ? 3 : 0}>
            <div className={"userTourAddFormQuestion"}>
              <NewCompButton
                onClick={() => add(initialInputFormCompValues)}
                icon={<PlusOutlined />}>
                {t("common:formBuilder.addAQuestion")}
              </NewCompButton>
            </div>

            <Stack
              row
              wrap={{xs: "wrap", sm: "nowrap"}}
              gap={2}
              className="userTourAddOtherComponents">
              <NewCompButton
                onClick={() => add(initialContentFormCompValues)}
                icon={formCompIcons.content}>
                {t("common:formBuilder.addAFreeContent")}
              </NewCompButton>

              {!nested && (
                <NewCompButton
                  onClick={() => add(initialPanelFormCompValues)}
                  icon={formCompIcons.panel}>
                  {t("common:formBuilder.addAPanel")}
                </NewCompButton>
              )}
            </Stack>
          </Stack>
        </>
      )}
    </Form.List>
  );
};
