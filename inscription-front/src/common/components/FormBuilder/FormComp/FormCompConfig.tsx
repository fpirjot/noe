import {useTranslation} from "react-i18next";
import {Button, Collapse, Form, FormListOperation, Tabs, Tooltip} from "antd";
import {
  ArrowUpOutlined,
  EyeOutlined,
  FilePdfOutlined,
  MinusCircleOutlined,
} from "@ant-design/icons";
import {TextInput} from "../../../inputs/TextInput";
import {GeneralFormCompConfig} from "./GeneralFormCompConfig";
import {ConditionalFormCompConfig} from "./ConditionalFormCompConfig";
import {formCompCustomPropsConfig} from "../formCompCustomProps/formCompCustomPropsConfig";
import {SelectInput} from "../../../inputs/SelectInput";
import React, {CSSProperties, useMemo} from "react";
import {TextAreaInput} from "../../../inputs/TextAreaInput";
import {useFormCompsFormInstance} from "../atoms/useFormCompsFormInstance";
import {camelCaseify, generateRandomId} from "../../../../utils/stringUtilities";
import {formCompInputTypes, FormCompType} from "../formCompCustomProps/FormCompInputType";
import {FormCompInputType} from "../formCompCustomProps/FormCompInputType";
import {NamePath} from "rc-field-form/es/interface";
import {formCompIcons} from "../formCompCustomProps/formCompIcons";
import {useSafeAutoModifField} from "../atoms/useSafeAutoModifField";
import {useIsNewElement} from "../atoms/useIsNewElement";
import {Stack} from "../../../layout/Stack";

// Each array represents a list of types that are compatible with each other
// in the way they are stored in the databases.
const INTERCOMPATIBLE_FORM_COMP_TYPE_GROUPS: Array<Array<FormCompInputType>> = [
  ["text", "longText", "url", "email"],
  ["radioGroup", "select"],
  ["checkboxGroup", "multiSelect"],
  ["datetime", "day"],
];

// Find an intercompatibility group that contains both typeA and typeB
const areInterCompatible = (typeA: FormCompInputType, typeB: FormCompInputType) =>
  INTERCOMPATIBLE_FORM_COMP_TYPE_GROUPS.find(
    (intercompatibleGroup) =>
      intercompatibleGroup.includes(typeA) && intercompatibleGroup.includes(typeB)
  );

export const createDatabaseKey = (inputString: string) => {
  if (!inputString) return "";

  // CamelCaseify the string
  const camelCaseString = camelCaseify(inputString);

  // Ensure the first character is a letter (database key requirements)
  const key = camelCaseString.charAt(0).match(/[a-zA-Z]/) ? camelCaseString : `_${camelCaseString}`;

  // Add a nonce for uniqueness
  return `${key}_${generateRandomId(3)}`;
};

export const FormCompConfig = ({
  rootName,
  name,
  nested,
  remove,
  moveDown,
  moveUp,
}: {
  rootName: NamePath;
  name: number;
  remove: FormListOperation["remove"];
  nested: boolean;
  moveUp?: () => void;
  moveDown?: () => void;
}) => {
  const thisCompRootName = [...rootName, name];
  const {t} = useTranslation();
  const form = useFormCompsFormInstance();
  const {isNewElement, hasFirstReRendered} = useIsNewElement(thisCompRootName);

  const keyNamePath: NamePath = [...thisCompRootName, "key"];
  const keyVal = Form.useWatch(keyNamePath, form);
  const modifKeyBasedOnValue = useSafeAutoModifField(thisCompRootName, "key", isNewElement);

  const typeVal = Form.useWatch([...thisCompRootName, "type"], form);
  const isInput = formCompInputTypes.includes(typeVal);
  const isPanel = typeVal === "panel";

  const customProps = formCompCustomPropsConfig[typeVal as FormCompInputType];
  const MainControls = customProps?.MainControls;

  const RemoveButton = () => (
    <Button onClick={() => remove(name)} danger type={"link"} icon={<MinusCircleOutlined />} />
  );

  const panelTitleLinesSkeleton = useMemo(() => {
    if (typeVal === "panel") {
      return (
        <Stack gap={1} mt={2}>
          {Array.from({length: 3}, () => (
            <div
              style={{
                minWidth: 100,
                width: Math.ceil(Math.random() * 10) + 40 + "%",
                height: 10,
                borderRadius: 4,
                background: "var(--text)",
              }}
            />
          ))}
        </Stack>
      );
    } else return null;
  }, [typeVal]);

  const FormCompTitle = () => {
    const labelVal = Form.useWatch([...thisCompRootName, "label"], form);
    const inPdfExportVal = Form.useWatch([...thisCompRootName, "inPdfExport"], form);
    const displayNameVal = Form.useWatch([...thisCompRootName, "displayName"], form);

    return (
      <>
        {!keyVal ? (
          <span style={{color: "gray"}}>
            {isInput
              ? t("common:formBuilder.newFormInputComp")
              : isPanel
              ? t("common:formBuilder.newFormPanelComp")
              : t("common:formBuilder.newFormContentComp")}
          </span>
        ) : (
          <Stack row>
            <Stack row sx={{whiteSpace: "pre-wrap"}} gap={2}>
              <div title={t(`common:formBuilder.types.${typeVal}`)}>
                {formCompIcons[typeVal as FormCompType]}
              </div>
              {isInput ? (
                labelVal
              ) : isPanel ? (
                <strong>{labelVal}</strong>
              ) : (
                t("common:formBuilder.types.content")
              )}
            </Stack>
            {(displayNameVal || inPdfExportVal) && (
              <Stack row gap={2} mx={2} alignStart>
                {displayNameVal && (
                  <div>
                    <EyeOutlined title={t("common:formBuilder.schema.showInColumns.label")} />
                  </div>
                )}
                {inPdfExportVal && (
                  <div>
                    <FilePdfOutlined title={t("common:formBuilder.schema.inPdfExport.label")} />
                  </div>
                )}
              </Stack>
            )}
          </Stack>
        )}
        <div style={{opacity: keyVal ? 0.15 : 0.05}}>{panelTitleLinesSkeleton}</div>
      </>
    );
  };

  // Main component styles
  // - Make dashed border if new question with no key yet
  // - Make border bigger if panel component
  const collapseStyle: CSSProperties = {
    borderStyle: keyVal ? undefined : "dashed",
    borderWidth: isPanel ? "2px 2px 0 2px" : undefined, // Except bottom border, which is managed by the panel component
  };
  // Do the same but only for bottom border, the rest is managed by collapse component
  const collapsePanelStyle: CSSProperties = {
    borderBottomStyle: keyVal ? undefined : "dashed",
    borderBottomWidth: isPanel ? 2 : undefined,
  };

  return hasFirstReRendered ? (
    // Open the collapse if it is a newly created question, otherwise first display it as closed
    <Collapse
      defaultActiveKey={isNewElement ? "mainPanel" : undefined}
      size={nested ? "middle" : "large"} // If the component is nested in a panel component, make its size smaller
      style={collapseStyle}>
      <Collapse.Panel
        header={<FormCompTitle />}
        style={collapsePanelStyle}
        extra={
          <Stack
            row
            alignCenter
            justifyCenter
            gap={2}
            my={"-4px"}
            onClick={(e) => e.stopPropagation()}
            height={"100%"}>
            {moveUp && (
              <Tooltip title={"Move up"}>
                <Button type={"text"} size={"small"} icon={<ArrowUpOutlined />} onClick={moveUp} />
              </Tooltip>
            )}
            {moveDown && (
              <Tooltip title={"Move down"}>
                <Button
                  type={"text"}
                  size={"small"}
                  icon={<ArrowUpOutlined style={{transform: "rotate(180deg)"}} />}
                  onClick={moveDown}
                />
              </Tooltip>
            )}
            <RemoveButton />
          </Stack>
        }
        key={"mainPanel"}>
        {isInput && (
          <div className="container-grid two-per-row">
            <TextAreaInput
              label={t("common:formBuilder.schema.label.label")}
              placeholder={t("common:formBuilder.schema.label.placeholder")}
              autoSize={{minRows: 1}}
              name={[name, "label"]}
              required
              autoFocus={!!isNewElement} // Focus if new
              onChange={(event) => {
                // If the key is not defined in the component value, it means it's a new component.
                // So pre-fill it unless the user touches the field
                modifKeyBasedOnValue(event.target.value);
              }}
            />

            <SelectInput
              name={[name, "type"]}
              label={t("common:formBuilder.schema.type.label")}
              placeholder={t("common:formBuilder.schema.type.placeholder")}
              options={formCompInputTypes.map((type) => {
                // Disable option if the question is not new and if types are not compatible
                const disabled = !isNewElement && !areInterCompatible(typeVal, type);
                return {
                  label: (
                    <Stack row alignCenter>
                      <div style={{width: 18, textAlign: "center", marginRight: 8}}>
                        {formCompIcons[type as FormCompType]}
                      </div>
                      {t(`common:formBuilder.types.${type}`)}
                    </Stack>
                  ),
                  value: type,
                  disabled,
                  title: disabled
                    ? t("common:formBuilder.schema.type.typesAreIncompatible")
                    : undefined,
                };
              })}
              popupMatchSelectWidth={false}
              required
            />
          </div>
        )}

        {MainControls && (
          <MainControls
            name={name}
            modifKeyBasedOnValue={modifKeyBasedOnValue}
            rootName={rootName}
            isNewElement={isNewElement}
          />
        )}

        <Tabs
          items={[
            ...(isInput
              ? [
                  {
                    label: t("common:formBuilder.tabs.general"),
                    key: "general",
                    forceRender: true, // Force render so the key can be initialized to something
                    children: (
                      <GeneralFormCompConfig
                        name={name}
                        customProps={customProps?.generalTab}
                        rootName={rootName}
                      />
                    ),
                  },
                ]
              : []),
            {
              label: t("common:formBuilder.tabs.displayConditions"),
              key: "displayConditions",
              children: <ConditionalFormCompConfig name={name} rootName={rootName} />,
            },
            {
              label: t("common:formBuilder.tabs.advanced"),
              key: "advanced",
              forceRender: true, // Force render so the key can be initialized to something
              children: (
                <TextInput
                  name={[name, "key"]}
                  disabled={!isNewElement}
                  label={t("common:formBuilder.schema.key.label")}
                  placeholder={t("common:formBuilder.schema.key.placeholder")}
                  required
                />
              ),
            },
          ]}
        />
      </Collapse.Panel>
    </Collapse>
  ) : null;
};
