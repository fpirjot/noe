import {TextInput} from "../../../inputs/TextInput";
import React from "react";
import {Trans, useTranslation} from "react-i18next";

import {SelectInput} from "../../../inputs/SelectInput";
import {flattenFormComponents} from "../../../../utils/registrationsUtilities";
import {FormCompInput} from "./FormComp";
import {Alert, Collapse, Form} from "antd";
import {useFormCompsFormInstance} from "../atoms/useFormCompsFormInstance";
import {CardElement} from "../../CardElement";
import {NamePath} from "rc-field-form/es/interface";
import {QuestionCircleOutlined} from "@ant-design/icons";

export function ConditionalFormCompConfig({rootName, name}: {rootName: NamePath; name: number}) {
  const {t} = useTranslation();
  const form = useFormCompsFormInstance();
  const formComponents = Form.useWatch(rootName, form);

  return (
    <div className="container-grid" style={{width: "100%"}}>
      <div className="containerH" style={{gap: 8, flexWrap: "wrap", marginBottom: 16}}>
        <SelectInput
          name={[name, "conditional", "show"]}
          placeholder={t("common:formBuilder.schema.conditional.showPlaceholder")}
          options={[
            {
              value: "show",
              label: t("common:formBuilder.schema.conditional.show"),
            },
            {
              value: "doNotShow",
              label: t("common:formBuilder.schema.conditional.doNotShow"),
            },
          ]}
          formItemProps={{style: {marginBottom: 0, minWidth: 100, maxWidth: 300}}}
          allowClear
          popupMatchSelectWidth={false}
        />
        <div className={"containerH"} style={{gap: 8, alignItems: "baseline"}}>
          {t("common:formBuilder.schema.conditional.whenQuestion")}

          <SelectInput
            name={[name, "conditional", "when"]}
            placeholder={t("common:formBuilder.schema.conditional.question")}
            formItemProps={{style: {marginBottom: 0, minWidth: 100, maxWidth: 300}}}
            options={flattenFormComponents(formComponents).map((component: FormCompInput) => ({
              value: component.key,
              label: component.label,
            }))}
            showSearch
            allowClear
            popupMatchSelectWidth={300}
          />
        </div>

        <div className={"containerH"} style={{gap: 8, alignItems: "baseline", flexGrow: 1}}>
          <span style={{whiteSpace: "nowrap"}}>
            {t("common:formBuilder.schema.conditional.isEqualTo")}
          </span>
          <TextInput
            name={[name, "conditional", "eq"]}
            placeholder={t("common:formBuilder.schema.conditional.conditions")}
            inputProps={{style: {width: "100%"}}}
            formItemProps={{style: {marginBottom: 0, width: "100%"}}}
          />
        </div>
      </div>

      <Collapse>
        <Collapse.Panel
          key={1}
          header={
            <>
              <QuestionCircleOutlined style={{marginRight: 8}} />
              {t("common:formBuilder.schema.conditional.helpForDisplayConditions")}
            </>
          }>
          <ConditionalFieldsHelpContent />
        </Collapse.Panel>
      </Collapse>
    </div>
  );
}

const ConditionalFieldsHelpContent = () => {
  const {t} = useTranslation();
  return (
    <>
      <Alert
        message={t("common:formBuilder.displayConditionsHelp.notice")}
        style={{marginBottom: 26}}
      />

      <CardElement size={"small"} title={"Conditions simples"}>
        <Trans
          ns={"common"}
          i18nKey={"formBuilder.displayConditionsHelp.simpleConditions"}
          components={{exampleText: <span style={{color: "grey"}} />, li: <li />, ul: <ul />}}
        />
      </CardElement>
      <CardElement size={"small"} title={"Conditions avancées"}>
        <Trans
          ns={"common"}
          i18nKey={"formBuilder.displayConditionsHelp.advancedConditions"}
          components={{exampleText: <span style={{color: "grey"}} />, li: <li />, ul: <ul />}}
        />
      </CardElement>
    </>
  );
};
