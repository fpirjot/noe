import {TextInput} from "../../../inputs/TextInput";
import {SwitchInputInline} from "../../../inputs/SwitchInput";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {Form} from "antd";
import {useFormCompsFormInstance} from "../atoms/useFormCompsFormInstance";
import {
  CustomPropsGroup,
  FormComponentCustomPropsConfig,
} from "../formCompCustomProps/formCompCustomPropsConfig";
import {useHasFirstReRendered} from "../atoms/useHasFirstReRendered";
import {NamePath} from "rc-field-form/es/interface";
import {EyeOutlined, FilePdfOutlined} from "@ant-design/icons";

const DisplayNameInput: CustomPropsGroup = ({rootName, name}) => {
  const form = useFormCompsFormInstance();
  const displayNameValue = Form.useWatch([...rootName, name, "displayName"], form);
  const {t} = useTranslation();
  const [showInColumns, setShowInColumns] = useState<boolean>();

  const hasFirstReRendered = useHasFirstReRendered();
  useEffect(() => {
    hasFirstReRendered && setShowInColumns(!!displayNameValue);
  }, [hasFirstReRendered]);

  const changeShowInColumns = (value: boolean) => {
    if (!value) {
      form.setFieldValue([...rootName, name, "displayName"], undefined);
      form.setFieldValue([...rootName, name, "inPdfExport"], undefined);
    }
    setShowInColumns(value);
  };

  return hasFirstReRendered ? (
    <>
      <SwitchInputInline
        label={t("common:formBuilder.schema.showInColumns.label")}
        checked={showInColumns}
        checkedChildren={<EyeOutlined />}
        onChange={changeShowInColumns}
      />

      <SwitchInputInline
        label={t("common:formBuilder.schema.inPdfExport.label")}
        name={[name, "inPdfExport"]}
        checkedChildren={<FilePdfOutlined />}
        disabled={!displayNameValue}
        title={
          !showInColumns
            ? t("common:formBuilder.schema.inPdfExport.shouldActivateDisplayName")
            : undefined
        }
      />
      {showInColumns && (
        <TextInput
          label={t("common:formBuilder.schema.displayName.label")}
          placeholder={t("common:formBuilder.schema.displayName.placeholder")}
          required
          name={[name, "displayName"]}
        />
      )}
    </>
  ) : null;
};

export function GeneralFormCompConfig({
  rootName,
  name,
  customProps: CustomProps,
}: {
  rootName: NamePath;
  name: number;
  customProps?: FormComponentCustomPropsConfig["generalTab"];
}) {
  const {t} = useTranslation();

  return (
    <div className={"container-grid three-per-row"} style={{alignItems: "start"}}>
      {/* Left column */}
      <div className={"container-grid"}>
        {CustomProps && <CustomProps name={name} rootName={rootName} />}
        <TextInput
          label={t("common:formBuilder.schema.description.label")}
          placeholder={t("common:formBuilder.schema.description.placeholder")}
          name={[name, "description"]}
        />
      </div>

      {/* Middle column */}
      <div className={"container-grid"} style={{gap: 16}}>
        <DisplayNameInput name={name} rootName={rootName} />
      </div>

      {/* Right column */}
      <div className={"container-grid"} style={{gap: 16}}>
        <SwitchInputInline
          label={t("common:formBuilder.schema.required.label")}
          name={[name, "required"]}
        />
        <SwitchInputInline
          label={t("common:formBuilder.schema.disabled.label")}
          name={[name, "disabled"]}
        />
      </div>
    </div>
  );
}
