import slate, {pluginFactories} from "@react-page/plugins-slate";
import {t} from "i18next";
import {ColorPickerField} from "@react-page/editor";
import "@react-page/plugins-slate/lib/index.css";

/**
 * Add color customization in Slate's, Text plugin toolbar
 * https://react-page.github.io/docs/#/slate?id=example-simple-color-plugin
 */
export const slateTextColor = pluginFactories.createComponentPlugin({
  addHoverButton: true,
  addToolbarButton: true,
  type: "SetColor",
  object: "mark",
  icon: t("common:reactPage.colors.color"),
  label: t("common:reactPage.colors.setColor"),
  Component: "span",
  getStyle: ({color}) => ({color}),
  controls: {
    type: "autoform",
    schema: {
      required: ["color"],
      properties: {
        color: {
          type: "string",
          default: "rgba(0,0,255,1)",
          uniforms: {component: ColorPickerField},
        },
      },
    },
  },
});
export const slateP = slate((def) => ({
  ...def,
  title:
    t("common:reactPage.slatePlugin.title") + " - " + t("common:reactPage.slatePlugin.description"),
  icon: null,
  hideInMenu: true,
  plugins: {
    ...def.plugins,
    custom: {custom1: slateTextColor},
  },
}));
