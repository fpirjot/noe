import {CellPlugin} from "@react-page/editor";
import TextDisplayer from "../TextDisplayer";
import {Suspense} from "react";
import {lazyWithRetry} from "../../../utils/lazyWithRetry";

const TextEditor = lazyWithRetry(() => import("../../components/TextEditor"));

export const quillEditorP: CellPlugin<{html: string}> = {
  allowClickInside: true,

  Renderer: (props) => {
    return props.readOnly ? (
      <TextDisplayer value={props.data.html} />
    ) : (
      <div style={{margin: "-4px -11px"}}>
        <Suspense fallback={null}>
          <TextEditor
            value={props.data.html}
            onChange={(value) => {
              props.onChange({html: value});
            }}
            toolbarSupercharge={[[{header: [1, 2, 3, 4, 5, 6, false]}]]}
          />
        </Suspense>
      </div>
    );
  },
  id: "quill",
  version: 1,

  title: "Text",
  description: "An advanced text area",
  controls: {
    type: "custom",
    Component: () => null,
  },
};
