import {connectField} from "uniforms";
import {ColorInput} from "../../inputs/ColorInput";
import {NumberInput} from "../../inputs/NumberInput";
import {SwitchInput} from "../../inputs/SwitchInput";

export const colorPickerComponent = connectField(({value, label, onChange, ...props}) => {
  return (
    <ColorInput
      label={label}
      value={value}
      disabledAlpha={false}
      onChange={(val) => onChange("#" + val.toHex())}
      formItemProps={{style: {marginBottom: 0}}}
    />
  );
});
export const numberComponent = connectField(({value, label, onChange, ...props}) => {
  return (
    <NumberInput
      label={label}
      value={value}
      onChange={onChange}
      bordered
      addonAfter={"px"}
      style={{maxWidth: 110}}
      formItemProps={{style: {marginBottom: 0}}}
    />
  );
});
export const switchComponent = connectField(({value, label, onChange, ...props}) => {
  return (
    <SwitchInput
      label={label}
      checked={value}
      onChange={(value) => onChange(value)}
      formItemProps={{style: {marginBottom: 0}}}
    />
  );
});
