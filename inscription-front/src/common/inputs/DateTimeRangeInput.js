import {FormItem, FormItemProps} from "./FormItem";
import {RangePickerProps} from "antd/es/date-picker";
import {DatePicker} from "antd";
import React from "react";
import type {DateTimeInputProps} from "./DateTimeInput";
import {DatePickerComponent} from "./DateTimeInput";

/**
 * Range Datetime input
 */
export const DateTimeRangeInput = (props: FormItemProps<DateTimeInputProps & RangePickerProps>) => (
  <FormItem {...props}>
    <DatePickerComponent isRange DatePickerComponent={DatePicker.RangePicker} />
  </FormItem>
);
