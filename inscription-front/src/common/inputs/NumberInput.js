import {InputNumber, InputNumberProps} from "antd";
import React from "react";
import {FormItem, FormItemProps} from "./FormItem";

export const NumberInput = (props: FormItemProps<InputNumberProps>) => (
  <FormItem {...props}>
    <InputNumber bordered={false} keyboard />
  </FormItem>
);
