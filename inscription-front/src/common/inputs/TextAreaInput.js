import {FormItem, FormItemProps} from "./FormItem";
import {Input} from "antd";
import React from "react";
import {TextAreaProps} from "antd/es/input";

export const TextAreaInput = (props: FormItemProps<TextAreaProps>) => (
  <FormItem {...props}>
    <Input.TextArea
      bordered={false}
      autoSize={{minRows: 2}}
      // allow to return to the lines by overriding the validation of the web page with "enter"
      onKeyDown={(event) => event.key === "Enter" && event.stopPropagation()}
    />
  </FormItem>
);
