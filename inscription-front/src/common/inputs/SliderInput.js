import {Slider} from "antd";
import React from "react";
import {FormItem, FormItemProps} from "./FormItem";
import type {SliderRangeProps, SliderSingleProps} from "antd/es/slider";

export const SliderInput = (props: FormItemProps<SliderSingleProps | SliderRangeProps>) => (
  <FormItem {...props}>
    <Slider />
  </FormItem>
);
