import {TimePicker, TimePickerProps} from "antd";
import {FormItem, FormItemProps} from "./FormItem";
import React from "react";

export const TimeInput = (props: FormItemProps<TimePickerProps>) => (
  <FormItem {...props}>
    <TimePicker minuteStep={5} bordered={false} format="HH:mm" />
  </FormItem>
);
