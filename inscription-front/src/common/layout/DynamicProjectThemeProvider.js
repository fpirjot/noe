import {ConfigProvider} from "antd";
import {useSelector} from "react-redux";
import {viewSelectors} from "../../features/view";
import {useThemeToken} from "../hooks/useThemeToken";
import {currentProjectSelectors} from "../../features/currentProject";
import {useThemedAppMethods} from "../hooks/useThemedAppMethods";

const setCSSVariables = (vars) => {
  const htmlStyle = document.getElementsByTagName("html")[0].style;
  Object.entries(vars).forEach((v) =>
    v[1] !== null ? htmlStyle.setProperty(v[0], v[1]) : htmlStyle.removeProperty(v[0])
  );
};

export const setAppTheme = ({primary, bg, accent1, accent2} = {}) =>
  setCSSVariables({
    "--noe-primary": primary || null,
    "--noe-bg": bg || null,
    "--noe-accent-1": accent1 || null,
    "--noe-accent-1-90": accent1 || null,
    "--noe-accent-1-85": accent1 || null,
    "--noe-accent-2": accent2 || null,
    "--noe-accent-2-85": accent2 || null,
  });

export const useDarkModeTheme = () => {
  // Modify the html to add a light or dark mode class
  const darkMode = useSelector(viewSelectors.selectDarkMode);
  document.getElementsByTagName("html")[0].className = darkMode ? "dark-mode" : "light-mode";

  // Instanciate the message and notifcation methods with the theme
  useThemedAppMethods();
};

// Set up the CSS variables for the user theme
export const DynamicProjectThemeProvider = ({children}) => {
  const theme = useSelector((s) => currentProjectSelectors.selectProject(s).theme);

  const darkMode = useSelector(viewSelectors.selectDarkMode);
  const {colorBgElevated} = useThemeToken();

  if (theme) {
    setAppTheme({
      ...theme,
      bg: darkMode ? colorBgElevated : theme.bg, // in dark mode, override the bg color to the bg elevated color.
    });
  }

  const primaryColor = theme?.primary;

  return (
    <ConfigProvider
      theme={
        primaryColor && {
          token: {
            colorPrimary: primaryColor,
            colorLink: primaryColor,
          },
        }
      }>
      {children}
    </ConfigProvider>
  );
};
