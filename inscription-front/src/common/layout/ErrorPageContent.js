import React from "react";
import {Result} from "antd";
import {useTranslation} from "react-i18next";

export default function ErrorPageContent({extra}) {
  const {t} = useTranslation();
  return (
    <Result
      style={{margin: "auto"}}
      status="404"
      title={t("common:errorBoundary.title")}
      subTitle={
        <>
          <p>{t("common:errorBoundary.subTitle.helpUs")}</p>

          <p>
            <a
              href={`mailto:${process.env.REACT_APP_CONTACT_US_EMAIL}?subject=Bug%20NOÉ%20(${window.location.href})`}
              rel="noreferrer">
              {t("common:errorBoundary.subTitle.sendEmail")}
            </a>
          </p>
          <p>
            <a href="https://m.me/noeappio" target="_blank" rel="noreferrer">
              {t("common:errorBoundary.subTitle.socialNetworks")}
            </a>
          </p>
          <p>{t("common:errorBoundary.subTitle.takesOneMinute")}</p>
          <p>{t("common:thanks")} 🥰</p>
        </>
      }
      extra={extra}
    />
  );
}
