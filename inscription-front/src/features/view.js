import {createSelector, createSlice} from "@reduxjs/toolkit";
import {getLocalStorage, setLocalStorage} from "../utils/localStorageUtilities";
import {DEFAULT_AGENDA_PARAMS, getAgendaDisplayStartDate} from "../utils/agendaUtilities";

export const paginationPageSizes = [10, 20, 40, 60, 100];
export const defaultPagination = {pageSize: paginationPageSizes[2], current: 1};

export const getUrlSearchParams = () => {
  const urlSearchString = window.location.search;
  const searchParamsInfo = {};
  if (urlSearchString?.length > 0) {
    const urlSearchParams = new URLSearchParams(urlSearchString);
    searchParamsInfo.firstName = urlSearchParams.get("firstName");
    searchParamsInfo.lastName = urlSearchParams.get("lastName");
    searchParamsInfo.email = urlSearchParams.get("email");
    searchParamsInfo.ticketId = urlSearchParams.get("ticketId");
    searchParamsInfo.invitationToken = urlSearchParams.get("invitationToken");
  }
  return searchParamsInfo;
};

export const viewSlice = createSlice({
  name: "view",
  initialState: {
    darkMode: getLocalStorage("darkMode"),
    searchParams: getUrlSearchParams(),
    scroll: {},
    sorting: {},
    search: {},
    pagination: {},
    isModified: {},
    columnsBlacklist: getLocalStorage("columnsBlacklist", {}),
    agendaParams: getLocalStorage("agendaParams", DEFAULT_AGENDA_PARAMS),
    sessionsViewMode: getLocalStorage("sessionsViewMode", "list"),
  },
  reducers: {
    setSearchParams: (state, action) => {
      state.searchParams = action.payload;
    },
    changeScroll: (state, action) => {
      state.scroll = {
        ...state.scroll,
        [window.location.pathname]: action.payload,
      };
    },
    changeSorting: (state, action) => {
      state.sorting = {
        ...state.sorting,
        [window.location.pathname]: action.payload,
      };
    },
    changeSearch: (state, action) => {
      state.search = {
        ...state.search,
        [window.location.pathname]: action.payload,
      };
    },
    changePagination: (state, action) => {
      state.pagination = {
        ...state.pagination,
        [window.location.pathname]: action.payload,
      };
    },
    changeDarkMode: (state, action) => {
      state.darkMode = action.payload;
      setLocalStorage("darkMode", state.darkMode);
    },
    changeIsModified: (state, action) => {
      state.isModified = {
        ...state.isModified,
        [window.location.pathname]: action.payload,
      };
    },
    changeColumnsBlacklist: (state, action) => {
      state.columnsBlacklist = {
        ...state.columnsBlacklist,
        [action.payload.key]: action.payload.blacklist,
      };
      setLocalStorage("columnsBlacklist", state.columnsBlacklist);
    },
    changeAgendaParams: (state, action) => {
      const newParams = {
        ...state.agendaParams,
        [action.payload.projectId]: {
          ...state.agendaParams[action.payload.projectId],
          ...action.payload.params,
        },
      };
      state.agendaParams = newParams;
      setLocalStorage("agendaParams", newParams);
    },
    changeSessionsViewMode: (state, action) => {
      state.sessionsViewMode = action.payload;
      setLocalStorage("sessionsViewMode", action.payload);
    },
  },
});

const asyncActions = {};

export const viewSelectors = {
  selectDarkMode: (state) => state.view.darkMode,
  selectIsModified: (state) => state.view.isModified[window.location.pathname],
  selectScroll: (state) => state.view.scroll[window.location.pathname],
  selectSorting: (state) => state.view.sorting[window.location.pathname],
  selectSearch: (state) => state.view.search[window.location.pathname],
  selectPagination: (state) => state.view.pagination[window.location.pathname],
  selectColumnsBlacklist: (key) => (state) => state.view.columnsBlacklist[key],
  selectSearchParams: (state) => state.view.searchParams,
  selectSessionsViewMode: (state) => state.view.sessionsViewMode,
};

// Memoized selector for agenda params
viewSelectors.selectAgendaParams = createSelector(
  [(state) => state.view.agendaParams, (state) => state.currentProject.project],
  (agendaParams, project) => {
    const currentProjectAgendaParams = agendaParams?.[project._id];
    return {
      ...DEFAULT_AGENDA_PARAMS,
      ...currentProjectAgendaParams,
      currentAgendaDate:
        currentProjectAgendaParams?.currentAgendaDate || getAgendaDisplayStartDate(project),
    };
  }
);

export const viewReducer = viewSlice.reducer;

export const viewActions = {
  ...viewSlice.actions,
  ...asyncActions,
};
