export const smoothScrollIntoView = (elem: HTMLElement) => {
  // If not visible, don't scroll into view
  if (window.getComputedStyle(elem).display === "none") return;

  const rect = elem.getBoundingClientRect();
  const options = {inline: "nearest", behavior: "smooth"};

  // If the element is below the view, scroll down until we see the end of the element
  if (rect.bottom > window.innerHeight) elem.scrollIntoView({block: "end", ...options});

  // If the element is above the view, scroll up until we see the start of the element
  if (rect.top < 0) elem.scrollIntoView({block: "start", ...options});

  // Else, do nothing
};
