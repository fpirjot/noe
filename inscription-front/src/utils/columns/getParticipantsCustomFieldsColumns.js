import {flattenFormComponents} from "../registrationsUtilities";
import {cleanAnswer} from "./cleanAnswer";
import {listOfClickableElements} from "../listOfClickableElements";
import {sorter} from "../sorters";
import {EditableCell} from "../../common/components/EditableCell";
import {EyeOutlined} from "@ant-design/icons";
import i18n from "i18next";

type GetParticipantsCustomFieldsColumnsParams = {
  project: any,
  nameSuffix?: string,
  getAllFormAnswers?: (record: any) => Array<any>,
  onEdit?: (record: any) => Promise<void>,
};

// Get the columns generated from the participant custom data
export const getParticipantsCustomFieldsColumns = ({
  project,
  nameSuffix,
  getAllFormAnswers = (record) => [record.formAnswers],
  onEdit,
}: GetParticipantsCustomFieldsColumnsParams) => {
  const flatFormComponents = flattenFormComponents(project.formComponents).filter(
    (comp) => comp.displayName
  );

  const renderAnswersList = (
    record,
    formComp,
    render = (formAnswers) => cleanAnswer(formAnswers?.[formComp.key], undefined, formComp)
  ) => listOfClickableElements(getAllFormAnswers(record), render);

  return (
    flatFormComponents?.map((formComp) => ({
      title: (
        <>
          <EyeOutlined
            title={i18n.t("projects:schema.formComponents.label")}
            style={{opacity: 0.8}}
          />{" "}
          {formComp.displayName + (nameSuffix ? ` ${nameSuffix}` : "")}
        </>
      ),
      dataIndex: formComp.displayName,
      render: (text, record) =>
        renderAnswersList(record, formComp, (formAnswers) => (
          <EditableCell
            value={formAnswers?.[formComp.key]}
            formComp={formComp}
            onChange={
              onEdit
                ? async (value) =>
                    await onEdit({
                      _id: record._id,
                      formAnswers: {...formAnswers, [formComp.key]: value},
                    })
                : undefined
            }
          />
        )),
      sorter: (a, b) => sorter.text(renderAnswersList(a, formComp), renderAnswersList(b, formComp)),
      searchable: true,
      searchText: (record) => renderAnswersList(record, formComp),
    })) || []
  );
};
