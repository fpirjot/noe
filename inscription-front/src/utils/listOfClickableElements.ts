import {ReactNode} from "react";

export const listOfClickableElements = <T>(
  elements: Array<T>,
  clickableElement: (element: T, index: number) => ReactNode
) =>
  elements
    ?.map((el, index) => el && clickableElement(el, index))
    .reduce((acc, el) => (acc ? [acc, ", ", el] : el), null) || "";
