### Résumé en une ligne :



### Marche à suivre pour reproduire le bug :

- 

### Ce qui devrait se passer :



### Ce qui se passe à la place :



### Screenshots :



### Fichiers (export du projet sur lequel reproduire le bug, etc) :



> Je m'assure que:
> - J'ai mis le label "bug" ou "regression"
> - J'ai mis les labels qui correspondent à mon issue ("back", "front", "ia", et la "priority::X")