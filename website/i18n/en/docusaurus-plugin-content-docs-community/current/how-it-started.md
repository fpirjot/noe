---
sidebar_label: How it started
sidebar_position: 4
---

# How did the NOÉ project get started?

The NOÉ project was launched by Alternatiba in 2019. Alternatiba is a social and climate justice association.
climate justice. Originally, NOÉ was created to help organise Climate Camps (which are gatherings of
activists throughout France to learn about climate and social justice issues, meet each other
share and federate the climate movement).

Then we saw that NOÉ was also attracting other actors and associations, who also needed a natively horizontal tool for organising
natively horizontal tool for the organisation of their events... This is how NOÉ started to be used in
festivals, and other seminars.