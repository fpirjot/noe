// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion
import {WEBSITE_URL} from "./src/css/common/sharedConfig";

const editUrl = "https://gitlab.com/alternatiba/noe/-/tree/master/website/";

/** @type {import("@docusaurus/types").Config} */
const config = {
  title: "NOÉ",
  tagline: "Votre nouveau QG événementiel.",
  url: WEBSITE_URL,
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenAnchors: "throw",
  onBrokenMarkdownLinks: "throw",
  favicon: "favicons/favicon.ico",

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "fr",
    locales: ["fr", "en"],
    localeConfigs: {
      en: {label: "English"},
      fr: {label: "Français"},
    },
  },

  customFields: {
    CONTACT_US_EMAIL: process.env.REACT_APP_CONTACT_US_EMAIL,
    API_DOMAIN: new URL(process.env.REACT_APP_API_URL).hostname,
    ORGA_FRONT_URL: process.env.REACT_APP_ORGA_FRONT_URL,
    INSCRIPTION_FRONT_URL: process.env.REACT_APP_INSCRIPTION_FRONT_URL,
  },

  presets: [
    [
      "classic",
      /** @type {import("@docusaurus/preset-classic").Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebarsDocs.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl,
          editLocalizedFiles: true,
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl,
          editLocalizedFiles: true,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.less"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import("@docusaurus/preset-classic").ThemeConfig} */
    ({
      image: "img/common/social-card-website.gif", // Image for social cards sharing
      colorMode: {
        defaultMode: "light",
        disableSwitch: true,
        respectPrefersColorScheme: false,
      },
      navbar: {
        title: "NOÉ",
        hideOnScroll: true,
        logo: {
          alt: "NOÉ logo",
          src: "img/common/logo-colors.svg",
          width: 30,
          style: {paddingRight: 5},
        },
        items: [
          // LEFT
          {
            label: "Documentation",
            to: "/docs",
            position: "left",
          },
          {
            label: "Centre d'aide",
            to: "/community/help-center",
            position: "left",
          },
          {
            label: "Open-roadmap",
            to: "/community/open-roadmap",
            position: "left",
          },

          // RIGHT
          // TODO We don't have anything to search for yet
          // {
          //   type: "search",
          //   position: "right",
          // },
          {
            type: "custom-connectToNOEButton",
            position: "right",
          },
          {
            label: "Discord",
            href: "https://discord.gg/hdrJnFqmXJ",
            position: "right",
          },
          {
            label: "Faire un don",
            href: "https://opencollective.com/noeappio/contribute/participation-libre-pour-mon-evenement-46912",
            position: "right",
          },
          {
            type: "localeDropdown",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        logo: {
          alt: "NOÉ logo",
          src: "img/common/logo-colors-with-white-text.svg",
          style: {paddingTop: 30, width: 200, opacity: 0.7},
        },
        links: [
          {
            title: "learn",
            items: [
              {
                label: "introduction",
                to: "/docs",
              },
              {
                label: "getting-started",
                to: "/docs/getting-started",
              },
              {
                label: "developers",
                to: "/docs/developers",
              },
            ],
          },
          {
            title: "community",
            items: [
              {
                label: "Centre d'aide",
                to: "/community/help-center",
              },
              {
                label: "Open-Roadmap",
                to: "/community/open-roadmap",
              },
              {
                label: "Traduire NOÉ",
                to: "/community/translate",
              },
            ],
          },
          {
            title: "social-links",
            items: [
              {
                label: "Discord",
                href: "https://discord.gg/hdrJnFqmXJ",
              },
              {
                label: "Open Collective",
                href: "https://opencollective.com/noeappio",
              },
              {
                label: "Telegram",
                href: "https://t.me/noeappio",
              },
              {
                label: "Facebook",
                href: "https://facebook.com/noeappio",
              },
              {
                label: "Instagram",
                href: "https://instagram.com/noeappio",
              },
            ],
          },
          {
            title: "more info",
            items: [
              {
                label: "our team",
                href: "/team",
              },
              {
                label: "GitLab",
                href: "https://gitlab.com/alternatiba/noe",
              },
              {
                label: "contact us",
                href: "mailto:" + process.env.REACT_APP_CONTACT_US_EMAIL,
              },
            ],
          },
        ],
      },
    }),

  plugins: [
    "docusaurus-plugin-less",
    [
      "content-docs",
      /** @type {import('@docusaurus/plugin-content-docs').Options} */
      ({
        id: "community",
        path: "community",
        routeBasePath: "community",
        sidebarPath: require.resolve("./sidebarsOther.js"),
      }),
    ],
    [
      "ideal-image",
      /** @type {import('@docusaurus/plugin-ideal-image').PluginOptions} */
      ({
        quality: 70,
        max: 1030,
        min: 640,
        steps: 2,
        disableInDev: true,
      }),
    ],
    [
      "pwa",
      /** @type {import('@docusaurus/plugin-content-pwa').Options} */
      {
        offlineModeActivationStrategies: ["appInstalled", "standalone", "queryString"],
        pwaHead: [
          {tagName: "link", rel: "icon", href: "img/favicons/android-chrome-192x192.png"},
          {tagName: "link", rel: "manifest", href: "manifest.json"},
          {tagName: "meta", name: "theme-color", content: "#000A2E"},
        ],
      },
    ],
    // TODO We don't have anything to search for yet
    // require.resolve("docusaurus-lunr-search"),
  ],
};

module.exports = config;
