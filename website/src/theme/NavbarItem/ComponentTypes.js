import ComponentTypes from "@theme-original/NavbarItem/ComponentTypes";
import {ConnectToNOEButton} from "@site/src/components/ConnectToNOEButton";

export default {
  ...ComponentTypes,
  "custom-connectToNOEButton": ConnectToNOEButton,
};
