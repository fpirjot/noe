import {ConfigProvider} from "antd";
import {CssVarsProvider, extendTheme} from "@mui/joy/styles";
import {LIGHT_THEME} from "./../css/common/theme";
import {useLocation} from "@docusaurus/router";
import {useEffect, useState} from "react";

const muiTheme = extendTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 996,
      lg: 1200,
      xl: 1536,
    },
  },
  spacing: 4,
});

export default function Root({children}) {
  const location = useLocation();

  const [isScrolled, setIsScrolled] = useState(false);

  useEffect(() => {
    const onScroll = () => setIsScrolled(window.scrollY > 200);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, {passive: true});
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <CssVarsProvider theme={muiTheme}>
      <ConfigProvider componentSize="large" theme={LIGHT_THEME}>
        {/* Store the path inside so we can access it with css */}
        <main data-route={location.pathname} data-is-scrolled={isScrolled}>
          {children}
        </main>
      </ConfigProvider>
    </CssVarsProvider>
  );
}
