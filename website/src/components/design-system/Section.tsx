import {Box, BoxProps} from "@mui/joy";
import React, {CSSProperties} from "react";

const Section = (props: BoxProps) => <Box component={"section"} {...props} />;

type SectionWithContainer = BoxProps & {maxWidth?: CSSProperties["maxWidth"]};
export const SectionWithContainer = ({children, maxWidth, ...props}: SectionWithContainer) => (
  <Section {...props}>
    <Box sx={{py: 16, px: {xs: 6, sm: 8, md: 12}, maxWidth}} className="container">
      {children}
    </Box>
  </Section>
);

export const SectionPrimary = ({children, ...props}: SectionWithContainer) => (
  <SectionWithContainer
    {...props}
    sx={{background: "var(--noe-gradient)", color: "white", ...props.sx}}>
    {children}
  </SectionWithContainer>
);

export const SectionSecondary = ({children, ...props}: SectionWithContainer) => (
  <SectionWithContainer {...props} sx={{background: "#F5F5F5", ...props.sx}}>
    {children}
  </SectionWithContainer>
);

export const SectionTertiary = ({children, ...props}: SectionWithContainer) => (
  <SectionWithContainer {...props} sx={{background: "white", ...props.sx}}>
    {children}
  </SectionWithContainer>
);

export const SectionPrimaryInverse = ({children, ...props}: SectionWithContainer) => (
  <SectionWithContainer {...props} sx={{background: "var(--noe-bg)", color: "white", ...props.sx}}>
    {children}
  </SectionWithContainer>
);
