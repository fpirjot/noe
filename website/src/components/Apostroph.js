import {Box} from "@mui/joy";

export function Apostroph() {
  return (
    <Box sx={{maxHeight: 50, mt: -4}}>
      <span style={{color: "grey", fontSize: 70, marginLeft: -25}}>‟</span>
    </Box>
  );
}
