import SendEmailButton from "./SendEmailButton";
import Translate from "@docusaurus/Translate";
import Link from "@docusaurus/Link";
import {SectionSecondary} from "./design-system/Section";
import {Stack} from "@mui/joy";

export function ContactUsBanner({showTeamButton = true}) {
  return (
    <SectionSecondary maxWidth={"45rem"}>
      <Stack gap={6} alignItems={"center"}>
        <p className={"displayText"} style={{textAlign: "center"}}>
          <Translate id="homepage.contactUs" />
        </p>

        <SendEmailButton className="button--primary button--lg" />

        <Link className="button button--secondary" href="https://discord.gg/hdrJnFqmXJ">
          <Translate id="homepage.contactUs.joinCommunityOnDiscord" />
        </Link>

        {showTeamButton && (
          <Link href="/team">
            <Translate id="team.tagline" />
          </Link>
        )}
      </Stack>
    </SectionSecondary>
  );
}
