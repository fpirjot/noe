import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Translate from "@docusaurus/Translate";

export default function SendEmailButton({className = "button--primary button--md"}) {
  const {siteConfig} = useDocusaurusContext();
  const email = siteConfig.customFields.CONTACT_US_EMAIL;
  return (
    <Link
      className={`button ${className}`}
      style={{textDecoration: "none"}}
      href={`mailto:${email}`}>
      <div
        style={{
          display: "flex",
          gap: "0.4rem",
          flexWrap: "wrap",
          maxWidth: "70vw",
          justifyContent: "center",
        }}>
        <Translate id="sendEmailTo" />
        <span style={{textDecoration: "underline"}}>{email}</span> ️✉️
      </div>
    </Link>
  );
}
