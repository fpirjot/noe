import {SectionSecondary} from "../../components/design-system/Section";
import {Box, Stack} from "@mui/joy";
import Translate from "@docusaurus/Translate";
import Link from "@docusaurus/Link";

export function SummaryAndLinks() {
  return (
    <SectionSecondary maxWidth={"45rem"}>
      <Stack gap={6}>
        <Box className={"displayText"} textAlign={"center"}>
          <p>
            <Translate id="homepage.summary" />
          </p>
          <p>
            <Translate id="homepage.summaryOpensource" />
          </p>
          <Link className="button button--secondary" href="/team">
            <Translate id="team.tagline" />
          </Link>
        </Box>
      </Stack>
    </SectionSecondary>
  );
}
