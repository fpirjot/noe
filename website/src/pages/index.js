import Layout from "@theme/Layout";
import {translate} from "@docusaurus/Translate";
import FeaturesGrid from "./_index/FeaturesGrid";
import {ContactUsBanner} from "../components/ContactUsBanner";
import {SummaryAndLinks} from "./_index/SummaryAndLinks";
import {YourFirstPlanningIn4Minutes} from "./_index/YourFirstPlanningIn4Minutes";
import {NoeHeroHeader} from "./_index/NoeHeroHeader";
import {TheyTrustUs} from "./_index/TheyTrustUs";

export default function Home() {
  return (
    <Layout
      title={translate({id: "homepage.tagline"})}
      description={translate({id: "homepage.summary"})}>
      <NoeHeroHeader />
      <SummaryAndLinks />
      <FeaturesGrid />
      <YourFirstPlanningIn4Minutes />
      <TheyTrustUs />
      <ContactUsBanner showTeamButton={false} />
    </Layout>
  );
}
