import {combineSolutions} from "../helpers/iaUtilities";

interface part {
  id: any;
  occurrence: any;
  nonBlockingActivity: boolean;
  session: any;
  key: any;
  keyCategory: any;
  colorCategory: any;
  dependencies: any;
  length: any;
  title: any;
  ordered: any;
  spacing: any;
  multi: any;
  volunteerFactor: any;
  inscriptionFree: any;
  secret: any;
  trainingDependencies: any;
  slotsAvaible: any;
  weight: number;
  isScheduleFrozen: boolean;
}

interface roomAvailability {
  startAvailability: Date;
  endAvailability: Date;
  keyRoom: string;
  type: string;
  equipment: any[];
  lengthAvailability: number;
  nbAvailability: number;
}

module.exports = class SlotsGenerator {
  mute: any;
  constructor(mute: any) {
    this.mute = mute;
  }

  filterOutBadRooms(
    roomAvailability: roomAvailability[],
    candidate: any,
    availabilitySlots: any[]
  ) {
    let roomAvailabilityCandidates = roomAvailability.map((roomAvailability: roomAvailability) => {
      const overlappingTime = (daySlot: any): number => {
        const rm = {...roomAvailability};
        rm.startAvailability = new Date(
          Math.max.apply(null, [rm.startAvailability, daySlot.start])
        );
        rm.endAvailability = new Date(Math.min.apply(null, [rm.endAvailability, daySlot.end]));
        return rm.endAvailability.getTime() - rm.startAvailability.getTime();
      };
      const matchingDayAvailability = availabilitySlots
        .sort((slot_a, slot_b) => {
          const res = overlappingTime(slot_a) - overlappingTime(slot_b);
          if (res < 0) {
            return -1;
          } else if (res > 0) {
            return 1;
          }
          return 0;
        })
        .reverse();
      if (matchingDayAvailability.length > 0) {
        const daySlot = matchingDayAvailability[0];
        if (daySlot.start > roomAvailability.startAvailability) {
          roomAvailability.startAvailability = daySlot.start;
        }
        if (daySlot.end < roomAvailability.endAvailability) {
          roomAvailability.endAvailability = daySlot.end;
        }
      }
      return roomAvailability;
    });
    roomAvailabilityCandidates = roomAvailabilityCandidates.filter(
      (candidate: any) => candidate.endAvailability >= candidate.startAvailability
    );
    return roomAvailabilityCandidates;
  }

  //TODO ce generateur genere des possibilités de placement d'activité et non de plage.
  //Cela fonctionnne mais sous-entend que lors ce qu'une activité est placé par le solver, l'enssemble des plages de celle ci sont placés à la suite.
  //refont globale pour que L'IA traite des slots plutot que des activity à prévoir; sans doute une heuristique qui evalue que les slots de la meme activity soient rapprohés.
  generate(candidate: any, resources: any, config: any) {
    // the launcher will be able to launch the IA
    const availabilitySlots = config.project.availabilitySlots;
    let roomAvailabilityCandidates = resources.roomAvailability;
    roomAvailabilityCandidates = this.filterOutBadRooms(
      roomAvailabilityCandidates,
      candidate,
      availabilitySlots
    );

    if (candidate.roomAffectation.length > 0) {
      roomAvailabilityCandidates = roomAvailabilityCandidates.filter((ra: any) => {
        let roomValidity = false;
        candidate.roomAffectation.forEach((roomAffectation: any) => {
          if (roomAffectation == ra.keyRoom) {
            roomValidity = true;
          }
        });
        return roomValidity;
      });
    }

    if (candidate.equipment.length > 0) {
      roomAvailabilityCandidates = roomAvailabilityCandidates.filter((ra: any) => {
        let validEquipment = 0;
        const roomValidity = false;
        candidate.equipment.forEach((trainingEquipment: any) => {
          const roomEquimentSearch = ra.equipment.filter((e: any) => e == trainingEquipment.key);
          if (roomEquimentSearch.length > 0) {
            validEquipment++;
          }
        });
        return validEquipment == candidate.equipment.length;
      });
    }

    const parts = [];

    if (candidate.slots.length === 0) {
      const slotAvailability = this.generateSlotAvailability(
        roomAvailabilityCandidates,
        resources,
        config,
        candidate
      );

      const nbFrozenSessions = candidate.existingSessions.filter(
        (session: any) => session.isScheduleFrozen
      ).length;
      let occurrence = 1;
      // Les Sessions frozen generent également des candidates.
      // Ils seront transformé en initialState par le Solver
      for (const session of candidate.existingSessions.filter(
        (_session: any) => _session.isScheduleFrozen
      )) {
        parts.push(this.partFactory(candidate, occurrence, undefined, session));
        occurrence++;
      }
      for (let i = nbFrozenSessions; i < candidate.occurrence; i++) {
        parts.push(this.partFactory(candidate, occurrence, slotAvailability));
        occurrence++;
      }
    }

    return parts;
  }

  slotFactory(
    startStepTime: Date,
    endStepTime: Date,
    roomAvailabilityCandidates: any,
    trainerAvailabilityValid: any
  ) {
    return {
      startStepTime: startStepTime,
      endStepTime: endStepTime,
      keyRoom: roomAvailabilityCandidates.keyRoom,
      keyTrainer: trainerAvailabilityValid.map((taV: any) => taV.keyTrainer),
      roomAvailability: roomAvailabilityCandidates,
      trainerAvailability: trainerAvailabilityValid,
      roomStartAvailability: roomAvailabilityCandidates.startAvailability,
    };
  }

  partFactory(
    candidate: any,
    occurrence: number,
    slotAvailability: any[],
    session: any = undefined
  ): part {
    return {
      id: candidate.key + "[" + occurrence + "]",
      title: candidate.title,
      nonBlockingActivity: candidate.nonBlockingActivity,
      occurrence: occurrence,
      key: candidate.key,
      keyCategory: candidate.keyCategory,
      colorCategory: candidate.colorCategory,
      length: candidate.length,
      slotsAvaible: slotAvailability,
      session: session,
      //TODO add places, stewrds, slots places, slots steward, slotsSynchor...
      ordered: candidate.ordered,
      spacing: candidate.spacing,
      //TODO multi seems to be deprecated
      multi: candidate.multi,
      // dependencies of parts computing at solver
      dependencies: undefined,
      volunteerFactor: candidate.volunteerFactor,
      inscriptionFree: candidate.inscriptionFree,
      secret: candidate.secret,
      weight: slotAvailability
        ? (candidate.length * candidate.length) / slotAvailability.length
        : undefined,
      trainingDependencies: candidate.dependencies,
      isScheduleFrozen: session != undefined,
    };
  }

  generateSlotAvailability(
    roomAvailabilityCandidates: any,
    resources: any,
    config: any,
    activity: any
  ) {
    const slotAvailability: any[] = [];
    roomAvailabilityCandidates.forEach((roomAvailability: any) => {
      const startRoomTime = roomAvailability.startAvailability;
      const endRoomTime = roomAvailability.endAvailability;
      const lengthRoomTime = (endRoomTime - startRoomTime) / 1000 / 60;
      const slotStepNb = lengthRoomTime / config.lengthMinuteStep;
      for (let startStep = 0; startStep < slotStepNb; startStep++) {
        const startStepTime = new Date(
          startRoomTime.getTime() + startStep * config.lengthMinuteStep * 60 * 1000
        );
        const endStepTime = new Date(startStepTime.getTime() + activity.length * 60 * 1000);
        if (endStepTime <= endRoomTime) {
          const trainerAvailabilityValid: any[] = resources.trainerAvailability.filter(
            (ta: any) =>
              ta.affectation.filter((a: any) => a == activity.key).length > 0 &&
              ta.startAvailability.getTime() <= startStepTime.getTime() &&
              ta.endAvailability.getTime() >= endStepTime.getTime()
          );

          if (
            !activity.minNumberOfStewards ||
            (activity.minNumberOfStewards &&
              trainerAvailabilityValid.length >= activity.minNumberOfStewards)
          ) {
            // We have to create all the possibilities that satisfy stewards availabilities and the min number of stewards

            const allStewardsCombinations: Array<any> = combineSolutions(
              trainerAvailabilityValid,
              activity.minNumberOfStewards
            );

            allStewardsCombinations.forEach((stewardsCombination) =>
              slotAvailability.push(
                this.slotFactory(startStepTime, endStepTime, roomAvailability, stewardsCombination)
              )
            );
          }
        }
      }
    });
    if (slotAvailability.length === 0) {
      console.error("Empty slots");
    }
    return slotAvailability;
  }
};
