const NodeClass = require("./node");
const SlotsGenerator = require("./slotsGenerator");
module.exports = class Solver {
  mute: any;
  roomAvailability: any;

  constructor(mute: any) {
    this.mute = mute; // Write or not the execution tree in the console
  }

  /**
   * Execute the solver and return the result and its metadata
   * @param data the data given by the launcher in order to solve the problem
   * @param config the IA engine configuration
   * @returns a hash that contains the solution and sums up all the infos about the calculation that occurred
   */
  execute(data: any, config: any) {
    // Basic checks and reformating
    if (config.project) {
      config.project.start = new Date(config.project.start);
      config.project.end = new Date(config.project.end);
    }
    for (const roomAvailability of data.roomAvailability) {
      roomAvailability.startAvailability = new Date(roomAvailability.startAvailability);
      roomAvailability.endAvailability = new Date(roomAvailability.endAvailability);
      roomAvailability.lengthAvailability =
        (roomAvailability.endAvailability - roomAvailability.startAvailability) / 1000 / 60;
      roomAvailability.nbAvailability =
        roomAvailability.lengthAvailability / config.lengthMinuteStep - 1;
    }
    for (const trainerAvailability of data.trainerAvailability) {
      trainerAvailability.startAvailability = new Date(trainerAvailability.startAvailability);
      trainerAvailability.endAvailability = new Date(trainerAvailability.endAvailability);
    }
    for (const slot of config.project.availabilitySlots) {
      slot.start = new Date(slot.start);
      slot.end = new Date(slot.end);
    }

    this.roomAvailability = data.roomAvailability;
    // Array of candidates: all the possibilities (= nodes) that we will create. "The Horizon of possibles" we can say
    let candidateSessions: any = [];

    // Generate the horizon of possibles.
    // We take each activity, we generate all te possibilities where it can be placed, with no filter at all, and
    // concatenate that in a single array.
    const slotsGenerator = new SlotsGenerator();
    for (const activity of data.training) {
      activity.occurrence = activity.occurrence || 1;
      const slotsCandidates = slotsGenerator.generate(activity, data, config);
      candidateSessions = candidateSessions.concat(slotsCandidates);
    }

    // STILL NOT USED IN BACKOFFICE
    // In the case of activities that depend from other activities
    // (for ex. "Video montage" must be after "Initiation to video"): set dependencies from activities to sessions,
    // because sessions should know it, not only the activities
    // TODO : see if the soltsGenerator could actually do that too
    const partsWithoutDependencies = candidateSessions.filter(
      (p: any) => p.dependencies == undefined
    );
    partsWithoutDependencies.forEach((pwd: any) => {
      pwd.dependencies = [];
      if (pwd.trainingDependencies && pwd.trainingDependencies.length > 0) {
        pwd.trainingDependencies.forEach((td: any) => {
          const partDependencies = candidateSessions.filter((p: any) => p.key == td.dependency);
          partDependencies.forEach((pd: any) => {
            pwd.dependencies.push({
              dependency: pd.id,
              isOnlyAiDependency: td.isOnlyAiDependency,
            });
          });
        });
      }
    });

    // STILL NOT USED IN BACKOFFICE
    // Compute inverse dependencies (same as above). useful to know if an activity has some others that depend on it,
    // because we could prioritize them at the beginning of the event for ex.
    candidateSessions.forEach((p: any) => {
      p.reverseDependencies = [];
      candidateSessions.forEach((searchForReverse: any) => {
        const reverseDependencies = searchForReverse.dependencies.filter(
          (d: any) => d.dependency == p.id
        );
        if (reverseDependencies.length > 0) {
          p.reverseDependencies.push({
            dependency: searchForReverse.id,
            isOnlyAiDependency: reverseDependencies[0].isOnlyAiDependency,
          });
        }
      });
    });

    // Compute the dependencies depth (if there are mutliple levels in activities paths) and adjust the candidates weights
    candidateSessions.forEach((p: any) => {
      const dependeciesDepth = this.getDependeciesDepth(p, candidateSessions, 0);
      p.dependenciesDepth = dependeciesDepth;
      // Define the weight of a session. The bigger the weight, the sooner it will be selected by the A* algo
      // Weight previously computed in the slotsGenerator, but redefined here for those activities
      // TODO investigate if it really used or not, and where
      p.weight = dependeciesDepth * 1000000 + p.weight ? p.weight : 0;
    });

    // Build the initialState by including the frozen sessions as starting point
    const initalState = candidateSessions
      .filter((c: any) => c.isScheduleFrozen)
      .map((c: any) => {
        return {
          key: c.key,
          id: c.id,
          slots: c.session?.slots,
          start: new Date(c.session.start),
          end: new Date(c.session.end),
          // TODO plus complexe : devra prendre en considération l'ensemble de places et
          //  affinage avec les slots lors ce que cela sera pris en compte. here we only take the first room,
          //  so if the frozen session is in multiple rooms it will not block the following ones, only the first
          keyRoom: c.session?.places[0],
          // TODO plus complexe : devra prendre en considération l'ensemble de stewards et
          //  affinage avec les slots lors ce que cela sera pris en compte
          keyTrainer: c.session?.stewards,
          keyCategory: c.keyCategory,
          colorCategory: c.colorCategory,
          length: c.length,
          isScheduleFrozen: c.isScheduleFrozen,
          secret: c.secret,
          dependencies: c.dependencies,
          trainingDependencies: c.trainingDependencies,
          inscriptionFree: c.inscriptionFree,
          volunteerFactor: c.volunteerFactor,
          dependenciesDepth: c.dependenciesDepth,
          reverseDependencies: c.reverseDependencies,
        };
      });

    // Remove the frozen sessions from the candidates
    const skippedId = [];
    candidateSessions = candidateSessions.filter((c: any) => !c.isScheduleFrozen);

    // Filter all the sessions that from scratch, can not be placed at all. Those not placeable sessions wil increase
    // the number of skipped sessions (linked to the skippedLimit number in the engine configuration)
    const notEnoughResources = candidateSessions.filter((c: any) => {
      if (c.slotsAvaible.length == 0) {
        // console.log(c)
        console.warn(`Activity ${c.title} ${c.id} has no slots available`);
      }
      return c.slotsAvaible.length == 0;
    });
    notEnoughResources.forEach((ner: any) => {
      skippedId.push({
        id: ner.id,
        reason:
          "Pas assez de ressources : problème de disponibilité pour les encadrants et les espaces",
      });
    });
    candidateSessions = candidateSessions.filter((c: any) => c.slotsAvaible.length > 0);

    // Sort by weight in order to process the highest candidates first
    candidateSessions = candidateSessions.sort((a: any, b: any) => b.weight - a.weight);

    console.log("Session candidates length:", candidateSessions.length);
    console.log("Inital State length:", initalState.length);

    // LAUNCH THE A* ALGO
    // TODO isolate that in a function

    // The nodes stack
    const allNodes: any = [];

    // The abandoned paths
    let allSlotDisable: any = [];

    // Push the first node of the A* tree in the stack
    const initNode = new NodeClass(config, candidateSessions, data, initalState, 0);
    allNodes.push(initNode);

    // data for the output tree that will be fed by the algo
    let solution;
    let partialSolution;
    let iteration: any = 0;
    let failedId: any = [];
    const failedNode: any = [];

    // A* execution algo.
    // "The solver make the algo, the node makes the intelligence"
    while (solution == undefined) {
      iteration++;

      if (allNodes.length == 0) {
        if (partialSolution != undefined) {
          solution = partialSolution;
          solution.conclusion =
            "Solution partielle trouvée: pas de succès mais pas d'autres solutions";
        } else {
          // If no solution found, and that we have no partial sulution, then it's over, end the game
          solution = {
            conclusion: "Pas de solution : aucune activité ne peut être calculée",
          };
        }
      } else {
        allNodes.sort((a: any, b: any) => b.heuristic - a.heuristic);
        const firstNode = allNodes.shift();
        const children = firstNode.takeStep();

        switch (firstNode.status) {
          case "pending":
            if (partialSolution == undefined || firstNode.depth > partialSolution.depth) {
              partialSolution = firstNode;
            }
            if (this.mute != true) {
              for (let c: any = 0; c < firstNode.depth; c++) {
                process.stdout.write(" ");
              }
              console.log(
                "* iteration:",
                iteration,
                " stack size:",
                allNodes.length,
                " last id:",
                firstNode.candidate.id,
                " last heuristic:",
                firstNode.heuristic.toPrecision(2),
                " last solution length:",
                firstNode.solutions.length
              );
            }
            children.forEach((c: any) => {
              if (c.candidate != undefined) {
                const validDepth = this.getValidDepth(candidateSessions, c.depth, failedId, config);
                if (validDepth != c.depth) {
                  const newNode = new NodeClass(
                    c.config,
                    c.stack,
                    c.resources,
                    c.solutions,
                    validDepth
                  );
                  allNodes.push(newNode);
                } else {
                  allNodes.push(c);
                }
              } else {
                allNodes.push(c);
              }
            });
            break;
          case "success":
            solution = firstNode;
            solution.conclusion = "Solution complète trouvée";
            break;
          case "failed":
            if (partialSolution == undefined || firstNode.depth > partialSolution.depth) {
              partialSolution = firstNode;
            }

            for (let c = 0; c < firstNode.depth; c++) {
              process.stdout.write(" ");
            }
            console.log("|| last id:", firstNode.candidate.id);

            allSlotDisable = allSlotDisable.concat(firstNode.slotDisable);
            failedId = failedId.concat(firstNode.candidate.id);
            failedNode.push(firstNode);

            if (allNodes.length == 0 && firstNode.depth < firstNode.stack.length) {
              skippedId.push({
                id: firstNode.candidate.id,
                reason: "Trop de conflits sans alternatives",
              });
              const rescuedNodes = failedNode.filter((n: any) => n.depth == firstNode.depth);
              rescuedNodes.forEach((rn: any) => {
                const newNode = new NodeClass(
                  rn.config,
                  rn.stack,
                  rn.resources,
                  rn.solutions,
                  firstNode.depth + 1
                );
                allNodes.push(newNode);
              });
            } else {
              const validDepth = this.getValidDepth(
                candidateSessions,
                firstNode.depth,
                failedId,
                config
              );
              if (validDepth != firstNode.depth) {
                skippedId.push({
                  id: firstNode.candidate.id,
                  reason: "Trop de conflits avec des alternatives sautées",
                });
                const skippedNodes = allNodes.filter(
                  (n: any) => n.candidate.id == firstNode.candidate.id
                );
                skippedNodes.forEach((n: any) => {
                  const newNode = new NodeClass(
                    n.config,
                    n.stack,
                    n.resources,
                    n.solutions,
                    validDepth
                  );
                  allNodes.push(newNode);
                  allNodes.splice(allNodes.indexOf(n), 1);
                });
              }
            }
            break;
          default:
        }

        if (iteration > config.maxIteration) {
          // if (iteration > 2) {
          solution = partialSolution;
          solution.conclusion =
            "PARTIAL SOLUTION FOUND: did not continue because the IA reached the maximal number of iterations allowed";
        }
      }
    }

    const finalSlotDisable: {
      trainer: any[];
      room: any[];
      dependencyBefore: any[];
      dependencyAfter: any[];
    } = {
      trainer: [],
      room: [],
      dependencyBefore: [],
      dependencyAfter: [],
    };

    allSlotDisable.forEach((currentValue: any) => {
      if (currentValue.conflict == "trainer") {
        const existingDisable = finalSlotDisable.trainer.filter((fds) => {
          return (
            fds.id == currentValue.id &&
            fds.keyTrainer == currentValue.keyTrainer &&
            fds.slotConflict == currentValue.slotConflict
          );
        });
        if (existingDisable.length > 0) {
          existingDisable[0].occurrence = existingDisable[0].occurrence + 1;
        } else {
          finalSlotDisable.trainer.push({
            id: currentValue.id,
            keyTrainer: currentValue.keyTrainer,
            occurrence: 1,
            slotConflict: currentValue.slotConflict,
          });
        }
      }
      if (currentValue.conflict == "room") {
        const existingDisable = finalSlotDisable.room.filter((fds) => {
          return (
            fds.id == currentValue.id &&
            fds.keyRoom == currentValue.keyRoom &&
            fds.slotConflict == currentValue.slotConflict
          );
        });
        if (existingDisable.length > 0) {
          existingDisable[0].occurrence = existingDisable[0].occurrence + 1;
        } else {
          finalSlotDisable.room.push({
            id: currentValue.id,
            keyRoom: currentValue.keyRoom,
            occurrence: 1,
            slotConflict: currentValue.slotConflict,
          });
        }
      }
      if (currentValue.conflict == "dependencyBefore") {
        const existingDisable = finalSlotDisable.dependencyBefore.filter((fds) => {
          return fds.id == currentValue.id && fds.dependencies == currentValue.dependencies;
        });
        if (existingDisable.length > 0) {
          existingDisable[0].occurrence = existingDisable[0].occurrence + 1;
        } else {
          finalSlotDisable.dependencyBefore.push({
            id: currentValue.id,
            dependencies: currentValue.dependencies,
            occurrence: 1,
          });
        }
      }
      if (currentValue.conflict == "dependencyAfter") {
        const existingDisable = finalSlotDisable.dependencyAfter.filter((fds) => {
          return (
            fds.id == currentValue.id && fds.reverseDependencies == currentValue.reverseDependencies
          );
        });
        if (existingDisable.length > 0) {
          existingDisable[0].occurrence = existingDisable[0].occurrence + 1;
        } else {
          finalSlotDisable.dependencyAfter.push({
            id: currentValue.id,
            reverseDependencies: currentValue.reverseDependencies,
            occurrence: 1,
          });
        }
      }
    });

    const out = {
      solution: solution.solutions,
      conclusion: solution.conclusion,
      config: config,
      data: data,
      initalState: initalState,
      finalSlotDisable: finalSlotDisable,
      skippedId: skippedId,
    };

    console.log("conclusion", solution.conclusion);
    console.log("***** skippedId *****", skippedId);

    console.log("iteration number", iteration);
    console.log("last depth", solution.depth);
    console.log("last heuristic", solution.heuristic);
    console.log("last heuristicDetail", solution.heuristicDetail);

    return out;
  }

  //ByPass candidates ever abandonned
  getValidDepth(candidates: any, currentDepth: any, failedId: any, config: any) {
    let out = currentDepth;
    let solution = false;
    while (solution == false) {
      const EverAbandonedId = failedId.filter((f: any) => f == candidates[out].id);
      if (EverAbandonedId.length > config.skippedLimit && out < candidates.length - 1) {
        out++;
      } else {
        solution = true;
      }
    }
    return out;
  }

  getDependeciesDepth(candidate: any, candidates: any, depth: any) {
    // console.log('- getDependeciesDepth',candidate.id,depth);
    let currentDepth = depth;
    // console.log(candidate.dependencies);
    candidate.reverseDependencies.forEach((d: any) => {
      // console.log(d);
      const dependeciesSlot = candidates.filter((c: any) => c.id == d.dependency);
      // console.log('-- dependeciesSlot',dependeciesSlot);
      const dependecyDepth = this.getDependeciesDepth(dependeciesSlot[0], candidates, depth + 1);
      currentDepth = Math.max(currentDepth, dependecyDepth);
    });
    return currentDepth;
  }
};
